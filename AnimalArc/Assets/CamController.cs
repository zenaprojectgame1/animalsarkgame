using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Cam1;
    public GameObject Cam2;
    void Start()
    {
        StartCoroutine(CamSetting());

    }

    IEnumerator CamSetting()
    {
        Cam1.SetActive(true);
        yield return new WaitForSeconds(0f);
        Cam1.SetActive(false);
        //Keep restarting IEnumerator forever.
    }

}
