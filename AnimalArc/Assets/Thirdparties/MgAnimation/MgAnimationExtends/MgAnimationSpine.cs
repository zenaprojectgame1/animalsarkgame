﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;/*
using Spine.Unity;
using Spine;*/

public class MgAnimationSpine : MgAnimation
{/*
    #region Setup Spine
    private Spine.AnimationState _spineAnimationState;
    public SkeletonGraphic skeletonGraphic;
    bool isSpineSetup = false;
    public Spine.AnimationState SpineAnimationState
    {
        get
        {
            if (_spineAnimationState == null)
            {
                SetupSpine();
            }
            return _spineAnimationState;
        }
    }

    [SerializeField] [SpineAnimation] string showAnimName;
    [SerializeField] [SpineAnimation] string hideAnimName;
    public List<string> otherAnims;


    //[SerializeField] AnimationReferenceAsset showAnim;
    //[SerializeField] AnimationReferenceAsset hideAnim;

    void SetupSpine()
    {
        if (!isSpineSetup)
        {
            skeletonGraphic = GetComponentInChildren<SkeletonGraphic>();
            //Debug.Log(SkeletonAnimation);
            //spineAnimationState = SkeletonAnimation.AnimationState;
            _spineAnimationState = skeletonGraphic.AnimationState;
            isSpineSetup = true;
        }
    }
    #endregion

    private void Start()
    {
        SetupSpine();
    }

    protected override void Awake()
    {
        //SetupSpine();
    }

    public override void Show()
    {
        base.Show();
        //Debug.Log("show");
        //Debug.Log(spineAnimationState);
        skeletonGraphic.color = Color.white;
        SpineAnimationState.SetAnimation(0, showAnimName, false);
    }

    public override void Hide(bool immediately = false)
    {
        //Debug.Log("hide");
        if (immediately)
        {
            skeletonGraphic.color = new Color(1f, 1f, 1f, 0f);
        }
        else
        {
            SpineAnimationState.SetAnimation(0, hideAnimName, false);
        }
    }

    public void Anim(int id = 0, bool loop = false)
    {
        SpineAnimationState.SetAnimation(0, otherAnims[id], loop);
    }

    public void AddAnim(int id = 0)
    {
        SpineAnimationState.AddAnimation(0, otherAnims.GetIndexOrHighest(id), false, 0f);
    }*/
}
