﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(CanvasGroup))]
public class MgAnimationFade : MgAnimation
{
    [SerializeField] float animDuration = MgAnimation.ANIM_DUR;

    public override void Show()
    {
        var cg = GetComponent<CanvasGroup>();
        cg.DOKill();

        if (gameObject.activeInHierarchy)
        {
            cg.DOFade(1, animDuration);
        }
        else
        {
            cg.alpha = 1;
        }
    }

    public override void Hide(bool immediately = false)
    {
        var cg = GetComponent<CanvasGroup>();
        cg.DOKill();

        if (immediately)
        {
            cg.alpha = 0;
        }
        else
        {
            cg.DOFade(0, animDuration);
        }
    }
}
