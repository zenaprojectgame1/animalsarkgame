﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MgAnimation : MonoBehaviour
{
    public const float ANIM_DUR = 0.5f;

    protected virtual void Awake()
    {
    }

    protected virtual void OnDisable()
    {
    }

    public virtual void Show()
    {
        //Debug.Log("show " + gameObject.name);
    }

    public virtual void Hide(bool immediately = false)
    {
    }
}
