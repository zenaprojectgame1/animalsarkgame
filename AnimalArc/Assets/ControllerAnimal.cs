using UnityEngine;

public class ControllerAnimal : MonoBehaviour
{
    private Rigidbody m_Rigidbody;
    private ConfigurableJoint m_ConfigurableJoint;
    private JointDrive m_DriveMove;
    private JointDrive m_DriveIdleX;
    private JointDrive m_DriveIdleYZ;
    private bool m_Move;

    private void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_ConfigurableJoint = GetComponent<ConfigurableJoint>();
        
        m_DriveMove = new JointDrive
        {
            positionSpring = 0,
            positionDamper = 0,
            maximumForce = 3.402823e+38f
        };
        m_DriveIdleX = new JointDrive
        {
            positionSpring = 100,
            positionDamper = 30,
            maximumForce = 3.402823e+38f
        };
        m_DriveIdleYZ = new JointDrive
        {
            positionSpring = 100,
            positionDamper = 2.5f,
            maximumForce = 3.402823e+38f
        };
    }

    private void SetAngularDrive(JointDrive _driveX, JointDrive _driveY)
    {
        m_ConfigurableJoint.angularXDrive = _driveX;
        m_ConfigurableJoint.angularYZDrive = _driveY;
    }

    private void FixedUpdate()
    {
        bool lastMove = m_Move;
        m_Move = m_Rigidbody.velocity.sqrMagnitude > 0.2f;
        if (m_Move && !lastMove) SetAngularDrive(m_DriveMove, m_DriveMove);
        if (!m_Move && lastMove) SetAngularDrive(m_DriveIdleX, m_DriveIdleYZ);
    }
}