using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailController : MonoBehaviour
{
    // Start is called before the first frame update
    LineRenderer m_LineRenderer;
    public Vector3[] fingerPositions;
    public GameObject[] ListButterflyLevel1Now;
    public GameObject[] ListButterflyLevel2Now;
    public GameObject[] ListButterflyLevel3Now;



    public List<GameObject> ListButterflyLevel1;
    public List<GameObject> ListButterflyLevel2;
    public List<GameObject> ListButterflyLevel3;

    public ParticleSystem destroy;
    struct myLine
    {
        public Vector3 StartPoint;
        public Vector3 EndPoint;
    };
    void Start()
    {
        ListButterflyLevel1Now = GameObject.FindGameObjectsWithTag("ListButterflyLevel1");
        ListButterflyLevel2Now = GameObject.FindGameObjectsWithTag("ListButterflyLevel2");
        ListButterflyLevel3Now = GameObject.FindGameObjectsWithTag("ListButterflyLevel3");
        m_LineRenderer = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            fingerPositions = new Vector3[m_LineRenderer.positionCount];
            m_LineRenderer.GetPositions(fingerPositions);
        }
        if (Input.GetMouseButtonUp(0))
        {
            checkButterfly(ListButterflyLevel1Now, 1);
            checkButterfly(ListButterflyLevel2Now, 2);
            checkButterfly(ListButterflyLevel3Now, 3);
            RemoveButterfly();
        }
    }

    public bool IsPointInPolygon(Vector3 point, Vector3[] polygon)
    {
        int polygonLength = polygon.Length, i = 0;
        bool inside = false;
        float pointX = point.x, pointY = point.y;
        float startX, startY, endX, endY;
        Vector2 endPoint = polygon[polygonLength - 1];
        endX = endPoint.x;
        endY = endPoint.y;

        while (i < polygonLength)
        {
            startX = endX; startY = endY;
            endPoint = polygon[i++];
            endX = endPoint.x; endY = endPoint.y;
            inside ^= (endY > pointY ^ startY > pointY) /* ? pointY inside [startY;endY] segment ? */
                      && /* if so, test if it is under the segment */
                      ((pointX - endX) < (pointY - endY) * (startX - endX) / (startY - endY));
        }
        return inside;
    }

    private void checkButterfly(GameObject[] Butterflys, int number)
    {
        for (int i = 0; i < Butterflys.Length; i++)
        {
            if (IsPointInPolygon(Butterflys[i].transform.position))
            {
                if (number == 1) ListButterflyLevel1.Add(Butterflys[i]);
                else if (number == 2) ListButterflyLevel2.Add(Butterflys[i]);
                else if (number == 3) ListButterflyLevel3.Add(Butterflys[i]);
            }
        }
    }
    private void RemoveButterfly()
    {
        if (ListButterflyLevel1.Count > 1 && ListButterflyLevel2.Count == 0 && ListButterflyLevel3.Count == 0)
        {
            DestroyButterflies(ListButterflyLevel1);
        }
        else if (ListButterflyLevel2.Count > 1 && ListButterflyLevel1.Count == 0 && ListButterflyLevel3.Count == 0)
        {
            DestroyButterflies(ListButterflyLevel2);
        }
        else if (ListButterflyLevel3.Count > 1 && ListButterflyLevel1.Count == 0 && ListButterflyLevel2.Count == 0)
        {
            DestroyButterflies(ListButterflyLevel3);
        }
        else
        {
            Debug.Log("Fail");
            Destroy(gameObject);
        }
    }

    private bool IsPointInPolygon(Vector3 point)
    {
        int polygonLength = fingerPositions.Length, i = 0;
        bool inside = false;
        // x, y for tested point.
        float pointX = point.x, pointY = point.y;
        // start / end point for the current polygon segment.
        float startX, startY, endX, endY;
        Vector2 endPoint = fingerPositions[polygonLength - 1];
        endX = endPoint.x;
        endY = endPoint.y;
        while (i < polygonLength)
        {
            startX = endX; startY = endY;
            endPoint = fingerPositions[i++];
            endX = endPoint.x; endY = endPoint.y;
            //
            inside ^= (endY > pointY ^ startY > pointY) /* ? pointY inside [startY;endY] segment ? */
                      && /* if so, test if it is under the segment */
                      ((pointX - endX) < (pointY - endY) * (startX - endX) / (startY - endY));
        }
        return inside;
    }

    private void DestroyButterflies(List<GameObject> ListButterflys)
    {

        for (int i = 0; i < ListButterflys.Count; i++)
        {
            Destroy(ListButterflys[i]);
        }

        Destroy(this.gameObject);
    }

    private Vector2 Centroid(Vector2 a, Vector2 b, Vector2 c)
    {
        Vector2 centroid = new Vector2((a.x + b.x + c.x) / 3, (a.y + b.y + c.y) / 3);
        return centroid;
    }
}
