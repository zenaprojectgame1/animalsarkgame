using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Lean.Common;
using Lean.Touch;
using UnityEngine;
using UnityEngine.Serialization;

public class ZoomCinemachine : MonoBehaviour
{
    public LeanFingerFilter Use = new LeanFingerFilter(true);
    [SerializeField] private CinemachineVirtualCamera m_Camera;
    [SerializeField] private float zoom;
    [SerializeField] private float damping;
    [SerializeField] private float clampMin;
    [SerializeField] private float clampMax;
    private CinemachineTransposer m_Transposer;
    private Vector3 currentZoom = Vector3.zero;

    private void Start()
    {
        m_Transposer = m_Camera.GetCinemachineComponent<CinemachineTransposer>();
        currentZoom.z = zoom;
    }


    protected virtual void LateUpdate()
    {
        float pinchRatio = LeanGesture.GetPinchRatio(Use.UpdateAndGetFingers());
        if (pinchRatio != 1.0f)
        {
            zoom = Mathf.Clamp(zoom * pinchRatio, clampMin, clampMax);
        }
        var factor = LeanHelper.GetDampenFactor(damping, Time.deltaTime);
        currentZoom.z = Mathf.Lerp(currentZoom.z, zoom, factor);
        SetZoom(currentZoom);
    }

    private void SetZoom(Vector3 current)
    {
        m_Transposer.m_FollowOffset = current;
    }
}