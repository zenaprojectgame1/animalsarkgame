using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SS.View;

public class techGameController : Controller
{
    public const string TECHGAME_SCENE_NAME = "techGame";

    public override string SceneName()
    {
        return TECHGAME_SCENE_NAME;
    }
}