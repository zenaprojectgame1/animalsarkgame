﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Lean.Common.LeanLoadScene::set_SceneName(System.String)
extern void LeanLoadScene_set_SceneName_m2ED0105FFC95BE203FACA9F61193EEBCB974E4DD (void);
// 0x00000002 System.String Lean.Common.LeanLoadScene::get_SceneName()
extern void LeanLoadScene_get_SceneName_m8145D8B162B0CFD5F34F909BB4C8B7FB87A1C00E (void);
// 0x00000003 System.Void Lean.Common.LeanLoadScene::set_ASync(System.Boolean)
extern void LeanLoadScene_set_ASync_mDCF18F6AE71C005ED3835B33221765913D3BD90B (void);
// 0x00000004 System.Boolean Lean.Common.LeanLoadScene::get_ASync()
extern void LeanLoadScene_get_ASync_mB4CE59DED48DEAD12417ABACA86DF437FD2E24FA (void);
// 0x00000005 System.Void Lean.Common.LeanLoadScene::set_Additive(System.Boolean)
extern void LeanLoadScene_set_Additive_m24B4612740C13CDBBB8386A044498E7281F89FE8 (void);
// 0x00000006 System.Boolean Lean.Common.LeanLoadScene::get_Additive()
extern void LeanLoadScene_get_Additive_m322E489626E33055A4D35451A35D252DF5F5456C (void);
// 0x00000007 System.Void Lean.Common.LeanLoadScene::Load()
extern void LeanLoadScene_Load_mF187C0667FAA85B99C8FF3201D6C0B5AA34D5F8C (void);
// 0x00000008 System.Void Lean.Common.LeanLoadScene::Load(System.String)
extern void LeanLoadScene_Load_mD5209D8A38ADCA5D3A70D0241FBC10B4C8592DA9 (void);
// 0x00000009 System.Void Lean.Common.LeanLoadScene::.ctor()
extern void LeanLoadScene__ctor_m25BDE8C86D5EC876FDB243F09F6FE58F8D8B8DAF (void);
// 0x0000000A System.Void Lean.Common.LeanPongBall::set_StartSpeed(System.Single)
extern void LeanPongBall_set_StartSpeed_m4F5B70EB025BB916FFECE2A46D676F6369B315C6 (void);
// 0x0000000B System.Single Lean.Common.LeanPongBall::get_StartSpeed()
extern void LeanPongBall_get_StartSpeed_mCE4AA5D8A1B87A748564792256D916F915FACF02 (void);
// 0x0000000C System.Void Lean.Common.LeanPongBall::set_Spread(System.Single)
extern void LeanPongBall_set_Spread_m3C0F91B175C69DBB4EE3A6E5F2481CF86EED52F8 (void);
// 0x0000000D System.Single Lean.Common.LeanPongBall::get_Spread()
extern void LeanPongBall_get_Spread_m43F880905B1FBCF30298CC4034FB8C024BEFA28E (void);
// 0x0000000E System.Void Lean.Common.LeanPongBall::set_Acceleration(System.Single)
extern void LeanPongBall_set_Acceleration_m0B545A4076044C34CB40CD4B90B89F4B33F862B6 (void);
// 0x0000000F System.Single Lean.Common.LeanPongBall::get_Acceleration()
extern void LeanPongBall_get_Acceleration_m8481C1C6EEAF222087530491136C94FA7DD48193 (void);
// 0x00000010 System.Void Lean.Common.LeanPongBall::set_Bounds(System.Single)
extern void LeanPongBall_set_Bounds_m06D57F69C927264240D0D0E368781852A410EDC6 (void);
// 0x00000011 System.Single Lean.Common.LeanPongBall::get_Bounds()
extern void LeanPongBall_get_Bounds_mE247CBDEA869E44F32542B0B430BE38DA4A52B52 (void);
// 0x00000012 System.Void Lean.Common.LeanPongBall::Awake()
extern void LeanPongBall_Awake_m564F6C3F4EE949E07347035A72754AD1462D4940 (void);
// 0x00000013 System.Void Lean.Common.LeanPongBall::FixedUpdate()
extern void LeanPongBall_FixedUpdate_m628C88A56020D91AE1D8A31808CA250901AAA125 (void);
// 0x00000014 System.Void Lean.Common.LeanPongBall::ResetPositionAndVelocity()
extern void LeanPongBall_ResetPositionAndVelocity_mF76B0F5A6C3834E3206B56AC4A3BA754233C54F9 (void);
// 0x00000015 System.Void Lean.Common.LeanPongBall::.ctor()
extern void LeanPongBall__ctor_mD5E0CDEE9CA4E3A3BCFC4E0F30FED2352C1C8CF1 (void);
// 0x00000016 System.Void Lean.Common.LeanSelectedString::set_Format(System.String)
extern void LeanSelectedString_set_Format_mDD3F78ADC7C4FEAC83C01B4FB1002505293E5802 (void);
// 0x00000017 System.String Lean.Common.LeanSelectedString::get_Format()
extern void LeanSelectedString_get_Format_m673DF60C47A6A42D5ECCCD8136F7BDA940930909 (void);
// 0x00000018 Lean.Common.LeanSelectedString/StringEvent Lean.Common.LeanSelectedString::get_OnString()
extern void LeanSelectedString_get_OnString_mDEB2E676C5F2958D5A4E2D68F5F9333AB280A654 (void);
// 0x00000019 System.Void Lean.Common.LeanSelectedString::UpdateNow()
extern void LeanSelectedString_UpdateNow_m1BC9220D2DE3F693201134E1F6064E32653ED8D1 (void);
// 0x0000001A System.Void Lean.Common.LeanSelectedString::OnEnable()
extern void LeanSelectedString_OnEnable_m16992CAC8173F4D3C22FE311594446E4A630F005 (void);
// 0x0000001B System.Void Lean.Common.LeanSelectedString::OnDisable()
extern void LeanSelectedString_OnDisable_m1CF47D4DC172E4113FBF42C587F0F878E7CE3933 (void);
// 0x0000001C System.Void Lean.Common.LeanSelectedString::HandleA(Lean.Common.LeanSelectable)
extern void LeanSelectedString_HandleA_mE8370CFB2AE2F05683AE31FE550D28DBEF92AA92 (void);
// 0x0000001D System.Void Lean.Common.LeanSelectedString::HandleB(Lean.Common.LeanSelect,Lean.Common.LeanSelectable)
extern void LeanSelectedString_HandleB_mF99FC0B65998AE1D5AB1A2E3CC617CB132FF40B6 (void);
// 0x0000001E System.Void Lean.Common.LeanSelectedString::.ctor()
extern void LeanSelectedString__ctor_m6E26E9B641D69787BF4E7F34C02777EE1C43D824 (void);
// 0x0000001F System.Void Lean.Common.LeanSelectedString/StringEvent::.ctor()
extern void StringEvent__ctor_m8B9DA6C411D24AC3E7EC9C088B2C0708E148CD86 (void);
// 0x00000020 System.Void Lean.Common.LeanChase::set_Destination(UnityEngine.Transform)
extern void LeanChase_set_Destination_m4B417A4FAD51E222F012D23CCA799B9BBF84FC9D (void);
// 0x00000021 UnityEngine.Transform Lean.Common.LeanChase::get_Destination()
extern void LeanChase_get_Destination_m5B995A8A91341D562D23817D6ED70702BFE09DFE (void);
// 0x00000022 System.Void Lean.Common.LeanChase::set_DestinationOffset(UnityEngine.Vector3)
extern void LeanChase_set_DestinationOffset_mA8CC85B85EBCE60E0437B9582CD2CA02C47CF1ED (void);
// 0x00000023 UnityEngine.Vector3 Lean.Common.LeanChase::get_DestinationOffset()
extern void LeanChase_get_DestinationOffset_m0B814669036006F986A34F783FFC609633F680FF (void);
// 0x00000024 System.Void Lean.Common.LeanChase::set_Position(UnityEngine.Vector3)
extern void LeanChase_set_Position_m578E880779FD29E1FC93907F9CF65C37583E0955 (void);
// 0x00000025 UnityEngine.Vector3 Lean.Common.LeanChase::get_Position()
extern void LeanChase_get_Position_m8FC61FF9D808DC315D66D461996A39BAEDC2C47D (void);
// 0x00000026 System.Void Lean.Common.LeanChase::set_Offset(UnityEngine.Vector3)
extern void LeanChase_set_Offset_mD5C3FCCD97B6866BC96266BA9927BE9B7D669F05 (void);
// 0x00000027 UnityEngine.Vector3 Lean.Common.LeanChase::get_Offset()
extern void LeanChase_get_Offset_m29B46A1EADD2BF684E4F101D862D08DADD1FAAE0 (void);
// 0x00000028 System.Void Lean.Common.LeanChase::set_Damping(System.Single)
extern void LeanChase_set_Damping_mE0159149B79BD9F3B29CDFB17A45DDDF9EE59FD8 (void);
// 0x00000029 System.Single Lean.Common.LeanChase::get_Damping()
extern void LeanChase_get_Damping_m48AAB2482223995F01E76C9AB67381C0C5B580D3 (void);
// 0x0000002A System.Void Lean.Common.LeanChase::set_Linear(System.Single)
extern void LeanChase_set_Linear_mBEFF74E30E9B7EE753E7BBB8F6D3CD94E9BF3D02 (void);
// 0x0000002B System.Single Lean.Common.LeanChase::get_Linear()
extern void LeanChase_get_Linear_m4F277391006372547E74FAF3A1277DA8E5B5091C (void);
// 0x0000002C System.Void Lean.Common.LeanChase::set_IgnoreZ(System.Boolean)
extern void LeanChase_set_IgnoreZ_m7A51ED9585C05FF11ECF9215CD84367E1A173E6D (void);
// 0x0000002D System.Boolean Lean.Common.LeanChase::get_IgnoreZ()
extern void LeanChase_get_IgnoreZ_m3528736EEF8E8F93AB799D34E72259AF4B04E4AC (void);
// 0x0000002E System.Void Lean.Common.LeanChase::set_Continuous(System.Boolean)
extern void LeanChase_set_Continuous_m93C0B0C97F1B7770D3864B4964EC220C3B519C02 (void);
// 0x0000002F System.Boolean Lean.Common.LeanChase::get_Continuous()
extern void LeanChase_get_Continuous_m8015B968FA947E21C53B82073B9C68923A776685 (void);
// 0x00000030 System.Void Lean.Common.LeanChase::set_SetPositionOnStart(System.Boolean)
extern void LeanChase_set_SetPositionOnStart_mBC3A1027E1609743749D741360BFDC846316DC62 (void);
// 0x00000031 System.Boolean Lean.Common.LeanChase::get_SetPositionOnStart()
extern void LeanChase_get_SetPositionOnStart_mC6C6190438DC5FE1C3EB760CBB2A95DA7309667A (void);
// 0x00000032 System.Void Lean.Common.LeanChase::SetPosition(UnityEngine.Vector3)
extern void LeanChase_SetPosition_m4E3325B6201176D23E13CFE8500267FE6268F4C9 (void);
// 0x00000033 System.Void Lean.Common.LeanChase::SnapToPosition()
extern void LeanChase_SnapToPosition_m57CB8EEDB18D3532BC7E19A220315C87BE77F76D (void);
// 0x00000034 System.Void Lean.Common.LeanChase::Start()
extern void LeanChase_Start_m16FB1D836146782581D4FD13EC281DF4DDCC1775 (void);
// 0x00000035 System.Void Lean.Common.LeanChase::Update()
extern void LeanChase_Update_m13CADF00437272117590F5EDFCD79521711F227B (void);
// 0x00000036 System.Void Lean.Common.LeanChase::UpdatePosition(System.Single,System.Single)
extern void LeanChase_UpdatePosition_mC2057A7EF766D685BF6089C1C8F427221534EBD8 (void);
// 0x00000037 System.Void Lean.Common.LeanChase::.ctor()
extern void LeanChase__ctor_m52B784594A600BA71866AF2FDD6E3F6486A8884D (void);
// 0x00000038 System.Void Lean.Common.LeanChaseRigidbody::SetPosition(UnityEngine.Vector3)
extern void LeanChaseRigidbody_SetPosition_m5FC81EDB57C467D6B433E70A356D2F46475445C7 (void);
// 0x00000039 System.Void Lean.Common.LeanChaseRigidbody::OnEnable()
extern void LeanChaseRigidbody_OnEnable_m379DD92BC2DA32B729578736B22FE840425C52FF (void);
// 0x0000003A System.Void Lean.Common.LeanChaseRigidbody::UpdatePosition(System.Single,System.Single)
extern void LeanChaseRigidbody_UpdatePosition_mB9CDFA98A03A334B7E0C750EB71619C50939502E (void);
// 0x0000003B System.Void Lean.Common.LeanChaseRigidbody::LateUpdate()
extern void LeanChaseRigidbody_LateUpdate_m71F608B10BACF21F7D17DC292FEC74E3511FD5F6 (void);
// 0x0000003C System.Void Lean.Common.LeanChaseRigidbody::.ctor()
extern void LeanChaseRigidbody__ctor_mE6D41D192AE3E6F69AFB9A80FB41C9CCA2697353 (void);
// 0x0000003D System.Void Lean.Common.LeanChaseRigidbody2D::set_Axis(Lean.Common.LeanChaseRigidbody2D/AxisType)
extern void LeanChaseRigidbody2D_set_Axis_mCBAE3A6AB8E3BDE2E91B9C77A8AEF970F6AF900C (void);
// 0x0000003E Lean.Common.LeanChaseRigidbody2D/AxisType Lean.Common.LeanChaseRigidbody2D::get_Axis()
extern void LeanChaseRigidbody2D_get_Axis_mBA7A8C46FAB209EDFC3FD1E36D97F6F8A0510CBB (void);
// 0x0000003F System.Void Lean.Common.LeanChaseRigidbody2D::SetPosition(UnityEngine.Vector3)
extern void LeanChaseRigidbody2D_SetPosition_mC686C1879ADBF2C4903413999CDC55F830187A28 (void);
// 0x00000040 System.Void Lean.Common.LeanChaseRigidbody2D::OnEnable()
extern void LeanChaseRigidbody2D_OnEnable_m553BC6C23A8E9DC737F45FB52A8BAD09CD34F5EB (void);
// 0x00000041 System.Void Lean.Common.LeanChaseRigidbody2D::UpdatePosition(System.Single,System.Single)
extern void LeanChaseRigidbody2D_UpdatePosition_mCC5599305720B4B408DA7B969C69856608841560 (void);
// 0x00000042 System.Void Lean.Common.LeanChaseRigidbody2D::LateUpdate()
extern void LeanChaseRigidbody2D_LateUpdate_m73A7F3E274BAD7F1E9A0927055CD4F53653FA199 (void);
// 0x00000043 System.Void Lean.Common.LeanChaseRigidbody2D::.ctor()
extern void LeanChaseRigidbody2D__ctor_mE9DB26371426D08BA5B0DCD091F57A50B4E3FA38 (void);
// 0x00000044 System.Void Lean.Common.LeanConstrainLocalPosition::set_X(System.Boolean)
extern void LeanConstrainLocalPosition_set_X_mAD83783123F79A72A9F12E95395FAB7BAA903A39 (void);
// 0x00000045 System.Boolean Lean.Common.LeanConstrainLocalPosition::get_X()
extern void LeanConstrainLocalPosition_get_X_m67B07C8FE618F2154ABA6E8E664FCC0F4D984D8C (void);
// 0x00000046 System.Void Lean.Common.LeanConstrainLocalPosition::set_XMin(System.Single)
extern void LeanConstrainLocalPosition_set_XMin_mC656465B533E53C7AEB15659AC6460BBFFDF10A9 (void);
// 0x00000047 System.Single Lean.Common.LeanConstrainLocalPosition::get_XMin()
extern void LeanConstrainLocalPosition_get_XMin_mCC8AD3F827341A18348EFEBBAEBA71B467BA554A (void);
// 0x00000048 System.Void Lean.Common.LeanConstrainLocalPosition::set_XMax(System.Single)
extern void LeanConstrainLocalPosition_set_XMax_m9910F0214A043EEAF3D3F1AE59AE1720C2AC17B0 (void);
// 0x00000049 System.Single Lean.Common.LeanConstrainLocalPosition::get_XMax()
extern void LeanConstrainLocalPosition_get_XMax_m003C3709A5F9EDE2700ED0C6469BF4CF3AF25637 (void);
// 0x0000004A System.Void Lean.Common.LeanConstrainLocalPosition::set_Y(System.Boolean)
extern void LeanConstrainLocalPosition_set_Y_m2C82E1B5757C211B7AF81738D63ECD8D87B7C741 (void);
// 0x0000004B System.Boolean Lean.Common.LeanConstrainLocalPosition::get_Y()
extern void LeanConstrainLocalPosition_get_Y_mEA52AD0CF11BBFD0D2B94258B4ED70D44A96D50E (void);
// 0x0000004C System.Void Lean.Common.LeanConstrainLocalPosition::set_YMin(System.Single)
extern void LeanConstrainLocalPosition_set_YMin_m053AA71C148615BB55575E45945E2E43913D7E13 (void);
// 0x0000004D System.Single Lean.Common.LeanConstrainLocalPosition::get_YMin()
extern void LeanConstrainLocalPosition_get_YMin_mD8F6457D65A7389CC2A660AF7ACFB88953F762F3 (void);
// 0x0000004E System.Void Lean.Common.LeanConstrainLocalPosition::set_YMax(System.Single)
extern void LeanConstrainLocalPosition_set_YMax_mF4B6AFBCBDC780255C46B92B8D08C8C0D0115F8E (void);
// 0x0000004F System.Single Lean.Common.LeanConstrainLocalPosition::get_YMax()
extern void LeanConstrainLocalPosition_get_YMax_m30DF80B2B44446FF68B6FEE051D0AC4F4B386E15 (void);
// 0x00000050 System.Void Lean.Common.LeanConstrainLocalPosition::set_Z(System.Boolean)
extern void LeanConstrainLocalPosition_set_Z_mF332B85515D6A8C86D7F7C5DE2F6A055A022D385 (void);
// 0x00000051 System.Boolean Lean.Common.LeanConstrainLocalPosition::get_Z()
extern void LeanConstrainLocalPosition_get_Z_m78FDB5819E98F4C17D4748D1F09DCCDC910EACA0 (void);
// 0x00000052 System.Void Lean.Common.LeanConstrainLocalPosition::set_ZMin(System.Single)
extern void LeanConstrainLocalPosition_set_ZMin_m75E4E15812B1391DE6D1B62318C60ED4995B2A27 (void);
// 0x00000053 System.Single Lean.Common.LeanConstrainLocalPosition::get_ZMin()
extern void LeanConstrainLocalPosition_get_ZMin_m636355DB3B64DF4BF19961435EC005C134BE28FD (void);
// 0x00000054 System.Void Lean.Common.LeanConstrainLocalPosition::set_ZMax(System.Single)
extern void LeanConstrainLocalPosition_set_ZMax_mA2B8A677983D66D7351948E8ECC5630766F69AC6 (void);
// 0x00000055 System.Single Lean.Common.LeanConstrainLocalPosition::get_ZMax()
extern void LeanConstrainLocalPosition_get_ZMax_mEEDB3D918A48EE4BE7D2036D48815DA995F75560 (void);
// 0x00000056 System.Void Lean.Common.LeanConstrainLocalPosition::LateUpdate()
extern void LeanConstrainLocalPosition_LateUpdate_mB46A64B6E9F4CD18BE667DF37CD6186C6EFA5BBC (void);
// 0x00000057 System.Boolean Lean.Common.LeanConstrainLocalPosition::DoClamp(UnityEngine.Vector3&)
extern void LeanConstrainLocalPosition_DoClamp_mDE07C31D8E4B4AAF42B8888E35C9BEFCFBE6F032 (void);
// 0x00000058 System.Void Lean.Common.LeanConstrainLocalPosition::.ctor()
extern void LeanConstrainLocalPosition__ctor_mC1A418817360C0E970290737182A139DFB30A6AF (void);
// 0x00000059 System.Void Lean.Common.LeanConstrainScale::set_Minimum(System.Boolean)
extern void LeanConstrainScale_set_Minimum_m251A573F6590A018DE1E70E5D39FA76E6FD52C07 (void);
// 0x0000005A System.Boolean Lean.Common.LeanConstrainScale::get_Minimum()
extern void LeanConstrainScale_get_Minimum_m57E567839AD95B2F9B3B39173934DC9426EDC3C0 (void);
// 0x0000005B System.Void Lean.Common.LeanConstrainScale::set_MinimumScale(UnityEngine.Vector3)
extern void LeanConstrainScale_set_MinimumScale_m214C7808BFF71863D2CDC222B2322F1A93A01C27 (void);
// 0x0000005C UnityEngine.Vector3 Lean.Common.LeanConstrainScale::get_MinimumScale()
extern void LeanConstrainScale_get_MinimumScale_mF778974B999A2E7313293DAFEDC7020541EE0078 (void);
// 0x0000005D System.Void Lean.Common.LeanConstrainScale::set_Maximum(System.Boolean)
extern void LeanConstrainScale_set_Maximum_mCA057A767ADF058CCACF697CFF0B99D192FD7710 (void);
// 0x0000005E System.Boolean Lean.Common.LeanConstrainScale::get_Maximum()
extern void LeanConstrainScale_get_Maximum_mE987A283D7A786C94275EDB32D7D17C8221D0940 (void);
// 0x0000005F System.Void Lean.Common.LeanConstrainScale::set_MaximumScale(UnityEngine.Vector3)
extern void LeanConstrainScale_set_MaximumScale_mB797B3E64284B4A41FCA8FD1F608EDB4CB378C5D (void);
// 0x00000060 UnityEngine.Vector3 Lean.Common.LeanConstrainScale::get_MaximumScale()
extern void LeanConstrainScale_get_MaximumScale_mF2495B32C2F0930FBD0387B05C2E0B175BD02A30 (void);
// 0x00000061 System.Void Lean.Common.LeanConstrainScale::LateUpdate()
extern void LeanConstrainScale_LateUpdate_m13114E07385885B30BFD67EAA5E2E3D50AFE817D (void);
// 0x00000062 System.Void Lean.Common.LeanConstrainScale::.ctor()
extern void LeanConstrainScale__ctor_mBA2CA4DE365CAAB7C7AC007B0971C4F68E2A9F5E (void);
// 0x00000063 System.Void Lean.Common.LeanConstrainToAxis::set_RelativeTo(UnityEngine.Transform)
extern void LeanConstrainToAxis_set_RelativeTo_mA58C8E6E74E71B7A47DE3C31E998C21199666D47 (void);
// 0x00000064 UnityEngine.Transform Lean.Common.LeanConstrainToAxis::get_RelativeTo()
extern void LeanConstrainToAxis_get_RelativeTo_m0962A62577CC73B4CF4897E81C767AD553CD465B (void);
// 0x00000065 System.Void Lean.Common.LeanConstrainToAxis::set_Axis(Lean.Common.LeanConstrainToAxis/AxisType)
extern void LeanConstrainToAxis_set_Axis_m8E8E0E9AB2CF9A055DB0BCC89FE18103555E58FD (void);
// 0x00000066 Lean.Common.LeanConstrainToAxis/AxisType Lean.Common.LeanConstrainToAxis::get_Axis()
extern void LeanConstrainToAxis_get_Axis_m49A54C8ED223B4849D1C37650DF0227F9B854B97 (void);
// 0x00000067 System.Void Lean.Common.LeanConstrainToAxis::set_Minimum(System.Single)
extern void LeanConstrainToAxis_set_Minimum_mBDFABDE230CC06B5B8C88288CE8DA993DF43279A (void);
// 0x00000068 System.Single Lean.Common.LeanConstrainToAxis::get_Minimum()
extern void LeanConstrainToAxis_get_Minimum_m6540EAB02B9E75BC822309265CB6553C6EDB8A28 (void);
// 0x00000069 System.Void Lean.Common.LeanConstrainToAxis::set_Maximum(System.Single)
extern void LeanConstrainToAxis_set_Maximum_mAAD02F722B1D6DCC7E759BF2BCD45BD14F6559DB (void);
// 0x0000006A System.Single Lean.Common.LeanConstrainToAxis::get_Maximum()
extern void LeanConstrainToAxis_get_Maximum_m29766FE7417A750B3B2E95B9C0EBA345DD818610 (void);
// 0x0000006B System.Void Lean.Common.LeanConstrainToAxis::LateUpdate()
extern void LeanConstrainToAxis_LateUpdate_m5B1545419FE135D90AB597D9E3A8EE051240EED7 (void);
// 0x0000006C System.Void Lean.Common.LeanConstrainToAxis::.ctor()
extern void LeanConstrainToAxis__ctor_m8485E0B411B154168221661D40D9D9EBECE1C2B5 (void);
// 0x0000006D System.Void Lean.Common.LeanConstrainToBox::set_RelativeTo(UnityEngine.Transform)
extern void LeanConstrainToBox_set_RelativeTo_m38496154408486DB97FF2CBAD49E0F4B31332615 (void);
// 0x0000006E UnityEngine.Transform Lean.Common.LeanConstrainToBox::get_RelativeTo()
extern void LeanConstrainToBox_get_RelativeTo_m2DC2C9EE1A1F2A30E81F2207C99DB845208E164E (void);
// 0x0000006F System.Void Lean.Common.LeanConstrainToBox::set_Size(UnityEngine.Vector3)
extern void LeanConstrainToBox_set_Size_mE2E14A848A4CE0F7E4E47B06F3016FA1BC9434BD (void);
// 0x00000070 UnityEngine.Vector3 Lean.Common.LeanConstrainToBox::get_Size()
extern void LeanConstrainToBox_get_Size_mAB207C0FE92218E4687EBFFF236AA9C366C42893 (void);
// 0x00000071 System.Void Lean.Common.LeanConstrainToBox::set_Center(UnityEngine.Vector3)
extern void LeanConstrainToBox_set_Center_m2E78633E1B721092F0767FC2B2F1057EA837776B (void);
// 0x00000072 UnityEngine.Vector3 Lean.Common.LeanConstrainToBox::get_Center()
extern void LeanConstrainToBox_get_Center_m29872D9E06F6ACBC01B5D236461AF38D614CEB21 (void);
// 0x00000073 System.Void Lean.Common.LeanConstrainToBox::LateUpdate()
extern void LeanConstrainToBox_LateUpdate_m13DBB397A57DFEB7F5C114CDB541E2EDFF8893B4 (void);
// 0x00000074 System.Void Lean.Common.LeanConstrainToBox::OnDrawGizmosSelected()
extern void LeanConstrainToBox_OnDrawGizmosSelected_m9E4CD774118EE8B9908D278B8A28812584C63A3C (void);
// 0x00000075 System.Void Lean.Common.LeanConstrainToBox::.ctor()
extern void LeanConstrainToBox__ctor_m7529C5B4B6D15BEE1499F89E3A039922BE11AA6A (void);
// 0x00000076 System.Void Lean.Common.LeanConstrainToCollider::set_Collider(UnityEngine.Collider)
extern void LeanConstrainToCollider_set_Collider_m3978AE45AE75031AE8A11833E9EA530E562B72D6 (void);
// 0x00000077 UnityEngine.Collider Lean.Common.LeanConstrainToCollider::get_Collider()
extern void LeanConstrainToCollider_get_Collider_m504560B9DCCD845ADC98C6A4B8ED88F13C64C35F (void);
// 0x00000078 System.Void Lean.Common.LeanConstrainToCollider::LateUpdate()
extern void LeanConstrainToCollider_LateUpdate_m6EE7044651E03B22B80BD34AA7C573EC028CA510 (void);
// 0x00000079 System.Void Lean.Common.LeanConstrainToCollider::.ctor()
extern void LeanConstrainToCollider__ctor_m95961D7BC999D153F09F1570CD20D4FED8EB5DA2 (void);
// 0x0000007A System.Collections.Generic.List`1<UnityEngine.Collider> Lean.Common.LeanConstrainToColliders::get_Colliders()
extern void LeanConstrainToColliders_get_Colliders_m4C102D8D9F3AC0AC33F27DE0E371A1693B5E4B45 (void);
// 0x0000007B System.Void Lean.Common.LeanConstrainToColliders::LateUpdate()
extern void LeanConstrainToColliders_LateUpdate_mAFD576AB6385123178CFB85F59BB695992DA5E97 (void);
// 0x0000007C System.Void Lean.Common.LeanConstrainToColliders::.ctor()
extern void LeanConstrainToColliders__ctor_mE059B3DE0FFD2FD89A849DAA77E27FAC4B7B008B (void);
// 0x0000007D System.Void Lean.Common.LeanConstrainToDirection::set_Forward(UnityEngine.Vector3)
extern void LeanConstrainToDirection_set_Forward_m4F5266A47998011059524C773BDDF063D2D4E051 (void);
// 0x0000007E UnityEngine.Vector3 Lean.Common.LeanConstrainToDirection::get_Forward()
extern void LeanConstrainToDirection_get_Forward_m3F8BB76DF10F6874C7AF37EF141C153BE465D52A (void);
// 0x0000007F System.Void Lean.Common.LeanConstrainToDirection::set_Direction(UnityEngine.Vector3)
extern void LeanConstrainToDirection_set_Direction_m00D4777DA262E2C296C298A34509ED8230495913 (void);
// 0x00000080 UnityEngine.Vector3 Lean.Common.LeanConstrainToDirection::get_Direction()
extern void LeanConstrainToDirection_get_Direction_m6857C0EA19AFB918D3AFFC428995DFD6664DBA25 (void);
// 0x00000081 System.Void Lean.Common.LeanConstrainToDirection::set_RelativeTo(UnityEngine.Transform)
extern void LeanConstrainToDirection_set_RelativeTo_m91F12D706AEFA90476432D0B0232F161835596F8 (void);
// 0x00000082 UnityEngine.Transform Lean.Common.LeanConstrainToDirection::get_RelativeTo()
extern void LeanConstrainToDirection_get_RelativeTo_m1A7D2A6F9B4F8DE48E52B06D670FDBB65AFE5067 (void);
// 0x00000083 System.Void Lean.Common.LeanConstrainToDirection::set_MinAngle(System.Single)
extern void LeanConstrainToDirection_set_MinAngle_m64EFA6A16CE7F4DB6C96F3DC83C8FF7FA6BAD2E6 (void);
// 0x00000084 System.Single Lean.Common.LeanConstrainToDirection::get_MinAngle()
extern void LeanConstrainToDirection_get_MinAngle_m55C9ED6CB306000B787D3F7264F5511AD9BBCD09 (void);
// 0x00000085 System.Void Lean.Common.LeanConstrainToDirection::set_MaxAngle(System.Single)
extern void LeanConstrainToDirection_set_MaxAngle_mD99C02C8DD33BC01EF5D03B61F230DB87E8AFD92 (void);
// 0x00000086 System.Single Lean.Common.LeanConstrainToDirection::get_MaxAngle()
extern void LeanConstrainToDirection_get_MaxAngle_m8B41F8419AEA2122BA358F2C9DE8B23934AB7AB8 (void);
// 0x00000087 System.Void Lean.Common.LeanConstrainToDirection::LateUpdate()
extern void LeanConstrainToDirection_LateUpdate_m3051C3906932C76756C908549CE7DAD5BCC4D79C (void);
// 0x00000088 System.Void Lean.Common.LeanConstrainToDirection::.ctor()
extern void LeanConstrainToDirection__ctor_m7C49FE309E857DB14C57D6CC25A300859EE5440D (void);
// 0x00000089 System.Void Lean.Common.LeanConstrainToOrthographic::set_Camera(UnityEngine.Camera)
extern void LeanConstrainToOrthographic_set_Camera_mDE50A15C5D4C74D818930B406468E20F06FFBE8C (void);
// 0x0000008A UnityEngine.Camera Lean.Common.LeanConstrainToOrthographic::get_Camera()
extern void LeanConstrainToOrthographic_get_Camera_mBD597A4FF84B1CEDE9170C51DE40A90D9DB7B8F5 (void);
// 0x0000008B System.Void Lean.Common.LeanConstrainToOrthographic::set_Plane(Lean.Common.LeanPlane)
extern void LeanConstrainToOrthographic_set_Plane_m9228924C81D36F4F7C28867B909637BEB4D891F9 (void);
// 0x0000008C Lean.Common.LeanPlane Lean.Common.LeanConstrainToOrthographic::get_Plane()
extern void LeanConstrainToOrthographic_get_Plane_m6B84298794DD9028C82A6799B18CCB235B7D9E4A (void);
// 0x0000008D System.Void Lean.Common.LeanConstrainToOrthographic::LateUpdate()
extern void LeanConstrainToOrthographic_LateUpdate_mEF4C7DE3328B66DFF151E01561F828A428475BAD (void);
// 0x0000008E System.Void Lean.Common.LeanConstrainToOrthographic::.ctor()
extern void LeanConstrainToOrthographic__ctor_m2401A731C108582073CBC62DFABCEB81D4BF26F3 (void);
// 0x0000008F System.Void Lean.Common.LeanDelayedValue::set_Delay(System.Single)
extern void LeanDelayedValue_set_Delay_m80EEA757DD0952CEFC93E80A6A39EB2A964659A7 (void);
// 0x00000090 System.Single Lean.Common.LeanDelayedValue::get_Delay()
extern void LeanDelayedValue_get_Delay_mCADA79B012CD4B533513B9048135B0EF8E399E99 (void);
// 0x00000091 System.Void Lean.Common.LeanDelayedValue::set_AutoClear(System.Boolean)
extern void LeanDelayedValue_set_AutoClear_m96CDB97151C7A01EEA7469E721E80483423EC9F9 (void);
// 0x00000092 System.Boolean Lean.Common.LeanDelayedValue::get_AutoClear()
extern void LeanDelayedValue_get_AutoClear_m1EF176FF69C8EF4B7A75B6B8E57D95729073004E (void);
// 0x00000093 Lean.Common.LeanDelayedValue/FloatEvent Lean.Common.LeanDelayedValue::get_OnValueX()
extern void LeanDelayedValue_get_OnValueX_m42FDDC59270ABCCFA0A83E777A91139B6069077F (void);
// 0x00000094 Lean.Common.LeanDelayedValue/FloatEvent Lean.Common.LeanDelayedValue::get_OnValueY()
extern void LeanDelayedValue_get_OnValueY_m53860944C363B18C3568A76651284EC183B56FC4 (void);
// 0x00000095 Lean.Common.LeanDelayedValue/FloatEvent Lean.Common.LeanDelayedValue::get_OnValueZ()
extern void LeanDelayedValue_get_OnValueZ_m9A790DE011976CF286DBA4F1B531C1E2EF4C4F8F (void);
// 0x00000096 Lean.Common.LeanDelayedValue/Vector2Event Lean.Common.LeanDelayedValue::get_OnValueXY()
extern void LeanDelayedValue_get_OnValueXY_m1E553EC2BA43E0823F9DF78D04D0ADDB6898FBFD (void);
// 0x00000097 Lean.Common.LeanDelayedValue/Vector3Event Lean.Common.LeanDelayedValue::get_OnValueXYZ()
extern void LeanDelayedValue_get_OnValueXYZ_mFDB4A3A1A6B95BF6CCAD23898549C14AF2031353 (void);
// 0x00000098 System.Void Lean.Common.LeanDelayedValue::SetX(System.Single)
extern void LeanDelayedValue_SetX_m128B250DB3932F24F7BC78ECC263048888FC4F6F (void);
// 0x00000099 System.Void Lean.Common.LeanDelayedValue::SetY(System.Single)
extern void LeanDelayedValue_SetY_m28FB43C1FE86D2F3AAB74E30B09EE316BC1C6367 (void);
// 0x0000009A System.Void Lean.Common.LeanDelayedValue::SetZ(System.Single)
extern void LeanDelayedValue_SetZ_m36C0A66CAA6BE653F9E2051797ECD6C5F6EAE8D8 (void);
// 0x0000009B System.Void Lean.Common.LeanDelayedValue::SetXY(UnityEngine.Vector2)
extern void LeanDelayedValue_SetXY_m1DFBF30FB51F296B16F427D50CCDF5E76417F59B (void);
// 0x0000009C System.Void Lean.Common.LeanDelayedValue::SetXYZ(UnityEngine.Vector3)
extern void LeanDelayedValue_SetXYZ_mF59642239A344121365DEDECF4173E2B462F766D (void);
// 0x0000009D System.Void Lean.Common.LeanDelayedValue::Clear()
extern void LeanDelayedValue_Clear_mCB87F767636FB562EF4BCA1E0D2FFDB31C7DFB17 (void);
// 0x0000009E System.Void Lean.Common.LeanDelayedValue::Update()
extern void LeanDelayedValue_Update_m3B396C55FEE8DEED324E75F27E8619C2FD7F5A32 (void);
// 0x0000009F System.Void Lean.Common.LeanDelayedValue::Submit(UnityEngine.Vector3)
extern void LeanDelayedValue_Submit_mA96A1003C525BD63930E9788173F980E6201B573 (void);
// 0x000000A0 System.Void Lean.Common.LeanDelayedValue::.ctor()
extern void LeanDelayedValue__ctor_mBA6A9BC44C9C6F87EFAB11B32D7EB59ACC365F21 (void);
// 0x000000A1 System.Void Lean.Common.LeanDelayedValue/FloatEvent::.ctor()
extern void FloatEvent__ctor_m0BA5B83E9B18D2FAC447D657B9A230D091C76AD9 (void);
// 0x000000A2 System.Void Lean.Common.LeanDelayedValue/Vector2Event::.ctor()
extern void Vector2Event__ctor_m916F31F42BBAD15B7A6C1EDA5CEDF9602DA5A9E5 (void);
// 0x000000A3 System.Void Lean.Common.LeanDelayedValue/Vector3Event::.ctor()
extern void Vector3Event__ctor_m46A172A92CCE399A273FEB663F160782B91FA48D (void);
// 0x000000A4 Lean.Common.LeanDeselected/LeanSelectableEvent Lean.Common.LeanDeselected::get_OnSelectable()
extern void LeanDeselected_get_OnSelectable_m839470D2C651AD8E359C69823F052F25E1232227 (void);
// 0x000000A5 System.Void Lean.Common.LeanDeselected::OnEnable()
extern void LeanDeselected_OnEnable_mC99CE99CB827767D116850E1B515C1D9BFF525B0 (void);
// 0x000000A6 System.Void Lean.Common.LeanDeselected::OnDisable()
extern void LeanDeselected_OnDisable_m1A2AA2679718A60D24B7DF07EFABCB24EC87DB90 (void);
// 0x000000A7 System.Void Lean.Common.LeanDeselected::HandleAnyDeselected(Lean.Common.LeanSelect,Lean.Common.LeanSelectable)
extern void LeanDeselected_HandleAnyDeselected_m87DC8B73BAD0B65E309257D42342ED45D8945D5A (void);
// 0x000000A8 System.Void Lean.Common.LeanDeselected::.ctor()
extern void LeanDeselected__ctor_m3A57013051947A16C4AF86A62A385B7978EFBFE2 (void);
// 0x000000A9 System.Void Lean.Common.LeanDeselected/LeanSelectableEvent::.ctor()
extern void LeanSelectableEvent__ctor_mDF3F9EECEF296751D6960A0CC5018D844E1473B3 (void);
// 0x000000AA System.Void Lean.Common.LeanFollow::set_Threshold(System.Single)
extern void LeanFollow_set_Threshold_mB10181E711B21BD85CECC34B1238B9096E15F01F (void);
// 0x000000AB System.Single Lean.Common.LeanFollow::get_Threshold()
extern void LeanFollow_get_Threshold_m68A40FD40FE0388CD2252C42F3CA8AF4CEEB1C80 (void);
// 0x000000AC System.Void Lean.Common.LeanFollow::set_Speed(System.Single)
extern void LeanFollow_set_Speed_m429CD3556F3CB14DF7A26D3CCABB087BB83F4578 (void);
// 0x000000AD System.Single Lean.Common.LeanFollow::get_Speed()
extern void LeanFollow_get_Speed_m3F02F106B38CAA6F849A14DE42865421C908E38A (void);
// 0x000000AE UnityEngine.Events.UnityEvent Lean.Common.LeanFollow::get_OnReachedDestination()
extern void LeanFollow_get_OnReachedDestination_m043DA2E4E5D6C2EBFEB743791D75CBE931E28651 (void);
// 0x000000AF System.Void Lean.Common.LeanFollow::ClearPositions()
extern void LeanFollow_ClearPositions_m485D7C6E05A7854AB051A25D237388886E716E8C (void);
// 0x000000B0 System.Void Lean.Common.LeanFollow::SnapToNextPosition()
extern void LeanFollow_SnapToNextPosition_mB65155AE801812A8D0003300C67727A368953FBB (void);
// 0x000000B1 System.Void Lean.Common.LeanFollow::AddPosition(UnityEngine.Vector3)
extern void LeanFollow_AddPosition_m4F09D74AECBFB1F48E4CA14F01D8462219299137 (void);
// 0x000000B2 System.Void Lean.Common.LeanFollow::Update()
extern void LeanFollow_Update_m41E1B7205F65A1616FA6B58C499CB40C54F05B4A (void);
// 0x000000B3 System.Void Lean.Common.LeanFollow::TrimPositions()
extern void LeanFollow_TrimPositions_m6549191E7ED3A1BB8762C63F58A0233E4D65B492 (void);
// 0x000000B4 System.Void Lean.Common.LeanFollow::.ctor()
extern void LeanFollow__ctor_mAD02154475CAF6542DCE59F305D77240357B35B6 (void);
// 0x000000B5 System.Void Lean.Common.LeanMaintainDistance::set_Direction(UnityEngine.Vector3)
extern void LeanMaintainDistance_set_Direction_mDE2FDA163F1BB99A8A4CF1638FDEBABF0ED9A7B0 (void);
// 0x000000B6 UnityEngine.Vector3 Lean.Common.LeanMaintainDistance::get_Direction()
extern void LeanMaintainDistance_get_Direction_m559333D57E88ED96443C2882185B78D43920E2FB (void);
// 0x000000B7 System.Void Lean.Common.LeanMaintainDistance::set_DirectionSpace(UnityEngine.Space)
extern void LeanMaintainDistance_set_DirectionSpace_m50FD326EA0932621E4169B5798610186C78E1D3E (void);
// 0x000000B8 UnityEngine.Space Lean.Common.LeanMaintainDistance::get_DirectionSpace()
extern void LeanMaintainDistance_get_DirectionSpace_m6CE9F97907AB15BA422D3D7F36EDDA5B3FE5A81F (void);
// 0x000000B9 System.Void Lean.Common.LeanMaintainDistance::set_Distance(System.Single)
extern void LeanMaintainDistance_set_Distance_m59A1F6F92D0EE200DFB508EC88F96C57BB6F925E (void);
// 0x000000BA System.Single Lean.Common.LeanMaintainDistance::get_Distance()
extern void LeanMaintainDistance_get_Distance_m9EE34C0FD19125650F2092A53EEF2B0055A2E9D6 (void);
// 0x000000BB System.Void Lean.Common.LeanMaintainDistance::set_Damping(System.Single)
extern void LeanMaintainDistance_set_Damping_m3E83073288B3C80A71B71FB0222695D4362AF697 (void);
// 0x000000BC System.Single Lean.Common.LeanMaintainDistance::get_Damping()
extern void LeanMaintainDistance_get_Damping_m150B69D54BB457678692142D946D6B779BF5F4CB (void);
// 0x000000BD System.Void Lean.Common.LeanMaintainDistance::set_Clamp(System.Boolean)
extern void LeanMaintainDistance_set_Clamp_m92DB42527494F04697E331BE61DC9B401071E7B6 (void);
// 0x000000BE System.Boolean Lean.Common.LeanMaintainDistance::get_Clamp()
extern void LeanMaintainDistance_get_Clamp_mFF723E96FBD8C53B32E90B81CAEB6B9E973B3622 (void);
// 0x000000BF System.Void Lean.Common.LeanMaintainDistance::set_ClampMin(System.Single)
extern void LeanMaintainDistance_set_ClampMin_m9ABE98819D74FE084A577BF2B7414B22A5E58292 (void);
// 0x000000C0 System.Single Lean.Common.LeanMaintainDistance::get_ClampMin()
extern void LeanMaintainDistance_get_ClampMin_m80400A462104EC53073DE1EB03BF204D6F8728F6 (void);
// 0x000000C1 System.Void Lean.Common.LeanMaintainDistance::set_ClampMax(System.Single)
extern void LeanMaintainDistance_set_ClampMax_mF3EC942C9F064AA8BB6EE79CA68B4929C61BBC41 (void);
// 0x000000C2 System.Single Lean.Common.LeanMaintainDistance::get_ClampMax()
extern void LeanMaintainDistance_get_ClampMax_m051A71DDE7B771BF321214EDA973C2ED40F77258 (void);
// 0x000000C3 System.Void Lean.Common.LeanMaintainDistance::set_CollisionLayers(UnityEngine.LayerMask)
extern void LeanMaintainDistance_set_CollisionLayers_m7A0C9D359425B8F683AE505E7BFC1100E671C874 (void);
// 0x000000C4 UnityEngine.LayerMask Lean.Common.LeanMaintainDistance::get_CollisionLayers()
extern void LeanMaintainDistance_get_CollisionLayers_m55A1CAF14219422A7D1A3577A07B01D40375A76B (void);
// 0x000000C5 System.Void Lean.Common.LeanMaintainDistance::set_CollisionRadius(System.Single)
extern void LeanMaintainDistance_set_CollisionRadius_m18B24AA07ABD4C4E821C8DE4BAE0A09CD29F445E (void);
// 0x000000C6 System.Single Lean.Common.LeanMaintainDistance::get_CollisionRadius()
extern void LeanMaintainDistance_get_CollisionRadius_mA6BC598E241A2F471B6255D2B48888E52DF41C9B (void);
// 0x000000C7 System.Void Lean.Common.LeanMaintainDistance::AddDistance(System.Single)
extern void LeanMaintainDistance_AddDistance_m14637F30A24E9E12AFE958E90AFF4297F9CD5D99 (void);
// 0x000000C8 System.Void Lean.Common.LeanMaintainDistance::MultiplyDistance(System.Single)
extern void LeanMaintainDistance_MultiplyDistance_mDE30ECD65838ED2E21249870C81DA705C71BE79C (void);
// 0x000000C9 System.Void Lean.Common.LeanMaintainDistance::Start()
extern void LeanMaintainDistance_Start_m63153D80A752AF6D66998691FD9BE42336DA3570 (void);
// 0x000000CA System.Void Lean.Common.LeanMaintainDistance::LateUpdate()
extern void LeanMaintainDistance_LateUpdate_mA7BD4270E21A518CAF9390CC8F5835649E34DC7B (void);
// 0x000000CB System.Void Lean.Common.LeanMaintainDistance::.ctor()
extern void LeanMaintainDistance__ctor_mDB109767140D0873E448C0C70F12028FFB1EEC37 (void);
// 0x000000CC System.Void Lean.Common.LeanManualRescale::set_Target(UnityEngine.GameObject)
extern void LeanManualRescale_set_Target_m84B9D62189054579F7015A7FD150020C172F19A8 (void);
// 0x000000CD UnityEngine.GameObject Lean.Common.LeanManualRescale::get_Target()
extern void LeanManualRescale_get_Target_m8F2738CA275C4CBF3822E2E0A2ABD6F7FA8E0729 (void);
// 0x000000CE System.Void Lean.Common.LeanManualRescale::set_AxesA(UnityEngine.Vector3)
extern void LeanManualRescale_set_AxesA_mC910AC62382C897BD5368F5CD8A21D184F2CD484 (void);
// 0x000000CF UnityEngine.Vector3 Lean.Common.LeanManualRescale::get_AxesA()
extern void LeanManualRescale_get_AxesA_mB657741F1FD943467FE452508513D2CECE38D43F (void);
// 0x000000D0 System.Void Lean.Common.LeanManualRescale::set_AxesB(UnityEngine.Vector3)
extern void LeanManualRescale_set_AxesB_mE94F16663F766B65BAE8315355D985BAABE38E3A (void);
// 0x000000D1 UnityEngine.Vector3 Lean.Common.LeanManualRescale::get_AxesB()
extern void LeanManualRescale_get_AxesB_m82733E0DB4F69126DBD219ED9B0144CDB3EC79AE (void);
// 0x000000D2 System.Void Lean.Common.LeanManualRescale::set_Multiplier(System.Single)
extern void LeanManualRescale_set_Multiplier_m01B4B93D6A0A712436A83E6AFBDCC693EB122B0D (void);
// 0x000000D3 System.Single Lean.Common.LeanManualRescale::get_Multiplier()
extern void LeanManualRescale_get_Multiplier_mC05603AB6DAD8BFE32F16CC21C9CDD9DD8FA955F (void);
// 0x000000D4 System.Void Lean.Common.LeanManualRescale::set_Damping(System.Single)
extern void LeanManualRescale_set_Damping_mBD7F09FB5E045A7CAE63E901F1E728566C326855 (void);
// 0x000000D5 System.Single Lean.Common.LeanManualRescale::get_Damping()
extern void LeanManualRescale_get_Damping_mAD6EB67F4924B3E86924A7E6151A5C863757E245 (void);
// 0x000000D6 System.Void Lean.Common.LeanManualRescale::set_ScaleByTime(System.Boolean)
extern void LeanManualRescale_set_ScaleByTime_mE78249CFA38F7E5D1A777989DE058E0E96FC6E6B (void);
// 0x000000D7 System.Boolean Lean.Common.LeanManualRescale::get_ScaleByTime()
extern void LeanManualRescale_get_ScaleByTime_mFCD9E98DFBC30AB7ED6354A1326DC884A8AA3015 (void);
// 0x000000D8 System.Void Lean.Common.LeanManualRescale::set_DefaultScale(UnityEngine.Vector3)
extern void LeanManualRescale_set_DefaultScale_m63C75315D9AD4F5A33A176CB8A644042644A8240 (void);
// 0x000000D9 UnityEngine.Vector3 Lean.Common.LeanManualRescale::get_DefaultScale()
extern void LeanManualRescale_get_DefaultScale_m9E561F3A1B8778410EC83A3D930F30037F3F21B1 (void);
// 0x000000DA System.Void Lean.Common.LeanManualRescale::ResetScale()
extern void LeanManualRescale_ResetScale_m73FDFECE254BA9F6FEC221B9415F56013705C68F (void);
// 0x000000DB System.Void Lean.Common.LeanManualRescale::SnapToTarget()
extern void LeanManualRescale_SnapToTarget_m9878D0C99CFF0AF6D57525E981F88E00B02C0AE4 (void);
// 0x000000DC System.Void Lean.Common.LeanManualRescale::AddScaleA(System.Single)
extern void LeanManualRescale_AddScaleA_mD5524D74C62D7C89F215C20BC621EF8CEE814F35 (void);
// 0x000000DD System.Void Lean.Common.LeanManualRescale::AddScaleB(System.Single)
extern void LeanManualRescale_AddScaleB_mC748CF0E1066D9064760C0823663E75A0C0B2DCD (void);
// 0x000000DE System.Void Lean.Common.LeanManualRescale::AddScaleAB(UnityEngine.Vector2)
extern void LeanManualRescale_AddScaleAB_mCDC1ECB47FD8A323621182FFEE45D182579362C0 (void);
// 0x000000DF System.Void Lean.Common.LeanManualRescale::AddScale(UnityEngine.Vector3)
extern void LeanManualRescale_AddScale_mD12DAD81D73F43BDD78ACAA006AE6439FED78663 (void);
// 0x000000E0 System.Void Lean.Common.LeanManualRescale::Update()
extern void LeanManualRescale_Update_m2315B9BA8F1A4B6C9C9E41C0833441593E73BC08 (void);
// 0x000000E1 System.Void Lean.Common.LeanManualRescale::UpdateScale(System.Single)
extern void LeanManualRescale_UpdateScale_mC372F982E450375A60D0B61588116AAB8012A8A1 (void);
// 0x000000E2 System.Void Lean.Common.LeanManualRescale::.ctor()
extern void LeanManualRescale__ctor_mB99E0D9BB1F7F12C1CBC743B85B6830D2A2CBCCB (void);
// 0x000000E3 System.Void Lean.Common.LeanManualRotate::set_Target(UnityEngine.GameObject)
extern void LeanManualRotate_set_Target_m79CF6EB8F30718DBA31926FAB22BF2A171E5A1AD (void);
// 0x000000E4 UnityEngine.GameObject Lean.Common.LeanManualRotate::get_Target()
extern void LeanManualRotate_get_Target_mD1ADAF00F73BBEA1E3B008E202B3337312BA9D0E (void);
// 0x000000E5 System.Void Lean.Common.LeanManualRotate::set_Space(UnityEngine.Space)
extern void LeanManualRotate_set_Space_m0CF947853F4C40DC9CA25B409B2879BFECB4E298 (void);
// 0x000000E6 UnityEngine.Space Lean.Common.LeanManualRotate::get_Space()
extern void LeanManualRotate_get_Space_m35CB54E7091146E8B2F61CFA09F2CBB4244CEAD6 (void);
// 0x000000E7 System.Void Lean.Common.LeanManualRotate::set_AxisA(UnityEngine.Vector3)
extern void LeanManualRotate_set_AxisA_m1B5B2219D77A9EA4347887004DEB7BF9E7D5A9C1 (void);
// 0x000000E8 UnityEngine.Vector3 Lean.Common.LeanManualRotate::get_AxisA()
extern void LeanManualRotate_get_AxisA_m986E5AFCC38CFB1F0FE5127C450CAC6BED28B4BB (void);
// 0x000000E9 System.Void Lean.Common.LeanManualRotate::set_AxisB(UnityEngine.Vector3)
extern void LeanManualRotate_set_AxisB_m57AACB491431E1C36FE7EF9CECBC60C51862860E (void);
// 0x000000EA UnityEngine.Vector3 Lean.Common.LeanManualRotate::get_AxisB()
extern void LeanManualRotate_get_AxisB_m296C1CBA18C3951A82152C6687F0AA29B6CAE628 (void);
// 0x000000EB System.Void Lean.Common.LeanManualRotate::set_Multiplier(System.Single)
extern void LeanManualRotate_set_Multiplier_m7ADC6E13743035565D2DE1040D7CE4EC984B82F3 (void);
// 0x000000EC System.Single Lean.Common.LeanManualRotate::get_Multiplier()
extern void LeanManualRotate_get_Multiplier_mCDEACC8118D376B35F40FF17D2759AA5F90B5655 (void);
// 0x000000ED System.Void Lean.Common.LeanManualRotate::set_Damping(System.Single)
extern void LeanManualRotate_set_Damping_mD3BA0C24468A695E3425CACE57BD812203FBB614 (void);
// 0x000000EE System.Single Lean.Common.LeanManualRotate::get_Damping()
extern void LeanManualRotate_get_Damping_mB2530B1922AE3CC5715D8377E7A83F9C7082B27D (void);
// 0x000000EF System.Void Lean.Common.LeanManualRotate::set_ScaleByTime(System.Boolean)
extern void LeanManualRotate_set_ScaleByTime_mAD22F074D3E678DEFF02DA51E61C1C05746CCD33 (void);
// 0x000000F0 System.Boolean Lean.Common.LeanManualRotate::get_ScaleByTime()
extern void LeanManualRotate_get_ScaleByTime_mDA93E8A9BBF9E2BD61351406850A267B5FCF4480 (void);
// 0x000000F1 System.Void Lean.Common.LeanManualRotate::set_DefaultRotation(UnityEngine.Vector3)
extern void LeanManualRotate_set_DefaultRotation_m7511ECD3B02B8A1B105C73056E95C265F83C3087 (void);
// 0x000000F2 UnityEngine.Vector3 Lean.Common.LeanManualRotate::get_DefaultRotation()
extern void LeanManualRotate_get_DefaultRotation_m65E0FBF8FAFC19F0CF99DEB51591E803E8E11105 (void);
// 0x000000F3 System.Void Lean.Common.LeanManualRotate::ResetRotation()
extern void LeanManualRotate_ResetRotation_m90126CB30981A7298DA85A664644C7850E10DC8B (void);
// 0x000000F4 System.Void Lean.Common.LeanManualRotate::SnapToTarget()
extern void LeanManualRotate_SnapToTarget_m0F590A6B43C6D3760E4BEF4A975054D80AE2338F (void);
// 0x000000F5 System.Void Lean.Common.LeanManualRotate::StopRotation()
extern void LeanManualRotate_StopRotation_m9E83B607C247214965CD067ACFC18D71F25A75D0 (void);
// 0x000000F6 System.Void Lean.Common.LeanManualRotate::RotateA(System.Single)
extern void LeanManualRotate_RotateA_m7FE6D4F994370DB14504C52D3FFC79962977E303 (void);
// 0x000000F7 System.Void Lean.Common.LeanManualRotate::RotateB(System.Single)
extern void LeanManualRotate_RotateB_m265340CF9B1B21B0ECBB5050433374C2C9CB1CAA (void);
// 0x000000F8 System.Void Lean.Common.LeanManualRotate::RotateAB(UnityEngine.Vector2)
extern void LeanManualRotate_RotateAB_m063A2C24160D0102457B7003473BE24CFCEC76AB (void);
// 0x000000F9 System.Void Lean.Common.LeanManualRotate::Update()
extern void LeanManualRotate_Update_m3C2959E1C2B1E8544E88386A88AF15AC2A67AC0D (void);
// 0x000000FA System.Void Lean.Common.LeanManualRotate::UpdateRotation(System.Single)
extern void LeanManualRotate_UpdateRotation_m75B29FAE114355304C8BBFB518BC955430848590 (void);
// 0x000000FB System.Void Lean.Common.LeanManualRotate::.ctor()
extern void LeanManualRotate__ctor_mA03A81D7A765BFF06C0095A4F8AB0EE580FCD7CD (void);
// 0x000000FC System.Void Lean.Common.LeanManualTorque::set_Target(UnityEngine.GameObject)
extern void LeanManualTorque_set_Target_m5A57BDF13826FD12291E3BF202167AFE2A95B283 (void);
// 0x000000FD UnityEngine.GameObject Lean.Common.LeanManualTorque::get_Target()
extern void LeanManualTorque_get_Target_m91E0805B83D2373221C8CD45FC0AD5DD22FD4986 (void);
// 0x000000FE System.Void Lean.Common.LeanManualTorque::set_Mode(UnityEngine.ForceMode)
extern void LeanManualTorque_set_Mode_m75758CD411F7D9716E8ACF1953C1597A0EC638A3 (void);
// 0x000000FF UnityEngine.ForceMode Lean.Common.LeanManualTorque::get_Mode()
extern void LeanManualTorque_get_Mode_m6AC5F7B03B90A1C61FFD8175FC7766A3EB4C9B5B (void);
// 0x00000100 System.Void Lean.Common.LeanManualTorque::set_Multiplier(System.Single)
extern void LeanManualTorque_set_Multiplier_mCD164A7F4FDA73114D9922AFCA0BF027731B867D (void);
// 0x00000101 System.Single Lean.Common.LeanManualTorque::get_Multiplier()
extern void LeanManualTorque_get_Multiplier_m8E57E0D028BBBF66D6C1381CAF55E2A6A12376A4 (void);
// 0x00000102 System.Void Lean.Common.LeanManualTorque::set_Space(UnityEngine.Space)
extern void LeanManualTorque_set_Space_m67D234EEF3DD2DAD5B1A184AAC63FE387EE76A3C (void);
// 0x00000103 UnityEngine.Space Lean.Common.LeanManualTorque::get_Space()
extern void LeanManualTorque_get_Space_m517376ED0DE646776BFD55ED797E8B26513DC347 (void);
// 0x00000104 System.Void Lean.Common.LeanManualTorque::set_AxisA(UnityEngine.Vector3)
extern void LeanManualTorque_set_AxisA_m82707290401C5FAA9AEB125615774299FEAA8A7F (void);
// 0x00000105 UnityEngine.Vector3 Lean.Common.LeanManualTorque::get_AxisA()
extern void LeanManualTorque_get_AxisA_m1F6C97530B7711FD15362ED660A3781F2C21155E (void);
// 0x00000106 System.Void Lean.Common.LeanManualTorque::set_AxisB(UnityEngine.Vector3)
extern void LeanManualTorque_set_AxisB_mDA4E467B47E4DD9D8F961F4A5904ABCDB9C83F39 (void);
// 0x00000107 UnityEngine.Vector3 Lean.Common.LeanManualTorque::get_AxisB()
extern void LeanManualTorque_get_AxisB_m4F9A308E54F46FEB4BE239DB0AA69F0C3D5CA398 (void);
// 0x00000108 System.Void Lean.Common.LeanManualTorque::AddTorqueA(System.Single)
extern void LeanManualTorque_AddTorqueA_m8F02F5E586AD4D448551017D695501F11C8928F9 (void);
// 0x00000109 System.Void Lean.Common.LeanManualTorque::AddTorqueB(System.Single)
extern void LeanManualTorque_AddTorqueB_m2580DDCAA5AB26EABC9046B0E01042BC97616FDB (void);
// 0x0000010A System.Void Lean.Common.LeanManualTorque::AddTorqueAB(UnityEngine.Vector2)
extern void LeanManualTorque_AddTorqueAB_m7D09F21DF203205426774262D303E170949218F0 (void);
// 0x0000010B System.Void Lean.Common.LeanManualTorque::AddTorqueFromTo(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanManualTorque_AddTorqueFromTo_m23F47462D5F5EE8BAE48498605AAD9230E8B57DA (void);
// 0x0000010C System.Void Lean.Common.LeanManualTorque::AddTorque(UnityEngine.Vector3)
extern void LeanManualTorque_AddTorque_m1A073879D238BE3E69361A3E6DEF460F892CA458 (void);
// 0x0000010D System.Void Lean.Common.LeanManualTorque::.ctor()
extern void LeanManualTorque__ctor_m88F64175855F9D9ED7CDE602477FA0127550A552 (void);
// 0x0000010E System.Void Lean.Common.LeanManualTranslate::set_Target(UnityEngine.GameObject)
extern void LeanManualTranslate_set_Target_m2F916EE37BDC584BC198DE6ADC764991EAB79F90 (void);
// 0x0000010F UnityEngine.GameObject Lean.Common.LeanManualTranslate::get_Target()
extern void LeanManualTranslate_get_Target_m84D456CBE905C062B2747429E3B70025F92151E9 (void);
// 0x00000110 System.Void Lean.Common.LeanManualTranslate::set_Space(UnityEngine.Space)
extern void LeanManualTranslate_set_Space_m61C4EDFF4FE87CC1589E0416768F28065DA1E76A (void);
// 0x00000111 UnityEngine.Space Lean.Common.LeanManualTranslate::get_Space()
extern void LeanManualTranslate_get_Space_mB77FB744CDE7CBA2D4B826A786161AEF01604CF9 (void);
// 0x00000112 System.Void Lean.Common.LeanManualTranslate::set_DirectionA(UnityEngine.Vector3)
extern void LeanManualTranslate_set_DirectionA_m6322DB155340B030BC7DF231E9F0991530ED28C7 (void);
// 0x00000113 UnityEngine.Vector3 Lean.Common.LeanManualTranslate::get_DirectionA()
extern void LeanManualTranslate_get_DirectionA_mF08DEC10315312E82982D87EB046DD9224B0725D (void);
// 0x00000114 System.Void Lean.Common.LeanManualTranslate::set_DirectionB(UnityEngine.Vector3)
extern void LeanManualTranslate_set_DirectionB_m6EE5F9203D5E877DD99B9EA269F57DB4514E0954 (void);
// 0x00000115 UnityEngine.Vector3 Lean.Common.LeanManualTranslate::get_DirectionB()
extern void LeanManualTranslate_get_DirectionB_m2B505D1A5B66D4FBDCF9A3FA1127746F59F68897 (void);
// 0x00000116 System.Void Lean.Common.LeanManualTranslate::set_Multiplier(System.Single)
extern void LeanManualTranslate_set_Multiplier_m217828A198AA77DE588E175AD329AB9EB871400A (void);
// 0x00000117 System.Single Lean.Common.LeanManualTranslate::get_Multiplier()
extern void LeanManualTranslate_get_Multiplier_m52207693F3CB2AF2625FDD6D973BDA6767B5B9A2 (void);
// 0x00000118 System.Void Lean.Common.LeanManualTranslate::set_Damping(System.Single)
extern void LeanManualTranslate_set_Damping_mB2A587AF7267B6CB2E57A8AA84096FA6F9A8D01C (void);
// 0x00000119 System.Single Lean.Common.LeanManualTranslate::get_Damping()
extern void LeanManualTranslate_get_Damping_m32C6C1826E70F741814524B672565C6EE1819A3E (void);
// 0x0000011A System.Void Lean.Common.LeanManualTranslate::set_ScaleByTime(System.Boolean)
extern void LeanManualTranslate_set_ScaleByTime_mA57146DBCE9A4A9C7F280CB689128848B8325422 (void);
// 0x0000011B System.Boolean Lean.Common.LeanManualTranslate::get_ScaleByTime()
extern void LeanManualTranslate_get_ScaleByTime_m0D706B9C88994CC1A36837E641C1AEA381B12D79 (void);
// 0x0000011C System.Void Lean.Common.LeanManualTranslate::set_DefaultPosition(UnityEngine.Vector3)
extern void LeanManualTranslate_set_DefaultPosition_m1898D629DC42C50DE40CEDC854AC2E9E303BEB51 (void);
// 0x0000011D UnityEngine.Vector3 Lean.Common.LeanManualTranslate::get_DefaultPosition()
extern void LeanManualTranslate_get_DefaultPosition_m0374FC5830642EA47B552722F20162292C0FC0EC (void);
// 0x0000011E System.Void Lean.Common.LeanManualTranslate::ResetPosition()
extern void LeanManualTranslate_ResetPosition_m96A8915E69D6DCF055E70D2C67B37035BEEE89E4 (void);
// 0x0000011F System.Void Lean.Common.LeanManualTranslate::SnapToTarget()
extern void LeanManualTranslate_SnapToTarget_m1EC30AA794459BDEC88980D4DC737EF87868F7B8 (void);
// 0x00000120 System.Void Lean.Common.LeanManualTranslate::TranslateA(System.Single)
extern void LeanManualTranslate_TranslateA_m13039C5771CD72C027D18F98F65E7884D2033324 (void);
// 0x00000121 System.Void Lean.Common.LeanManualTranslate::TranslateB(System.Single)
extern void LeanManualTranslate_TranslateB_m7DCC6507B0104E6E5F909035403C25B09A2CFD14 (void);
// 0x00000122 System.Void Lean.Common.LeanManualTranslate::TranslateAB(UnityEngine.Vector2)
extern void LeanManualTranslate_TranslateAB_mF271CB96C47ECD8429EA5005DB314A71763586D9 (void);
// 0x00000123 System.Void Lean.Common.LeanManualTranslate::Translate(UnityEngine.Vector3)
extern void LeanManualTranslate_Translate_m8944EDB703932E5D26E4950A3C3B9D625B6FB125 (void);
// 0x00000124 System.Void Lean.Common.LeanManualTranslate::TranslateWorld(UnityEngine.Vector3)
extern void LeanManualTranslate_TranslateWorld_m808CB3A9138FB0A537F26E2270A47319E66F7DC4 (void);
// 0x00000125 System.Void Lean.Common.LeanManualTranslate::Update()
extern void LeanManualTranslate_Update_m962B9AF1F9354336017FBB46ED298445F815973A (void);
// 0x00000126 System.Void Lean.Common.LeanManualTranslate::UpdatePosition(System.Single)
extern void LeanManualTranslate_UpdatePosition_m3369710F97FFBD77B2C38C1D2BA3DA21BD97C29E (void);
// 0x00000127 System.Void Lean.Common.LeanManualTranslate::.ctor()
extern void LeanManualTranslate__ctor_m0140F5E11EA9A60DD3A7F602BCE6BF5F6ECEA230 (void);
// 0x00000128 System.Void Lean.Common.LeanManualTranslateRigidbody::set_Target(UnityEngine.GameObject)
extern void LeanManualTranslateRigidbody_set_Target_m218A9F0BFCF3C79A644EEAF454CA5D670BB60A2F (void);
// 0x00000129 UnityEngine.GameObject Lean.Common.LeanManualTranslateRigidbody::get_Target()
extern void LeanManualTranslateRigidbody_get_Target_m6DDAE1EDDF17976AC7BA5A79B2CAC979192B704D (void);
// 0x0000012A System.Void Lean.Common.LeanManualTranslateRigidbody::set_Space(UnityEngine.Space)
extern void LeanManualTranslateRigidbody_set_Space_m55C1D7C527B3BED2ADD76396ED15C86431ED54C4 (void);
// 0x0000012B UnityEngine.Space Lean.Common.LeanManualTranslateRigidbody::get_Space()
extern void LeanManualTranslateRigidbody_get_Space_mBB7B324A6E1ADB0B2AD05A2999FBDF6C4B9C6291 (void);
// 0x0000012C System.Void Lean.Common.LeanManualTranslateRigidbody::set_DirectionA(UnityEngine.Vector3)
extern void LeanManualTranslateRigidbody_set_DirectionA_mAD93A1E04AC1888655B9BA4948BA2B595F572F76 (void);
// 0x0000012D UnityEngine.Vector3 Lean.Common.LeanManualTranslateRigidbody::get_DirectionA()
extern void LeanManualTranslateRigidbody_get_DirectionA_mFBDA741F30BFC8A34910D1865DF1E3741B55F6B6 (void);
// 0x0000012E System.Void Lean.Common.LeanManualTranslateRigidbody::set_DirectionB(UnityEngine.Vector3)
extern void LeanManualTranslateRigidbody_set_DirectionB_m45FC85A6DB274DEC6261C920BE715BF9B198EC29 (void);
// 0x0000012F UnityEngine.Vector3 Lean.Common.LeanManualTranslateRigidbody::get_DirectionB()
extern void LeanManualTranslateRigidbody_get_DirectionB_m548A113BE54962FCAFF191603CBD30370EE3B816 (void);
// 0x00000130 System.Void Lean.Common.LeanManualTranslateRigidbody::set_Multiplier(System.Single)
extern void LeanManualTranslateRigidbody_set_Multiplier_m53CBE2D0616E24AC98E99A8DD4BE6786FED24C4F (void);
// 0x00000131 System.Single Lean.Common.LeanManualTranslateRigidbody::get_Multiplier()
extern void LeanManualTranslateRigidbody_get_Multiplier_m2634A34592B70D71538996A082B19B6C5685E93B (void);
// 0x00000132 System.Void Lean.Common.LeanManualTranslateRigidbody::set_Damping(System.Single)
extern void LeanManualTranslateRigidbody_set_Damping_mC0B94B6431B14A7FB0F1C0FB748771E418A4C0BA (void);
// 0x00000133 System.Single Lean.Common.LeanManualTranslateRigidbody::get_Damping()
extern void LeanManualTranslateRigidbody_get_Damping_mB4D3181F5E3EEABEC3F6D942D979681D465801B1 (void);
// 0x00000134 System.Void Lean.Common.LeanManualTranslateRigidbody::set_ScaleByTime(System.Boolean)
extern void LeanManualTranslateRigidbody_set_ScaleByTime_m3D45B29B9F964C2919154A8C4A76575A2802FB8C (void);
// 0x00000135 System.Boolean Lean.Common.LeanManualTranslateRigidbody::get_ScaleByTime()
extern void LeanManualTranslateRigidbody_get_ScaleByTime_mEDA7CCFFFB352BB92D759D1AF2B0A5F2B3B0632C (void);
// 0x00000136 System.Void Lean.Common.LeanManualTranslateRigidbody::set_Inertia(System.Single)
extern void LeanManualTranslateRigidbody_set_Inertia_m4BAA9526BD3E60BB8A4B7AA53C1D730D120ABCD8 (void);
// 0x00000137 System.Single Lean.Common.LeanManualTranslateRigidbody::get_Inertia()
extern void LeanManualTranslateRigidbody_get_Inertia_m455C0BCAB8A465CBB79778091C215FA8F8DDE922 (void);
// 0x00000138 System.Void Lean.Common.LeanManualTranslateRigidbody::set_ResetVelocityInUpdate(System.Boolean)
extern void LeanManualTranslateRigidbody_set_ResetVelocityInUpdate_m706A76F582D94A705A6255C8CF34E2406F3A23D7 (void);
// 0x00000139 System.Boolean Lean.Common.LeanManualTranslateRigidbody::get_ResetVelocityInUpdate()
extern void LeanManualTranslateRigidbody_get_ResetVelocityInUpdate_m9AD6E787F3A19CF1500790B2C83E79FFEF8A4330 (void);
// 0x0000013A System.Void Lean.Common.LeanManualTranslateRigidbody::TranslateA(System.Single)
extern void LeanManualTranslateRigidbody_TranslateA_m2512574F1FA14245DF678C15DFE7C6F3ACFFBF8E (void);
// 0x0000013B System.Void Lean.Common.LeanManualTranslateRigidbody::TranslateB(System.Single)
extern void LeanManualTranslateRigidbody_TranslateB_m7D68B41187FDEE9AAB4E7394368345FF1BD53FD9 (void);
// 0x0000013C System.Void Lean.Common.LeanManualTranslateRigidbody::TranslateAB(UnityEngine.Vector2)
extern void LeanManualTranslateRigidbody_TranslateAB_m5F8A2754F0AFEC899C9029345731D690543D59BD (void);
// 0x0000013D System.Void Lean.Common.LeanManualTranslateRigidbody::Translate(UnityEngine.Vector3)
extern void LeanManualTranslateRigidbody_Translate_m9F58D02453E363EC583AEFBAF08B4005F258B461 (void);
// 0x0000013E System.Void Lean.Common.LeanManualTranslateRigidbody::TranslateWorld(UnityEngine.Vector3)
extern void LeanManualTranslateRigidbody_TranslateWorld_mCB5466DD39E3518796FDA874CC684CBEA12C0916 (void);
// 0x0000013F System.Void Lean.Common.LeanManualTranslateRigidbody::FixedUpdate()
extern void LeanManualTranslateRigidbody_FixedUpdate_mF97E6DF6E4AA10B3E54268291D213C4CBE2372DF (void);
// 0x00000140 System.Void Lean.Common.LeanManualTranslateRigidbody::Update()
extern void LeanManualTranslateRigidbody_Update_m3E5C3E997651A62B96D873B293DCD9DB8C078E0E (void);
// 0x00000141 System.Void Lean.Common.LeanManualTranslateRigidbody::.ctor()
extern void LeanManualTranslateRigidbody__ctor_m364FCAB3A8DE27BF57C0E81FAC4F6F8FD2F13D61 (void);
// 0x00000142 System.Void Lean.Common.LeanManualTranslateRigidbody2D::set_Target(UnityEngine.GameObject)
extern void LeanManualTranslateRigidbody2D_set_Target_m730D9D452EF5737BFA834687E0570FF6370183AA (void);
// 0x00000143 UnityEngine.GameObject Lean.Common.LeanManualTranslateRigidbody2D::get_Target()
extern void LeanManualTranslateRigidbody2D_get_Target_mBBC0B11F1E109DDF32EFCF2BBCAF4AA4B2288157 (void);
// 0x00000144 System.Void Lean.Common.LeanManualTranslateRigidbody2D::set_Space(UnityEngine.Space)
extern void LeanManualTranslateRigidbody2D_set_Space_m45F229F681D2D8478D1C9D251F58F89AD5E6FD6E (void);
// 0x00000145 UnityEngine.Space Lean.Common.LeanManualTranslateRigidbody2D::get_Space()
extern void LeanManualTranslateRigidbody2D_get_Space_m587138B50F3FE51CC99D00C0BBC871F0B6C1E931 (void);
// 0x00000146 System.Void Lean.Common.LeanManualTranslateRigidbody2D::set_DirectionA(UnityEngine.Vector3)
extern void LeanManualTranslateRigidbody2D_set_DirectionA_mE011A41C9C33F8CAE5505D9DBF90E1B1CD3BB20A (void);
// 0x00000147 UnityEngine.Vector3 Lean.Common.LeanManualTranslateRigidbody2D::get_DirectionA()
extern void LeanManualTranslateRigidbody2D_get_DirectionA_m1CC1E079E216EC3437F84649E364ED5990AA102C (void);
// 0x00000148 System.Void Lean.Common.LeanManualTranslateRigidbody2D::set_DirectionB(UnityEngine.Vector3)
extern void LeanManualTranslateRigidbody2D_set_DirectionB_m5F000934E92224D50AB587912D6FC7A2FE1322AC (void);
// 0x00000149 UnityEngine.Vector3 Lean.Common.LeanManualTranslateRigidbody2D::get_DirectionB()
extern void LeanManualTranslateRigidbody2D_get_DirectionB_m3BF99E7A7731DEB0CB8551B8BA395B8040B82802 (void);
// 0x0000014A System.Void Lean.Common.LeanManualTranslateRigidbody2D::set_Multiplier(System.Single)
extern void LeanManualTranslateRigidbody2D_set_Multiplier_m3E3FF627D1E797AB0432DCCCB87C647929B17AFD (void);
// 0x0000014B System.Single Lean.Common.LeanManualTranslateRigidbody2D::get_Multiplier()
extern void LeanManualTranslateRigidbody2D_get_Multiplier_m501D411439661AA01F83F7514EFF995FB44BD983 (void);
// 0x0000014C System.Void Lean.Common.LeanManualTranslateRigidbody2D::set_ScaleByTime(System.Boolean)
extern void LeanManualTranslateRigidbody2D_set_ScaleByTime_mF2D67A98DA453143A12BDF7D293D0B6F77EA863F (void);
// 0x0000014D System.Boolean Lean.Common.LeanManualTranslateRigidbody2D::get_ScaleByTime()
extern void LeanManualTranslateRigidbody2D_get_ScaleByTime_m8BD472C4AB2F4E968240BCE21893086B6EDB443F (void);
// 0x0000014E System.Void Lean.Common.LeanManualTranslateRigidbody2D::set_Damping(System.Single)
extern void LeanManualTranslateRigidbody2D_set_Damping_m3EBAF02DA592756EDEBC12451F5DB8960AB15592 (void);
// 0x0000014F System.Single Lean.Common.LeanManualTranslateRigidbody2D::get_Damping()
extern void LeanManualTranslateRigidbody2D_get_Damping_m1F179EDD7D703DDF2834EF28787BB4B2FC5B9188 (void);
// 0x00000150 System.Void Lean.Common.LeanManualTranslateRigidbody2D::set_ResetVelocityInUpdate(System.Boolean)
extern void LeanManualTranslateRigidbody2D_set_ResetVelocityInUpdate_m7165E35D25D0CD20537B4624565F1AACECD6FCAA (void);
// 0x00000151 System.Boolean Lean.Common.LeanManualTranslateRigidbody2D::get_ResetVelocityInUpdate()
extern void LeanManualTranslateRigidbody2D_get_ResetVelocityInUpdate_mC309E0B63A050D2ED7FC2CE86813C3815329C76D (void);
// 0x00000152 System.Void Lean.Common.LeanManualTranslateRigidbody2D::TranslateA(System.Single)
extern void LeanManualTranslateRigidbody2D_TranslateA_mAEF6E40F62DB228ADFD7EDEFD87134B26C18913C (void);
// 0x00000153 System.Void Lean.Common.LeanManualTranslateRigidbody2D::TranslateB(System.Single)
extern void LeanManualTranslateRigidbody2D_TranslateB_mFC1193C30151699E8013D81A46916C0BB84A2004 (void);
// 0x00000154 System.Void Lean.Common.LeanManualTranslateRigidbody2D::TranslateAB(UnityEngine.Vector2)
extern void LeanManualTranslateRigidbody2D_TranslateAB_mE02A243C95275B2C111259FCA6456DF6AFD4C409 (void);
// 0x00000155 System.Void Lean.Common.LeanManualTranslateRigidbody2D::Translate(UnityEngine.Vector3)
extern void LeanManualTranslateRigidbody2D_Translate_m1809104B08E6DB8F3181DEF2F9F61C2DEFCB6810 (void);
// 0x00000156 System.Void Lean.Common.LeanManualTranslateRigidbody2D::TranslateWorld(UnityEngine.Vector3)
extern void LeanManualTranslateRigidbody2D_TranslateWorld_m9F700BC3060485925F81EFE299A3996E97868C76 (void);
// 0x00000157 System.Void Lean.Common.LeanManualTranslateRigidbody2D::FixedUpdate()
extern void LeanManualTranslateRigidbody2D_FixedUpdate_m4775EBDBF15FEFB7C30D1E0A80D48B1971C7CDD7 (void);
// 0x00000158 System.Void Lean.Common.LeanManualTranslateRigidbody2D::Update()
extern void LeanManualTranslateRigidbody2D_Update_m7BCF9FD87CCD38B4413962BE6AA0E7C38A6E806D (void);
// 0x00000159 System.Void Lean.Common.LeanManualTranslateRigidbody2D::.ctor()
extern void LeanManualTranslateRigidbody2D__ctor_m75F4A902BBDCA8BB284380FC2ED6B517371383E8 (void);
// 0x0000015A System.Void Lean.Common.LeanManualVelocity::set_Target(UnityEngine.GameObject)
extern void LeanManualVelocity_set_Target_m9616C9A93827FF8077C846944EB842314759A48C (void);
// 0x0000015B UnityEngine.GameObject Lean.Common.LeanManualVelocity::get_Target()
extern void LeanManualVelocity_get_Target_mB1682F8151356DCDB9F99E0D94DD1DDA01F8D0A6 (void);
// 0x0000015C System.Void Lean.Common.LeanManualVelocity::set_Mode(UnityEngine.ForceMode)
extern void LeanManualVelocity_set_Mode_m950EE81B3000D29D3C7571AA1E74F043D6C1F24D (void);
// 0x0000015D UnityEngine.ForceMode Lean.Common.LeanManualVelocity::get_Mode()
extern void LeanManualVelocity_get_Mode_m05EA5F80AB6C748C5055259AD8AABBCB8EE5C7F2 (void);
// 0x0000015E System.Void Lean.Common.LeanManualVelocity::set_Multiplier(System.Single)
extern void LeanManualVelocity_set_Multiplier_m66765CF3FDE98CE6852FDC636223FA6D45678984 (void);
// 0x0000015F System.Single Lean.Common.LeanManualVelocity::get_Multiplier()
extern void LeanManualVelocity_get_Multiplier_m3AB8D4F7B46A41F209EAF66BEE471AE0DF79A5C9 (void);
// 0x00000160 System.Void Lean.Common.LeanManualVelocity::set_Space(UnityEngine.Space)
extern void LeanManualVelocity_set_Space_m55815DD7525CA62A59E060F567B6B176235616E5 (void);
// 0x00000161 UnityEngine.Space Lean.Common.LeanManualVelocity::get_Space()
extern void LeanManualVelocity_get_Space_mE0FE0D9DF25DAAF67A4E5E0DD0980ACE440F442B (void);
// 0x00000162 System.Void Lean.Common.LeanManualVelocity::set_DirectionA(UnityEngine.Vector2)
extern void LeanManualVelocity_set_DirectionA_mE109EEBD3861F5A787F228B884627077E2467237 (void);
// 0x00000163 UnityEngine.Vector2 Lean.Common.LeanManualVelocity::get_DirectionA()
extern void LeanManualVelocity_get_DirectionA_mD36DE93B21987E624F5185F2AECAC12AEE2B685C (void);
// 0x00000164 System.Void Lean.Common.LeanManualVelocity::set_DirectionB(UnityEngine.Vector2)
extern void LeanManualVelocity_set_DirectionB_m001A5F6C81A82B5DD1BA9000EB3B513379BCF9F1 (void);
// 0x00000165 UnityEngine.Vector2 Lean.Common.LeanManualVelocity::get_DirectionB()
extern void LeanManualVelocity_get_DirectionB_m9DB5F6C7049829960C75E180DBB7C9A153D68820 (void);
// 0x00000166 System.Void Lean.Common.LeanManualVelocity::AddForceA(System.Single)
extern void LeanManualVelocity_AddForceA_m7347426E4453F010E4AB6328ADA87D41CBCF621D (void);
// 0x00000167 System.Void Lean.Common.LeanManualVelocity::AddForceB(System.Single)
extern void LeanManualVelocity_AddForceB_mF6D6D889688FA1190B1B673E897768C63910899F (void);
// 0x00000168 System.Void Lean.Common.LeanManualVelocity::AddForceAB(UnityEngine.Vector2)
extern void LeanManualVelocity_AddForceAB_m4C129CD80815EB660EF7B9072BE287321C6EB045 (void);
// 0x00000169 System.Void Lean.Common.LeanManualVelocity::AddForceFromTo(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanManualVelocity_AddForceFromTo_mD60F55DF73039656F16A683046ADD668EAF1DC01 (void);
// 0x0000016A System.Void Lean.Common.LeanManualVelocity::AddForce(UnityEngine.Vector3)
extern void LeanManualVelocity_AddForce_m2CC9D3456A1A2283D806385A11F3F3970F957846 (void);
// 0x0000016B System.Void Lean.Common.LeanManualVelocity::.ctor()
extern void LeanManualVelocity__ctor_m93F5D5D80B6D15844C480031B184694D0FE071C6 (void);
// 0x0000016C System.Void Lean.Common.LeanManualVelocity2D::set_Target(UnityEngine.GameObject)
extern void LeanManualVelocity2D_set_Target_m9164EB12BDA1281EBD62D5A44491FC2BC9C0D962 (void);
// 0x0000016D UnityEngine.GameObject Lean.Common.LeanManualVelocity2D::get_Target()
extern void LeanManualVelocity2D_get_Target_m8DE71E98FE1E17F4DFE0B1146BEE6F64350012A4 (void);
// 0x0000016E System.Void Lean.Common.LeanManualVelocity2D::set_Mode(UnityEngine.ForceMode2D)
extern void LeanManualVelocity2D_set_Mode_mB82D0A92CF4078811A001BA8F9B6723DFC23C7FC (void);
// 0x0000016F UnityEngine.ForceMode2D Lean.Common.LeanManualVelocity2D::get_Mode()
extern void LeanManualVelocity2D_get_Mode_mB11EA5E32EBC538F8872086B1788BEFA69B6799A (void);
// 0x00000170 System.Void Lean.Common.LeanManualVelocity2D::set_Multiplier(System.Single)
extern void LeanManualVelocity2D_set_Multiplier_mFA5A2A7F3E537DCD2A3EB5E87939068EA0463D5F (void);
// 0x00000171 System.Single Lean.Common.LeanManualVelocity2D::get_Multiplier()
extern void LeanManualVelocity2D_get_Multiplier_m545368385DA811F7B9F6003E98BC568B93713B24 (void);
// 0x00000172 System.Void Lean.Common.LeanManualVelocity2D::set_Space(UnityEngine.Space)
extern void LeanManualVelocity2D_set_Space_mC54E0525FB34AEAD57C0355D0A24FCAB15EB74D8 (void);
// 0x00000173 UnityEngine.Space Lean.Common.LeanManualVelocity2D::get_Space()
extern void LeanManualVelocity2D_get_Space_m22D8D01B38DE55DCE54D04C2CA5A3A3EBBD7DEFB (void);
// 0x00000174 System.Void Lean.Common.LeanManualVelocity2D::set_DirectionA(UnityEngine.Vector2)
extern void LeanManualVelocity2D_set_DirectionA_m058AC7628AA2AD6649D40A52C32C046C5CE26578 (void);
// 0x00000175 UnityEngine.Vector2 Lean.Common.LeanManualVelocity2D::get_DirectionA()
extern void LeanManualVelocity2D_get_DirectionA_mC63F6B4330F91808E5168567EC805530FB067605 (void);
// 0x00000176 System.Void Lean.Common.LeanManualVelocity2D::set_DirectionB(UnityEngine.Vector2)
extern void LeanManualVelocity2D_set_DirectionB_m7D57C449EBDB3648682379B2F02D8AA3C1B32230 (void);
// 0x00000177 UnityEngine.Vector2 Lean.Common.LeanManualVelocity2D::get_DirectionB()
extern void LeanManualVelocity2D_get_DirectionB_m4B96C786733C2332A427FA893398D0B50FD6FD64 (void);
// 0x00000178 System.Void Lean.Common.LeanManualVelocity2D::AddForceA(System.Single)
extern void LeanManualVelocity2D_AddForceA_mE7A12F4B8E7E9A556F608BD440ED15372225F5CF (void);
// 0x00000179 System.Void Lean.Common.LeanManualVelocity2D::AddForceB(System.Single)
extern void LeanManualVelocity2D_AddForceB_m3AFFD56BC70D663E9F76B8270B6EF26C8473B7BB (void);
// 0x0000017A System.Void Lean.Common.LeanManualVelocity2D::AddForceAB(UnityEngine.Vector2)
extern void LeanManualVelocity2D_AddForceAB_m9C5A6184A65078FAA2E8BD24A30D94C6FA00BBF1 (void);
// 0x0000017B System.Void Lean.Common.LeanManualVelocity2D::AddForceFromTo(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanManualVelocity2D_AddForceFromTo_mBF6D3E2C649D573A4198CCE0B3A7657F0A36488B (void);
// 0x0000017C System.Void Lean.Common.LeanManualVelocity2D::AddForce(UnityEngine.Vector3)
extern void LeanManualVelocity2D_AddForce_m172B84C4BDB6E576BD4C9B80D47FACBF7052495C (void);
// 0x0000017D System.Void Lean.Common.LeanManualVelocity2D::.ctor()
extern void LeanManualVelocity2D__ctor_m2F9C923A17F4A46E49F2CC7B4A7EB05E2906C268 (void);
// 0x0000017E System.Void Lean.Common.LeanOrbit::set_Camera(UnityEngine.Camera)
extern void LeanOrbit_set_Camera_m956A46855C8DC9BA514D7C5E8635B775380BCBDD (void);
// 0x0000017F UnityEngine.Camera Lean.Common.LeanOrbit::get_Camera()
extern void LeanOrbit_get_Camera_m50045C1C5E077213425489B123071DD8563C7BF0 (void);
// 0x00000180 System.Void Lean.Common.LeanOrbit::set_Pivot(UnityEngine.Transform)
extern void LeanOrbit_set_Pivot_m4BA35B2A72B2F28020FA9D54F86985AECE85C509 (void);
// 0x00000181 UnityEngine.Transform Lean.Common.LeanOrbit::get_Pivot()
extern void LeanOrbit_get_Pivot_mA21A1CFDC991C955D29EF5025C6ACBBB62ACB444 (void);
// 0x00000182 System.Void Lean.Common.LeanOrbit::set_Damping(System.Single)
extern void LeanOrbit_set_Damping_mDFBFF1722B9478C351469153448D16FED53DAF6C (void);
// 0x00000183 System.Single Lean.Common.LeanOrbit::get_Damping()
extern void LeanOrbit_get_Damping_mB1E90BF7D8FE0458FF1DCC55C0B192249A3B3502 (void);
// 0x00000184 System.Void Lean.Common.LeanOrbit::set_PitchSensitivity(System.Single)
extern void LeanOrbit_set_PitchSensitivity_m047727B39E72180C2F8AD11F39CCD6D8281FCF15 (void);
// 0x00000185 System.Single Lean.Common.LeanOrbit::get_PitchSensitivity()
extern void LeanOrbit_get_PitchSensitivity_m85A25BAF7E409304337DFA1612352E4F5784F5BE (void);
// 0x00000186 System.Void Lean.Common.LeanOrbit::set_YawSensitivity(System.Single)
extern void LeanOrbit_set_YawSensitivity_m653A7AEB2C6CA4EF907F6110498DB268BB7B51A7 (void);
// 0x00000187 System.Single Lean.Common.LeanOrbit::get_YawSensitivity()
extern void LeanOrbit_get_YawSensitivity_m5C3E5D41AE5AA6A41ADA75C02BBE6228EBCEDC3E (void);
// 0x00000188 System.Void Lean.Common.LeanOrbit::Rotate(UnityEngine.Vector2)
extern void LeanOrbit_Rotate_m2FA2639AFCC15F253E6A74A64BA814E747711EB5 (void);
// 0x00000189 UnityEngine.Vector3 Lean.Common.LeanOrbit::GetPivotPoint()
extern void LeanOrbit_GetPivotPoint_m03838A756586FDFA4423C0FF636F3D5A12E69CD1 (void);
// 0x0000018A System.Void Lean.Common.LeanOrbit::RotatePitch(System.Single)
extern void LeanOrbit_RotatePitch_mC41237B5223E0C801A7E97DAC31356FC8BA1F320 (void);
// 0x0000018B System.Void Lean.Common.LeanOrbit::RotateYaw(System.Single)
extern void LeanOrbit_RotateYaw_m02E596B3FD795103D28F295F1DEDB9F3E3408A2C (void);
// 0x0000018C System.Void Lean.Common.LeanOrbit::LateUpdate()
extern void LeanOrbit_LateUpdate_m71C703AC971AC66455B5BAC1A398DB091BFBB43F (void);
// 0x0000018D System.Single Lean.Common.LeanOrbit::GetSensitivity()
extern void LeanOrbit_GetSensitivity_m4DD35C087B5EEE625914A4F556E1723CD47B302A (void);
// 0x0000018E System.Void Lean.Common.LeanOrbit::.ctor()
extern void LeanOrbit__ctor_m335E105B465C1D7843F2977C60AE53847678E217 (void);
// 0x0000018F System.Void Lean.Common.LeanPitchYaw::set_Camera(UnityEngine.Camera)
extern void LeanPitchYaw_set_Camera_mBCF1DC0B1B0DC607EDF3C43659F75CDB6D4B696E (void);
// 0x00000190 UnityEngine.Camera Lean.Common.LeanPitchYaw::get_Camera()
extern void LeanPitchYaw_get_Camera_m2D04E9AF6B9A41325FC783250107A4F8CAD16D5A (void);
// 0x00000191 System.Void Lean.Common.LeanPitchYaw::set_Damping(System.Single)
extern void LeanPitchYaw_set_Damping_m616B535A24B325F648E34DC5A19BA8BB1DE6427E (void);
// 0x00000192 System.Single Lean.Common.LeanPitchYaw::get_Damping()
extern void LeanPitchYaw_get_Damping_m0491D89476912EEDAD195738820AF118F8919C3B (void);
// 0x00000193 System.Void Lean.Common.LeanPitchYaw::set_DefaultRotation(UnityEngine.Vector2)
extern void LeanPitchYaw_set_DefaultRotation_m7A433AB1CB4A3AD8D23255C75BEB551C9F890893 (void);
// 0x00000194 UnityEngine.Vector2 Lean.Common.LeanPitchYaw::get_DefaultRotation()
extern void LeanPitchYaw_get_DefaultRotation_m7741933BAF634474669E044AE215771682358889 (void);
// 0x00000195 System.Void Lean.Common.LeanPitchYaw::set_Pitch(System.Single)
extern void LeanPitchYaw_set_Pitch_m559AFC2CF0E80CA6BE276922DF7A3E985A000D3D (void);
// 0x00000196 System.Single Lean.Common.LeanPitchYaw::get_Pitch()
extern void LeanPitchYaw_get_Pitch_mA3480A01D5AD307DFB2ED4E93F291875264B6085 (void);
// 0x00000197 System.Void Lean.Common.LeanPitchYaw::set_PitchSensitivity(System.Single)
extern void LeanPitchYaw_set_PitchSensitivity_m396ABC5ED03DD35F3E5E7668D1A7DCB1B9846191 (void);
// 0x00000198 System.Single Lean.Common.LeanPitchYaw::get_PitchSensitivity()
extern void LeanPitchYaw_get_PitchSensitivity_m601639AFEEA8C86D630660EF1417C1E9D4C97D0C (void);
// 0x00000199 System.Void Lean.Common.LeanPitchYaw::set_PitchClamp(System.Boolean)
extern void LeanPitchYaw_set_PitchClamp_mBB942C54337E04815BFA9E953DC39828EB7195D2 (void);
// 0x0000019A System.Boolean Lean.Common.LeanPitchYaw::get_PitchClamp()
extern void LeanPitchYaw_get_PitchClamp_m0B99CE48FA617A0FD3C7F073A6FF2A8E0931470B (void);
// 0x0000019B System.Void Lean.Common.LeanPitchYaw::set_PitchMin(System.Single)
extern void LeanPitchYaw_set_PitchMin_m30339C0DA52EEA6C50B32BC646A30993FE3AAB46 (void);
// 0x0000019C System.Single Lean.Common.LeanPitchYaw::get_PitchMin()
extern void LeanPitchYaw_get_PitchMin_m3D670300D8B526D969EA08252D515227DF09776C (void);
// 0x0000019D System.Void Lean.Common.LeanPitchYaw::set_PitchMax(System.Single)
extern void LeanPitchYaw_set_PitchMax_m0452A09AE83E3A3A1E1894519D68C0D0AE975C51 (void);
// 0x0000019E System.Single Lean.Common.LeanPitchYaw::get_PitchMax()
extern void LeanPitchYaw_get_PitchMax_m28FCB3A30167B472A4B539FF3E81CEF6F5927F01 (void);
// 0x0000019F System.Void Lean.Common.LeanPitchYaw::set_Yaw(System.Single)
extern void LeanPitchYaw_set_Yaw_mBCEF0AE1F4C9B6F1F4C29B5437AAF21EC4E3FCE9 (void);
// 0x000001A0 System.Single Lean.Common.LeanPitchYaw::get_Yaw()
extern void LeanPitchYaw_get_Yaw_mD82CE9D89675A93F359DD6298E8E1A3714F1BC88 (void);
// 0x000001A1 System.Void Lean.Common.LeanPitchYaw::set_YawSensitivity(System.Single)
extern void LeanPitchYaw_set_YawSensitivity_mA1348DB280413A62CCEC9DB2FF469CEFC3586A14 (void);
// 0x000001A2 System.Single Lean.Common.LeanPitchYaw::get_YawSensitivity()
extern void LeanPitchYaw_get_YawSensitivity_m15D1AA40E73E81654999C0E4C2DC834245DEF76F (void);
// 0x000001A3 System.Void Lean.Common.LeanPitchYaw::set_YawClamp(System.Boolean)
extern void LeanPitchYaw_set_YawClamp_mA18A61712CEC278F428B18E22C4B77A5BBACBDDE (void);
// 0x000001A4 System.Boolean Lean.Common.LeanPitchYaw::get_YawClamp()
extern void LeanPitchYaw_get_YawClamp_m8E4A95685F591B77C8962F1969CEACFA5D71FC8B (void);
// 0x000001A5 System.Void Lean.Common.LeanPitchYaw::set_YawMin(System.Single)
extern void LeanPitchYaw_set_YawMin_mC8E7980AE074D18885657B2FF0A0D01ABBE6808B (void);
// 0x000001A6 System.Single Lean.Common.LeanPitchYaw::get_YawMin()
extern void LeanPitchYaw_get_YawMin_m361774128B4F6CE5905EC3DA63F08336F28A73F1 (void);
// 0x000001A7 System.Void Lean.Common.LeanPitchYaw::set_YawMax(System.Single)
extern void LeanPitchYaw_set_YawMax_m7029B2BFF66C2A4DE5296914BD216BD1EA94A3BE (void);
// 0x000001A8 System.Single Lean.Common.LeanPitchYaw::get_YawMax()
extern void LeanPitchYaw_get_YawMax_mF7BE9E5C5C7B5AFD0550636C45D10EA2DA9AEA7C (void);
// 0x000001A9 System.Void Lean.Common.LeanPitchYaw::ResetRotation()
extern void LeanPitchYaw_ResetRotation_m223BD5FE2B0466FEB5871EBC174A3075CF872B25 (void);
// 0x000001AA System.Void Lean.Common.LeanPitchYaw::RotateToPosition(UnityEngine.Vector3)
extern void LeanPitchYaw_RotateToPosition_m20E7020FA3358DB93961F9D393EFC3F24D44500C (void);
// 0x000001AB System.Void Lean.Common.LeanPitchYaw::RotateToDirection(UnityEngine.Vector3)
extern void LeanPitchYaw_RotateToDirection_mE317A4988EE00F3FDBA70F537DFB5139EF669F78 (void);
// 0x000001AC System.Void Lean.Common.LeanPitchYaw::SetPitch(System.Single)
extern void LeanPitchYaw_SetPitch_m09112DF07D0F19309C9124529EBF10F022296E1F (void);
// 0x000001AD System.Void Lean.Common.LeanPitchYaw::SetYaw(System.Single)
extern void LeanPitchYaw_SetYaw_mFE08E8FA472751B81CA1F22DE6FE998665379366 (void);
// 0x000001AE System.Void Lean.Common.LeanPitchYaw::RotateToScreenPosition(UnityEngine.Vector2)
extern void LeanPitchYaw_RotateToScreenPosition_m74B9E59E5961B90FC99011A71E6CAE4FF648FA24 (void);
// 0x000001AF System.Void Lean.Common.LeanPitchYaw::Rotate(UnityEngine.Vector2)
extern void LeanPitchYaw_Rotate_m324D4AE00A54B15BEBE9BD5E7E3B4B9F25CAA0B7 (void);
// 0x000001B0 System.Void Lean.Common.LeanPitchYaw::RotatePitch(System.Single)
extern void LeanPitchYaw_RotatePitch_m8F367D587778317286F92D3A55CF62C0BCBEB4AE (void);
// 0x000001B1 System.Void Lean.Common.LeanPitchYaw::RotateYaw(System.Single)
extern void LeanPitchYaw_RotateYaw_m779455632CC8618E44A907EBA907361B70F74968 (void);
// 0x000001B2 System.Void Lean.Common.LeanPitchYaw::Start()
extern void LeanPitchYaw_Start_mDFE0D9052CF7E91EFEF888F04370345B373041F7 (void);
// 0x000001B3 System.Void Lean.Common.LeanPitchYaw::LateUpdate()
extern void LeanPitchYaw_LateUpdate_m96B8971B4F1744829F315D7694B87DF372CD4419 (void);
// 0x000001B4 System.Single Lean.Common.LeanPitchYaw::GetSensitivity()
extern void LeanPitchYaw_GetSensitivity_mEC5D1A9E365D0A3426657093799581CC802B8F11 (void);
// 0x000001B5 System.Void Lean.Common.LeanPitchYaw::.ctor()
extern void LeanPitchYaw__ctor_mEDF7FB4740D576FDFCD0C41884856A9DCB3A0492 (void);
// 0x000001B6 System.Void Lean.Common.LeanPitchYawAutoRotate::set_Delay(System.Single)
extern void LeanPitchYawAutoRotate_set_Delay_mE6CD675696F15D676FCEFB6242F4B1EAEC31096C (void);
// 0x000001B7 System.Single Lean.Common.LeanPitchYawAutoRotate::get_Delay()
extern void LeanPitchYawAutoRotate_get_Delay_m97BB87BD566AC75054D91AF2FCDDB9DF4C852EF2 (void);
// 0x000001B8 System.Void Lean.Common.LeanPitchYawAutoRotate::set_Speed(System.Single)
extern void LeanPitchYawAutoRotate_set_Speed_m42B5D40CB4352768E838D7EC9E5286F13FC5CBEC (void);
// 0x000001B9 System.Single Lean.Common.LeanPitchYawAutoRotate::get_Speed()
extern void LeanPitchYawAutoRotate_get_Speed_mC9533601205B78754532AD087EDE057F99749DE5 (void);
// 0x000001BA System.Void Lean.Common.LeanPitchYawAutoRotate::set_Acceleration(System.Single)
extern void LeanPitchYawAutoRotate_set_Acceleration_mCE51F179F8C6239F89963014046AB39A0A941C46 (void);
// 0x000001BB System.Single Lean.Common.LeanPitchYawAutoRotate::get_Acceleration()
extern void LeanPitchYawAutoRotate_get_Acceleration_mA287A8102B273CAA04E5C46AF09FBB65A704D560 (void);
// 0x000001BC System.Void Lean.Common.LeanPitchYawAutoRotate::OnEnable()
extern void LeanPitchYawAutoRotate_OnEnable_m52E78CD0E9EEFF2ABC6FB39CDE601E066DA423A9 (void);
// 0x000001BD System.Void Lean.Common.LeanPitchYawAutoRotate::LateUpdate()
extern void LeanPitchYawAutoRotate_LateUpdate_mFF5BAEA5D4021C1A6DABA404E3E0FBDEBFF8332C (void);
// 0x000001BE System.Void Lean.Common.LeanPitchYawAutoRotate::.ctor()
extern void LeanPitchYawAutoRotate__ctor_mBE3D9012E382F13039208C8DBA7857339B7D25F9 (void);
// 0x000001BF System.Collections.Generic.List`1<UnityEngine.Events.UnityEvent> Lean.Common.LeanRandomEvents::get_Events()
extern void LeanRandomEvents_get_Events_m3A4D10A61482B329FFE378AB9792168FBAE2ADF0 (void);
// 0x000001C0 System.Void Lean.Common.LeanRandomEvents::Invoke()
extern void LeanRandomEvents_Invoke_mCEA3D4832552ACC6AAF1895C8DADF995E45CF735 (void);
// 0x000001C1 System.Void Lean.Common.LeanRandomEvents::.ctor()
extern void LeanRandomEvents__ctor_mAE9CECE65269AEBD271ED31A0C8D615DB241468A (void);
// 0x000001C2 System.Void Lean.Common.LeanRemapValue::set_OldMin(UnityEngine.Vector3)
extern void LeanRemapValue_set_OldMin_m675349685B45D7F25F67981EA3107DFB25D0E1C4 (void);
// 0x000001C3 UnityEngine.Vector3 Lean.Common.LeanRemapValue::get_OldMin()
extern void LeanRemapValue_get_OldMin_mE4E93BB8BB2B3611199385098B1619D92B263FB9 (void);
// 0x000001C4 System.Void Lean.Common.LeanRemapValue::set_OldMax(UnityEngine.Vector3)
extern void LeanRemapValue_set_OldMax_mE5407E53BF9D37B1385346821052434EA12E2558 (void);
// 0x000001C5 UnityEngine.Vector3 Lean.Common.LeanRemapValue::get_OldMax()
extern void LeanRemapValue_get_OldMax_mF5343F42B8AC1B4C1A32C48675B36BD51407DB74 (void);
// 0x000001C6 System.Void Lean.Common.LeanRemapValue::set_NewMin(UnityEngine.Vector3)
extern void LeanRemapValue_set_NewMin_m7C45F32102AE9DC15AE2ED443D72789B347D7396 (void);
// 0x000001C7 UnityEngine.Vector3 Lean.Common.LeanRemapValue::get_NewMin()
extern void LeanRemapValue_get_NewMin_mF07798FDEF1E234A9A8E0721942E227EDCEB11E8 (void);
// 0x000001C8 System.Void Lean.Common.LeanRemapValue::set_NewMax(UnityEngine.Vector3)
extern void LeanRemapValue_set_NewMax_mEDB3F3B7E2F2884CB2057995E8EDB6F801654630 (void);
// 0x000001C9 UnityEngine.Vector3 Lean.Common.LeanRemapValue::get_NewMax()
extern void LeanRemapValue_get_NewMax_m83D119C691DDE9BDD55F9BD1516FFEE90302CCE1 (void);
// 0x000001CA Lean.Common.LeanRemapValue/FloatEvent Lean.Common.LeanRemapValue::get_OnValueX()
extern void LeanRemapValue_get_OnValueX_mA43B948F478BEEF11243C31581C892247018157C (void);
// 0x000001CB Lean.Common.LeanRemapValue/FloatEvent Lean.Common.LeanRemapValue::get_OnValueY()
extern void LeanRemapValue_get_OnValueY_mCFCAFD48C3FD4452BB4B64E2D64D4EC2D4DDFC23 (void);
// 0x000001CC Lean.Common.LeanRemapValue/FloatEvent Lean.Common.LeanRemapValue::get_OnValueZ()
extern void LeanRemapValue_get_OnValueZ_m41D14BF21C009E354E654B43CC50D3E9951FC608 (void);
// 0x000001CD Lean.Common.LeanRemapValue/Vector2Event Lean.Common.LeanRemapValue::get_OnValueXY()
extern void LeanRemapValue_get_OnValueXY_m6ACDE425CFC234686E26BAABFEEC54A945C8D7E7 (void);
// 0x000001CE Lean.Common.LeanRemapValue/Vector3Event Lean.Common.LeanRemapValue::get_OnValueXYZ()
extern void LeanRemapValue_get_OnValueXYZ_m5397A7AFB331F96650077E02FA1CD97D28C5BC8E (void);
// 0x000001CF System.Void Lean.Common.LeanRemapValue::SetX(System.Single)
extern void LeanRemapValue_SetX_m4A6F465E54CA0950477B3A7C07D663C3CAC90549 (void);
// 0x000001D0 System.Void Lean.Common.LeanRemapValue::SetY(System.Single)
extern void LeanRemapValue_SetY_m2E23F1D67B68416E61148553DECD061F828A845A (void);
// 0x000001D1 System.Void Lean.Common.LeanRemapValue::SetZ(System.Single)
extern void LeanRemapValue_SetZ_m88FEDFE3391812EAF1FB521A3A5DDB0800984A82 (void);
// 0x000001D2 System.Void Lean.Common.LeanRemapValue::SetXY(UnityEngine.Vector2)
extern void LeanRemapValue_SetXY_m89ED281338799D8233DD890331094E393C94D35C (void);
// 0x000001D3 System.Void Lean.Common.LeanRemapValue::SetXYZ(UnityEngine.Vector3)
extern void LeanRemapValue_SetXYZ_mA5213BA36FCB642F14E0B57426804BCFB2F5E501 (void);
// 0x000001D4 System.Void Lean.Common.LeanRemapValue::Remap(UnityEngine.Vector3)
extern void LeanRemapValue_Remap_mD38D8C5DBAB85CB6E1B682E83F80AEDF625ED289 (void);
// 0x000001D5 System.Void Lean.Common.LeanRemapValue::.ctor()
extern void LeanRemapValue__ctor_m650A0C915EEB24200B8284FA3476B3FA97A6817A (void);
// 0x000001D6 System.Void Lean.Common.LeanRemapValue/FloatEvent::.ctor()
extern void FloatEvent__ctor_mC91E22BE3CDE75DBF7005ACF481498240224EA2A (void);
// 0x000001D7 System.Void Lean.Common.LeanRemapValue/Vector2Event::.ctor()
extern void Vector2Event__ctor_m72C868E0964FB1F4469FA17AD5BDDB3D13B73B2E (void);
// 0x000001D8 System.Void Lean.Common.LeanRemapValue/Vector3Event::.ctor()
extern void Vector3Event__ctor_m5D5712391DAD9447C2CD56255674557A243F576D (void);
// 0x000001D9 System.Void Lean.Common.LeanRevertTransform::set_Damping(System.Single)
extern void LeanRevertTransform_set_Damping_m8C3EA1F89005C2104F9B87AB2B26D2DB577B5D32 (void);
// 0x000001DA System.Single Lean.Common.LeanRevertTransform::get_Damping()
extern void LeanRevertTransform_get_Damping_m5A7CBA3B1ACA2FBD5A55542D4CCF22290D3D7882 (void);
// 0x000001DB System.Void Lean.Common.LeanRevertTransform::set_RecordOnStart(System.Boolean)
extern void LeanRevertTransform_set_RecordOnStart_m1E218E92B422C85C84423FD459A8C6D6CAEF8BE4 (void);
// 0x000001DC System.Boolean Lean.Common.LeanRevertTransform::get_RecordOnStart()
extern void LeanRevertTransform_get_RecordOnStart_mE644AB9D67E0C1D97FD98E4F614406F9B06F5609 (void);
// 0x000001DD System.Void Lean.Common.LeanRevertTransform::set_RevertPosition(System.Boolean)
extern void LeanRevertTransform_set_RevertPosition_mA70439B8C2793191F577D4E9471B432D9D9DBE11 (void);
// 0x000001DE System.Boolean Lean.Common.LeanRevertTransform::get_RevertPosition()
extern void LeanRevertTransform_get_RevertPosition_mF0883C9705BFE64ABC322020ABD39BFACDB49362 (void);
// 0x000001DF System.Void Lean.Common.LeanRevertTransform::set_RevertRotation(System.Boolean)
extern void LeanRevertTransform_set_RevertRotation_m653759CBD32090356AD69F2DCA77A30FE1A7B187 (void);
// 0x000001E0 System.Boolean Lean.Common.LeanRevertTransform::get_RevertRotation()
extern void LeanRevertTransform_get_RevertRotation_mA46513EE4BE53C259BE9410BF6988F912540204A (void);
// 0x000001E1 System.Void Lean.Common.LeanRevertTransform::set_RevertScale(System.Boolean)
extern void LeanRevertTransform_set_RevertScale_m05D355998343CB24B0DE9576165FD97A85C4B3DD (void);
// 0x000001E2 System.Boolean Lean.Common.LeanRevertTransform::get_RevertScale()
extern void LeanRevertTransform_get_RevertScale_mF816AC88BDAF11C8BAC0FA3F0F52658F44D51219 (void);
// 0x000001E3 System.Void Lean.Common.LeanRevertTransform::set_ThresholdPosition(System.Single)
extern void LeanRevertTransform_set_ThresholdPosition_m48218ED8B95A66E41F0D2E0A5D80C451A596D579 (void);
// 0x000001E4 System.Single Lean.Common.LeanRevertTransform::get_ThresholdPosition()
extern void LeanRevertTransform_get_ThresholdPosition_mBB003057B342A26C88EF3C6649BC4A37B79103BB (void);
// 0x000001E5 System.Void Lean.Common.LeanRevertTransform::set_ThresholdRotation(System.Single)
extern void LeanRevertTransform_set_ThresholdRotation_m8EF291EC90837AF64A8AAE5D99D8BCF3E19988D4 (void);
// 0x000001E6 System.Single Lean.Common.LeanRevertTransform::get_ThresholdRotation()
extern void LeanRevertTransform_get_ThresholdRotation_m409E07CDDCBA8E90431ABA68C5AD40485960646E (void);
// 0x000001E7 System.Void Lean.Common.LeanRevertTransform::set_ThresholdScale(System.Single)
extern void LeanRevertTransform_set_ThresholdScale_m517764D780D547491F483E6E4D1421DD23883350 (void);
// 0x000001E8 System.Single Lean.Common.LeanRevertTransform::get_ThresholdScale()
extern void LeanRevertTransform_get_ThresholdScale_m95465595DD16800E6AE16C73E7DFAD8436050F15 (void);
// 0x000001E9 System.Void Lean.Common.LeanRevertTransform::set_TargetPosition(UnityEngine.Vector3)
extern void LeanRevertTransform_set_TargetPosition_mF28BD735538FFC080F3E09DB3E230C7856200C73 (void);
// 0x000001EA UnityEngine.Vector3 Lean.Common.LeanRevertTransform::get_TargetPosition()
extern void LeanRevertTransform_get_TargetPosition_m450B504DBADF03D931EFA76C4B39DC85693CF4A5 (void);
// 0x000001EB System.Void Lean.Common.LeanRevertTransform::set_TargetRotation(UnityEngine.Quaternion)
extern void LeanRevertTransform_set_TargetRotation_mB664BA5811BEAF9AB836E28944C1A2F52B644E51 (void);
// 0x000001EC UnityEngine.Quaternion Lean.Common.LeanRevertTransform::get_TargetRotation()
extern void LeanRevertTransform_get_TargetRotation_mAE166838D525AF44F52F9997A99A9AAE8282B9C2 (void);
// 0x000001ED System.Void Lean.Common.LeanRevertTransform::set_TargetScale(UnityEngine.Vector3)
extern void LeanRevertTransform_set_TargetScale_mF98F910BB320597AA0A06D03206BA883806353C6 (void);
// 0x000001EE UnityEngine.Vector3 Lean.Common.LeanRevertTransform::get_TargetScale()
extern void LeanRevertTransform_get_TargetScale_m8935285C1A6BCA57061DA62165935E1EDA648FDA (void);
// 0x000001EF System.Void Lean.Common.LeanRevertTransform::Start()
extern void LeanRevertTransform_Start_mC4F2E59C04E59B534855C1514304FFA53AC83D0F (void);
// 0x000001F0 System.Void Lean.Common.LeanRevertTransform::Revert()
extern void LeanRevertTransform_Revert_m5348B2D6D10C01686894612A2DEBA6DE904E53B3 (void);
// 0x000001F1 System.Void Lean.Common.LeanRevertTransform::StopRevert()
extern void LeanRevertTransform_StopRevert_mE0BF7A16895C20C0F4657E03A90EC26975250020 (void);
// 0x000001F2 System.Void Lean.Common.LeanRevertTransform::RecordTransform()
extern void LeanRevertTransform_RecordTransform_mDEA71175D5321AFBA577541B88E38284D039D961 (void);
// 0x000001F3 System.Void Lean.Common.LeanRevertTransform::Update()
extern void LeanRevertTransform_Update_mA4B8F53D8BB2576900890C3F8B8C00512C569ECE (void);
// 0x000001F4 System.Boolean Lean.Common.LeanRevertTransform::ReachedTarget()
extern void LeanRevertTransform_ReachedTarget_m6FE4FF0FB80AC2BA7D9149BA5B3C05CECE98C459 (void);
// 0x000001F5 System.Void Lean.Common.LeanRevertTransform::.ctor()
extern void LeanRevertTransform__ctor_m9E5598D8272FBA11BF1091C94A464DDEBE4F7F66 (void);
// 0x000001F6 System.Void Lean.Common.LeanRotateToPosition::set_Target(UnityEngine.Transform)
extern void LeanRotateToPosition_set_Target_mB8F52EFC148ABEECEC788403E90CD98B85153909 (void);
// 0x000001F7 UnityEngine.Transform Lean.Common.LeanRotateToPosition::get_Target()
extern void LeanRotateToPosition_get_Target_m2C718ADCAD75F46B9D204BD34534E4B861A635FE (void);
// 0x000001F8 System.Void Lean.Common.LeanRotateToPosition::set_Position(Lean.Common.LeanRotateToPosition/PositionType)
extern void LeanRotateToPosition_set_Position_m68A49883B15A790FA1600942B3CB3FDB09839318 (void);
// 0x000001F9 Lean.Common.LeanRotateToPosition/PositionType Lean.Common.LeanRotateToPosition::get_Position()
extern void LeanRotateToPosition_get_Position_m3D6ACE57F65B1B2F941BFA95860DBFE88360E411 (void);
// 0x000001FA System.Void Lean.Common.LeanRotateToPosition::set_Threshold(System.Single)
extern void LeanRotateToPosition_set_Threshold_m0A35348914E9E7C6A30E0A33CA93A05976BEA6D5 (void);
// 0x000001FB System.Single Lean.Common.LeanRotateToPosition::get_Threshold()
extern void LeanRotateToPosition_get_Threshold_m70E6259F79EE4D2449E1D56F3E891E9C09B814F7 (void);
// 0x000001FC System.Void Lean.Common.LeanRotateToPosition::set_Invert(System.Boolean)
extern void LeanRotateToPosition_set_Invert_m14153BC577091FE6BE5437F67AA654E1DD71A6C5 (void);
// 0x000001FD System.Boolean Lean.Common.LeanRotateToPosition::get_Invert()
extern void LeanRotateToPosition_get_Invert_m54A788B84992D3B0234ED88F0A3E34ACF3E6BEDF (void);
// 0x000001FE System.Void Lean.Common.LeanRotateToPosition::set_RotateTo(Lean.Common.LeanRotateToPosition/RotateType)
extern void LeanRotateToPosition_set_RotateTo_m0DF6465B91AB57AEEE285D22E5E621025AC1F4D1 (void);
// 0x000001FF Lean.Common.LeanRotateToPosition/RotateType Lean.Common.LeanRotateToPosition::get_RotateTo()
extern void LeanRotateToPosition_get_RotateTo_m8FB0E464EC99429CD4892E35D848ACC7531630D7 (void);
// 0x00000200 System.Void Lean.Common.LeanRotateToPosition::set_Damping(System.Single)
extern void LeanRotateToPosition_set_Damping_m486F2BC018708E3EEBD37F04566BEA52F6A8667C (void);
// 0x00000201 System.Single Lean.Common.LeanRotateToPosition::get_Damping()
extern void LeanRotateToPosition_get_Damping_m03FD0D5E56679F66699AA2B60464B9D18FB4E95F (void);
// 0x00000202 UnityEngine.Transform Lean.Common.LeanRotateToPosition::get_FinalTransform()
extern void LeanRotateToPosition_get_FinalTransform_m7420192F159D19CBEB8213970F1121CB09211D7E (void);
// 0x00000203 System.Void Lean.Common.LeanRotateToPosition::SetPosition(UnityEngine.Vector3)
extern void LeanRotateToPosition_SetPosition_mC884823A7CAC867D6666352CAAE4D4DD5ADC0028 (void);
// 0x00000204 System.Void Lean.Common.LeanRotateToPosition::SetDelta(UnityEngine.Vector3)
extern void LeanRotateToPosition_SetDelta_mECBA4518FEED32078BB36C5F540F01C6F65C5477 (void);
// 0x00000205 System.Void Lean.Common.LeanRotateToPosition::ResetPosition()
extern void LeanRotateToPosition_ResetPosition_mB77AF404A4D534855D3D8E0A34298E86F054DCCC (void);
// 0x00000206 System.Void Lean.Common.LeanRotateToPosition::Start()
extern void LeanRotateToPosition_Start_mF771390E07B4CCAFF4C5599F4C963EB2CBE819D3 (void);
// 0x00000207 System.Void Lean.Common.LeanRotateToPosition::OnEnable()
extern void LeanRotateToPosition_OnEnable_mFDC739E9AC8C260DA8A9F1BA4F1162E8372CC1E0 (void);
// 0x00000208 System.Void Lean.Common.LeanRotateToPosition::LateUpdate()
extern void LeanRotateToPosition_LateUpdate_mB81BFE2F5A087457636F9A1138DE5B10B7E3852A (void);
// 0x00000209 System.Void Lean.Common.LeanRotateToPosition::UpdateRotation(UnityEngine.Transform,UnityEngine.Vector3)
extern void LeanRotateToPosition_UpdateRotation_mC9F3CBCA1090C798889AC9E1204F33E389C3E9D5 (void);
// 0x0000020A System.Void Lean.Common.LeanRotateToPosition::.ctor()
extern void LeanRotateToPosition__ctor_mCF6BBD3EBFDD0D5EEC49258365D579231880DB2E (void);
// 0x0000020B System.Void Lean.Common.LeanRotateToRigidbody2D::set_Damping(System.Single)
extern void LeanRotateToRigidbody2D_set_Damping_mA6F4B728F76FC7262D785042AFBEBEAF84733435 (void);
// 0x0000020C System.Single Lean.Common.LeanRotateToRigidbody2D::get_Damping()
extern void LeanRotateToRigidbody2D_get_Damping_m21449CC1F72D6D8041C78D25A006063B2FE14341 (void);
// 0x0000020D System.Void Lean.Common.LeanRotateToRigidbody2D::OnEnable()
extern void LeanRotateToRigidbody2D_OnEnable_mF45807B5E515D7DCF086048DF44BD4B62219F4E6 (void);
// 0x0000020E System.Void Lean.Common.LeanRotateToRigidbody2D::Start()
extern void LeanRotateToRigidbody2D_Start_mC375F96C658D73A652CB03B4EB7FD55A4976ECDF (void);
// 0x0000020F System.Void Lean.Common.LeanRotateToRigidbody2D::LateUpdate()
extern void LeanRotateToRigidbody2D_LateUpdate_m8020AEDE5A7B67378BBEB993841FFA34834B4085 (void);
// 0x00000210 System.Void Lean.Common.LeanRotateToRigidbody2D::.ctor()
extern void LeanRotateToRigidbody2D__ctor_m8C3D4E24445C6A5BE023F6E9036EF2FB15AFF72E (void);
// 0x00000211 Lean.Common.LeanSelected/LeanSelectableEvent Lean.Common.LeanSelected::get_OnSelectable()
extern void LeanSelected_get_OnSelectable_mE9E162DF96F4F0D8E489027A8B2E3C7AED8D6306 (void);
// 0x00000212 System.Void Lean.Common.LeanSelected::OnEnable()
extern void LeanSelected_OnEnable_m013397E14A655147E3A06A0A68FEBDA8CA3AD44E (void);
// 0x00000213 System.Void Lean.Common.LeanSelected::OnDisable()
extern void LeanSelected_OnDisable_m7122C0170245CB360EE9AA82A3F5540853BC6ABA (void);
// 0x00000214 System.Void Lean.Common.LeanSelected::HandleSelectGlobal(Lean.Common.LeanSelect,Lean.Common.LeanSelectable)
extern void LeanSelected_HandleSelectGlobal_m7B72DBBDB11E25BE5C91E365A5F3F130C981EF18 (void);
// 0x00000215 System.Void Lean.Common.LeanSelected::.ctor()
extern void LeanSelected__ctor_mA87196128123CCD4DA9DCD812E41559D540CBAE8 (void);
// 0x00000216 System.Void Lean.Common.LeanSelected/LeanSelectableEvent::.ctor()
extern void LeanSelectableEvent__ctor_mC3F52B3C581033AC509F7AA63DC7ECF3B90265EA (void);
// 0x00000217 Lean.Common.LeanSelectedCount/IntEvent Lean.Common.LeanSelectedCount::get_OnCount()
extern void LeanSelectedCount_get_OnCount_mCB537DD097383D04155D56EE1D44AEF1A53FF526 (void);
// 0x00000218 System.Void Lean.Common.LeanSelectedCount::set_MatchMin(System.Int32)
extern void LeanSelectedCount_set_MatchMin_m26C85CF8F47A94150A3CAE614AD654AB46E27A2F (void);
// 0x00000219 System.Int32 Lean.Common.LeanSelectedCount::get_MatchMin()
extern void LeanSelectedCount_get_MatchMin_mEB87EAE39F69027DC3B6B4D54E3C34A63490C2D4 (void);
// 0x0000021A System.Void Lean.Common.LeanSelectedCount::set_MatchMax(System.Int32)
extern void LeanSelectedCount_set_MatchMax_mE027EE5307BEFC90DEC896D47E1223A4D1968C35 (void);
// 0x0000021B System.Int32 Lean.Common.LeanSelectedCount::get_MatchMax()
extern void LeanSelectedCount_get_MatchMax_mB1E396CD95B3F0C8A206864622CB124A4A49FE20 (void);
// 0x0000021C UnityEngine.Events.UnityEvent Lean.Common.LeanSelectedCount::get_OnMatch()
extern void LeanSelectedCount_get_OnMatch_m28614F27E2E4D8F3CAD9FB7B6DFAC96C3B569DEA (void);
// 0x0000021D UnityEngine.Events.UnityEvent Lean.Common.LeanSelectedCount::get_OnUnmatch()
extern void LeanSelectedCount_get_OnUnmatch_m6FB9BB918C126AD2C8B3CB03DB2D764E676254E4 (void);
// 0x0000021E System.Void Lean.Common.LeanSelectedCount::set_Matched(System.Boolean)
extern void LeanSelectedCount_set_Matched_m1DBA98C84269A263C710A7BB3A62DD5ACDDFDC41 (void);
// 0x0000021F System.Boolean Lean.Common.LeanSelectedCount::get_Matched()
extern void LeanSelectedCount_get_Matched_m401E07556AE8EA9C6092BB2D9AD8E88702271674 (void);
// 0x00000220 System.Void Lean.Common.LeanSelectedCount::OnEnable()
extern void LeanSelectedCount_OnEnable_m6995BE07B6EB80E52841A130AF6FF7983605243C (void);
// 0x00000221 System.Void Lean.Common.LeanSelectedCount::OnDisable()
extern void LeanSelectedCount_OnDisable_m9E879E59A573D0DBCAE33BCB0F5D126A0746DEED (void);
// 0x00000222 System.Void Lean.Common.LeanSelectedCount::HandleAnyA(Lean.Common.LeanSelect,Lean.Common.LeanSelectable)
extern void LeanSelectedCount_HandleAnyA_mE79D8A11D988DBFE35A363C68FDF15BDE44DDCAA (void);
// 0x00000223 System.Void Lean.Common.LeanSelectedCount::HandleAnyB(Lean.Common.LeanSelectable)
extern void LeanSelectedCount_HandleAnyB_m3B8ACAD57DC33B22E82CCC557C5B2E8AB06DF48C (void);
// 0x00000224 System.Void Lean.Common.LeanSelectedCount::SetMatched(System.Boolean)
extern void LeanSelectedCount_SetMatched_mA27A118C35D6CB496FB2280D0F3D1060B726AD1E (void);
// 0x00000225 System.Void Lean.Common.LeanSelectedCount::UpdateState()
extern void LeanSelectedCount_UpdateState_mF2F74FFD4DFC2FC7A6B53B1478DBD21ECAAEE42D (void);
// 0x00000226 System.Void Lean.Common.LeanSelectedCount::.ctor()
extern void LeanSelectedCount__ctor_mA1957F796D5816CF8824755B26A8E50BCCD0983F (void);
// 0x00000227 System.Void Lean.Common.LeanSelectedCount/IntEvent::.ctor()
extern void IntEvent__ctor_mF734AF9D44691E28CCA10926AAE13AAD40CD7130 (void);
// 0x00000228 System.Void Lean.Common.LeanSelectedRatio::set_Inverse(System.Boolean)
extern void LeanSelectedRatio_set_Inverse_m0DB89FEF720136D8F175EF392A6326F5EF46973A (void);
// 0x00000229 System.Boolean Lean.Common.LeanSelectedRatio::get_Inverse()
extern void LeanSelectedRatio_get_Inverse_mC0105A09549922643199FB45B30EBC0534568C54 (void);
// 0x0000022A Lean.Common.LeanSelectedRatio/FloatEvent Lean.Common.LeanSelectedRatio::get_OnRatio()
extern void LeanSelectedRatio_get_OnRatio_m8AF316C32422A606EBE46EB9BBD07B84E4BBE7D8 (void);
// 0x0000022B System.Void Lean.Common.LeanSelectedRatio::UpdateNow()
extern void LeanSelectedRatio_UpdateNow_m6CC9B5812A2A7DFC348E8D01A4FB1C1AA5EE53EE (void);
// 0x0000022C System.Void Lean.Common.LeanSelectedRatio::Update()
extern void LeanSelectedRatio_Update_mF042118F939E34268AE3030ACE7A27F0794D7E66 (void);
// 0x0000022D System.Void Lean.Common.LeanSelectedRatio::.ctor()
extern void LeanSelectedRatio__ctor_m7195F8E4C9261520CFCA5F072292FFF5F2986178 (void);
// 0x0000022E System.Void Lean.Common.LeanSelectedRatio/FloatEvent::.ctor()
extern void FloatEvent__ctor_m0FA5660715550455FA4A1C77CA26F9DC3DC67EF6 (void);
// 0x0000022F System.Void Lean.Common.LeanSmoothedValue::set_Damping(System.Single)
extern void LeanSmoothedValue_set_Damping_m6F0D19865B0FFDACDA78ED72B274BC8F4298C622 (void);
// 0x00000230 System.Single Lean.Common.LeanSmoothedValue::get_Damping()
extern void LeanSmoothedValue_get_Damping_m222F3903B0579246329D5E5A2840A796CF7FF981 (void);
// 0x00000231 System.Void Lean.Common.LeanSmoothedValue::set_Threshold(System.Single)
extern void LeanSmoothedValue_set_Threshold_m07C32333FACE027C1AE931F28A94E3E3A1BC7B0D (void);
// 0x00000232 System.Single Lean.Common.LeanSmoothedValue::get_Threshold()
extern void LeanSmoothedValue_get_Threshold_m3DD7954ABA57E8C9972C0214C9568189517C6513 (void);
// 0x00000233 System.Void Lean.Common.LeanSmoothedValue::set_AutoStop(System.Boolean)
extern void LeanSmoothedValue_set_AutoStop_m9BF8776DAB4D7F2D2EB6F38FAA832139CB50E410 (void);
// 0x00000234 System.Boolean Lean.Common.LeanSmoothedValue::get_AutoStop()
extern void LeanSmoothedValue_get_AutoStop_mA450F101A84977F0B7EB12EF8C472295E8B0CC82 (void);
// 0x00000235 Lean.Common.LeanSmoothedValue/FloatEvent Lean.Common.LeanSmoothedValue::get_OnValueX()
extern void LeanSmoothedValue_get_OnValueX_mBC14FF895C015A63BDD052D0B05ED7EAAC302D0D (void);
// 0x00000236 Lean.Common.LeanSmoothedValue/FloatEvent Lean.Common.LeanSmoothedValue::get_OnValueY()
extern void LeanSmoothedValue_get_OnValueY_m992ACBB54FADABFFA22FA34F690736FF69C0D362 (void);
// 0x00000237 Lean.Common.LeanSmoothedValue/FloatEvent Lean.Common.LeanSmoothedValue::get_OnValueZ()
extern void LeanSmoothedValue_get_OnValueZ_mE8FF2F4A1F66BBA36AF609688B04038C10C38AF5 (void);
// 0x00000238 Lean.Common.LeanSmoothedValue/Vector2Event Lean.Common.LeanSmoothedValue::get_OnValueXY()
extern void LeanSmoothedValue_get_OnValueXY_m37B1CCB55F4701B31A9845FDFF3AC51A3665D5C2 (void);
// 0x00000239 Lean.Common.LeanSmoothedValue/Vector3Event Lean.Common.LeanSmoothedValue::get_OnValueXYZ()
extern void LeanSmoothedValue_get_OnValueXYZ_m235DC638A1D0188CA74A5EF5F5DD0D60B3F0C912 (void);
// 0x0000023A System.Void Lean.Common.LeanSmoothedValue::SetX(System.Single)
extern void LeanSmoothedValue_SetX_mF5D610310A84BAEB13E0DAED78CE02CCAB174B5B (void);
// 0x0000023B System.Void Lean.Common.LeanSmoothedValue::SetY(System.Single)
extern void LeanSmoothedValue_SetY_mCCC3E2601DBF8AE786429AAB320E3B6000905C02 (void);
// 0x0000023C System.Void Lean.Common.LeanSmoothedValue::SetZ(System.Single)
extern void LeanSmoothedValue_SetZ_mF9195EFA8E62D38CE0820D22255B24AD12ABAF2C (void);
// 0x0000023D System.Void Lean.Common.LeanSmoothedValue::SetXY(UnityEngine.Vector2)
extern void LeanSmoothedValue_SetXY_mEEE26263311471FF70D2FFC6F4F457FFA29DCB01 (void);
// 0x0000023E System.Void Lean.Common.LeanSmoothedValue::SetXYZ(UnityEngine.Vector3)
extern void LeanSmoothedValue_SetXYZ_m5363ADA1D179E68E33728B5AD70FF6B94F5DDFC1 (void);
// 0x0000023F System.Void Lean.Common.LeanSmoothedValue::SnapToTarget()
extern void LeanSmoothedValue_SnapToTarget_mAAD719FFB73FA11E07C3FC06E60D88F5F9170C07 (void);
// 0x00000240 System.Void Lean.Common.LeanSmoothedValue::Stop()
extern void LeanSmoothedValue_Stop_m198D507C40ADC95949ADA95209F08BBB464DDC4B (void);
// 0x00000241 System.Void Lean.Common.LeanSmoothedValue::Update()
extern void LeanSmoothedValue_Update_m637FFDEBD4BB0C8BBC3A111E7A42462FD6373DC2 (void);
// 0x00000242 System.Void Lean.Common.LeanSmoothedValue::Submit(UnityEngine.Vector3)
extern void LeanSmoothedValue_Submit_m0DFA40F54DC060B33FF8A47840B5C389B26C4E6D (void);
// 0x00000243 System.Void Lean.Common.LeanSmoothedValue::.ctor()
extern void LeanSmoothedValue__ctor_mF40384D1D7A305BBAEFF7C1B7EF87F2F5248CCB5 (void);
// 0x00000244 System.Void Lean.Common.LeanSmoothedValue/FloatEvent::.ctor()
extern void FloatEvent__ctor_mF11915206747896B326EEBB3756CF8CED8437FFC (void);
// 0x00000245 System.Void Lean.Common.LeanSmoothedValue/Vector2Event::.ctor()
extern void Vector2Event__ctor_mF5A585C45F610ACE1379BDA5357DFCBA62417C46 (void);
// 0x00000246 System.Void Lean.Common.LeanSmoothedValue/Vector3Event::.ctor()
extern void Vector3Event__ctor_mCEE379DF2753E85D0EDA8FDF1DAC061E15D21F28 (void);
// 0x00000247 System.Void Lean.Common.LeanSpawnBetween::set_Prefab(UnityEngine.Transform)
extern void LeanSpawnBetween_set_Prefab_m07F3F38240551EC84BCE5CE7A786C61687B60D51 (void);
// 0x00000248 UnityEngine.Transform Lean.Common.LeanSpawnBetween::get_Prefab()
extern void LeanSpawnBetween_get_Prefab_mD581E6DAB0EF6E26296DB4D7C204C8A503322845 (void);
// 0x00000249 System.Void Lean.Common.LeanSpawnBetween::set_VelocityMultiplier(System.Single)
extern void LeanSpawnBetween_set_VelocityMultiplier_m0AD8E0E41ECF39FBDB89C301544471C0AF8235F8 (void);
// 0x0000024A System.Single Lean.Common.LeanSpawnBetween::get_VelocityMultiplier()
extern void LeanSpawnBetween_get_VelocityMultiplier_m487C2FD6233E29A9BF5ACB84D3715F68146DEB80 (void);
// 0x0000024B System.Void Lean.Common.LeanSpawnBetween::set_VelocityMin(System.Single)
extern void LeanSpawnBetween_set_VelocityMin_m3B5B0E20A3679795203EDF250155AC4C9CDE8928 (void);
// 0x0000024C System.Single Lean.Common.LeanSpawnBetween::get_VelocityMin()
extern void LeanSpawnBetween_get_VelocityMin_m389AFEFF8E79EFF59C6C31AD05AC1773C6CFD630 (void);
// 0x0000024D System.Void Lean.Common.LeanSpawnBetween::set_VelocityMax(System.Single)
extern void LeanSpawnBetween_set_VelocityMax_m04F26FC0696AB17263235DD3A59DE54BB15FF7E9 (void);
// 0x0000024E System.Single Lean.Common.LeanSpawnBetween::get_VelocityMax()
extern void LeanSpawnBetween_get_VelocityMax_mEB9689F6ABCC85376C640F32363A97F399442DD7 (void);
// 0x0000024F System.Void Lean.Common.LeanSpawnBetween::Spawn(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanSpawnBetween_Spawn_m8FA11433B8B6F641D8DB64606571DA17FE54B778 (void);
// 0x00000250 System.Void Lean.Common.LeanSpawnBetween::.ctor()
extern void LeanSpawnBetween__ctor_m8D925BC4FFD405A54D30AFF4F686102B21A1ADE1 (void);
// 0x00000251 System.Void Lean.Common.LeanSwap::set_Index(System.Int32)
extern void LeanSwap_set_Index_m8B04E79C9D1DADD290E6693BE9BA7C0795F54DC8 (void);
// 0x00000252 System.Int32 Lean.Common.LeanSwap::get_Index()
extern void LeanSwap_get_Index_m6B11D857DE3B173121F4CEBAF2C7E029FC7D2714 (void);
// 0x00000253 System.Collections.Generic.List`1<UnityEngine.Transform> Lean.Common.LeanSwap::get_Prefabs()
extern void LeanSwap_get_Prefabs_mAFB41E2531F8487FEB09212860B90259E2DEBF41 (void);
// 0x00000254 System.Void Lean.Common.LeanSwap::UpdateSwap()
extern void LeanSwap_UpdateSwap_m3DB8B10CDCF43394B088F0C3AAA33CAE08F1C3BE (void);
// 0x00000255 System.Void Lean.Common.LeanSwap::SwapTo(System.Int32)
extern void LeanSwap_SwapTo_mF62DD99A4E07F03269DC80D64B96AC746E6BB192 (void);
// 0x00000256 System.Void Lean.Common.LeanSwap::SwapToPrevious()
extern void LeanSwap_SwapToPrevious_mB1DB144F6390035AF0AF874F8C5C810FFF496F2B (void);
// 0x00000257 System.Void Lean.Common.LeanSwap::SwapToNext()
extern void LeanSwap_SwapToNext_m1476858973D0B0AAE9F539A24E0F0CD036367F6A (void);
// 0x00000258 UnityEngine.Transform Lean.Common.LeanSwap::GetPrefab()
extern void LeanSwap_GetPrefab_m5ADC15A171025D8DDB50C1AF98D7B414355EEA74 (void);
// 0x00000259 System.Void Lean.Common.LeanSwap::.ctor()
extern void LeanSwap__ctor_m4C0EE8FC7A5EB43B8846F2FFB1C73A9E8838EE64 (void);
// 0x0000025A Lean.Common.LeanThresholdDelta/FloatEvent Lean.Common.LeanThresholdDelta::get_OnDeltaX()
extern void LeanThresholdDelta_get_OnDeltaX_m47D4D2C7A3D7DC5ABBD91AF09F26BDA7B3995063 (void);
// 0x0000025B Lean.Common.LeanThresholdDelta/FloatEvent Lean.Common.LeanThresholdDelta::get_OnDeltaY()
extern void LeanThresholdDelta_get_OnDeltaY_m848FC1DA603C7D62B924F6CCC3FD3A7AC2526F66 (void);
// 0x0000025C Lean.Common.LeanThresholdDelta/FloatEvent Lean.Common.LeanThresholdDelta::get_OnDeltaZ()
extern void LeanThresholdDelta_get_OnDeltaZ_m55A11F5DD82943238F8244D89B8D131508A3B71C (void);
// 0x0000025D Lean.Common.LeanThresholdDelta/Vector2Event Lean.Common.LeanThresholdDelta::get_OnDeltaXY()
extern void LeanThresholdDelta_get_OnDeltaXY_mAB8B927E4C5B52CC3D3FEC7586D1BD745FE19C80 (void);
// 0x0000025E Lean.Common.LeanThresholdDelta/Vector3Event Lean.Common.LeanThresholdDelta::get_OnDeltaXYZ()
extern void LeanThresholdDelta_get_OnDeltaXYZ_m264B455D2F00C95B123265D3E7B2CF2D85CAFAF1 (void);
// 0x0000025F System.Void Lean.Common.LeanThresholdDelta::AddXY(UnityEngine.Vector2)
extern void LeanThresholdDelta_AddXY_mDAA245C45038E138E55F51EB4201AE690E327C1C (void);
// 0x00000260 System.Void Lean.Common.LeanThresholdDelta::AddXYZ(UnityEngine.Vector3)
extern void LeanThresholdDelta_AddXYZ_mCD0492C9C331B73D8095FC28D42F674515D99E8D (void);
// 0x00000261 System.Void Lean.Common.LeanThresholdDelta::AddX(System.Single)
extern void LeanThresholdDelta_AddX_mF90E8E8229F858B01F3B9CF8B8247BF66F4D0FD7 (void);
// 0x00000262 System.Void Lean.Common.LeanThresholdDelta::AddY(System.Single)
extern void LeanThresholdDelta_AddY_mD0A621F7E1AE6DD496BEB57BCD96B9523A339C79 (void);
// 0x00000263 System.Void Lean.Common.LeanThresholdDelta::AddZ(System.Single)
extern void LeanThresholdDelta_AddZ_mA7103E6525461C3AC7ACA9459B2979F9BAD285D7 (void);
// 0x00000264 System.Void Lean.Common.LeanThresholdDelta::Update()
extern void LeanThresholdDelta_Update_m87B4CB842A467E1B5F4F8A07040B10CB6ABCEBD7 (void);
// 0x00000265 System.Void Lean.Common.LeanThresholdDelta::.ctor()
extern void LeanThresholdDelta__ctor_m6FB551D8D5C0B219CCE6A188425683E23C27E44A (void);
// 0x00000266 System.Void Lean.Common.LeanThresholdDelta/FloatEvent::.ctor()
extern void FloatEvent__ctor_m9F3D19263E9C7A4AC2A65C8ADC7B628439C1FB7C (void);
// 0x00000267 System.Void Lean.Common.LeanThresholdDelta/Vector2Event::.ctor()
extern void Vector2Event__ctor_mCEE16563E24441051C7A2725FE120AE959F9F586 (void);
// 0x00000268 System.Void Lean.Common.LeanThresholdDelta/Vector3Event::.ctor()
extern void Vector3Event__ctor_m1ADFEB168C21214373EE755DCA8AE831F6A913AB (void);
// 0x00000269 Lean.Common.LeanThresholdPosition/FloatEvent Lean.Common.LeanThresholdPosition::get_OnPositionX()
extern void LeanThresholdPosition_get_OnPositionX_m6E4B8029D852580887605E7912C3A97272EE3630 (void);
// 0x0000026A Lean.Common.LeanThresholdPosition/FloatEvent Lean.Common.LeanThresholdPosition::get_OnPositionY()
extern void LeanThresholdPosition_get_OnPositionY_m302793C43B29DCCDD1D5AEE8A05CB0EC50977A24 (void);
// 0x0000026B Lean.Common.LeanThresholdPosition/FloatEvent Lean.Common.LeanThresholdPosition::get_OnPositionZ()
extern void LeanThresholdPosition_get_OnPositionZ_mDD2FC2DBF7022C9F9847A3E5B0D761B75476B84D (void);
// 0x0000026C Lean.Common.LeanThresholdPosition/Vector2Event Lean.Common.LeanThresholdPosition::get_OnPositionXY()
extern void LeanThresholdPosition_get_OnPositionXY_mB674C3F5580E30331E7629A8D407D541577AE3C9 (void);
// 0x0000026D Lean.Common.LeanThresholdPosition/Vector3Event Lean.Common.LeanThresholdPosition::get_OnPositionXYZ()
extern void LeanThresholdPosition_get_OnPositionXYZ_m20AA2818C62D818A030794880752A7AA3DF26C6B (void);
// 0x0000026E System.Void Lean.Common.LeanThresholdPosition::AddXY(UnityEngine.Vector2)
extern void LeanThresholdPosition_AddXY_mE6E132CB5D6E2621C46BF203F3E494E525EAABD9 (void);
// 0x0000026F System.Void Lean.Common.LeanThresholdPosition::AddXYZ(UnityEngine.Vector3)
extern void LeanThresholdPosition_AddXYZ_m06E90611854FA44BB9D5B112F0F32A376FE6998D (void);
// 0x00000270 System.Void Lean.Common.LeanThresholdPosition::AddX(System.Single)
extern void LeanThresholdPosition_AddX_mDF38D46966AAAFE18E764233D8910B9006C25D2E (void);
// 0x00000271 System.Void Lean.Common.LeanThresholdPosition::AddY(System.Single)
extern void LeanThresholdPosition_AddY_mE35D1A1430A38C7DE8576F9C744CD3373BEFA245 (void);
// 0x00000272 System.Void Lean.Common.LeanThresholdPosition::AddZ(System.Single)
extern void LeanThresholdPosition_AddZ_mE06B76B4B3E59F300589FCBFE12F44CE7AE34B50 (void);
// 0x00000273 System.Void Lean.Common.LeanThresholdPosition::SetXY(UnityEngine.Vector2)
extern void LeanThresholdPosition_SetXY_m8A6D0EE079DAEB98A63959BFB49CE8B48825D181 (void);
// 0x00000274 System.Void Lean.Common.LeanThresholdPosition::SetXYZ(UnityEngine.Vector3)
extern void LeanThresholdPosition_SetXYZ_m40BCE26E08C1D6B0CABC2ABA4AD456EDC250AB42 (void);
// 0x00000275 System.Void Lean.Common.LeanThresholdPosition::Update()
extern void LeanThresholdPosition_Update_mC3557A8858BF4FED1E4023035B7F43670E23D8D1 (void);
// 0x00000276 System.Void Lean.Common.LeanThresholdPosition::.ctor()
extern void LeanThresholdPosition__ctor_m741FA9415666E10A9945CC4FB319370565817FCC (void);
// 0x00000277 System.Void Lean.Common.LeanThresholdPosition/FloatEvent::.ctor()
extern void FloatEvent__ctor_mC81BBA6160F9865C6F893F87C6BA1D58E4D566A5 (void);
// 0x00000278 System.Void Lean.Common.LeanThresholdPosition/Vector2Event::.ctor()
extern void Vector2Event__ctor_mA66C3E7F65543CEA3D53270FC27F95C7FDD5F0AE (void);
// 0x00000279 System.Void Lean.Common.LeanThresholdPosition/Vector3Event::.ctor()
extern void Vector3Event__ctor_mDA566D599413F7136C8CD5DBDE8B6B73E82C74DB (void);
// 0x0000027A Lean.Common.LeanValue/FloatEvent Lean.Common.LeanValue::get_OnValueX()
extern void LeanValue_get_OnValueX_mE28F301B04490B1023068DD3D79C56859EC8F9D4 (void);
// 0x0000027B Lean.Common.LeanValue/FloatEvent Lean.Common.LeanValue::get_OnValueY()
extern void LeanValue_get_OnValueY_mAF0CCD8F538C9B953624FF6C1AC4DBF6324738A1 (void);
// 0x0000027C Lean.Common.LeanValue/FloatEvent Lean.Common.LeanValue::get_OnValueZ()
extern void LeanValue_get_OnValueZ_mE2F0DF9E95590563861EBEB5DC725467E5425879 (void);
// 0x0000027D Lean.Common.LeanValue/Vector2Event Lean.Common.LeanValue::get_OnValueXY()
extern void LeanValue_get_OnValueXY_m9720AA0EE57EDB8BFCC176C4AC09D2CDF388C8E9 (void);
// 0x0000027E Lean.Common.LeanValue/Vector3Event Lean.Common.LeanValue::get_OnValueXYZ()
extern void LeanValue_get_OnValueXYZ_m7C50A607575FB47CC305C438CEEAC3C667145E2A (void);
// 0x0000027F System.Void Lean.Common.LeanValue::set_Current(UnityEngine.Vector3)
extern void LeanValue_set_Current_m76CBF87E069DDC81A6ABD35D20F0AA6A28AA22F3 (void);
// 0x00000280 UnityEngine.Vector3 Lean.Common.LeanValue::get_Current()
extern void LeanValue_get_Current_m801BD6A74D70F36CEC54FB4B0911FB654422BA3C (void);
// 0x00000281 System.Void Lean.Common.LeanValue::SetX(System.Single)
extern void LeanValue_SetX_mD8358A218B3508400D264AD51C0A512DCD884F66 (void);
// 0x00000282 System.Void Lean.Common.LeanValue::SetY(System.Single)
extern void LeanValue_SetY_m66815FD614F9B0C86ECEA353BFB7FC18CFA16FCF (void);
// 0x00000283 System.Void Lean.Common.LeanValue::SetZ(System.Single)
extern void LeanValue_SetZ_mC00277EC365C88D5045A8A30BB7554175785F0E0 (void);
// 0x00000284 System.Void Lean.Common.LeanValue::SetXY(UnityEngine.Vector2)
extern void LeanValue_SetXY_mFC49FA04029D7E5683F02C71E67D3B50C8994A97 (void);
// 0x00000285 System.Void Lean.Common.LeanValue::SetXYZ(UnityEngine.Vector3)
extern void LeanValue_SetXYZ_mD284769B0C82F6316AC481336A9AA01522FD493F (void);
// 0x00000286 System.Void Lean.Common.LeanValue::Submit()
extern void LeanValue_Submit_mC4CFFC6C9561C63E16EAE0FCAD1A3F4360697656 (void);
// 0x00000287 System.Void Lean.Common.LeanValue::.ctor()
extern void LeanValue__ctor_m6CFE4F4EE37214FAC042CA1CB98D16EDC43FA70C (void);
// 0x00000288 System.Void Lean.Common.LeanValue/FloatEvent::.ctor()
extern void FloatEvent__ctor_m49A01AC6A1067C84C694C5C2249BCC64E842E93A (void);
// 0x00000289 System.Void Lean.Common.LeanValue/Vector2Event::.ctor()
extern void Vector2Event__ctor_m2C0150BD087FD407F60713DE58C5A114DE2C079A (void);
// 0x0000028A System.Void Lean.Common.LeanValue/Vector3Event::.ctor()
extern void Vector3Event__ctor_m3B7AC0DAFFC7A08A5E44A94D77FB3CD9C72BD07F (void);
static Il2CppMethodPointer s_methodPointers[650] = 
{
	LeanLoadScene_set_SceneName_m2ED0105FFC95BE203FACA9F61193EEBCB974E4DD,
	LeanLoadScene_get_SceneName_m8145D8B162B0CFD5F34F909BB4C8B7FB87A1C00E,
	LeanLoadScene_set_ASync_mDCF18F6AE71C005ED3835B33221765913D3BD90B,
	LeanLoadScene_get_ASync_mB4CE59DED48DEAD12417ABACA86DF437FD2E24FA,
	LeanLoadScene_set_Additive_m24B4612740C13CDBBB8386A044498E7281F89FE8,
	LeanLoadScene_get_Additive_m322E489626E33055A4D35451A35D252DF5F5456C,
	LeanLoadScene_Load_mF187C0667FAA85B99C8FF3201D6C0B5AA34D5F8C,
	LeanLoadScene_Load_mD5209D8A38ADCA5D3A70D0241FBC10B4C8592DA9,
	LeanLoadScene__ctor_m25BDE8C86D5EC876FDB243F09F6FE58F8D8B8DAF,
	LeanPongBall_set_StartSpeed_m4F5B70EB025BB916FFECE2A46D676F6369B315C6,
	LeanPongBall_get_StartSpeed_mCE4AA5D8A1B87A748564792256D916F915FACF02,
	LeanPongBall_set_Spread_m3C0F91B175C69DBB4EE3A6E5F2481CF86EED52F8,
	LeanPongBall_get_Spread_m43F880905B1FBCF30298CC4034FB8C024BEFA28E,
	LeanPongBall_set_Acceleration_m0B545A4076044C34CB40CD4B90B89F4B33F862B6,
	LeanPongBall_get_Acceleration_m8481C1C6EEAF222087530491136C94FA7DD48193,
	LeanPongBall_set_Bounds_m06D57F69C927264240D0D0E368781852A410EDC6,
	LeanPongBall_get_Bounds_mE247CBDEA869E44F32542B0B430BE38DA4A52B52,
	LeanPongBall_Awake_m564F6C3F4EE949E07347035A72754AD1462D4940,
	LeanPongBall_FixedUpdate_m628C88A56020D91AE1D8A31808CA250901AAA125,
	LeanPongBall_ResetPositionAndVelocity_mF76B0F5A6C3834E3206B56AC4A3BA754233C54F9,
	LeanPongBall__ctor_mD5E0CDEE9CA4E3A3BCFC4E0F30FED2352C1C8CF1,
	LeanSelectedString_set_Format_mDD3F78ADC7C4FEAC83C01B4FB1002505293E5802,
	LeanSelectedString_get_Format_m673DF60C47A6A42D5ECCCD8136F7BDA940930909,
	LeanSelectedString_get_OnString_mDEB2E676C5F2958D5A4E2D68F5F9333AB280A654,
	LeanSelectedString_UpdateNow_m1BC9220D2DE3F693201134E1F6064E32653ED8D1,
	LeanSelectedString_OnEnable_m16992CAC8173F4D3C22FE311594446E4A630F005,
	LeanSelectedString_OnDisable_m1CF47D4DC172E4113FBF42C587F0F878E7CE3933,
	LeanSelectedString_HandleA_mE8370CFB2AE2F05683AE31FE550D28DBEF92AA92,
	LeanSelectedString_HandleB_mF99FC0B65998AE1D5AB1A2E3CC617CB132FF40B6,
	LeanSelectedString__ctor_m6E26E9B641D69787BF4E7F34C02777EE1C43D824,
	StringEvent__ctor_m8B9DA6C411D24AC3E7EC9C088B2C0708E148CD86,
	LeanChase_set_Destination_m4B417A4FAD51E222F012D23CCA799B9BBF84FC9D,
	LeanChase_get_Destination_m5B995A8A91341D562D23817D6ED70702BFE09DFE,
	LeanChase_set_DestinationOffset_mA8CC85B85EBCE60E0437B9582CD2CA02C47CF1ED,
	LeanChase_get_DestinationOffset_m0B814669036006F986A34F783FFC609633F680FF,
	LeanChase_set_Position_m578E880779FD29E1FC93907F9CF65C37583E0955,
	LeanChase_get_Position_m8FC61FF9D808DC315D66D461996A39BAEDC2C47D,
	LeanChase_set_Offset_mD5C3FCCD97B6866BC96266BA9927BE9B7D669F05,
	LeanChase_get_Offset_m29B46A1EADD2BF684E4F101D862D08DADD1FAAE0,
	LeanChase_set_Damping_mE0159149B79BD9F3B29CDFB17A45DDDF9EE59FD8,
	LeanChase_get_Damping_m48AAB2482223995F01E76C9AB67381C0C5B580D3,
	LeanChase_set_Linear_mBEFF74E30E9B7EE753E7BBB8F6D3CD94E9BF3D02,
	LeanChase_get_Linear_m4F277391006372547E74FAF3A1277DA8E5B5091C,
	LeanChase_set_IgnoreZ_m7A51ED9585C05FF11ECF9215CD84367E1A173E6D,
	LeanChase_get_IgnoreZ_m3528736EEF8E8F93AB799D34E72259AF4B04E4AC,
	LeanChase_set_Continuous_m93C0B0C97F1B7770D3864B4964EC220C3B519C02,
	LeanChase_get_Continuous_m8015B968FA947E21C53B82073B9C68923A776685,
	LeanChase_set_SetPositionOnStart_mBC3A1027E1609743749D741360BFDC846316DC62,
	LeanChase_get_SetPositionOnStart_mC6C6190438DC5FE1C3EB760CBB2A95DA7309667A,
	LeanChase_SetPosition_m4E3325B6201176D23E13CFE8500267FE6268F4C9,
	LeanChase_SnapToPosition_m57CB8EEDB18D3532BC7E19A220315C87BE77F76D,
	LeanChase_Start_m16FB1D836146782581D4FD13EC281DF4DDCC1775,
	LeanChase_Update_m13CADF00437272117590F5EDFCD79521711F227B,
	LeanChase_UpdatePosition_mC2057A7EF766D685BF6089C1C8F427221534EBD8,
	LeanChase__ctor_m52B784594A600BA71866AF2FDD6E3F6486A8884D,
	LeanChaseRigidbody_SetPosition_m5FC81EDB57C467D6B433E70A356D2F46475445C7,
	LeanChaseRigidbody_OnEnable_m379DD92BC2DA32B729578736B22FE840425C52FF,
	LeanChaseRigidbody_UpdatePosition_mB9CDFA98A03A334B7E0C750EB71619C50939502E,
	LeanChaseRigidbody_LateUpdate_m71F608B10BACF21F7D17DC292FEC74E3511FD5F6,
	LeanChaseRigidbody__ctor_mE6D41D192AE3E6F69AFB9A80FB41C9CCA2697353,
	LeanChaseRigidbody2D_set_Axis_mCBAE3A6AB8E3BDE2E91B9C77A8AEF970F6AF900C,
	LeanChaseRigidbody2D_get_Axis_mBA7A8C46FAB209EDFC3FD1E36D97F6F8A0510CBB,
	LeanChaseRigidbody2D_SetPosition_mC686C1879ADBF2C4903413999CDC55F830187A28,
	LeanChaseRigidbody2D_OnEnable_m553BC6C23A8E9DC737F45FB52A8BAD09CD34F5EB,
	LeanChaseRigidbody2D_UpdatePosition_mCC5599305720B4B408DA7B969C69856608841560,
	LeanChaseRigidbody2D_LateUpdate_m73A7F3E274BAD7F1E9A0927055CD4F53653FA199,
	LeanChaseRigidbody2D__ctor_mE9DB26371426D08BA5B0DCD091F57A50B4E3FA38,
	LeanConstrainLocalPosition_set_X_mAD83783123F79A72A9F12E95395FAB7BAA903A39,
	LeanConstrainLocalPosition_get_X_m67B07C8FE618F2154ABA6E8E664FCC0F4D984D8C,
	LeanConstrainLocalPosition_set_XMin_mC656465B533E53C7AEB15659AC6460BBFFDF10A9,
	LeanConstrainLocalPosition_get_XMin_mCC8AD3F827341A18348EFEBBAEBA71B467BA554A,
	LeanConstrainLocalPosition_set_XMax_m9910F0214A043EEAF3D3F1AE59AE1720C2AC17B0,
	LeanConstrainLocalPosition_get_XMax_m003C3709A5F9EDE2700ED0C6469BF4CF3AF25637,
	LeanConstrainLocalPosition_set_Y_m2C82E1B5757C211B7AF81738D63ECD8D87B7C741,
	LeanConstrainLocalPosition_get_Y_mEA52AD0CF11BBFD0D2B94258B4ED70D44A96D50E,
	LeanConstrainLocalPosition_set_YMin_m053AA71C148615BB55575E45945E2E43913D7E13,
	LeanConstrainLocalPosition_get_YMin_mD8F6457D65A7389CC2A660AF7ACFB88953F762F3,
	LeanConstrainLocalPosition_set_YMax_mF4B6AFBCBDC780255C46B92B8D08C8C0D0115F8E,
	LeanConstrainLocalPosition_get_YMax_m30DF80B2B44446FF68B6FEE051D0AC4F4B386E15,
	LeanConstrainLocalPosition_set_Z_mF332B85515D6A8C86D7F7C5DE2F6A055A022D385,
	LeanConstrainLocalPosition_get_Z_m78FDB5819E98F4C17D4748D1F09DCCDC910EACA0,
	LeanConstrainLocalPosition_set_ZMin_m75E4E15812B1391DE6D1B62318C60ED4995B2A27,
	LeanConstrainLocalPosition_get_ZMin_m636355DB3B64DF4BF19961435EC005C134BE28FD,
	LeanConstrainLocalPosition_set_ZMax_mA2B8A677983D66D7351948E8ECC5630766F69AC6,
	LeanConstrainLocalPosition_get_ZMax_mEEDB3D918A48EE4BE7D2036D48815DA995F75560,
	LeanConstrainLocalPosition_LateUpdate_mB46A64B6E9F4CD18BE667DF37CD6186C6EFA5BBC,
	LeanConstrainLocalPosition_DoClamp_mDE07C31D8E4B4AAF42B8888E35C9BEFCFBE6F032,
	LeanConstrainLocalPosition__ctor_mC1A418817360C0E970290737182A139DFB30A6AF,
	LeanConstrainScale_set_Minimum_m251A573F6590A018DE1E70E5D39FA76E6FD52C07,
	LeanConstrainScale_get_Minimum_m57E567839AD95B2F9B3B39173934DC9426EDC3C0,
	LeanConstrainScale_set_MinimumScale_m214C7808BFF71863D2CDC222B2322F1A93A01C27,
	LeanConstrainScale_get_MinimumScale_mF778974B999A2E7313293DAFEDC7020541EE0078,
	LeanConstrainScale_set_Maximum_mCA057A767ADF058CCACF697CFF0B99D192FD7710,
	LeanConstrainScale_get_Maximum_mE987A283D7A786C94275EDB32D7D17C8221D0940,
	LeanConstrainScale_set_MaximumScale_mB797B3E64284B4A41FCA8FD1F608EDB4CB378C5D,
	LeanConstrainScale_get_MaximumScale_mF2495B32C2F0930FBD0387B05C2E0B175BD02A30,
	LeanConstrainScale_LateUpdate_m13114E07385885B30BFD67EAA5E2E3D50AFE817D,
	LeanConstrainScale__ctor_mBA2CA4DE365CAAB7C7AC007B0971C4F68E2A9F5E,
	LeanConstrainToAxis_set_RelativeTo_mA58C8E6E74E71B7A47DE3C31E998C21199666D47,
	LeanConstrainToAxis_get_RelativeTo_m0962A62577CC73B4CF4897E81C767AD553CD465B,
	LeanConstrainToAxis_set_Axis_m8E8E0E9AB2CF9A055DB0BCC89FE18103555E58FD,
	LeanConstrainToAxis_get_Axis_m49A54C8ED223B4849D1C37650DF0227F9B854B97,
	LeanConstrainToAxis_set_Minimum_mBDFABDE230CC06B5B8C88288CE8DA993DF43279A,
	LeanConstrainToAxis_get_Minimum_m6540EAB02B9E75BC822309265CB6553C6EDB8A28,
	LeanConstrainToAxis_set_Maximum_mAAD02F722B1D6DCC7E759BF2BCD45BD14F6559DB,
	LeanConstrainToAxis_get_Maximum_m29766FE7417A750B3B2E95B9C0EBA345DD818610,
	LeanConstrainToAxis_LateUpdate_m5B1545419FE135D90AB597D9E3A8EE051240EED7,
	LeanConstrainToAxis__ctor_m8485E0B411B154168221661D40D9D9EBECE1C2B5,
	LeanConstrainToBox_set_RelativeTo_m38496154408486DB97FF2CBAD49E0F4B31332615,
	LeanConstrainToBox_get_RelativeTo_m2DC2C9EE1A1F2A30E81F2207C99DB845208E164E,
	LeanConstrainToBox_set_Size_mE2E14A848A4CE0F7E4E47B06F3016FA1BC9434BD,
	LeanConstrainToBox_get_Size_mAB207C0FE92218E4687EBFFF236AA9C366C42893,
	LeanConstrainToBox_set_Center_m2E78633E1B721092F0767FC2B2F1057EA837776B,
	LeanConstrainToBox_get_Center_m29872D9E06F6ACBC01B5D236461AF38D614CEB21,
	LeanConstrainToBox_LateUpdate_m13DBB397A57DFEB7F5C114CDB541E2EDFF8893B4,
	LeanConstrainToBox_OnDrawGizmosSelected_m9E4CD774118EE8B9908D278B8A28812584C63A3C,
	LeanConstrainToBox__ctor_m7529C5B4B6D15BEE1499F89E3A039922BE11AA6A,
	LeanConstrainToCollider_set_Collider_m3978AE45AE75031AE8A11833E9EA530E562B72D6,
	LeanConstrainToCollider_get_Collider_m504560B9DCCD845ADC98C6A4B8ED88F13C64C35F,
	LeanConstrainToCollider_LateUpdate_m6EE7044651E03B22B80BD34AA7C573EC028CA510,
	LeanConstrainToCollider__ctor_m95961D7BC999D153F09F1570CD20D4FED8EB5DA2,
	LeanConstrainToColliders_get_Colliders_m4C102D8D9F3AC0AC33F27DE0E371A1693B5E4B45,
	LeanConstrainToColliders_LateUpdate_mAFD576AB6385123178CFB85F59BB695992DA5E97,
	LeanConstrainToColliders__ctor_mE059B3DE0FFD2FD89A849DAA77E27FAC4B7B008B,
	LeanConstrainToDirection_set_Forward_m4F5266A47998011059524C773BDDF063D2D4E051,
	LeanConstrainToDirection_get_Forward_m3F8BB76DF10F6874C7AF37EF141C153BE465D52A,
	LeanConstrainToDirection_set_Direction_m00D4777DA262E2C296C298A34509ED8230495913,
	LeanConstrainToDirection_get_Direction_m6857C0EA19AFB918D3AFFC428995DFD6664DBA25,
	LeanConstrainToDirection_set_RelativeTo_m91F12D706AEFA90476432D0B0232F161835596F8,
	LeanConstrainToDirection_get_RelativeTo_m1A7D2A6F9B4F8DE48E52B06D670FDBB65AFE5067,
	LeanConstrainToDirection_set_MinAngle_m64EFA6A16CE7F4DB6C96F3DC83C8FF7FA6BAD2E6,
	LeanConstrainToDirection_get_MinAngle_m55C9ED6CB306000B787D3F7264F5511AD9BBCD09,
	LeanConstrainToDirection_set_MaxAngle_mD99C02C8DD33BC01EF5D03B61F230DB87E8AFD92,
	LeanConstrainToDirection_get_MaxAngle_m8B41F8419AEA2122BA358F2C9DE8B23934AB7AB8,
	LeanConstrainToDirection_LateUpdate_m3051C3906932C76756C908549CE7DAD5BCC4D79C,
	LeanConstrainToDirection__ctor_m7C49FE309E857DB14C57D6CC25A300859EE5440D,
	LeanConstrainToOrthographic_set_Camera_mDE50A15C5D4C74D818930B406468E20F06FFBE8C,
	LeanConstrainToOrthographic_get_Camera_mBD597A4FF84B1CEDE9170C51DE40A90D9DB7B8F5,
	LeanConstrainToOrthographic_set_Plane_m9228924C81D36F4F7C28867B909637BEB4D891F9,
	LeanConstrainToOrthographic_get_Plane_m6B84298794DD9028C82A6799B18CCB235B7D9E4A,
	LeanConstrainToOrthographic_LateUpdate_mEF4C7DE3328B66DFF151E01561F828A428475BAD,
	LeanConstrainToOrthographic__ctor_m2401A731C108582073CBC62DFABCEB81D4BF26F3,
	LeanDelayedValue_set_Delay_m80EEA757DD0952CEFC93E80A6A39EB2A964659A7,
	LeanDelayedValue_get_Delay_mCADA79B012CD4B533513B9048135B0EF8E399E99,
	LeanDelayedValue_set_AutoClear_m96CDB97151C7A01EEA7469E721E80483423EC9F9,
	LeanDelayedValue_get_AutoClear_m1EF176FF69C8EF4B7A75B6B8E57D95729073004E,
	LeanDelayedValue_get_OnValueX_m42FDDC59270ABCCFA0A83E777A91139B6069077F,
	LeanDelayedValue_get_OnValueY_m53860944C363B18C3568A76651284EC183B56FC4,
	LeanDelayedValue_get_OnValueZ_m9A790DE011976CF286DBA4F1B531C1E2EF4C4F8F,
	LeanDelayedValue_get_OnValueXY_m1E553EC2BA43E0823F9DF78D04D0ADDB6898FBFD,
	LeanDelayedValue_get_OnValueXYZ_mFDB4A3A1A6B95BF6CCAD23898549C14AF2031353,
	LeanDelayedValue_SetX_m128B250DB3932F24F7BC78ECC263048888FC4F6F,
	LeanDelayedValue_SetY_m28FB43C1FE86D2F3AAB74E30B09EE316BC1C6367,
	LeanDelayedValue_SetZ_m36C0A66CAA6BE653F9E2051797ECD6C5F6EAE8D8,
	LeanDelayedValue_SetXY_m1DFBF30FB51F296B16F427D50CCDF5E76417F59B,
	LeanDelayedValue_SetXYZ_mF59642239A344121365DEDECF4173E2B462F766D,
	LeanDelayedValue_Clear_mCB87F767636FB562EF4BCA1E0D2FFDB31C7DFB17,
	LeanDelayedValue_Update_m3B396C55FEE8DEED324E75F27E8619C2FD7F5A32,
	LeanDelayedValue_Submit_mA96A1003C525BD63930E9788173F980E6201B573,
	LeanDelayedValue__ctor_mBA6A9BC44C9C6F87EFAB11B32D7EB59ACC365F21,
	FloatEvent__ctor_m0BA5B83E9B18D2FAC447D657B9A230D091C76AD9,
	Vector2Event__ctor_m916F31F42BBAD15B7A6C1EDA5CEDF9602DA5A9E5,
	Vector3Event__ctor_m46A172A92CCE399A273FEB663F160782B91FA48D,
	LeanDeselected_get_OnSelectable_m839470D2C651AD8E359C69823F052F25E1232227,
	LeanDeselected_OnEnable_mC99CE99CB827767D116850E1B515C1D9BFF525B0,
	LeanDeselected_OnDisable_m1A2AA2679718A60D24B7DF07EFABCB24EC87DB90,
	LeanDeselected_HandleAnyDeselected_m87DC8B73BAD0B65E309257D42342ED45D8945D5A,
	LeanDeselected__ctor_m3A57013051947A16C4AF86A62A385B7978EFBFE2,
	LeanSelectableEvent__ctor_mDF3F9EECEF296751D6960A0CC5018D844E1473B3,
	LeanFollow_set_Threshold_mB10181E711B21BD85CECC34B1238B9096E15F01F,
	LeanFollow_get_Threshold_m68A40FD40FE0388CD2252C42F3CA8AF4CEEB1C80,
	LeanFollow_set_Speed_m429CD3556F3CB14DF7A26D3CCABB087BB83F4578,
	LeanFollow_get_Speed_m3F02F106B38CAA6F849A14DE42865421C908E38A,
	LeanFollow_get_OnReachedDestination_m043DA2E4E5D6C2EBFEB743791D75CBE931E28651,
	LeanFollow_ClearPositions_m485D7C6E05A7854AB051A25D237388886E716E8C,
	LeanFollow_SnapToNextPosition_mB65155AE801812A8D0003300C67727A368953FBB,
	LeanFollow_AddPosition_m4F09D74AECBFB1F48E4CA14F01D8462219299137,
	LeanFollow_Update_m41E1B7205F65A1616FA6B58C499CB40C54F05B4A,
	LeanFollow_TrimPositions_m6549191E7ED3A1BB8762C63F58A0233E4D65B492,
	LeanFollow__ctor_mAD02154475CAF6542DCE59F305D77240357B35B6,
	LeanMaintainDistance_set_Direction_mDE2FDA163F1BB99A8A4CF1638FDEBABF0ED9A7B0,
	LeanMaintainDistance_get_Direction_m559333D57E88ED96443C2882185B78D43920E2FB,
	LeanMaintainDistance_set_DirectionSpace_m50FD326EA0932621E4169B5798610186C78E1D3E,
	LeanMaintainDistance_get_DirectionSpace_m6CE9F97907AB15BA422D3D7F36EDDA5B3FE5A81F,
	LeanMaintainDistance_set_Distance_m59A1F6F92D0EE200DFB508EC88F96C57BB6F925E,
	LeanMaintainDistance_get_Distance_m9EE34C0FD19125650F2092A53EEF2B0055A2E9D6,
	LeanMaintainDistance_set_Damping_m3E83073288B3C80A71B71FB0222695D4362AF697,
	LeanMaintainDistance_get_Damping_m150B69D54BB457678692142D946D6B779BF5F4CB,
	LeanMaintainDistance_set_Clamp_m92DB42527494F04697E331BE61DC9B401071E7B6,
	LeanMaintainDistance_get_Clamp_mFF723E96FBD8C53B32E90B81CAEB6B9E973B3622,
	LeanMaintainDistance_set_ClampMin_m9ABE98819D74FE084A577BF2B7414B22A5E58292,
	LeanMaintainDistance_get_ClampMin_m80400A462104EC53073DE1EB03BF204D6F8728F6,
	LeanMaintainDistance_set_ClampMax_mF3EC942C9F064AA8BB6EE79CA68B4929C61BBC41,
	LeanMaintainDistance_get_ClampMax_m051A71DDE7B771BF321214EDA973C2ED40F77258,
	LeanMaintainDistance_set_CollisionLayers_m7A0C9D359425B8F683AE505E7BFC1100E671C874,
	LeanMaintainDistance_get_CollisionLayers_m55A1CAF14219422A7D1A3577A07B01D40375A76B,
	LeanMaintainDistance_set_CollisionRadius_m18B24AA07ABD4C4E821C8DE4BAE0A09CD29F445E,
	LeanMaintainDistance_get_CollisionRadius_mA6BC598E241A2F471B6255D2B48888E52DF41C9B,
	LeanMaintainDistance_AddDistance_m14637F30A24E9E12AFE958E90AFF4297F9CD5D99,
	LeanMaintainDistance_MultiplyDistance_mDE30ECD65838ED2E21249870C81DA705C71BE79C,
	LeanMaintainDistance_Start_m63153D80A752AF6D66998691FD9BE42336DA3570,
	LeanMaintainDistance_LateUpdate_mA7BD4270E21A518CAF9390CC8F5835649E34DC7B,
	LeanMaintainDistance__ctor_mDB109767140D0873E448C0C70F12028FFB1EEC37,
	LeanManualRescale_set_Target_m84B9D62189054579F7015A7FD150020C172F19A8,
	LeanManualRescale_get_Target_m8F2738CA275C4CBF3822E2E0A2ABD6F7FA8E0729,
	LeanManualRescale_set_AxesA_mC910AC62382C897BD5368F5CD8A21D184F2CD484,
	LeanManualRescale_get_AxesA_mB657741F1FD943467FE452508513D2CECE38D43F,
	LeanManualRescale_set_AxesB_mE94F16663F766B65BAE8315355D985BAABE38E3A,
	LeanManualRescale_get_AxesB_m82733E0DB4F69126DBD219ED9B0144CDB3EC79AE,
	LeanManualRescale_set_Multiplier_m01B4B93D6A0A712436A83E6AFBDCC693EB122B0D,
	LeanManualRescale_get_Multiplier_mC05603AB6DAD8BFE32F16CC21C9CDD9DD8FA955F,
	LeanManualRescale_set_Damping_mBD7F09FB5E045A7CAE63E901F1E728566C326855,
	LeanManualRescale_get_Damping_mAD6EB67F4924B3E86924A7E6151A5C863757E245,
	LeanManualRescale_set_ScaleByTime_mE78249CFA38F7E5D1A777989DE058E0E96FC6E6B,
	LeanManualRescale_get_ScaleByTime_mFCD9E98DFBC30AB7ED6354A1326DC884A8AA3015,
	LeanManualRescale_set_DefaultScale_m63C75315D9AD4F5A33A176CB8A644042644A8240,
	LeanManualRescale_get_DefaultScale_m9E561F3A1B8778410EC83A3D930F30037F3F21B1,
	LeanManualRescale_ResetScale_m73FDFECE254BA9F6FEC221B9415F56013705C68F,
	LeanManualRescale_SnapToTarget_m9878D0C99CFF0AF6D57525E981F88E00B02C0AE4,
	LeanManualRescale_AddScaleA_mD5524D74C62D7C89F215C20BC621EF8CEE814F35,
	LeanManualRescale_AddScaleB_mC748CF0E1066D9064760C0823663E75A0C0B2DCD,
	LeanManualRescale_AddScaleAB_mCDC1ECB47FD8A323621182FFEE45D182579362C0,
	LeanManualRescale_AddScale_mD12DAD81D73F43BDD78ACAA006AE6439FED78663,
	LeanManualRescale_Update_m2315B9BA8F1A4B6C9C9E41C0833441593E73BC08,
	LeanManualRescale_UpdateScale_mC372F982E450375A60D0B61588116AAB8012A8A1,
	LeanManualRescale__ctor_mB99E0D9BB1F7F12C1CBC743B85B6830D2A2CBCCB,
	LeanManualRotate_set_Target_m79CF6EB8F30718DBA31926FAB22BF2A171E5A1AD,
	LeanManualRotate_get_Target_mD1ADAF00F73BBEA1E3B008E202B3337312BA9D0E,
	LeanManualRotate_set_Space_m0CF947853F4C40DC9CA25B409B2879BFECB4E298,
	LeanManualRotate_get_Space_m35CB54E7091146E8B2F61CFA09F2CBB4244CEAD6,
	LeanManualRotate_set_AxisA_m1B5B2219D77A9EA4347887004DEB7BF9E7D5A9C1,
	LeanManualRotate_get_AxisA_m986E5AFCC38CFB1F0FE5127C450CAC6BED28B4BB,
	LeanManualRotate_set_AxisB_m57AACB491431E1C36FE7EF9CECBC60C51862860E,
	LeanManualRotate_get_AxisB_m296C1CBA18C3951A82152C6687F0AA29B6CAE628,
	LeanManualRotate_set_Multiplier_m7ADC6E13743035565D2DE1040D7CE4EC984B82F3,
	LeanManualRotate_get_Multiplier_mCDEACC8118D376B35F40FF17D2759AA5F90B5655,
	LeanManualRotate_set_Damping_mD3BA0C24468A695E3425CACE57BD812203FBB614,
	LeanManualRotate_get_Damping_mB2530B1922AE3CC5715D8377E7A83F9C7082B27D,
	LeanManualRotate_set_ScaleByTime_mAD22F074D3E678DEFF02DA51E61C1C05746CCD33,
	LeanManualRotate_get_ScaleByTime_mDA93E8A9BBF9E2BD61351406850A267B5FCF4480,
	LeanManualRotate_set_DefaultRotation_m7511ECD3B02B8A1B105C73056E95C265F83C3087,
	LeanManualRotate_get_DefaultRotation_m65E0FBF8FAFC19F0CF99DEB51591E803E8E11105,
	LeanManualRotate_ResetRotation_m90126CB30981A7298DA85A664644C7850E10DC8B,
	LeanManualRotate_SnapToTarget_m0F590A6B43C6D3760E4BEF4A975054D80AE2338F,
	LeanManualRotate_StopRotation_m9E83B607C247214965CD067ACFC18D71F25A75D0,
	LeanManualRotate_RotateA_m7FE6D4F994370DB14504C52D3FFC79962977E303,
	LeanManualRotate_RotateB_m265340CF9B1B21B0ECBB5050433374C2C9CB1CAA,
	LeanManualRotate_RotateAB_m063A2C24160D0102457B7003473BE24CFCEC76AB,
	LeanManualRotate_Update_m3C2959E1C2B1E8544E88386A88AF15AC2A67AC0D,
	LeanManualRotate_UpdateRotation_m75B29FAE114355304C8BBFB518BC955430848590,
	LeanManualRotate__ctor_mA03A81D7A765BFF06C0095A4F8AB0EE580FCD7CD,
	LeanManualTorque_set_Target_m5A57BDF13826FD12291E3BF202167AFE2A95B283,
	LeanManualTorque_get_Target_m91E0805B83D2373221C8CD45FC0AD5DD22FD4986,
	LeanManualTorque_set_Mode_m75758CD411F7D9716E8ACF1953C1597A0EC638A3,
	LeanManualTorque_get_Mode_m6AC5F7B03B90A1C61FFD8175FC7766A3EB4C9B5B,
	LeanManualTorque_set_Multiplier_mCD164A7F4FDA73114D9922AFCA0BF027731B867D,
	LeanManualTorque_get_Multiplier_m8E57E0D028BBBF66D6C1381CAF55E2A6A12376A4,
	LeanManualTorque_set_Space_m67D234EEF3DD2DAD5B1A184AAC63FE387EE76A3C,
	LeanManualTorque_get_Space_m517376ED0DE646776BFD55ED797E8B26513DC347,
	LeanManualTorque_set_AxisA_m82707290401C5FAA9AEB125615774299FEAA8A7F,
	LeanManualTorque_get_AxisA_m1F6C97530B7711FD15362ED660A3781F2C21155E,
	LeanManualTorque_set_AxisB_mDA4E467B47E4DD9D8F961F4A5904ABCDB9C83F39,
	LeanManualTorque_get_AxisB_m4F9A308E54F46FEB4BE239DB0AA69F0C3D5CA398,
	LeanManualTorque_AddTorqueA_m8F02F5E586AD4D448551017D695501F11C8928F9,
	LeanManualTorque_AddTorqueB_m2580DDCAA5AB26EABC9046B0E01042BC97616FDB,
	LeanManualTorque_AddTorqueAB_m7D09F21DF203205426774262D303E170949218F0,
	LeanManualTorque_AddTorqueFromTo_m23F47462D5F5EE8BAE48498605AAD9230E8B57DA,
	LeanManualTorque_AddTorque_m1A073879D238BE3E69361A3E6DEF460F892CA458,
	LeanManualTorque__ctor_m88F64175855F9D9ED7CDE602477FA0127550A552,
	LeanManualTranslate_set_Target_m2F916EE37BDC584BC198DE6ADC764991EAB79F90,
	LeanManualTranslate_get_Target_m84D456CBE905C062B2747429E3B70025F92151E9,
	LeanManualTranslate_set_Space_m61C4EDFF4FE87CC1589E0416768F28065DA1E76A,
	LeanManualTranslate_get_Space_mB77FB744CDE7CBA2D4B826A786161AEF01604CF9,
	LeanManualTranslate_set_DirectionA_m6322DB155340B030BC7DF231E9F0991530ED28C7,
	LeanManualTranslate_get_DirectionA_mF08DEC10315312E82982D87EB046DD9224B0725D,
	LeanManualTranslate_set_DirectionB_m6EE5F9203D5E877DD99B9EA269F57DB4514E0954,
	LeanManualTranslate_get_DirectionB_m2B505D1A5B66D4FBDCF9A3FA1127746F59F68897,
	LeanManualTranslate_set_Multiplier_m217828A198AA77DE588E175AD329AB9EB871400A,
	LeanManualTranslate_get_Multiplier_m52207693F3CB2AF2625FDD6D973BDA6767B5B9A2,
	LeanManualTranslate_set_Damping_mB2A587AF7267B6CB2E57A8AA84096FA6F9A8D01C,
	LeanManualTranslate_get_Damping_m32C6C1826E70F741814524B672565C6EE1819A3E,
	LeanManualTranslate_set_ScaleByTime_mA57146DBCE9A4A9C7F280CB689128848B8325422,
	LeanManualTranslate_get_ScaleByTime_m0D706B9C88994CC1A36837E641C1AEA381B12D79,
	LeanManualTranslate_set_DefaultPosition_m1898D629DC42C50DE40CEDC854AC2E9E303BEB51,
	LeanManualTranslate_get_DefaultPosition_m0374FC5830642EA47B552722F20162292C0FC0EC,
	LeanManualTranslate_ResetPosition_m96A8915E69D6DCF055E70D2C67B37035BEEE89E4,
	LeanManualTranslate_SnapToTarget_m1EC30AA794459BDEC88980D4DC737EF87868F7B8,
	LeanManualTranslate_TranslateA_m13039C5771CD72C027D18F98F65E7884D2033324,
	LeanManualTranslate_TranslateB_m7DCC6507B0104E6E5F909035403C25B09A2CFD14,
	LeanManualTranslate_TranslateAB_mF271CB96C47ECD8429EA5005DB314A71763586D9,
	LeanManualTranslate_Translate_m8944EDB703932E5D26E4950A3C3B9D625B6FB125,
	LeanManualTranslate_TranslateWorld_m808CB3A9138FB0A537F26E2270A47319E66F7DC4,
	LeanManualTranslate_Update_m962B9AF1F9354336017FBB46ED298445F815973A,
	LeanManualTranslate_UpdatePosition_m3369710F97FFBD77B2C38C1D2BA3DA21BD97C29E,
	LeanManualTranslate__ctor_m0140F5E11EA9A60DD3A7F602BCE6BF5F6ECEA230,
	LeanManualTranslateRigidbody_set_Target_m218A9F0BFCF3C79A644EEAF454CA5D670BB60A2F,
	LeanManualTranslateRigidbody_get_Target_m6DDAE1EDDF17976AC7BA5A79B2CAC979192B704D,
	LeanManualTranslateRigidbody_set_Space_m55C1D7C527B3BED2ADD76396ED15C86431ED54C4,
	LeanManualTranslateRigidbody_get_Space_mBB7B324A6E1ADB0B2AD05A2999FBDF6C4B9C6291,
	LeanManualTranslateRigidbody_set_DirectionA_mAD93A1E04AC1888655B9BA4948BA2B595F572F76,
	LeanManualTranslateRigidbody_get_DirectionA_mFBDA741F30BFC8A34910D1865DF1E3741B55F6B6,
	LeanManualTranslateRigidbody_set_DirectionB_m45FC85A6DB274DEC6261C920BE715BF9B198EC29,
	LeanManualTranslateRigidbody_get_DirectionB_m548A113BE54962FCAFF191603CBD30370EE3B816,
	LeanManualTranslateRigidbody_set_Multiplier_m53CBE2D0616E24AC98E99A8DD4BE6786FED24C4F,
	LeanManualTranslateRigidbody_get_Multiplier_m2634A34592B70D71538996A082B19B6C5685E93B,
	LeanManualTranslateRigidbody_set_Damping_mC0B94B6431B14A7FB0F1C0FB748771E418A4C0BA,
	LeanManualTranslateRigidbody_get_Damping_mB4D3181F5E3EEABEC3F6D942D979681D465801B1,
	LeanManualTranslateRigidbody_set_ScaleByTime_m3D45B29B9F964C2919154A8C4A76575A2802FB8C,
	LeanManualTranslateRigidbody_get_ScaleByTime_mEDA7CCFFFB352BB92D759D1AF2B0A5F2B3B0632C,
	LeanManualTranslateRigidbody_set_Inertia_m4BAA9526BD3E60BB8A4B7AA53C1D730D120ABCD8,
	LeanManualTranslateRigidbody_get_Inertia_m455C0BCAB8A465CBB79778091C215FA8F8DDE922,
	LeanManualTranslateRigidbody_set_ResetVelocityInUpdate_m706A76F582D94A705A6255C8CF34E2406F3A23D7,
	LeanManualTranslateRigidbody_get_ResetVelocityInUpdate_m9AD6E787F3A19CF1500790B2C83E79FFEF8A4330,
	LeanManualTranslateRigidbody_TranslateA_m2512574F1FA14245DF678C15DFE7C6F3ACFFBF8E,
	LeanManualTranslateRigidbody_TranslateB_m7D68B41187FDEE9AAB4E7394368345FF1BD53FD9,
	LeanManualTranslateRigidbody_TranslateAB_m5F8A2754F0AFEC899C9029345731D690543D59BD,
	LeanManualTranslateRigidbody_Translate_m9F58D02453E363EC583AEFBAF08B4005F258B461,
	LeanManualTranslateRigidbody_TranslateWorld_mCB5466DD39E3518796FDA874CC684CBEA12C0916,
	LeanManualTranslateRigidbody_FixedUpdate_mF97E6DF6E4AA10B3E54268291D213C4CBE2372DF,
	LeanManualTranslateRigidbody_Update_m3E5C3E997651A62B96D873B293DCD9DB8C078E0E,
	LeanManualTranslateRigidbody__ctor_m364FCAB3A8DE27BF57C0E81FAC4F6F8FD2F13D61,
	LeanManualTranslateRigidbody2D_set_Target_m730D9D452EF5737BFA834687E0570FF6370183AA,
	LeanManualTranslateRigidbody2D_get_Target_mBBC0B11F1E109DDF32EFCF2BBCAF4AA4B2288157,
	LeanManualTranslateRigidbody2D_set_Space_m45F229F681D2D8478D1C9D251F58F89AD5E6FD6E,
	LeanManualTranslateRigidbody2D_get_Space_m587138B50F3FE51CC99D00C0BBC871F0B6C1E931,
	LeanManualTranslateRigidbody2D_set_DirectionA_mE011A41C9C33F8CAE5505D9DBF90E1B1CD3BB20A,
	LeanManualTranslateRigidbody2D_get_DirectionA_m1CC1E079E216EC3437F84649E364ED5990AA102C,
	LeanManualTranslateRigidbody2D_set_DirectionB_m5F000934E92224D50AB587912D6FC7A2FE1322AC,
	LeanManualTranslateRigidbody2D_get_DirectionB_m3BF99E7A7731DEB0CB8551B8BA395B8040B82802,
	LeanManualTranslateRigidbody2D_set_Multiplier_m3E3FF627D1E797AB0432DCCCB87C647929B17AFD,
	LeanManualTranslateRigidbody2D_get_Multiplier_m501D411439661AA01F83F7514EFF995FB44BD983,
	LeanManualTranslateRigidbody2D_set_ScaleByTime_mF2D67A98DA453143A12BDF7D293D0B6F77EA863F,
	LeanManualTranslateRigidbody2D_get_ScaleByTime_m8BD472C4AB2F4E968240BCE21893086B6EDB443F,
	LeanManualTranslateRigidbody2D_set_Damping_m3EBAF02DA592756EDEBC12451F5DB8960AB15592,
	LeanManualTranslateRigidbody2D_get_Damping_m1F179EDD7D703DDF2834EF28787BB4B2FC5B9188,
	LeanManualTranslateRigidbody2D_set_ResetVelocityInUpdate_m7165E35D25D0CD20537B4624565F1AACECD6FCAA,
	LeanManualTranslateRigidbody2D_get_ResetVelocityInUpdate_mC309E0B63A050D2ED7FC2CE86813C3815329C76D,
	LeanManualTranslateRigidbody2D_TranslateA_mAEF6E40F62DB228ADFD7EDEFD87134B26C18913C,
	LeanManualTranslateRigidbody2D_TranslateB_mFC1193C30151699E8013D81A46916C0BB84A2004,
	LeanManualTranslateRigidbody2D_TranslateAB_mE02A243C95275B2C111259FCA6456DF6AFD4C409,
	LeanManualTranslateRigidbody2D_Translate_m1809104B08E6DB8F3181DEF2F9F61C2DEFCB6810,
	LeanManualTranslateRigidbody2D_TranslateWorld_m9F700BC3060485925F81EFE299A3996E97868C76,
	LeanManualTranslateRigidbody2D_FixedUpdate_m4775EBDBF15FEFB7C30D1E0A80D48B1971C7CDD7,
	LeanManualTranslateRigidbody2D_Update_m7BCF9FD87CCD38B4413962BE6AA0E7C38A6E806D,
	LeanManualTranslateRigidbody2D__ctor_m75F4A902BBDCA8BB284380FC2ED6B517371383E8,
	LeanManualVelocity_set_Target_m9616C9A93827FF8077C846944EB842314759A48C,
	LeanManualVelocity_get_Target_mB1682F8151356DCDB9F99E0D94DD1DDA01F8D0A6,
	LeanManualVelocity_set_Mode_m950EE81B3000D29D3C7571AA1E74F043D6C1F24D,
	LeanManualVelocity_get_Mode_m05EA5F80AB6C748C5055259AD8AABBCB8EE5C7F2,
	LeanManualVelocity_set_Multiplier_m66765CF3FDE98CE6852FDC636223FA6D45678984,
	LeanManualVelocity_get_Multiplier_m3AB8D4F7B46A41F209EAF66BEE471AE0DF79A5C9,
	LeanManualVelocity_set_Space_m55815DD7525CA62A59E060F567B6B176235616E5,
	LeanManualVelocity_get_Space_mE0FE0D9DF25DAAF67A4E5E0DD0980ACE440F442B,
	LeanManualVelocity_set_DirectionA_mE109EEBD3861F5A787F228B884627077E2467237,
	LeanManualVelocity_get_DirectionA_mD36DE93B21987E624F5185F2AECAC12AEE2B685C,
	LeanManualVelocity_set_DirectionB_m001A5F6C81A82B5DD1BA9000EB3B513379BCF9F1,
	LeanManualVelocity_get_DirectionB_m9DB5F6C7049829960C75E180DBB7C9A153D68820,
	LeanManualVelocity_AddForceA_m7347426E4453F010E4AB6328ADA87D41CBCF621D,
	LeanManualVelocity_AddForceB_mF6D6D889688FA1190B1B673E897768C63910899F,
	LeanManualVelocity_AddForceAB_m4C129CD80815EB660EF7B9072BE287321C6EB045,
	LeanManualVelocity_AddForceFromTo_mD60F55DF73039656F16A683046ADD668EAF1DC01,
	LeanManualVelocity_AddForce_m2CC9D3456A1A2283D806385A11F3F3970F957846,
	LeanManualVelocity__ctor_m93F5D5D80B6D15844C480031B184694D0FE071C6,
	LeanManualVelocity2D_set_Target_m9164EB12BDA1281EBD62D5A44491FC2BC9C0D962,
	LeanManualVelocity2D_get_Target_m8DE71E98FE1E17F4DFE0B1146BEE6F64350012A4,
	LeanManualVelocity2D_set_Mode_mB82D0A92CF4078811A001BA8F9B6723DFC23C7FC,
	LeanManualVelocity2D_get_Mode_mB11EA5E32EBC538F8872086B1788BEFA69B6799A,
	LeanManualVelocity2D_set_Multiplier_mFA5A2A7F3E537DCD2A3EB5E87939068EA0463D5F,
	LeanManualVelocity2D_get_Multiplier_m545368385DA811F7B9F6003E98BC568B93713B24,
	LeanManualVelocity2D_set_Space_mC54E0525FB34AEAD57C0355D0A24FCAB15EB74D8,
	LeanManualVelocity2D_get_Space_m22D8D01B38DE55DCE54D04C2CA5A3A3EBBD7DEFB,
	LeanManualVelocity2D_set_DirectionA_m058AC7628AA2AD6649D40A52C32C046C5CE26578,
	LeanManualVelocity2D_get_DirectionA_mC63F6B4330F91808E5168567EC805530FB067605,
	LeanManualVelocity2D_set_DirectionB_m7D57C449EBDB3648682379B2F02D8AA3C1B32230,
	LeanManualVelocity2D_get_DirectionB_m4B96C786733C2332A427FA893398D0B50FD6FD64,
	LeanManualVelocity2D_AddForceA_mE7A12F4B8E7E9A556F608BD440ED15372225F5CF,
	LeanManualVelocity2D_AddForceB_m3AFFD56BC70D663E9F76B8270B6EF26C8473B7BB,
	LeanManualVelocity2D_AddForceAB_m9C5A6184A65078FAA2E8BD24A30D94C6FA00BBF1,
	LeanManualVelocity2D_AddForceFromTo_mBF6D3E2C649D573A4198CCE0B3A7657F0A36488B,
	LeanManualVelocity2D_AddForce_m172B84C4BDB6E576BD4C9B80D47FACBF7052495C,
	LeanManualVelocity2D__ctor_m2F9C923A17F4A46E49F2CC7B4A7EB05E2906C268,
	LeanOrbit_set_Camera_m956A46855C8DC9BA514D7C5E8635B775380BCBDD,
	LeanOrbit_get_Camera_m50045C1C5E077213425489B123071DD8563C7BF0,
	LeanOrbit_set_Pivot_m4BA35B2A72B2F28020FA9D54F86985AECE85C509,
	LeanOrbit_get_Pivot_mA21A1CFDC991C955D29EF5025C6ACBBB62ACB444,
	LeanOrbit_set_Damping_mDFBFF1722B9478C351469153448D16FED53DAF6C,
	LeanOrbit_get_Damping_mB1E90BF7D8FE0458FF1DCC55C0B192249A3B3502,
	LeanOrbit_set_PitchSensitivity_m047727B39E72180C2F8AD11F39CCD6D8281FCF15,
	LeanOrbit_get_PitchSensitivity_m85A25BAF7E409304337DFA1612352E4F5784F5BE,
	LeanOrbit_set_YawSensitivity_m653A7AEB2C6CA4EF907F6110498DB268BB7B51A7,
	LeanOrbit_get_YawSensitivity_m5C3E5D41AE5AA6A41ADA75C02BBE6228EBCEDC3E,
	LeanOrbit_Rotate_m2FA2639AFCC15F253E6A74A64BA814E747711EB5,
	LeanOrbit_GetPivotPoint_m03838A756586FDFA4423C0FF636F3D5A12E69CD1,
	LeanOrbit_RotatePitch_mC41237B5223E0C801A7E97DAC31356FC8BA1F320,
	LeanOrbit_RotateYaw_m02E596B3FD795103D28F295F1DEDB9F3E3408A2C,
	LeanOrbit_LateUpdate_m71C703AC971AC66455B5BAC1A398DB091BFBB43F,
	LeanOrbit_GetSensitivity_m4DD35C087B5EEE625914A4F556E1723CD47B302A,
	LeanOrbit__ctor_m335E105B465C1D7843F2977C60AE53847678E217,
	LeanPitchYaw_set_Camera_mBCF1DC0B1B0DC607EDF3C43659F75CDB6D4B696E,
	LeanPitchYaw_get_Camera_m2D04E9AF6B9A41325FC783250107A4F8CAD16D5A,
	LeanPitchYaw_set_Damping_m616B535A24B325F648E34DC5A19BA8BB1DE6427E,
	LeanPitchYaw_get_Damping_m0491D89476912EEDAD195738820AF118F8919C3B,
	LeanPitchYaw_set_DefaultRotation_m7A433AB1CB4A3AD8D23255C75BEB551C9F890893,
	LeanPitchYaw_get_DefaultRotation_m7741933BAF634474669E044AE215771682358889,
	LeanPitchYaw_set_Pitch_m559AFC2CF0E80CA6BE276922DF7A3E985A000D3D,
	LeanPitchYaw_get_Pitch_mA3480A01D5AD307DFB2ED4E93F291875264B6085,
	LeanPitchYaw_set_PitchSensitivity_m396ABC5ED03DD35F3E5E7668D1A7DCB1B9846191,
	LeanPitchYaw_get_PitchSensitivity_m601639AFEEA8C86D630660EF1417C1E9D4C97D0C,
	LeanPitchYaw_set_PitchClamp_mBB942C54337E04815BFA9E953DC39828EB7195D2,
	LeanPitchYaw_get_PitchClamp_m0B99CE48FA617A0FD3C7F073A6FF2A8E0931470B,
	LeanPitchYaw_set_PitchMin_m30339C0DA52EEA6C50B32BC646A30993FE3AAB46,
	LeanPitchYaw_get_PitchMin_m3D670300D8B526D969EA08252D515227DF09776C,
	LeanPitchYaw_set_PitchMax_m0452A09AE83E3A3A1E1894519D68C0D0AE975C51,
	LeanPitchYaw_get_PitchMax_m28FCB3A30167B472A4B539FF3E81CEF6F5927F01,
	LeanPitchYaw_set_Yaw_mBCEF0AE1F4C9B6F1F4C29B5437AAF21EC4E3FCE9,
	LeanPitchYaw_get_Yaw_mD82CE9D89675A93F359DD6298E8E1A3714F1BC88,
	LeanPitchYaw_set_YawSensitivity_mA1348DB280413A62CCEC9DB2FF469CEFC3586A14,
	LeanPitchYaw_get_YawSensitivity_m15D1AA40E73E81654999C0E4C2DC834245DEF76F,
	LeanPitchYaw_set_YawClamp_mA18A61712CEC278F428B18E22C4B77A5BBACBDDE,
	LeanPitchYaw_get_YawClamp_m8E4A95685F591B77C8962F1969CEACFA5D71FC8B,
	LeanPitchYaw_set_YawMin_mC8E7980AE074D18885657B2FF0A0D01ABBE6808B,
	LeanPitchYaw_get_YawMin_m361774128B4F6CE5905EC3DA63F08336F28A73F1,
	LeanPitchYaw_set_YawMax_m7029B2BFF66C2A4DE5296914BD216BD1EA94A3BE,
	LeanPitchYaw_get_YawMax_mF7BE9E5C5C7B5AFD0550636C45D10EA2DA9AEA7C,
	LeanPitchYaw_ResetRotation_m223BD5FE2B0466FEB5871EBC174A3075CF872B25,
	LeanPitchYaw_RotateToPosition_m20E7020FA3358DB93961F9D393EFC3F24D44500C,
	LeanPitchYaw_RotateToDirection_mE317A4988EE00F3FDBA70F537DFB5139EF669F78,
	LeanPitchYaw_SetPitch_m09112DF07D0F19309C9124529EBF10F022296E1F,
	LeanPitchYaw_SetYaw_mFE08E8FA472751B81CA1F22DE6FE998665379366,
	LeanPitchYaw_RotateToScreenPosition_m74B9E59E5961B90FC99011A71E6CAE4FF648FA24,
	LeanPitchYaw_Rotate_m324D4AE00A54B15BEBE9BD5E7E3B4B9F25CAA0B7,
	LeanPitchYaw_RotatePitch_m8F367D587778317286F92D3A55CF62C0BCBEB4AE,
	LeanPitchYaw_RotateYaw_m779455632CC8618E44A907EBA907361B70F74968,
	LeanPitchYaw_Start_mDFE0D9052CF7E91EFEF888F04370345B373041F7,
	LeanPitchYaw_LateUpdate_m96B8971B4F1744829F315D7694B87DF372CD4419,
	LeanPitchYaw_GetSensitivity_mEC5D1A9E365D0A3426657093799581CC802B8F11,
	LeanPitchYaw__ctor_mEDF7FB4740D576FDFCD0C41884856A9DCB3A0492,
	LeanPitchYawAutoRotate_set_Delay_mE6CD675696F15D676FCEFB6242F4B1EAEC31096C,
	LeanPitchYawAutoRotate_get_Delay_m97BB87BD566AC75054D91AF2FCDDB9DF4C852EF2,
	LeanPitchYawAutoRotate_set_Speed_m42B5D40CB4352768E838D7EC9E5286F13FC5CBEC,
	LeanPitchYawAutoRotate_get_Speed_mC9533601205B78754532AD087EDE057F99749DE5,
	LeanPitchYawAutoRotate_set_Acceleration_mCE51F179F8C6239F89963014046AB39A0A941C46,
	LeanPitchYawAutoRotate_get_Acceleration_mA287A8102B273CAA04E5C46AF09FBB65A704D560,
	LeanPitchYawAutoRotate_OnEnable_m52E78CD0E9EEFF2ABC6FB39CDE601E066DA423A9,
	LeanPitchYawAutoRotate_LateUpdate_mFF5BAEA5D4021C1A6DABA404E3E0FBDEBFF8332C,
	LeanPitchYawAutoRotate__ctor_mBE3D9012E382F13039208C8DBA7857339B7D25F9,
	LeanRandomEvents_get_Events_m3A4D10A61482B329FFE378AB9792168FBAE2ADF0,
	LeanRandomEvents_Invoke_mCEA3D4832552ACC6AAF1895C8DADF995E45CF735,
	LeanRandomEvents__ctor_mAE9CECE65269AEBD271ED31A0C8D615DB241468A,
	LeanRemapValue_set_OldMin_m675349685B45D7F25F67981EA3107DFB25D0E1C4,
	LeanRemapValue_get_OldMin_mE4E93BB8BB2B3611199385098B1619D92B263FB9,
	LeanRemapValue_set_OldMax_mE5407E53BF9D37B1385346821052434EA12E2558,
	LeanRemapValue_get_OldMax_mF5343F42B8AC1B4C1A32C48675B36BD51407DB74,
	LeanRemapValue_set_NewMin_m7C45F32102AE9DC15AE2ED443D72789B347D7396,
	LeanRemapValue_get_NewMin_mF07798FDEF1E234A9A8E0721942E227EDCEB11E8,
	LeanRemapValue_set_NewMax_mEDB3F3B7E2F2884CB2057995E8EDB6F801654630,
	LeanRemapValue_get_NewMax_m83D119C691DDE9BDD55F9BD1516FFEE90302CCE1,
	LeanRemapValue_get_OnValueX_mA43B948F478BEEF11243C31581C892247018157C,
	LeanRemapValue_get_OnValueY_mCFCAFD48C3FD4452BB4B64E2D64D4EC2D4DDFC23,
	LeanRemapValue_get_OnValueZ_m41D14BF21C009E354E654B43CC50D3E9951FC608,
	LeanRemapValue_get_OnValueXY_m6ACDE425CFC234686E26BAABFEEC54A945C8D7E7,
	LeanRemapValue_get_OnValueXYZ_m5397A7AFB331F96650077E02FA1CD97D28C5BC8E,
	LeanRemapValue_SetX_m4A6F465E54CA0950477B3A7C07D663C3CAC90549,
	LeanRemapValue_SetY_m2E23F1D67B68416E61148553DECD061F828A845A,
	LeanRemapValue_SetZ_m88FEDFE3391812EAF1FB521A3A5DDB0800984A82,
	LeanRemapValue_SetXY_m89ED281338799D8233DD890331094E393C94D35C,
	LeanRemapValue_SetXYZ_mA5213BA36FCB642F14E0B57426804BCFB2F5E501,
	LeanRemapValue_Remap_mD38D8C5DBAB85CB6E1B682E83F80AEDF625ED289,
	LeanRemapValue__ctor_m650A0C915EEB24200B8284FA3476B3FA97A6817A,
	FloatEvent__ctor_mC91E22BE3CDE75DBF7005ACF481498240224EA2A,
	Vector2Event__ctor_m72C868E0964FB1F4469FA17AD5BDDB3D13B73B2E,
	Vector3Event__ctor_m5D5712391DAD9447C2CD56255674557A243F576D,
	LeanRevertTransform_set_Damping_m8C3EA1F89005C2104F9B87AB2B26D2DB577B5D32,
	LeanRevertTransform_get_Damping_m5A7CBA3B1ACA2FBD5A55542D4CCF22290D3D7882,
	LeanRevertTransform_set_RecordOnStart_m1E218E92B422C85C84423FD459A8C6D6CAEF8BE4,
	LeanRevertTransform_get_RecordOnStart_mE644AB9D67E0C1D97FD98E4F614406F9B06F5609,
	LeanRevertTransform_set_RevertPosition_mA70439B8C2793191F577D4E9471B432D9D9DBE11,
	LeanRevertTransform_get_RevertPosition_mF0883C9705BFE64ABC322020ABD39BFACDB49362,
	LeanRevertTransform_set_RevertRotation_m653759CBD32090356AD69F2DCA77A30FE1A7B187,
	LeanRevertTransform_get_RevertRotation_mA46513EE4BE53C259BE9410BF6988F912540204A,
	LeanRevertTransform_set_RevertScale_m05D355998343CB24B0DE9576165FD97A85C4B3DD,
	LeanRevertTransform_get_RevertScale_mF816AC88BDAF11C8BAC0FA3F0F52658F44D51219,
	LeanRevertTransform_set_ThresholdPosition_m48218ED8B95A66E41F0D2E0A5D80C451A596D579,
	LeanRevertTransform_get_ThresholdPosition_mBB003057B342A26C88EF3C6649BC4A37B79103BB,
	LeanRevertTransform_set_ThresholdRotation_m8EF291EC90837AF64A8AAE5D99D8BCF3E19988D4,
	LeanRevertTransform_get_ThresholdRotation_m409E07CDDCBA8E90431ABA68C5AD40485960646E,
	LeanRevertTransform_set_ThresholdScale_m517764D780D547491F483E6E4D1421DD23883350,
	LeanRevertTransform_get_ThresholdScale_m95465595DD16800E6AE16C73E7DFAD8436050F15,
	LeanRevertTransform_set_TargetPosition_mF28BD735538FFC080F3E09DB3E230C7856200C73,
	LeanRevertTransform_get_TargetPosition_m450B504DBADF03D931EFA76C4B39DC85693CF4A5,
	LeanRevertTransform_set_TargetRotation_mB664BA5811BEAF9AB836E28944C1A2F52B644E51,
	LeanRevertTransform_get_TargetRotation_mAE166838D525AF44F52F9997A99A9AAE8282B9C2,
	LeanRevertTransform_set_TargetScale_mF98F910BB320597AA0A06D03206BA883806353C6,
	LeanRevertTransform_get_TargetScale_m8935285C1A6BCA57061DA62165935E1EDA648FDA,
	LeanRevertTransform_Start_mC4F2E59C04E59B534855C1514304FFA53AC83D0F,
	LeanRevertTransform_Revert_m5348B2D6D10C01686894612A2DEBA6DE904E53B3,
	LeanRevertTransform_StopRevert_mE0BF7A16895C20C0F4657E03A90EC26975250020,
	LeanRevertTransform_RecordTransform_mDEA71175D5321AFBA577541B88E38284D039D961,
	LeanRevertTransform_Update_mA4B8F53D8BB2576900890C3F8B8C00512C569ECE,
	LeanRevertTransform_ReachedTarget_m6FE4FF0FB80AC2BA7D9149BA5B3C05CECE98C459,
	LeanRevertTransform__ctor_m9E5598D8272FBA11BF1091C94A464DDEBE4F7F66,
	LeanRotateToPosition_set_Target_mB8F52EFC148ABEECEC788403E90CD98B85153909,
	LeanRotateToPosition_get_Target_m2C718ADCAD75F46B9D204BD34534E4B861A635FE,
	LeanRotateToPosition_set_Position_m68A49883B15A790FA1600942B3CB3FDB09839318,
	LeanRotateToPosition_get_Position_m3D6ACE57F65B1B2F941BFA95860DBFE88360E411,
	LeanRotateToPosition_set_Threshold_m0A35348914E9E7C6A30E0A33CA93A05976BEA6D5,
	LeanRotateToPosition_get_Threshold_m70E6259F79EE4D2449E1D56F3E891E9C09B814F7,
	LeanRotateToPosition_set_Invert_m14153BC577091FE6BE5437F67AA654E1DD71A6C5,
	LeanRotateToPosition_get_Invert_m54A788B84992D3B0234ED88F0A3E34ACF3E6BEDF,
	LeanRotateToPosition_set_RotateTo_m0DF6465B91AB57AEEE285D22E5E621025AC1F4D1,
	LeanRotateToPosition_get_RotateTo_m8FB0E464EC99429CD4892E35D848ACC7531630D7,
	LeanRotateToPosition_set_Damping_m486F2BC018708E3EEBD37F04566BEA52F6A8667C,
	LeanRotateToPosition_get_Damping_m03FD0D5E56679F66699AA2B60464B9D18FB4E95F,
	LeanRotateToPosition_get_FinalTransform_m7420192F159D19CBEB8213970F1121CB09211D7E,
	LeanRotateToPosition_SetPosition_mC884823A7CAC867D6666352CAAE4D4DD5ADC0028,
	LeanRotateToPosition_SetDelta_mECBA4518FEED32078BB36C5F540F01C6F65C5477,
	LeanRotateToPosition_ResetPosition_mB77AF404A4D534855D3D8E0A34298E86F054DCCC,
	LeanRotateToPosition_Start_mF771390E07B4CCAFF4C5599F4C963EB2CBE819D3,
	LeanRotateToPosition_OnEnable_mFDC739E9AC8C260DA8A9F1BA4F1162E8372CC1E0,
	LeanRotateToPosition_LateUpdate_mB81BFE2F5A087457636F9A1138DE5B10B7E3852A,
	LeanRotateToPosition_UpdateRotation_mC9F3CBCA1090C798889AC9E1204F33E389C3E9D5,
	LeanRotateToPosition__ctor_mCF6BBD3EBFDD0D5EEC49258365D579231880DB2E,
	LeanRotateToRigidbody2D_set_Damping_mA6F4B728F76FC7262D785042AFBEBEAF84733435,
	LeanRotateToRigidbody2D_get_Damping_m21449CC1F72D6D8041C78D25A006063B2FE14341,
	LeanRotateToRigidbody2D_OnEnable_mF45807B5E515D7DCF086048DF44BD4B62219F4E6,
	LeanRotateToRigidbody2D_Start_mC375F96C658D73A652CB03B4EB7FD55A4976ECDF,
	LeanRotateToRigidbody2D_LateUpdate_m8020AEDE5A7B67378BBEB993841FFA34834B4085,
	LeanRotateToRigidbody2D__ctor_m8C3D4E24445C6A5BE023F6E9036EF2FB15AFF72E,
	LeanSelected_get_OnSelectable_mE9E162DF96F4F0D8E489027A8B2E3C7AED8D6306,
	LeanSelected_OnEnable_m013397E14A655147E3A06A0A68FEBDA8CA3AD44E,
	LeanSelected_OnDisable_m7122C0170245CB360EE9AA82A3F5540853BC6ABA,
	LeanSelected_HandleSelectGlobal_m7B72DBBDB11E25BE5C91E365A5F3F130C981EF18,
	LeanSelected__ctor_mA87196128123CCD4DA9DCD812E41559D540CBAE8,
	LeanSelectableEvent__ctor_mC3F52B3C581033AC509F7AA63DC7ECF3B90265EA,
	LeanSelectedCount_get_OnCount_mCB537DD097383D04155D56EE1D44AEF1A53FF526,
	LeanSelectedCount_set_MatchMin_m26C85CF8F47A94150A3CAE614AD654AB46E27A2F,
	LeanSelectedCount_get_MatchMin_mEB87EAE39F69027DC3B6B4D54E3C34A63490C2D4,
	LeanSelectedCount_set_MatchMax_mE027EE5307BEFC90DEC896D47E1223A4D1968C35,
	LeanSelectedCount_get_MatchMax_mB1E396CD95B3F0C8A206864622CB124A4A49FE20,
	LeanSelectedCount_get_OnMatch_m28614F27E2E4D8F3CAD9FB7B6DFAC96C3B569DEA,
	LeanSelectedCount_get_OnUnmatch_m6FB9BB918C126AD2C8B3CB03DB2D764E676254E4,
	LeanSelectedCount_set_Matched_m1DBA98C84269A263C710A7BB3A62DD5ACDDFDC41,
	LeanSelectedCount_get_Matched_m401E07556AE8EA9C6092BB2D9AD8E88702271674,
	LeanSelectedCount_OnEnable_m6995BE07B6EB80E52841A130AF6FF7983605243C,
	LeanSelectedCount_OnDisable_m9E879E59A573D0DBCAE33BCB0F5D126A0746DEED,
	LeanSelectedCount_HandleAnyA_mE79D8A11D988DBFE35A363C68FDF15BDE44DDCAA,
	LeanSelectedCount_HandleAnyB_m3B8ACAD57DC33B22E82CCC557C5B2E8AB06DF48C,
	LeanSelectedCount_SetMatched_mA27A118C35D6CB496FB2280D0F3D1060B726AD1E,
	LeanSelectedCount_UpdateState_mF2F74FFD4DFC2FC7A6B53B1478DBD21ECAAEE42D,
	LeanSelectedCount__ctor_mA1957F796D5816CF8824755B26A8E50BCCD0983F,
	IntEvent__ctor_mF734AF9D44691E28CCA10926AAE13AAD40CD7130,
	LeanSelectedRatio_set_Inverse_m0DB89FEF720136D8F175EF392A6326F5EF46973A,
	LeanSelectedRatio_get_Inverse_mC0105A09549922643199FB45B30EBC0534568C54,
	LeanSelectedRatio_get_OnRatio_m8AF316C32422A606EBE46EB9BBD07B84E4BBE7D8,
	LeanSelectedRatio_UpdateNow_m6CC9B5812A2A7DFC348E8D01A4FB1C1AA5EE53EE,
	LeanSelectedRatio_Update_mF042118F939E34268AE3030ACE7A27F0794D7E66,
	LeanSelectedRatio__ctor_m7195F8E4C9261520CFCA5F072292FFF5F2986178,
	FloatEvent__ctor_m0FA5660715550455FA4A1C77CA26F9DC3DC67EF6,
	LeanSmoothedValue_set_Damping_m6F0D19865B0FFDACDA78ED72B274BC8F4298C622,
	LeanSmoothedValue_get_Damping_m222F3903B0579246329D5E5A2840A796CF7FF981,
	LeanSmoothedValue_set_Threshold_m07C32333FACE027C1AE931F28A94E3E3A1BC7B0D,
	LeanSmoothedValue_get_Threshold_m3DD7954ABA57E8C9972C0214C9568189517C6513,
	LeanSmoothedValue_set_AutoStop_m9BF8776DAB4D7F2D2EB6F38FAA832139CB50E410,
	LeanSmoothedValue_get_AutoStop_mA450F101A84977F0B7EB12EF8C472295E8B0CC82,
	LeanSmoothedValue_get_OnValueX_mBC14FF895C015A63BDD052D0B05ED7EAAC302D0D,
	LeanSmoothedValue_get_OnValueY_m992ACBB54FADABFFA22FA34F690736FF69C0D362,
	LeanSmoothedValue_get_OnValueZ_mE8FF2F4A1F66BBA36AF609688B04038C10C38AF5,
	LeanSmoothedValue_get_OnValueXY_m37B1CCB55F4701B31A9845FDFF3AC51A3665D5C2,
	LeanSmoothedValue_get_OnValueXYZ_m235DC638A1D0188CA74A5EF5F5DD0D60B3F0C912,
	LeanSmoothedValue_SetX_mF5D610310A84BAEB13E0DAED78CE02CCAB174B5B,
	LeanSmoothedValue_SetY_mCCC3E2601DBF8AE786429AAB320E3B6000905C02,
	LeanSmoothedValue_SetZ_mF9195EFA8E62D38CE0820D22255B24AD12ABAF2C,
	LeanSmoothedValue_SetXY_mEEE26263311471FF70D2FFC6F4F457FFA29DCB01,
	LeanSmoothedValue_SetXYZ_m5363ADA1D179E68E33728B5AD70FF6B94F5DDFC1,
	LeanSmoothedValue_SnapToTarget_mAAD719FFB73FA11E07C3FC06E60D88F5F9170C07,
	LeanSmoothedValue_Stop_m198D507C40ADC95949ADA95209F08BBB464DDC4B,
	LeanSmoothedValue_Update_m637FFDEBD4BB0C8BBC3A111E7A42462FD6373DC2,
	LeanSmoothedValue_Submit_m0DFA40F54DC060B33FF8A47840B5C389B26C4E6D,
	LeanSmoothedValue__ctor_mF40384D1D7A305BBAEFF7C1B7EF87F2F5248CCB5,
	FloatEvent__ctor_mF11915206747896B326EEBB3756CF8CED8437FFC,
	Vector2Event__ctor_mF5A585C45F610ACE1379BDA5357DFCBA62417C46,
	Vector3Event__ctor_mCEE379DF2753E85D0EDA8FDF1DAC061E15D21F28,
	LeanSpawnBetween_set_Prefab_m07F3F38240551EC84BCE5CE7A786C61687B60D51,
	LeanSpawnBetween_get_Prefab_mD581E6DAB0EF6E26296DB4D7C204C8A503322845,
	LeanSpawnBetween_set_VelocityMultiplier_m0AD8E0E41ECF39FBDB89C301544471C0AF8235F8,
	LeanSpawnBetween_get_VelocityMultiplier_m487C2FD6233E29A9BF5ACB84D3715F68146DEB80,
	LeanSpawnBetween_set_VelocityMin_m3B5B0E20A3679795203EDF250155AC4C9CDE8928,
	LeanSpawnBetween_get_VelocityMin_m389AFEFF8E79EFF59C6C31AD05AC1773C6CFD630,
	LeanSpawnBetween_set_VelocityMax_m04F26FC0696AB17263235DD3A59DE54BB15FF7E9,
	LeanSpawnBetween_get_VelocityMax_mEB9689F6ABCC85376C640F32363A97F399442DD7,
	LeanSpawnBetween_Spawn_m8FA11433B8B6F641D8DB64606571DA17FE54B778,
	LeanSpawnBetween__ctor_m8D925BC4FFD405A54D30AFF4F686102B21A1ADE1,
	LeanSwap_set_Index_m8B04E79C9D1DADD290E6693BE9BA7C0795F54DC8,
	LeanSwap_get_Index_m6B11D857DE3B173121F4CEBAF2C7E029FC7D2714,
	LeanSwap_get_Prefabs_mAFB41E2531F8487FEB09212860B90259E2DEBF41,
	LeanSwap_UpdateSwap_m3DB8B10CDCF43394B088F0C3AAA33CAE08F1C3BE,
	LeanSwap_SwapTo_mF62DD99A4E07F03269DC80D64B96AC746E6BB192,
	LeanSwap_SwapToPrevious_mB1DB144F6390035AF0AF874F8C5C810FFF496F2B,
	LeanSwap_SwapToNext_m1476858973D0B0AAE9F539A24E0F0CD036367F6A,
	LeanSwap_GetPrefab_m5ADC15A171025D8DDB50C1AF98D7B414355EEA74,
	LeanSwap__ctor_m4C0EE8FC7A5EB43B8846F2FFB1C73A9E8838EE64,
	LeanThresholdDelta_get_OnDeltaX_m47D4D2C7A3D7DC5ABBD91AF09F26BDA7B3995063,
	LeanThresholdDelta_get_OnDeltaY_m848FC1DA603C7D62B924F6CCC3FD3A7AC2526F66,
	LeanThresholdDelta_get_OnDeltaZ_m55A11F5DD82943238F8244D89B8D131508A3B71C,
	LeanThresholdDelta_get_OnDeltaXY_mAB8B927E4C5B52CC3D3FEC7586D1BD745FE19C80,
	LeanThresholdDelta_get_OnDeltaXYZ_m264B455D2F00C95B123265D3E7B2CF2D85CAFAF1,
	LeanThresholdDelta_AddXY_mDAA245C45038E138E55F51EB4201AE690E327C1C,
	LeanThresholdDelta_AddXYZ_mCD0492C9C331B73D8095FC28D42F674515D99E8D,
	LeanThresholdDelta_AddX_mF90E8E8229F858B01F3B9CF8B8247BF66F4D0FD7,
	LeanThresholdDelta_AddY_mD0A621F7E1AE6DD496BEB57BCD96B9523A339C79,
	LeanThresholdDelta_AddZ_mA7103E6525461C3AC7ACA9459B2979F9BAD285D7,
	LeanThresholdDelta_Update_m87B4CB842A467E1B5F4F8A07040B10CB6ABCEBD7,
	LeanThresholdDelta__ctor_m6FB551D8D5C0B219CCE6A188425683E23C27E44A,
	FloatEvent__ctor_m9F3D19263E9C7A4AC2A65C8ADC7B628439C1FB7C,
	Vector2Event__ctor_mCEE16563E24441051C7A2725FE120AE959F9F586,
	Vector3Event__ctor_m1ADFEB168C21214373EE755DCA8AE831F6A913AB,
	LeanThresholdPosition_get_OnPositionX_m6E4B8029D852580887605E7912C3A97272EE3630,
	LeanThresholdPosition_get_OnPositionY_m302793C43B29DCCDD1D5AEE8A05CB0EC50977A24,
	LeanThresholdPosition_get_OnPositionZ_mDD2FC2DBF7022C9F9847A3E5B0D761B75476B84D,
	LeanThresholdPosition_get_OnPositionXY_mB674C3F5580E30331E7629A8D407D541577AE3C9,
	LeanThresholdPosition_get_OnPositionXYZ_m20AA2818C62D818A030794880752A7AA3DF26C6B,
	LeanThresholdPosition_AddXY_mE6E132CB5D6E2621C46BF203F3E494E525EAABD9,
	LeanThresholdPosition_AddXYZ_m06E90611854FA44BB9D5B112F0F32A376FE6998D,
	LeanThresholdPosition_AddX_mDF38D46966AAAFE18E764233D8910B9006C25D2E,
	LeanThresholdPosition_AddY_mE35D1A1430A38C7DE8576F9C744CD3373BEFA245,
	LeanThresholdPosition_AddZ_mE06B76B4B3E59F300589FCBFE12F44CE7AE34B50,
	LeanThresholdPosition_SetXY_m8A6D0EE079DAEB98A63959BFB49CE8B48825D181,
	LeanThresholdPosition_SetXYZ_m40BCE26E08C1D6B0CABC2ABA4AD456EDC250AB42,
	LeanThresholdPosition_Update_mC3557A8858BF4FED1E4023035B7F43670E23D8D1,
	LeanThresholdPosition__ctor_m741FA9415666E10A9945CC4FB319370565817FCC,
	FloatEvent__ctor_mC81BBA6160F9865C6F893F87C6BA1D58E4D566A5,
	Vector2Event__ctor_mA66C3E7F65543CEA3D53270FC27F95C7FDD5F0AE,
	Vector3Event__ctor_mDA566D599413F7136C8CD5DBDE8B6B73E82C74DB,
	LeanValue_get_OnValueX_mE28F301B04490B1023068DD3D79C56859EC8F9D4,
	LeanValue_get_OnValueY_mAF0CCD8F538C9B953624FF6C1AC4DBF6324738A1,
	LeanValue_get_OnValueZ_mE2F0DF9E95590563861EBEB5DC725467E5425879,
	LeanValue_get_OnValueXY_m9720AA0EE57EDB8BFCC176C4AC09D2CDF388C8E9,
	LeanValue_get_OnValueXYZ_m7C50A607575FB47CC305C438CEEAC3C667145E2A,
	LeanValue_set_Current_m76CBF87E069DDC81A6ABD35D20F0AA6A28AA22F3,
	LeanValue_get_Current_m801BD6A74D70F36CEC54FB4B0911FB654422BA3C,
	LeanValue_SetX_mD8358A218B3508400D264AD51C0A512DCD884F66,
	LeanValue_SetY_m66815FD614F9B0C86ECEA353BFB7FC18CFA16FCF,
	LeanValue_SetZ_mC00277EC365C88D5045A8A30BB7554175785F0E0,
	LeanValue_SetXY_mFC49FA04029D7E5683F02C71E67D3B50C8994A97,
	LeanValue_SetXYZ_mD284769B0C82F6316AC481336A9AA01522FD493F,
	LeanValue_Submit_mC4CFFC6C9561C63E16EAE0FCAD1A3F4360697656,
	LeanValue__ctor_m6CFE4F4EE37214FAC042CA1CB98D16EDC43FA70C,
	FloatEvent__ctor_m49A01AC6A1067C84C694C5C2249BCC64E842E93A,
	Vector2Event__ctor_m2C0150BD087FD407F60713DE58C5A114DE2C079A,
	Vector3Event__ctor_m3B7AC0DAFFC7A08A5E44A94D77FB3CD9C72BD07F,
};
static const int32_t s_InvokerIndices[650] = 
{
	3191,
	3775,
	3220,
	3812,
	3220,
	3812,
	3856,
	3191,
	3856,
	3238,
	3828,
	3238,
	3828,
	3238,
	3828,
	3238,
	3828,
	3856,
	3856,
	3856,
	3856,
	3191,
	3775,
	3775,
	3856,
	3856,
	3856,
	3191,
	1986,
	3856,
	3856,
	3191,
	3775,
	3263,
	3851,
	3263,
	3851,
	3263,
	3851,
	3238,
	3828,
	3238,
	3828,
	3220,
	3812,
	3220,
	3812,
	3220,
	3812,
	3263,
	3856,
	3856,
	3856,
	2018,
	3856,
	3263,
	3856,
	2018,
	3856,
	3856,
	3172,
	3756,
	3263,
	3856,
	2018,
	3856,
	3856,
	3220,
	3812,
	3238,
	3828,
	3238,
	3828,
	3220,
	3812,
	3238,
	3828,
	3238,
	3828,
	3220,
	3812,
	3238,
	3828,
	3238,
	3828,
	3856,
	2714,
	3856,
	3220,
	3812,
	3263,
	3851,
	3220,
	3812,
	3263,
	3851,
	3856,
	3856,
	3191,
	3775,
	3172,
	3756,
	3238,
	3828,
	3238,
	3828,
	3856,
	3856,
	3191,
	3775,
	3263,
	3851,
	3263,
	3851,
	3856,
	3856,
	3856,
	3191,
	3775,
	3856,
	3856,
	3775,
	3856,
	3856,
	3263,
	3851,
	3263,
	3851,
	3191,
	3775,
	3238,
	3828,
	3238,
	3828,
	3856,
	3856,
	3191,
	3775,
	3191,
	3775,
	3856,
	3856,
	3238,
	3828,
	3220,
	3812,
	3775,
	3775,
	3775,
	3775,
	3775,
	3238,
	3238,
	3238,
	3261,
	3263,
	3856,
	3856,
	3263,
	3856,
	3856,
	3856,
	3856,
	3775,
	3856,
	3856,
	1986,
	3856,
	3856,
	3238,
	3828,
	3238,
	3828,
	3775,
	3856,
	3856,
	3263,
	3856,
	3856,
	3856,
	3263,
	3851,
	3172,
	3756,
	3238,
	3828,
	3238,
	3828,
	3220,
	3812,
	3238,
	3828,
	3238,
	3828,
	3181,
	3764,
	3238,
	3828,
	3238,
	3238,
	3856,
	3856,
	3856,
	3191,
	3775,
	3263,
	3851,
	3263,
	3851,
	3238,
	3828,
	3238,
	3828,
	3220,
	3812,
	3263,
	3851,
	3856,
	3856,
	3238,
	3238,
	3261,
	3263,
	3856,
	3238,
	3856,
	3191,
	3775,
	3172,
	3756,
	3263,
	3851,
	3263,
	3851,
	3238,
	3828,
	3238,
	3828,
	3220,
	3812,
	3263,
	3851,
	3856,
	3856,
	3856,
	3238,
	3238,
	3261,
	3856,
	3238,
	3856,
	3191,
	3775,
	3172,
	3756,
	3238,
	3828,
	3172,
	3756,
	3263,
	3851,
	3263,
	3851,
	3238,
	3238,
	3261,
	2032,
	3263,
	3856,
	3191,
	3775,
	3172,
	3756,
	3263,
	3851,
	3263,
	3851,
	3238,
	3828,
	3238,
	3828,
	3220,
	3812,
	3263,
	3851,
	3856,
	3856,
	3238,
	3238,
	3261,
	3263,
	3263,
	3856,
	3238,
	3856,
	3191,
	3775,
	3172,
	3756,
	3263,
	3851,
	3263,
	3851,
	3238,
	3828,
	3238,
	3828,
	3220,
	3812,
	3238,
	3828,
	3220,
	3812,
	3238,
	3238,
	3261,
	3263,
	3263,
	3856,
	3856,
	3856,
	3191,
	3775,
	3172,
	3756,
	3263,
	3851,
	3263,
	3851,
	3238,
	3828,
	3220,
	3812,
	3238,
	3828,
	3220,
	3812,
	3238,
	3238,
	3261,
	3263,
	3263,
	3856,
	3856,
	3856,
	3191,
	3775,
	3172,
	3756,
	3238,
	3828,
	3172,
	3756,
	3261,
	3849,
	3261,
	3849,
	3238,
	3238,
	3261,
	2032,
	3263,
	3856,
	3191,
	3775,
	3172,
	3756,
	3238,
	3828,
	3172,
	3756,
	3261,
	3849,
	3261,
	3849,
	3238,
	3238,
	3261,
	2032,
	3263,
	3856,
	3191,
	3775,
	3191,
	3775,
	3238,
	3828,
	3238,
	3828,
	3238,
	3828,
	3261,
	3851,
	3238,
	3238,
	3856,
	3828,
	3856,
	3191,
	3775,
	3238,
	3828,
	3261,
	3849,
	3238,
	3828,
	3238,
	3828,
	3220,
	3812,
	3238,
	3828,
	3238,
	3828,
	3238,
	3828,
	3238,
	3828,
	3220,
	3812,
	3238,
	3828,
	3238,
	3828,
	3856,
	3263,
	3263,
	3238,
	3238,
	3261,
	3261,
	3238,
	3238,
	3856,
	3856,
	3828,
	3856,
	3238,
	3828,
	3238,
	3828,
	3238,
	3828,
	3856,
	3856,
	3856,
	3775,
	3856,
	3856,
	3263,
	3851,
	3263,
	3851,
	3263,
	3851,
	3263,
	3851,
	3775,
	3775,
	3775,
	3775,
	3775,
	3238,
	3238,
	3238,
	3261,
	3263,
	3263,
	3856,
	3856,
	3856,
	3856,
	3238,
	3828,
	3220,
	3812,
	3220,
	3812,
	3220,
	3812,
	3220,
	3812,
	3238,
	3828,
	3238,
	3828,
	3238,
	3828,
	3263,
	3851,
	3204,
	3790,
	3263,
	3851,
	3856,
	3856,
	3856,
	3856,
	3856,
	3812,
	3856,
	3191,
	3775,
	3172,
	3756,
	3238,
	3828,
	3220,
	3812,
	3172,
	3756,
	3238,
	3828,
	3775,
	3263,
	3263,
	3856,
	3856,
	3856,
	3856,
	1997,
	3856,
	3238,
	3828,
	3856,
	3856,
	3856,
	3856,
	3775,
	3856,
	3856,
	1986,
	3856,
	3856,
	3775,
	3172,
	3756,
	3172,
	3756,
	3775,
	3775,
	3220,
	3812,
	3856,
	3856,
	1986,
	3191,
	3220,
	3856,
	3856,
	3856,
	3220,
	3812,
	3775,
	3856,
	3856,
	3856,
	3856,
	3238,
	3828,
	3238,
	3828,
	3220,
	3812,
	3775,
	3775,
	3775,
	3775,
	3775,
	3238,
	3238,
	3238,
	3261,
	3263,
	3856,
	3856,
	3856,
	3263,
	3856,
	3856,
	3856,
	3856,
	3191,
	3775,
	3238,
	3828,
	3238,
	3828,
	3238,
	3828,
	2032,
	3856,
	3172,
	3756,
	3775,
	3856,
	3172,
	3856,
	3856,
	3775,
	3856,
	3775,
	3775,
	3775,
	3775,
	3775,
	3261,
	3263,
	3238,
	3238,
	3238,
	3856,
	3856,
	3856,
	3856,
	3856,
	3775,
	3775,
	3775,
	3775,
	3775,
	3261,
	3263,
	3238,
	3238,
	3238,
	3261,
	3263,
	3856,
	3856,
	3856,
	3856,
	3856,
	3775,
	3775,
	3775,
	3775,
	3775,
	3263,
	3851,
	3238,
	3238,
	3238,
	3261,
	3263,
	3856,
	3856,
	3856,
	3856,
	3856,
};
extern const CustomAttributesCacheGenerator g_LeanCommonPlus_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_LeanCommonPlus_CodeGenModule;
const Il2CppCodeGenModule g_LeanCommonPlus_CodeGenModule = 
{
	"LeanCommonPlus.dll",
	650,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_LeanCommonPlus_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
