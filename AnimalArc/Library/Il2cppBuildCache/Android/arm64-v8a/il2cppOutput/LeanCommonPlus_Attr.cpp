﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// UnityEngine.ContextMenu
struct ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// UnityEngine.DefaultExecutionOrder
struct DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210;
// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// UnityEngine.ContextMenu
struct ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.ContextMenu::menuItem
	String_t* ___menuItem_0;
	// System.Boolean UnityEngine.ContextMenu::validate
	bool ___validate_1;
	// System.Int32 UnityEngine.ContextMenu::priority
	int32_t ___priority_2;

public:
	inline static int32_t get_offset_of_menuItem_0() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___menuItem_0)); }
	inline String_t* get_menuItem_0() const { return ___menuItem_0; }
	inline String_t** get_address_of_menuItem_0() { return &___menuItem_0; }
	inline void set_menuItem_0(String_t* value)
	{
		___menuItem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___menuItem_0), (void*)value);
	}

	inline static int32_t get_offset_of_validate_1() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___validate_1)); }
	inline bool get_validate_1() const { return ___validate_1; }
	inline bool* get_address_of_validate_1() { return &___validate_1; }
	inline void set_validate_1(bool value)
	{
		___validate_1 = value;
	}

	inline static int32_t get_offset_of_priority_2() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___priority_2)); }
	inline int32_t get_priority_2() const { return ___priority_2; }
	inline int32_t* get_address_of_priority_2() { return &___priority_2; }
	inline void set_priority_2(int32_t value)
	{
		___priority_2 = value;
	}
};


// UnityEngine.DefaultExecutionOrder
struct DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 UnityEngine.DefaultExecutionOrder::m_Order
	int32_t ___m_Order_0;

public:
	inline static int32_t get_offset_of_m_Order_0() { return static_cast<int32_t>(offsetof(DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F, ___m_Order_0)); }
	inline int32_t get_m_Order_0() const { return ___m_Order_0; }
	inline int32_t* get_address_of_m_Order_0() { return &___m_Order_0; }
	inline void set_m_Order_0(int32_t value)
	{
		___m_Order_0 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_oldName_0), (void*)value);
	}
};


// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.HelpURLAttribute::m_Url
	String_t* ___m_Url_0;
	// System.Boolean UnityEngine.HelpURLAttribute::m_Dispatcher
	bool ___m_Dispatcher_1;
	// System.String UnityEngine.HelpURLAttribute::m_DispatchingFieldName
	String_t* ___m_DispatchingFieldName_2;

public:
	inline static int32_t get_offset_of_m_Url_0() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Url_0)); }
	inline String_t* get_m_Url_0() const { return ___m_Url_0; }
	inline String_t** get_address_of_m_Url_0() { return &___m_Url_0; }
	inline void set_m_Url_0(String_t* value)
	{
		___m_Url_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Url_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Dispatcher_1() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Dispatcher_1)); }
	inline bool get_m_Dispatcher_1() const { return ___m_Dispatcher_1; }
	inline bool* get_address_of_m_Dispatcher_1() { return &___m_Dispatcher_1; }
	inline void set_m_Dispatcher_1(bool value)
	{
		___m_Dispatcher_1 = value;
	}

	inline static int32_t get_offset_of_m_DispatchingFieldName_2() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_DispatchingFieldName_2)); }
	inline String_t* get_m_DispatchingFieldName_2() const { return ___m_DispatchingFieldName_2; }
	inline String_t** get_address_of_m_DispatchingFieldName_2() { return &___m_DispatchingFieldName_2; }
	inline void set_m_DispatchingFieldName_2(String_t* value)
	{
		___m_DispatchingFieldName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DispatchingFieldName_2), (void*)value);
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.HelpURLAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215 (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * __this, String_t* ___url0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ContextMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * __this, String_t* ___itemName0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * __this, String_t* ___oldName0, const RuntimeMethod* method);
// System.Void UnityEngine.DefaultExecutionOrder::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultExecutionOrder__ctor_m18F4188D26702C2E3EC0AB3C1FF4AA4F5329E7A9 (DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F * __this, int32_t ___order0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
static void LeanCommonPlus_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void LeanLoadScene_t63BB7AF22BA3D3D658E7E4FDCE6531ABC694564C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4C\x6F\x61\x64\x53\x63\x65\x6E\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x4C\x6F\x61\x64\x20\x53\x63\x65\x6E\x65"), NULL);
	}
}
static void LeanLoadScene_t63BB7AF22BA3D3D658E7E4FDCE6531ABC694564C_CustomAttributesCacheGenerator_sceneName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanLoadScene_t63BB7AF22BA3D3D658E7E4FDCE6531ABC694564C_CustomAttributesCacheGenerator_aSync(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanLoadScene_t63BB7AF22BA3D3D658E7E4FDCE6531ABC694564C_CustomAttributesCacheGenerator_additive(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanLoadScene_t63BB7AF22BA3D3D658E7E4FDCE6531ABC694564C_CustomAttributesCacheGenerator_LeanLoadScene_Load_mF187C0667FAA85B99C8FF3201D6C0B5AA34D5F8C(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x6F\x61\x64"), NULL);
	}
}
static void LeanPongBall_tF7ECBAAAD1BEC318CAC03807FE3A792F0D2E98AC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x50\x6F\x6E\x67\x42\x61\x6C\x6C"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x50\x6F\x6E\x67\x20\x42\x61\x6C\x6C"), NULL);
	}
}
static void LeanPongBall_tF7ECBAAAD1BEC318CAC03807FE3A792F0D2E98AC_CustomAttributesCacheGenerator_startSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPongBall_tF7ECBAAAD1BEC318CAC03807FE3A792F0D2E98AC_CustomAttributesCacheGenerator_spread(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPongBall_tF7ECBAAAD1BEC318CAC03807FE3A792F0D2E98AC_CustomAttributesCacheGenerator_acceleration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPongBall_tF7ECBAAAD1BEC318CAC03807FE3A792F0D2E98AC_CustomAttributesCacheGenerator_bounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectedString_t9F4A05B05FC48B3529E12F9FD950430A3C2806F3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x65\x64\x54\x65\x78\x74"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x65\x64\x20\x54\x65\x78\x74"), NULL);
	}
}
static void LeanSelectedString_t9F4A05B05FC48B3529E12F9FD950430A3C2806F3_CustomAttributesCacheGenerator_format(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectedString_t9F4A05B05FC48B3529E12F9FD950430A3C2806F3_CustomAttributesCacheGenerator_onString(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectedString_t9F4A05B05FC48B3529E12F9FD950430A3C2806F3_CustomAttributesCacheGenerator_LeanSelectedString_UpdateNow_m1BC9220D2DE3F693201134E1F6064E32653ED8D1(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x55\x70\x64\x61\x74\x65\x20\x4E\x6F\x77"), NULL);
	}
}
static void LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x43\x68\x61\x73\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x43\x68\x61\x73\x65"), NULL);
	}
}
static void LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_destination(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_destinationOffset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E\x4F\x66\x66\x73\x65\x74"), NULL);
	}
}
static void LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_position(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_offset(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x66\x66\x73\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x69\x6E\x67"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_linear(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x69\x6E\x65\x61\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_ignoreZ(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x67\x6E\x6F\x72\x65\x5A"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_continuous(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x74\x69\x6E\x75\x6F\x75\x73"), NULL);
	}
}
static void LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_setPositionOnStart(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x50\x6F\x73\x69\x74\x69\x6F\x6E\x4F\x6E\x53\x74\x61\x72\x74"), NULL);
	}
}
static void LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_LeanChase_SnapToPosition_m57CB8EEDB18D3532BC7E19A220315C87BE77F76D(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6E\x61\x70\x20\x54\x6F\x20\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanChaseRigidbody_t74F8982BC822CF58FA5DAEE7DDE27005084CB55C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x43\x68\x61\x73\x65\x20\x52\x69\x67\x69\x64\x62\x6F\x64\x79"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[2];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x43\x68\x61\x73\x65\x52\x69\x67\x69\x64\x62\x6F\x64\x79"), NULL);
	}
}
static void LeanChaseRigidbody2D_t89C4D24B703A3703CD2E939C3E50026623642D3E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x43\x68\x61\x73\x65\x52\x69\x67\x69\x64\x62\x6F\x64\x79\x32\x44"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x43\x68\x61\x73\x65\x20\x52\x69\x67\x69\x64\x62\x6F\x64\x79\x32\x44"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[2];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var), NULL);
	}
}
static void LeanChaseRigidbody2D_t89C4D24B703A3703CD2E939C3E50026623642D3E_CustomAttributesCacheGenerator_axis(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x4C\x6F\x63\x61\x6C\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x20\x4C\x6F\x63\x61\x6C\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
	{
		DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F * tmp = (DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F *)cache->attributes[2];
		DefaultExecutionOrder__ctor_m18F4188D26702C2E3EC0AB3C1FF4AA4F5329E7A9(tmp, 200LL, NULL);
	}
}
static void LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_x(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x58"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_xMin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x58\x4D\x69\x6E"), NULL);
	}
}
static void LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_xMax(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x58\x4D\x61\x78"), NULL);
	}
}
static void LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_y(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x59"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_yMin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x59\x4D\x69\x6E"), NULL);
	}
}
static void LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_yMax(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x59\x4D\x61\x78"), NULL);
	}
}
static void LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_z(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x5A"), NULL);
	}
}
static void LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_zMin(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x5A\x4D\x69\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_zMax(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x5A\x4D\x61\x78"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanConstrainScale_tF38E12B75C25DE21D131F8BFE531186D032BAC84_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F * tmp = (DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F *)cache->attributes[0];
		DefaultExecutionOrder__ctor_m18F4188D26702C2E3EC0AB3C1FF4AA4F5329E7A9(tmp, 200LL, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x20\x53\x63\x61\x6C\x65"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[2];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x53\x63\x61\x6C\x65"), NULL);
	}
}
static void LeanConstrainScale_tF38E12B75C25DE21D131F8BFE531186D032BAC84_CustomAttributesCacheGenerator_minimum(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D"), NULL);
	}
}
static void LeanConstrainScale_tF38E12B75C25DE21D131F8BFE531186D032BAC84_CustomAttributesCacheGenerator_minimumScale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x53\x63\x61\x6C\x65"), NULL);
	}
}
static void LeanConstrainScale_tF38E12B75C25DE21D131F8BFE531186D032BAC84_CustomAttributesCacheGenerator_maximum(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D"), NULL);
	}
}
static void LeanConstrainScale_tF38E12B75C25DE21D131F8BFE531186D032BAC84_CustomAttributesCacheGenerator_maximumScale(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x53\x63\x61\x6C\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanConstrainToAxis_t34EF2D644F2D7AB276B4759EC3342FA8334EE6AD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x20\x54\x6F\x20\x41\x78\x69\x73"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x54\x6F\x41\x78\x69\x73"), NULL);
	}
	{
		DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F * tmp = (DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F *)cache->attributes[2];
		DefaultExecutionOrder__ctor_m18F4188D26702C2E3EC0AB3C1FF4AA4F5329E7A9(tmp, 200LL, NULL);
	}
}
static void LeanConstrainToAxis_t34EF2D644F2D7AB276B4759EC3342FA8334EE6AD_CustomAttributesCacheGenerator_relativeTo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x6C\x61\x74\x69\x76\x65\x54\x6F"), NULL);
	}
}
static void LeanConstrainToAxis_t34EF2D644F2D7AB276B4759EC3342FA8334EE6AD_CustomAttributesCacheGenerator_axis(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x78\x69\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanConstrainToAxis_t34EF2D644F2D7AB276B4759EC3342FA8334EE6AD_CustomAttributesCacheGenerator_minimum(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanConstrainToAxis_t34EF2D644F2D7AB276B4759EC3342FA8334EE6AD_CustomAttributesCacheGenerator_maximum(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D"), NULL);
	}
}
static void LeanConstrainToBox_t2928F56A51BBF4D022A0CC2B12F5A83232589DAA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x20\x54\x6F\x20\x42\x6F\x78"), NULL);
	}
	{
		DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F * tmp = (DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F *)cache->attributes[1];
		DefaultExecutionOrder__ctor_m18F4188D26702C2E3EC0AB3C1FF4AA4F5329E7A9(tmp, 200LL, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[2];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x54\x6F\x42\x6F\x78"), NULL);
	}
}
static void LeanConstrainToBox_t2928F56A51BBF4D022A0CC2B12F5A83232589DAA_CustomAttributesCacheGenerator_relativeTo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x6C\x61\x74\x69\x76\x65\x54\x6F"), NULL);
	}
}
static void LeanConstrainToBox_t2928F56A51BBF4D022A0CC2B12F5A83232589DAA_CustomAttributesCacheGenerator_size(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x7A\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanConstrainToBox_t2928F56A51BBF4D022A0CC2B12F5A83232589DAA_CustomAttributesCacheGenerator_center(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x65\x6E\x74\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanConstrainToCollider_t793183BFE2F2F4019D48AB3740BB4FCF2ED1851A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F * tmp = (DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F *)cache->attributes[0];
		DefaultExecutionOrder__ctor_m18F4188D26702C2E3EC0AB3C1FF4AA4F5329E7A9(tmp, 200LL, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x54\x6F\x43\x6F\x6C\x6C\x69\x64\x65\x72"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x20\x54\x6F\x20\x43\x6F\x6C\x6C\x69\x64\x65\x72"), NULL);
	}
}
static void LeanConstrainToCollider_t793183BFE2F2F4019D48AB3740BB4FCF2ED1851A_CustomAttributesCacheGenerator__collider(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6C\x69\x64\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanConstrainToColliders_tDC235CBFF2CFB58D7DED421EB9C65B2A33BA777D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x54\x6F\x43\x6F\x6C\x6C\x69\x64\x65\x72\x73"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x20\x54\x6F\x20\x43\x6F\x6C\x6C\x69\x64\x65\x72\x73"), NULL);
	}
	{
		DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F * tmp = (DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F *)cache->attributes[2];
		DefaultExecutionOrder__ctor_m18F4188D26702C2E3EC0AB3C1FF4AA4F5329E7A9(tmp, 200LL, NULL);
	}
}
static void LeanConstrainToColliders_tDC235CBFF2CFB58D7DED421EB9C65B2A33BA777D_CustomAttributesCacheGenerator_colliders(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6C\x69\x64\x65\x72\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanConstrainToDirection_t4663FC584523F0D310959009BADF0D86DB6125AD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x20\x54\x6F\x20\x44\x69\x72\x65\x63\x74\x69\x6F\x6E"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x54\x6F\x44\x69\x72\x65\x63\x74\x69\x6F\x6E"), NULL);
	}
	{
		DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F * tmp = (DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F *)cache->attributes[2];
		DefaultExecutionOrder__ctor_m18F4188D26702C2E3EC0AB3C1FF4AA4F5329E7A9(tmp, 200LL, NULL);
	}
}
static void LeanConstrainToDirection_t4663FC584523F0D310959009BADF0D86DB6125AD_CustomAttributesCacheGenerator_forward(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x72\x77\x61\x72\x64"), NULL);
	}
}
static void LeanConstrainToDirection_t4663FC584523F0D310959009BADF0D86DB6125AD_CustomAttributesCacheGenerator_direction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x72\x65\x63\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanConstrainToDirection_t4663FC584523F0D310959009BADF0D86DB6125AD_CustomAttributesCacheGenerator_relativeTo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x72\x65\x6C\x61\x74\x69\x76\x65\x54\x6F"), NULL);
	}
}
static void LeanConstrainToDirection_t4663FC584523F0D310959009BADF0D86DB6125AD_CustomAttributesCacheGenerator_minAngle(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x41\x6E\x67\x6C\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[2];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 180.0f, NULL);
	}
}
static void LeanConstrainToDirection_t4663FC584523F0D310959009BADF0D86DB6125AD_CustomAttributesCacheGenerator_maxAngle(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 180.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x41\x6E\x67\x6C\x65"), NULL);
	}
}
static void LeanConstrainToOrthographic_t00ABF9C53E05BB000E15B5484D62889259A15107_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F * tmp = (DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F *)cache->attributes[0];
		DefaultExecutionOrder__ctor_m18F4188D26702C2E3EC0AB3C1FF4AA4F5329E7A9(tmp, 200LL, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x20\x54\x6F\x20\x4F\x72\x74\x68\x6F\x67\x72\x61\x70\x68\x69\x63"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[2];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x23\x4C\x65\x61\x6E\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x54\x6F\x4F\x72\x74\x68\x6F\x67\x72\x61\x70\x68\x69\x63"), NULL);
	}
}
static void LeanConstrainToOrthographic_t00ABF9C53E05BB000E15B5484D62889259A15107_CustomAttributesCacheGenerator__camera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void LeanConstrainToOrthographic_t00ABF9C53E05BB000E15B5484D62889259A15107_CustomAttributesCacheGenerator_plane(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x61\x6E\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x44\x65\x6C\x61\x79\x65\x64\x20\x56\x61\x6C\x75\x65"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x44\x65\x6C\x61\x79\x65\x64\x56\x61\x6C\x75\x65"), NULL);
	}
}
static void LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator_delay(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x6C\x61\x79"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator_autoClear(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x74\x6F\x43\x6C\x65\x61\x72"), NULL);
	}
}
static void LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator_onValueX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator_onValueY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator_onValueZ(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator_onValueXY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator_onValueXYZ(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator_snapshots(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDeselected_t30428A103BF20EC13D602A4E74F257375173A224_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x44\x65\x73\x65\x6C\x65\x63\x74\x65\x64"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x44\x65\x73\x65\x6C\x65\x63\x74\x65\x64"), NULL);
	}
}
static void LeanDeselected_t30428A103BF20EC13D602A4E74F257375173A224_CustomAttributesCacheGenerator_onSelectable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFollow_t8123D50E20D43EF755127A1EDE5003DECC36DC35_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x46\x6F\x6C\x6C\x6F\x77"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x46\x6F\x6C\x6C\x6F\x77"), NULL);
	}
}
static void LeanFollow_t8123D50E20D43EF755127A1EDE5003DECC36DC35_CustomAttributesCacheGenerator_threshold(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFollow_t8123D50E20D43EF755127A1EDE5003DECC36DC35_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x65\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFollow_t8123D50E20D43EF755127A1EDE5003DECC36DC35_CustomAttributesCacheGenerator_onReachedDestination(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFollow_t8123D50E20D43EF755127A1EDE5003DECC36DC35_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFollow_t8123D50E20D43EF755127A1EDE5003DECC36DC35_CustomAttributesCacheGenerator_LeanFollow_ClearPositions_m485D7C6E05A7854AB051A25D237388886E716E8C(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x65\x61\x72\x20\x50\x6F\x73\x69\x74\x69\x6F\x6E\x73"), NULL);
	}
}
static void LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x61\x69\x6E\x74\x61\x69\x6E\x44\x69\x73\x74\x61\x6E\x63\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x4D\x61\x69\x6E\x74\x61\x69\x6E\x20\x44\x69\x73\x74\x61\x6E\x63\x65"), NULL);
	}
}
static void LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_direction(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x72\x65\x63\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_directionSpace(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x72\x65\x63\x74\x69\x6F\x6E\x53\x70\x61\x63\x65"), NULL);
	}
}
static void LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_distance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x74\x61\x6E\x63\x65"), NULL);
	}
}
static void LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_clamp(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x61\x6D\x70"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x74\x61\x6E\x63\x65\x43\x6C\x61\x6D\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_clampMin(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x74\x61\x6E\x63\x65\x4D\x69\x6E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x61\x6D\x70\x4D\x69\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_clampMax(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x61\x6D\x70\x4D\x61\x78"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x74\x61\x6E\x63\x65\x4D\x61\x78"), NULL);
	}
}
static void LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_collisionLayers(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x4C\x61\x79\x65\x72\x73"), NULL);
	}
}
static void LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_collisionRadius(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x52\x61\x64\x69\x75\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_currentDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x61\x6E\x75\x61\x6C\x52\x65\x73\x63\x61\x6C\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x4D\x61\x6E\x75\x61\x6C\x20\x52\x65\x73\x63\x61\x6C\x65"), NULL);
	}
}
static void LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_axesA(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_axesB(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_multiplier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_scaleByTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_defaultScale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_remainingDelta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_LeanManualRescale_ResetScale_m73FDFECE254BA9F6FEC221B9415F56013705C68F(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x65\x74\x20\x53\x63\x61\x6C\x65"), NULL);
	}
}
static void LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_LeanManualRescale_SnapToTarget_m9878D0C99CFF0AF6D57525E981F88E00B02C0AE4(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6E\x61\x70\x20\x54\x6F\x20\x54\x61\x72\x67\x65\x74"), NULL);
	}
}
static void LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x4D\x61\x6E\x75\x61\x6C\x20\x52\x6F\x74\x61\x74\x65"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x61\x6E\x75\x61\x6C\x52\x6F\x74\x61\x74\x65"), NULL);
	}
}
static void LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_space(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x61\x63\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_axisA(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x78\x69\x73\x41"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_axisB(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x78\x69\x73\x42"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_multiplier(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
}
static void LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_scaleByTime(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x61\x6C\x65\x42\x79\x54\x69\x6D\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_defaultRotation(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x61\x75\x6C\x74\x52\x6F\x74\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_remainingDelta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_LeanManualRotate_ResetRotation_m90126CB30981A7298DA85A664644C7850E10DC8B(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x65\x74\x20\x52\x6F\x74\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_LeanManualRotate_SnapToTarget_m0F590A6B43C6D3760E4BEF4A975054D80AE2338F(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6E\x61\x70\x20\x54\x6F\x20\x54\x61\x72\x67\x65\x74"), NULL);
	}
}
static void LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_LeanManualRotate_StopRotation_m9E83B607C247214965CD067ACFC18D71F25A75D0(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x6F\x70\x20\x52\x6F\x74\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanManualTorque_tF64938201C50DB9084FBE808EF224B4909038F5F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x4D\x61\x6E\x75\x61\x6C\x20\x54\x6F\x72\x71\x75\x65"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x61\x6E\x75\x61\x6C\x54\x6F\x72\x71\x75\x65"), NULL);
	}
}
static void LeanManualTorque_tF64938201C50DB9084FBE808EF224B4909038F5F_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTorque_tF64938201C50DB9084FBE808EF224B4909038F5F_CustomAttributesCacheGenerator_mode(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTorque_tF64938201C50DB9084FBE808EF224B4909038F5F_CustomAttributesCacheGenerator_multiplier(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTorque_tF64938201C50DB9084FBE808EF224B4909038F5F_CustomAttributesCacheGenerator_space(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x61\x63\x65"), NULL);
	}
}
static void LeanManualTorque_tF64938201C50DB9084FBE808EF224B4909038F5F_CustomAttributesCacheGenerator_axisA(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x78\x69\x73\x41"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTorque_tF64938201C50DB9084FBE808EF224B4909038F5F_CustomAttributesCacheGenerator_axisB(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x78\x69\x73\x42"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x61\x6E\x75\x61\x6C\x54\x72\x61\x6E\x73\x6C\x61\x74\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x4D\x61\x6E\x75\x61\x6C\x20\x54\x72\x61\x6E\x73\x6C\x61\x74\x65"), NULL);
	}
}
static void LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_space(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x61\x63\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_directionA(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x72\x65\x63\x74\x69\x6F\x6E\x41"), NULL);
	}
}
static void LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_directionB(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x72\x65\x63\x74\x69\x6F\x6E\x42"), NULL);
	}
}
static void LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_multiplier(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
}
static void LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_scaleByTime(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x61\x6C\x65\x42\x79\x54\x69\x6D\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_defaultPosition(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x61\x75\x6C\x74\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_remainingDelta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_LeanManualTranslate_ResetPosition_m96A8915E69D6DCF055E70D2C67B37035BEEE89E4(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x65\x74\x20\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_LeanManualTranslate_SnapToTarget_m1EC30AA794459BDEC88980D4DC737EF87868F7B8(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6E\x61\x70\x20\x54\x6F\x20\x54\x61\x72\x67\x65\x74"), NULL);
	}
}
static void LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x61\x6E\x75\x61\x6C\x54\x72\x61\x6E\x73\x6C\x61\x74\x65\x52\x69\x67\x69\x64\x62\x6F\x64\x79"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x4D\x61\x6E\x75\x61\x6C\x20\x54\x72\x61\x6E\x73\x6C\x61\x74\x65\x20\x52\x69\x67\x69\x64\x62\x6F\x64\x79"), NULL);
	}
}
static void LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_space(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x61\x63\x65"), NULL);
	}
}
static void LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_directionA(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x72\x65\x63\x74\x69\x6F\x6E\x41"), NULL);
	}
}
static void LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_directionB(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x72\x65\x63\x74\x69\x6F\x6E\x42"), NULL);
	}
}
static void LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_multiplier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72"), NULL);
	}
}
static void LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
}
static void LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_scaleByTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x61\x6C\x65\x42\x79\x54\x69\x6D\x65"), NULL);
	}
}
static void LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_inertia(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x65\x72\x74\x69\x61"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[2];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_resetVelocityInUpdate(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x65\x74\x56\x65\x6C\x6F\x63\x69\x74\x79\x49\x6E\x55\x70\x64\x61\x74\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_remainingDelta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x4D\x61\x6E\x75\x61\x6C\x20\x54\x72\x61\x6E\x73\x6C\x61\x74\x65\x20\x52\x69\x67\x69\x64\x62\x6F\x64\x79\x32\x44"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x61\x6E\x75\x61\x6C\x54\x72\x61\x6E\x73\x6C\x61\x74\x65\x52\x69\x67\x69\x64\x62\x6F\x64\x79\x32\x44"), NULL);
	}
}
static void LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_space(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x61\x63\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_directionA(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x72\x65\x63\x74\x69\x6F\x6E\x41"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_directionB(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x72\x65\x63\x74\x69\x6F\x6E\x42"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_multiplier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72"), NULL);
	}
}
static void LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_scaleByTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x61\x6C\x65\x42\x79\x54\x69\x6D\x65"), NULL);
	}
}
static void LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_resetVelocityInUpdate(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x65\x74\x56\x65\x6C\x6F\x63\x69\x74\x79\x49\x6E\x55\x70\x64\x61\x74\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_remainingDelta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualVelocity_tE43CE9F4BC353A36B1896332F335E70C48EE1F68_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x61\x6E\x75\x61\x6C\x56\x65\x6C\x6F\x63\x69\x74\x79"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x4D\x61\x6E\x75\x61\x6C\x20\x56\x65\x6C\x6F\x63\x69\x74\x79"), NULL);
	}
}
static void LeanManualVelocity_tE43CE9F4BC353A36B1896332F335E70C48EE1F68_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
}
static void LeanManualVelocity_tE43CE9F4BC353A36B1896332F335E70C48EE1F68_CustomAttributesCacheGenerator_mode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), NULL);
	}
}
static void LeanManualVelocity_tE43CE9F4BC353A36B1896332F335E70C48EE1F68_CustomAttributesCacheGenerator_multiplier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72"), NULL);
	}
}
static void LeanManualVelocity_tE43CE9F4BC353A36B1896332F335E70C48EE1F68_CustomAttributesCacheGenerator_space(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x61\x63\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualVelocity_tE43CE9F4BC353A36B1896332F335E70C48EE1F68_CustomAttributesCacheGenerator_directionA(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x72\x65\x63\x74\x69\x6F\x6E\x41"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualVelocity_tE43CE9F4BC353A36B1896332F335E70C48EE1F68_CustomAttributesCacheGenerator_directionB(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x72\x65\x63\x74\x69\x6F\x6E\x42"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualVelocity2D_t6A1AE638B40E0C4586F4AD7CBD930C1E0B373E1F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4D\x61\x6E\x75\x61\x6C\x56\x65\x6C\x6F\x63\x69\x74\x79\x32\x44"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x4D\x61\x6E\x75\x61\x6C\x20\x56\x65\x6C\x6F\x63\x69\x74\x79\x20\x32\x44"), NULL);
	}
}
static void LeanManualVelocity2D_t6A1AE638B40E0C4586F4AD7CBD930C1E0B373E1F_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualVelocity2D_t6A1AE638B40E0C4586F4AD7CBD930C1E0B373E1F_CustomAttributesCacheGenerator_mode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), NULL);
	}
}
static void LeanManualVelocity2D_t6A1AE638B40E0C4586F4AD7CBD930C1E0B373E1F_CustomAttributesCacheGenerator_multiplier(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualVelocity2D_t6A1AE638B40E0C4586F4AD7CBD930C1E0B373E1F_CustomAttributesCacheGenerator_space(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x61\x63\x65"), NULL);
	}
}
static void LeanManualVelocity2D_t6A1AE638B40E0C4586F4AD7CBD930C1E0B373E1F_CustomAttributesCacheGenerator_directionA(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x72\x65\x63\x74\x69\x6F\x6E\x41"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanManualVelocity2D_t6A1AE638B40E0C4586F4AD7CBD930C1E0B373E1F_CustomAttributesCacheGenerator_directionB(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x72\x65\x63\x74\x69\x6F\x6E\x42"), NULL);
	}
}
static void LeanOrbit_tACFA62F4301E6D69C25B717E2766508BFB1EF3B6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x4F\x72\x62\x69\x74"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x4F\x72\x62\x69\x74"), NULL);
	}
}
static void LeanOrbit_tACFA62F4301E6D69C25B717E2766508BFB1EF3B6_CustomAttributesCacheGenerator__camera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void LeanOrbit_tACFA62F4301E6D69C25B717E2766508BFB1EF3B6_CustomAttributesCacheGenerator_pivot(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x69\x76\x6F\x74"), NULL);
	}
}
static void LeanOrbit_tACFA62F4301E6D69C25B717E2766508BFB1EF3B6_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x69\x6E\x67"), NULL);
	}
}
static void LeanOrbit_tACFA62F4301E6D69C25B717E2766508BFB1EF3B6_CustomAttributesCacheGenerator_pitchSensitivity(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x69\x74\x63\x68\x53\x65\x6E\x73\x69\x74\x69\x76\x69\x74\x79"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanOrbit_tACFA62F4301E6D69C25B717E2766508BFB1EF3B6_CustomAttributesCacheGenerator_yawSensitivity(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x59\x61\x77\x53\x65\x6E\x73\x69\x74\x69\x76\x69\x74\x79"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanOrbit_tACFA62F4301E6D69C25B717E2766508BFB1EF3B6_CustomAttributesCacheGenerator_remainingDelta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x50\x69\x74\x63\x68\x59\x61\x77"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x50\x69\x74\x63\x68\x20\x59\x61\x77"), NULL);
	}
}
static void LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator__camera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x69\x6E\x67"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_defaultRotation(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x61\x75\x6C\x74\x52\x6F\x74\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_pitch(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x69\x74\x63\x68"), NULL);
	}
}
static void LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_pitchSensitivity(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x69\x74\x63\x68\x53\x65\x6E\x73\x69\x74\x69\x76\x69\x74\x79"), NULL);
	}
}
static void LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_pitchClamp(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x69\x74\x63\x68\x43\x6C\x61\x6D\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_pitchMin(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x69\x74\x63\x68\x4D\x69\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_pitchMax(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x69\x74\x63\x68\x4D\x61\x78"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_yaw(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x59\x61\x77"), NULL);
	}
}
static void LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_yawSensitivity(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x59\x61\x77\x53\x65\x6E\x73\x69\x74\x69\x76\x69\x74\x79"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_yawClamp(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x59\x61\x77\x43\x6C\x61\x6D\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_yawMin(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x59\x61\x77\x4D\x69\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_yawMax(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x59\x61\x77\x4D\x61\x78"), NULL);
	}
}
static void LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_currentPitch(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_currentYaw(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_LeanPitchYaw_ResetRotation_m223BD5FE2B0466FEB5871EBC174A3075CF872B25(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x65\x74\x20\x52\x6F\x74\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanPitchYawAutoRotate_t6FC496B40CE91A09B6C1A024F744C2866576281F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x50\x69\x74\x63\x68\x20\x59\x61\x77\x20\x41\x75\x74\x6F\x20\x52\x6F\x74\x61\x74\x65"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[2];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x50\x69\x74\x63\x68\x59\x61\x77\x41\x75\x74\x6F\x52\x6F\x74\x61\x74\x65"), NULL);
	}
}
static void LeanPitchYawAutoRotate_t6FC496B40CE91A09B6C1A024F744C2866576281F_CustomAttributesCacheGenerator_delay(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x6C\x61\x79"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPitchYawAutoRotate_t6FC496B40CE91A09B6C1A024F744C2866576281F_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x65\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPitchYawAutoRotate_t6FC496B40CE91A09B6C1A024F744C2866576281F_CustomAttributesCacheGenerator_acceleration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x63\x63\x65\x6C\x65\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanPitchYawAutoRotate_t6FC496B40CE91A09B6C1A024F744C2866576281F_CustomAttributesCacheGenerator_idleTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPitchYawAutoRotate_t6FC496B40CE91A09B6C1A024F744C2866576281F_CustomAttributesCacheGenerator_strength(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPitchYawAutoRotate_t6FC496B40CE91A09B6C1A024F744C2866576281F_CustomAttributesCacheGenerator_expectedPitch(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPitchYawAutoRotate_t6FC496B40CE91A09B6C1A024F744C2866576281F_CustomAttributesCacheGenerator_expectedYaw(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRandomEvents_t6261D8ADDF7F1F1A8FBF4BF5980BD31E4DAAAFEB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x52\x61\x6E\x64\x6F\x6D\x20\x45\x76\x65\x6E\x74\x73"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x52\x61\x6E\x64\x6F\x6D\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
static void LeanRandomEvents_t6261D8ADDF7F1F1A8FBF4BF5980BD31E4DAAAFEB_CustomAttributesCacheGenerator_events(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRandomEvents_t6261D8ADDF7F1F1A8FBF4BF5980BD31E4DAAAFEB_CustomAttributesCacheGenerator_LeanRandomEvents_Invoke_mCEA3D4832552ACC6AAF1895C8DADF995E45CF735(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x6F\x6B\x65"), NULL);
	}
}
static void LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x52\x65\x6D\x61\x70\x56\x61\x6C\x75\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x52\x65\x6D\x61\x70\x20\x56\x61\x6C\x75\x65"), NULL);
	}
}
static void LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_oldMin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_oldMax(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_newMin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_newMax(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_onValueX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_onValueY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_onValueZ(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_onValueXY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_onValueXYZ(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x52\x65\x76\x65\x72\x74\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x52\x65\x76\x65\x72\x74\x20\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D"), NULL);
	}
}
static void LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_recordOnStart(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x63\x6F\x72\x64\x4F\x6E\x53\x74\x61\x72\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_revertPosition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x76\x65\x72\x74\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_revertRotation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x76\x65\x72\x74\x52\x6F\x74\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_revertScale(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x76\x65\x72\x74\x53\x63\x61\x6C\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_thresholdPosition(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x72\x65\x73\x68\x6F\x6C\x64\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_thresholdRotation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x72\x65\x73\x68\x6F\x6C\x64\x52\x6F\x74\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_thresholdScale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x72\x65\x73\x68\x6F\x6C\x64\x53\x63\x61\x6C\x65"), NULL);
	}
}
static void LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_targetPosition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_targetRotation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74\x52\x6F\x74\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_targetScale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74\x53\x63\x61\x6C\x65"), NULL);
	}
}
static void LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_reverting(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_LeanRevertTransform_Revert_m5348B2D6D10C01686894612A2DEBA6DE904E53B3(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x76\x65\x72\x74"), NULL);
	}
}
static void LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_LeanRevertTransform_StopRevert_mE0BF7A16895C20C0F4657E03A90EC26975250020(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x6F\x70\x20\x52\x65\x76\x65\x72\x74"), NULL);
	}
}
static void LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_LeanRevertTransform_RecordTransform_mDEA71175D5321AFBA577541B88E38284D039D961(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x63\x6F\x72\x64\x20\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D"), NULL);
	}
}
static void LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x52\x6F\x74\x61\x74\x65\x20\x54\x6F\x20\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x52\x6F\x74\x61\x74\x65\x54\x6F\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator_position(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator_threshold(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
}
static void LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator_invert(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x72\x74"), NULL);
	}
}
static void LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator_rotateTo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x6F\x74\x61\x74\x65\x54\x6F"), NULL);
	}
}
static void LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x69\x6E\x67"), NULL);
	}
}
static void LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator_previousPosition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator_previousDelta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRotateToRigidbody2D_t595746BE51910F0F84039D182A6BDDE54E8EFB78_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x52\x6F\x74\x61\x74\x65\x54\x6F\x52\x69\x67\x69\x64\x62\x6F\x64\x79\x32\x44"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x52\x6F\x74\x61\x74\x65\x20\x54\x6F\x20\x52\x69\x67\x69\x64\x62\x6F\x64\x79\x32\x44"), NULL);
	}
}
static void LeanRotateToRigidbody2D_t595746BE51910F0F84039D182A6BDDE54E8EFB78_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRotateToRigidbody2D_t595746BE51910F0F84039D182A6BDDE54E8EFB78_CustomAttributesCacheGenerator_previousPosition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRotateToRigidbody2D_t595746BE51910F0F84039D182A6BDDE54E8EFB78_CustomAttributesCacheGenerator_vector(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelected_t5D44E49D01845676F7183B78AFC9923E510086B5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x65\x64"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x65\x64"), NULL);
	}
}
static void LeanSelected_t5D44E49D01845676F7183B78AFC9923E510086B5_CustomAttributesCacheGenerator_onSelectable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectedCount_tFD93677E60574A08C3323E2C222FD04FB4681DB7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x65\x64\x20\x43\x6F\x75\x6E\x74"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x65\x64\x43\x6F\x75\x6E\x74"), NULL);
	}
}
static void LeanSelectedCount_tFD93677E60574A08C3323E2C222FD04FB4681DB7_CustomAttributesCacheGenerator_onCount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectedCount_tFD93677E60574A08C3323E2C222FD04FB4681DB7_CustomAttributesCacheGenerator_matchMin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x63\x68\x4D\x69\x6E"), NULL);
	}
}
static void LeanSelectedCount_tFD93677E60574A08C3323E2C222FD04FB4681DB7_CustomAttributesCacheGenerator_matchMax(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x63\x68\x4D\x61\x78"), NULL);
	}
}
static void LeanSelectedCount_tFD93677E60574A08C3323E2C222FD04FB4681DB7_CustomAttributesCacheGenerator_onMatch(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectedCount_tFD93677E60574A08C3323E2C222FD04FB4681DB7_CustomAttributesCacheGenerator_onUnmatch(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectedCount_tFD93677E60574A08C3323E2C222FD04FB4681DB7_CustomAttributesCacheGenerator_matched(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x69\x6E\x73\x69\x64\x65"), NULL);
	}
}
static void LeanSelectedRatio_t7EC2D7A7798F11697B26C4ED8322B39A89827BB7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x65\x64\x20\x52\x61\x74\x69\x6F"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x65\x64\x52\x61\x74\x69\x6F"), NULL);
	}
}
static void LeanSelectedRatio_t7EC2D7A7798F11697B26C4ED8322B39A89827BB7_CustomAttributesCacheGenerator_inverse(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectedRatio_t7EC2D7A7798F11697B26C4ED8322B39A89827BB7_CustomAttributesCacheGenerator_onRatio(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectedRatio_t7EC2D7A7798F11697B26C4ED8322B39A89827BB7_CustomAttributesCacheGenerator_LeanSelectedRatio_UpdateNow_m6CC9B5812A2A7DFC348E8D01A4FB1C1AA5EE53EE(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x55\x70\x64\x61\x74\x65\x20\x4E\x6F\x77"), NULL);
	}
}
static void LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x6D\x6F\x6F\x74\x68\x65\x64\x56\x61\x6C\x75\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x53\x6D\x6F\x6F\x74\x68\x65\x64\x20\x56\x61\x6C\x75\x65"), NULL);
	}
}
static void LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_threshold(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_autoStop(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x74\x6F\x53\x74\x6F\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_onValueX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_onValueY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_onValueZ(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_onValueXY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_onValueXYZ(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_currentValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_targetValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_targetSet(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSpawnBetween_t8D31F619002267293E9E0821E1B08F7611CA7F95_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x70\x61\x77\x6E\x42\x65\x74\x77\x65\x65\x6E"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x53\x70\x61\x77\x6E\x20\x42\x65\x74\x77\x65\x65\x6E"), NULL);
	}
}
static void LeanSpawnBetween_t8D31F619002267293E9E0821E1B08F7611CA7F95_CustomAttributesCacheGenerator_prefab(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x66\x61\x62"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSpawnBetween_t8D31F619002267293E9E0821E1B08F7611CA7F95_CustomAttributesCacheGenerator_velocityMultiplier(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x56\x65\x6C\x6F\x63\x69\x74\x79\x4D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSpawnBetween_t8D31F619002267293E9E0821E1B08F7611CA7F95_CustomAttributesCacheGenerator_velocityMin(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x56\x65\x6C\x6F\x63\x69\x74\x79\x4D\x69\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSpawnBetween_t8D31F619002267293E9E0821E1B08F7611CA7F95_CustomAttributesCacheGenerator_velocityMax(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x56\x65\x6C\x6F\x63\x69\x74\x79\x4D\x61\x78"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSwap_t30422744E12ADFDC5B9BD66AA6A23C21FF1F235E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x53\x77\x61\x70"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x53\x77\x61\x70"), NULL);
	}
}
static void LeanSwap_t30422744E12ADFDC5B9BD66AA6A23C21FF1F235E_CustomAttributesCacheGenerator_index(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x6C\x61\x79"), NULL);
	}
}
static void LeanSwap_t30422744E12ADFDC5B9BD66AA6A23C21FF1F235E_CustomAttributesCacheGenerator_prefabs(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x66\x61\x62\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSwap_t30422744E12ADFDC5B9BD66AA6A23C21FF1F235E_CustomAttributesCacheGenerator_clone(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSwap_t30422744E12ADFDC5B9BD66AA6A23C21FF1F235E_CustomAttributesCacheGenerator_clonePrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSwap_t30422744E12ADFDC5B9BD66AA6A23C21FF1F235E_CustomAttributesCacheGenerator_LeanSwap_UpdateSwap_m3DB8B10CDCF43394B088F0C3AAA33CAE08F1C3BE(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x55\x70\x64\x61\x74\x65\x20\x53\x77\x61\x70"), NULL);
	}
}
static void LeanSwap_t30422744E12ADFDC5B9BD66AA6A23C21FF1F235E_CustomAttributesCacheGenerator_LeanSwap_SwapToPrevious_mB1DB144F6390035AF0AF874F8C5C810FFF496F2B(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x53\x77\x61\x70\x20\x54\x6F\x20\x50\x72\x65\x76\x69\x6F\x75\x73"), NULL);
	}
}
static void LeanSwap_t30422744E12ADFDC5B9BD66AA6A23C21FF1F235E_CustomAttributesCacheGenerator_LeanSwap_SwapToNext_m1476858973D0B0AAE9F539A24E0F0CD036367F6A(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x53\x77\x61\x70\x20\x54\x6F\x20\x4E\x65\x78\x74"), NULL);
	}
}
static void LeanThresholdDelta_tC501FF25BEF5189142AB9539C650327A7452B2A8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x54\x68\x72\x65\x73\x68\x6F\x6C\x64\x20\x44\x65\x6C\x74\x61"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x54\x68\x72\x65\x73\x68\x6F\x6C\x64\x44\x65\x6C\x74\x61"), NULL);
	}
}
static void LeanThresholdDelta_tC501FF25BEF5189142AB9539C650327A7452B2A8_CustomAttributesCacheGenerator_onDeltaX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanThresholdDelta_tC501FF25BEF5189142AB9539C650327A7452B2A8_CustomAttributesCacheGenerator_onDeltaY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanThresholdDelta_tC501FF25BEF5189142AB9539C650327A7452B2A8_CustomAttributesCacheGenerator_onDeltaZ(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanThresholdDelta_tC501FF25BEF5189142AB9539C650327A7452B2A8_CustomAttributesCacheGenerator_onDeltaXY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanThresholdDelta_tC501FF25BEF5189142AB9539C650327A7452B2A8_CustomAttributesCacheGenerator_onDeltaXYZ(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanThresholdPosition_tE1AC7858685942EAD5CFF40ABD9FAF1BD0D7D798_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x54\x68\x72\x65\x73\x68\x6F\x6C\x64\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x54\x68\x72\x65\x73\x68\x6F\x6C\x64\x20\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanThresholdPosition_tE1AC7858685942EAD5CFF40ABD9FAF1BD0D7D798_CustomAttributesCacheGenerator_onPositionX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanThresholdPosition_tE1AC7858685942EAD5CFF40ABD9FAF1BD0D7D798_CustomAttributesCacheGenerator_onPositionY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanThresholdPosition_tE1AC7858685942EAD5CFF40ABD9FAF1BD0D7D798_CustomAttributesCacheGenerator_onPositionZ(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanThresholdPosition_tE1AC7858685942EAD5CFF40ABD9FAF1BD0D7D798_CustomAttributesCacheGenerator_onPositionXY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanThresholdPosition_tE1AC7858685942EAD5CFF40ABD9FAF1BD0D7D798_CustomAttributesCacheGenerator_onPositionXYZ(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanValue_t76626A54AAE9337498D0FF7DDB0E118C2C90652E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x56\x61\x6C\x75\x65"), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x50\x6C\x75\x73\x23\x4C\x65\x61\x6E\x56\x61\x6C\x75\x65"), NULL);
	}
}
static void LeanValue_t76626A54AAE9337498D0FF7DDB0E118C2C90652E_CustomAttributesCacheGenerator_onValueX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanValue_t76626A54AAE9337498D0FF7DDB0E118C2C90652E_CustomAttributesCacheGenerator_onValueY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanValue_t76626A54AAE9337498D0FF7DDB0E118C2C90652E_CustomAttributesCacheGenerator_onValueZ(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanValue_t76626A54AAE9337498D0FF7DDB0E118C2C90652E_CustomAttributesCacheGenerator_onValueXY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanValue_t76626A54AAE9337498D0FF7DDB0E118C2C90652E_CustomAttributesCacheGenerator_onValueXYZ(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanValue_t76626A54AAE9337498D0FF7DDB0E118C2C90652E_CustomAttributesCacheGenerator_current(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_LeanCommonPlus_AttributeGenerators[];
const CustomAttributesCacheGenerator g_LeanCommonPlus_AttributeGenerators[303] = 
{
	LeanLoadScene_t63BB7AF22BA3D3D658E7E4FDCE6531ABC694564C_CustomAttributesCacheGenerator,
	LeanPongBall_tF7ECBAAAD1BEC318CAC03807FE3A792F0D2E98AC_CustomAttributesCacheGenerator,
	LeanSelectedString_t9F4A05B05FC48B3529E12F9FD950430A3C2806F3_CustomAttributesCacheGenerator,
	LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator,
	LeanChaseRigidbody_t74F8982BC822CF58FA5DAEE7DDE27005084CB55C_CustomAttributesCacheGenerator,
	LeanChaseRigidbody2D_t89C4D24B703A3703CD2E939C3E50026623642D3E_CustomAttributesCacheGenerator,
	LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator,
	LeanConstrainScale_tF38E12B75C25DE21D131F8BFE531186D032BAC84_CustomAttributesCacheGenerator,
	LeanConstrainToAxis_t34EF2D644F2D7AB276B4759EC3342FA8334EE6AD_CustomAttributesCacheGenerator,
	LeanConstrainToBox_t2928F56A51BBF4D022A0CC2B12F5A83232589DAA_CustomAttributesCacheGenerator,
	LeanConstrainToCollider_t793183BFE2F2F4019D48AB3740BB4FCF2ED1851A_CustomAttributesCacheGenerator,
	LeanConstrainToColliders_tDC235CBFF2CFB58D7DED421EB9C65B2A33BA777D_CustomAttributesCacheGenerator,
	LeanConstrainToDirection_t4663FC584523F0D310959009BADF0D86DB6125AD_CustomAttributesCacheGenerator,
	LeanConstrainToOrthographic_t00ABF9C53E05BB000E15B5484D62889259A15107_CustomAttributesCacheGenerator,
	LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator,
	LeanDeselected_t30428A103BF20EC13D602A4E74F257375173A224_CustomAttributesCacheGenerator,
	LeanFollow_t8123D50E20D43EF755127A1EDE5003DECC36DC35_CustomAttributesCacheGenerator,
	LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator,
	LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator,
	LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator,
	LeanManualTorque_tF64938201C50DB9084FBE808EF224B4909038F5F_CustomAttributesCacheGenerator,
	LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator,
	LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator,
	LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator,
	LeanManualVelocity_tE43CE9F4BC353A36B1896332F335E70C48EE1F68_CustomAttributesCacheGenerator,
	LeanManualVelocity2D_t6A1AE638B40E0C4586F4AD7CBD930C1E0B373E1F_CustomAttributesCacheGenerator,
	LeanOrbit_tACFA62F4301E6D69C25B717E2766508BFB1EF3B6_CustomAttributesCacheGenerator,
	LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator,
	LeanPitchYawAutoRotate_t6FC496B40CE91A09B6C1A024F744C2866576281F_CustomAttributesCacheGenerator,
	LeanRandomEvents_t6261D8ADDF7F1F1A8FBF4BF5980BD31E4DAAAFEB_CustomAttributesCacheGenerator,
	LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator,
	LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator,
	LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator,
	LeanRotateToRigidbody2D_t595746BE51910F0F84039D182A6BDDE54E8EFB78_CustomAttributesCacheGenerator,
	LeanSelected_t5D44E49D01845676F7183B78AFC9923E510086B5_CustomAttributesCacheGenerator,
	LeanSelectedCount_tFD93677E60574A08C3323E2C222FD04FB4681DB7_CustomAttributesCacheGenerator,
	LeanSelectedRatio_t7EC2D7A7798F11697B26C4ED8322B39A89827BB7_CustomAttributesCacheGenerator,
	LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator,
	LeanSpawnBetween_t8D31F619002267293E9E0821E1B08F7611CA7F95_CustomAttributesCacheGenerator,
	LeanSwap_t30422744E12ADFDC5B9BD66AA6A23C21FF1F235E_CustomAttributesCacheGenerator,
	LeanThresholdDelta_tC501FF25BEF5189142AB9539C650327A7452B2A8_CustomAttributesCacheGenerator,
	LeanThresholdPosition_tE1AC7858685942EAD5CFF40ABD9FAF1BD0D7D798_CustomAttributesCacheGenerator,
	LeanValue_t76626A54AAE9337498D0FF7DDB0E118C2C90652E_CustomAttributesCacheGenerator,
	LeanLoadScene_t63BB7AF22BA3D3D658E7E4FDCE6531ABC694564C_CustomAttributesCacheGenerator_sceneName,
	LeanLoadScene_t63BB7AF22BA3D3D658E7E4FDCE6531ABC694564C_CustomAttributesCacheGenerator_aSync,
	LeanLoadScene_t63BB7AF22BA3D3D658E7E4FDCE6531ABC694564C_CustomAttributesCacheGenerator_additive,
	LeanPongBall_tF7ECBAAAD1BEC318CAC03807FE3A792F0D2E98AC_CustomAttributesCacheGenerator_startSpeed,
	LeanPongBall_tF7ECBAAAD1BEC318CAC03807FE3A792F0D2E98AC_CustomAttributesCacheGenerator_spread,
	LeanPongBall_tF7ECBAAAD1BEC318CAC03807FE3A792F0D2E98AC_CustomAttributesCacheGenerator_acceleration,
	LeanPongBall_tF7ECBAAAD1BEC318CAC03807FE3A792F0D2E98AC_CustomAttributesCacheGenerator_bounds,
	LeanSelectedString_t9F4A05B05FC48B3529E12F9FD950430A3C2806F3_CustomAttributesCacheGenerator_format,
	LeanSelectedString_t9F4A05B05FC48B3529E12F9FD950430A3C2806F3_CustomAttributesCacheGenerator_onString,
	LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_destination,
	LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_destinationOffset,
	LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_position,
	LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_offset,
	LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_damping,
	LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_linear,
	LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_ignoreZ,
	LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_continuous,
	LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_setPositionOnStart,
	LeanChaseRigidbody2D_t89C4D24B703A3703CD2E939C3E50026623642D3E_CustomAttributesCacheGenerator_axis,
	LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_x,
	LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_xMin,
	LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_xMax,
	LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_y,
	LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_yMin,
	LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_yMax,
	LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_z,
	LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_zMin,
	LeanConstrainLocalPosition_tC67B593AE8EEC1B3C82B369AC985A0561EA6491C_CustomAttributesCacheGenerator_zMax,
	LeanConstrainScale_tF38E12B75C25DE21D131F8BFE531186D032BAC84_CustomAttributesCacheGenerator_minimum,
	LeanConstrainScale_tF38E12B75C25DE21D131F8BFE531186D032BAC84_CustomAttributesCacheGenerator_minimumScale,
	LeanConstrainScale_tF38E12B75C25DE21D131F8BFE531186D032BAC84_CustomAttributesCacheGenerator_maximum,
	LeanConstrainScale_tF38E12B75C25DE21D131F8BFE531186D032BAC84_CustomAttributesCacheGenerator_maximumScale,
	LeanConstrainToAxis_t34EF2D644F2D7AB276B4759EC3342FA8334EE6AD_CustomAttributesCacheGenerator_relativeTo,
	LeanConstrainToAxis_t34EF2D644F2D7AB276B4759EC3342FA8334EE6AD_CustomAttributesCacheGenerator_axis,
	LeanConstrainToAxis_t34EF2D644F2D7AB276B4759EC3342FA8334EE6AD_CustomAttributesCacheGenerator_minimum,
	LeanConstrainToAxis_t34EF2D644F2D7AB276B4759EC3342FA8334EE6AD_CustomAttributesCacheGenerator_maximum,
	LeanConstrainToBox_t2928F56A51BBF4D022A0CC2B12F5A83232589DAA_CustomAttributesCacheGenerator_relativeTo,
	LeanConstrainToBox_t2928F56A51BBF4D022A0CC2B12F5A83232589DAA_CustomAttributesCacheGenerator_size,
	LeanConstrainToBox_t2928F56A51BBF4D022A0CC2B12F5A83232589DAA_CustomAttributesCacheGenerator_center,
	LeanConstrainToCollider_t793183BFE2F2F4019D48AB3740BB4FCF2ED1851A_CustomAttributesCacheGenerator__collider,
	LeanConstrainToColliders_tDC235CBFF2CFB58D7DED421EB9C65B2A33BA777D_CustomAttributesCacheGenerator_colliders,
	LeanConstrainToDirection_t4663FC584523F0D310959009BADF0D86DB6125AD_CustomAttributesCacheGenerator_forward,
	LeanConstrainToDirection_t4663FC584523F0D310959009BADF0D86DB6125AD_CustomAttributesCacheGenerator_direction,
	LeanConstrainToDirection_t4663FC584523F0D310959009BADF0D86DB6125AD_CustomAttributesCacheGenerator_relativeTo,
	LeanConstrainToDirection_t4663FC584523F0D310959009BADF0D86DB6125AD_CustomAttributesCacheGenerator_minAngle,
	LeanConstrainToDirection_t4663FC584523F0D310959009BADF0D86DB6125AD_CustomAttributesCacheGenerator_maxAngle,
	LeanConstrainToOrthographic_t00ABF9C53E05BB000E15B5484D62889259A15107_CustomAttributesCacheGenerator__camera,
	LeanConstrainToOrthographic_t00ABF9C53E05BB000E15B5484D62889259A15107_CustomAttributesCacheGenerator_plane,
	LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator_delay,
	LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator_autoClear,
	LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator_onValueX,
	LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator_onValueY,
	LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator_onValueZ,
	LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator_onValueXY,
	LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator_onValueXYZ,
	LeanDelayedValue_t433D3352E96987B4A5AEEB2DB7FA14D92F893D78_CustomAttributesCacheGenerator_snapshots,
	LeanDeselected_t30428A103BF20EC13D602A4E74F257375173A224_CustomAttributesCacheGenerator_onSelectable,
	LeanFollow_t8123D50E20D43EF755127A1EDE5003DECC36DC35_CustomAttributesCacheGenerator_threshold,
	LeanFollow_t8123D50E20D43EF755127A1EDE5003DECC36DC35_CustomAttributesCacheGenerator_speed,
	LeanFollow_t8123D50E20D43EF755127A1EDE5003DECC36DC35_CustomAttributesCacheGenerator_onReachedDestination,
	LeanFollow_t8123D50E20D43EF755127A1EDE5003DECC36DC35_CustomAttributesCacheGenerator_positions,
	LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_direction,
	LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_directionSpace,
	LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_distance,
	LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_damping,
	LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_clamp,
	LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_clampMin,
	LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_clampMax,
	LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_collisionLayers,
	LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_collisionRadius,
	LeanMaintainDistance_tFA0B723AA70F9B60A8CE6077C66ECF6B05A6093F_CustomAttributesCacheGenerator_currentDistance,
	LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_target,
	LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_axesA,
	LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_axesB,
	LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_multiplier,
	LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_damping,
	LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_scaleByTime,
	LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_defaultScale,
	LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_remainingDelta,
	LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_target,
	LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_space,
	LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_axisA,
	LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_axisB,
	LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_multiplier,
	LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_damping,
	LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_scaleByTime,
	LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_defaultRotation,
	LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_remainingDelta,
	LeanManualTorque_tF64938201C50DB9084FBE808EF224B4909038F5F_CustomAttributesCacheGenerator_target,
	LeanManualTorque_tF64938201C50DB9084FBE808EF224B4909038F5F_CustomAttributesCacheGenerator_mode,
	LeanManualTorque_tF64938201C50DB9084FBE808EF224B4909038F5F_CustomAttributesCacheGenerator_multiplier,
	LeanManualTorque_tF64938201C50DB9084FBE808EF224B4909038F5F_CustomAttributesCacheGenerator_space,
	LeanManualTorque_tF64938201C50DB9084FBE808EF224B4909038F5F_CustomAttributesCacheGenerator_axisA,
	LeanManualTorque_tF64938201C50DB9084FBE808EF224B4909038F5F_CustomAttributesCacheGenerator_axisB,
	LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_target,
	LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_space,
	LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_directionA,
	LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_directionB,
	LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_multiplier,
	LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_damping,
	LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_scaleByTime,
	LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_defaultPosition,
	LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_remainingDelta,
	LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_target,
	LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_space,
	LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_directionA,
	LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_directionB,
	LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_multiplier,
	LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_damping,
	LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_scaleByTime,
	LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_inertia,
	LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_resetVelocityInUpdate,
	LeanManualTranslateRigidbody_t63E9FC32362AD28A8E77A2BB0BA8B62ADF811E9C_CustomAttributesCacheGenerator_remainingDelta,
	LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_target,
	LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_space,
	LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_directionA,
	LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_directionB,
	LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_multiplier,
	LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_scaleByTime,
	LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_damping,
	LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_resetVelocityInUpdate,
	LeanManualTranslateRigidbody2D_tF319358B17DF1F3B4C72F9E4189A8EF47EA4F47D_CustomAttributesCacheGenerator_remainingDelta,
	LeanManualVelocity_tE43CE9F4BC353A36B1896332F335E70C48EE1F68_CustomAttributesCacheGenerator_target,
	LeanManualVelocity_tE43CE9F4BC353A36B1896332F335E70C48EE1F68_CustomAttributesCacheGenerator_mode,
	LeanManualVelocity_tE43CE9F4BC353A36B1896332F335E70C48EE1F68_CustomAttributesCacheGenerator_multiplier,
	LeanManualVelocity_tE43CE9F4BC353A36B1896332F335E70C48EE1F68_CustomAttributesCacheGenerator_space,
	LeanManualVelocity_tE43CE9F4BC353A36B1896332F335E70C48EE1F68_CustomAttributesCacheGenerator_directionA,
	LeanManualVelocity_tE43CE9F4BC353A36B1896332F335E70C48EE1F68_CustomAttributesCacheGenerator_directionB,
	LeanManualVelocity2D_t6A1AE638B40E0C4586F4AD7CBD930C1E0B373E1F_CustomAttributesCacheGenerator_target,
	LeanManualVelocity2D_t6A1AE638B40E0C4586F4AD7CBD930C1E0B373E1F_CustomAttributesCacheGenerator_mode,
	LeanManualVelocity2D_t6A1AE638B40E0C4586F4AD7CBD930C1E0B373E1F_CustomAttributesCacheGenerator_multiplier,
	LeanManualVelocity2D_t6A1AE638B40E0C4586F4AD7CBD930C1E0B373E1F_CustomAttributesCacheGenerator_space,
	LeanManualVelocity2D_t6A1AE638B40E0C4586F4AD7CBD930C1E0B373E1F_CustomAttributesCacheGenerator_directionA,
	LeanManualVelocity2D_t6A1AE638B40E0C4586F4AD7CBD930C1E0B373E1F_CustomAttributesCacheGenerator_directionB,
	LeanOrbit_tACFA62F4301E6D69C25B717E2766508BFB1EF3B6_CustomAttributesCacheGenerator__camera,
	LeanOrbit_tACFA62F4301E6D69C25B717E2766508BFB1EF3B6_CustomAttributesCacheGenerator_pivot,
	LeanOrbit_tACFA62F4301E6D69C25B717E2766508BFB1EF3B6_CustomAttributesCacheGenerator_damping,
	LeanOrbit_tACFA62F4301E6D69C25B717E2766508BFB1EF3B6_CustomAttributesCacheGenerator_pitchSensitivity,
	LeanOrbit_tACFA62F4301E6D69C25B717E2766508BFB1EF3B6_CustomAttributesCacheGenerator_yawSensitivity,
	LeanOrbit_tACFA62F4301E6D69C25B717E2766508BFB1EF3B6_CustomAttributesCacheGenerator_remainingDelta,
	LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator__camera,
	LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_damping,
	LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_defaultRotation,
	LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_pitch,
	LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_pitchSensitivity,
	LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_pitchClamp,
	LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_pitchMin,
	LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_pitchMax,
	LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_yaw,
	LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_yawSensitivity,
	LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_yawClamp,
	LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_yawMin,
	LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_yawMax,
	LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_currentPitch,
	LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_currentYaw,
	LeanPitchYawAutoRotate_t6FC496B40CE91A09B6C1A024F744C2866576281F_CustomAttributesCacheGenerator_delay,
	LeanPitchYawAutoRotate_t6FC496B40CE91A09B6C1A024F744C2866576281F_CustomAttributesCacheGenerator_speed,
	LeanPitchYawAutoRotate_t6FC496B40CE91A09B6C1A024F744C2866576281F_CustomAttributesCacheGenerator_acceleration,
	LeanPitchYawAutoRotate_t6FC496B40CE91A09B6C1A024F744C2866576281F_CustomAttributesCacheGenerator_idleTime,
	LeanPitchYawAutoRotate_t6FC496B40CE91A09B6C1A024F744C2866576281F_CustomAttributesCacheGenerator_strength,
	LeanPitchYawAutoRotate_t6FC496B40CE91A09B6C1A024F744C2866576281F_CustomAttributesCacheGenerator_expectedPitch,
	LeanPitchYawAutoRotate_t6FC496B40CE91A09B6C1A024F744C2866576281F_CustomAttributesCacheGenerator_expectedYaw,
	LeanRandomEvents_t6261D8ADDF7F1F1A8FBF4BF5980BD31E4DAAAFEB_CustomAttributesCacheGenerator_events,
	LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_oldMin,
	LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_oldMax,
	LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_newMin,
	LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_newMax,
	LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_onValueX,
	LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_onValueY,
	LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_onValueZ,
	LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_onValueXY,
	LeanRemapValue_t0A9332448F5C7A0EB6A2E4712CC29A58D7F03D3A_CustomAttributesCacheGenerator_onValueXYZ,
	LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_damping,
	LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_recordOnStart,
	LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_revertPosition,
	LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_revertRotation,
	LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_revertScale,
	LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_thresholdPosition,
	LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_thresholdRotation,
	LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_thresholdScale,
	LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_targetPosition,
	LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_targetRotation,
	LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_targetScale,
	LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_reverting,
	LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator_target,
	LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator_position,
	LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator_threshold,
	LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator_invert,
	LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator_rotateTo,
	LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator_damping,
	LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator_previousPosition,
	LeanRotateToPosition_tF31BBE30C6FAEB15A70B2B6514FE1FC1DBE88E73_CustomAttributesCacheGenerator_previousDelta,
	LeanRotateToRigidbody2D_t595746BE51910F0F84039D182A6BDDE54E8EFB78_CustomAttributesCacheGenerator_damping,
	LeanRotateToRigidbody2D_t595746BE51910F0F84039D182A6BDDE54E8EFB78_CustomAttributesCacheGenerator_previousPosition,
	LeanRotateToRigidbody2D_t595746BE51910F0F84039D182A6BDDE54E8EFB78_CustomAttributesCacheGenerator_vector,
	LeanSelected_t5D44E49D01845676F7183B78AFC9923E510086B5_CustomAttributesCacheGenerator_onSelectable,
	LeanSelectedCount_tFD93677E60574A08C3323E2C222FD04FB4681DB7_CustomAttributesCacheGenerator_onCount,
	LeanSelectedCount_tFD93677E60574A08C3323E2C222FD04FB4681DB7_CustomAttributesCacheGenerator_matchMin,
	LeanSelectedCount_tFD93677E60574A08C3323E2C222FD04FB4681DB7_CustomAttributesCacheGenerator_matchMax,
	LeanSelectedCount_tFD93677E60574A08C3323E2C222FD04FB4681DB7_CustomAttributesCacheGenerator_onMatch,
	LeanSelectedCount_tFD93677E60574A08C3323E2C222FD04FB4681DB7_CustomAttributesCacheGenerator_onUnmatch,
	LeanSelectedCount_tFD93677E60574A08C3323E2C222FD04FB4681DB7_CustomAttributesCacheGenerator_matched,
	LeanSelectedRatio_t7EC2D7A7798F11697B26C4ED8322B39A89827BB7_CustomAttributesCacheGenerator_inverse,
	LeanSelectedRatio_t7EC2D7A7798F11697B26C4ED8322B39A89827BB7_CustomAttributesCacheGenerator_onRatio,
	LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_damping,
	LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_threshold,
	LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_autoStop,
	LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_onValueX,
	LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_onValueY,
	LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_onValueZ,
	LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_onValueXY,
	LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_onValueXYZ,
	LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_currentValue,
	LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_targetValue,
	LeanSmoothedValue_tDE615CFEE6305B25F20AEB85DAD0456A5024F158_CustomAttributesCacheGenerator_targetSet,
	LeanSpawnBetween_t8D31F619002267293E9E0821E1B08F7611CA7F95_CustomAttributesCacheGenerator_prefab,
	LeanSpawnBetween_t8D31F619002267293E9E0821E1B08F7611CA7F95_CustomAttributesCacheGenerator_velocityMultiplier,
	LeanSpawnBetween_t8D31F619002267293E9E0821E1B08F7611CA7F95_CustomAttributesCacheGenerator_velocityMin,
	LeanSpawnBetween_t8D31F619002267293E9E0821E1B08F7611CA7F95_CustomAttributesCacheGenerator_velocityMax,
	LeanSwap_t30422744E12ADFDC5B9BD66AA6A23C21FF1F235E_CustomAttributesCacheGenerator_index,
	LeanSwap_t30422744E12ADFDC5B9BD66AA6A23C21FF1F235E_CustomAttributesCacheGenerator_prefabs,
	LeanSwap_t30422744E12ADFDC5B9BD66AA6A23C21FF1F235E_CustomAttributesCacheGenerator_clone,
	LeanSwap_t30422744E12ADFDC5B9BD66AA6A23C21FF1F235E_CustomAttributesCacheGenerator_clonePrefab,
	LeanThresholdDelta_tC501FF25BEF5189142AB9539C650327A7452B2A8_CustomAttributesCacheGenerator_onDeltaX,
	LeanThresholdDelta_tC501FF25BEF5189142AB9539C650327A7452B2A8_CustomAttributesCacheGenerator_onDeltaY,
	LeanThresholdDelta_tC501FF25BEF5189142AB9539C650327A7452B2A8_CustomAttributesCacheGenerator_onDeltaZ,
	LeanThresholdDelta_tC501FF25BEF5189142AB9539C650327A7452B2A8_CustomAttributesCacheGenerator_onDeltaXY,
	LeanThresholdDelta_tC501FF25BEF5189142AB9539C650327A7452B2A8_CustomAttributesCacheGenerator_onDeltaXYZ,
	LeanThresholdPosition_tE1AC7858685942EAD5CFF40ABD9FAF1BD0D7D798_CustomAttributesCacheGenerator_onPositionX,
	LeanThresholdPosition_tE1AC7858685942EAD5CFF40ABD9FAF1BD0D7D798_CustomAttributesCacheGenerator_onPositionY,
	LeanThresholdPosition_tE1AC7858685942EAD5CFF40ABD9FAF1BD0D7D798_CustomAttributesCacheGenerator_onPositionZ,
	LeanThresholdPosition_tE1AC7858685942EAD5CFF40ABD9FAF1BD0D7D798_CustomAttributesCacheGenerator_onPositionXY,
	LeanThresholdPosition_tE1AC7858685942EAD5CFF40ABD9FAF1BD0D7D798_CustomAttributesCacheGenerator_onPositionXYZ,
	LeanValue_t76626A54AAE9337498D0FF7DDB0E118C2C90652E_CustomAttributesCacheGenerator_onValueX,
	LeanValue_t76626A54AAE9337498D0FF7DDB0E118C2C90652E_CustomAttributesCacheGenerator_onValueY,
	LeanValue_t76626A54AAE9337498D0FF7DDB0E118C2C90652E_CustomAttributesCacheGenerator_onValueZ,
	LeanValue_t76626A54AAE9337498D0FF7DDB0E118C2C90652E_CustomAttributesCacheGenerator_onValueXY,
	LeanValue_t76626A54AAE9337498D0FF7DDB0E118C2C90652E_CustomAttributesCacheGenerator_onValueXYZ,
	LeanValue_t76626A54AAE9337498D0FF7DDB0E118C2C90652E_CustomAttributesCacheGenerator_current,
	LeanLoadScene_t63BB7AF22BA3D3D658E7E4FDCE6531ABC694564C_CustomAttributesCacheGenerator_LeanLoadScene_Load_mF187C0667FAA85B99C8FF3201D6C0B5AA34D5F8C,
	LeanSelectedString_t9F4A05B05FC48B3529E12F9FD950430A3C2806F3_CustomAttributesCacheGenerator_LeanSelectedString_UpdateNow_m1BC9220D2DE3F693201134E1F6064E32653ED8D1,
	LeanChase_tBD0C5CC24FA6451A6B15AEC520D894DBDF0BB33A_CustomAttributesCacheGenerator_LeanChase_SnapToPosition_m57CB8EEDB18D3532BC7E19A220315C87BE77F76D,
	LeanFollow_t8123D50E20D43EF755127A1EDE5003DECC36DC35_CustomAttributesCacheGenerator_LeanFollow_ClearPositions_m485D7C6E05A7854AB051A25D237388886E716E8C,
	LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_LeanManualRescale_ResetScale_m73FDFECE254BA9F6FEC221B9415F56013705C68F,
	LeanManualRescale_tC8156A62FA4B3BBE0538BD49B93811A31214A88E_CustomAttributesCacheGenerator_LeanManualRescale_SnapToTarget_m9878D0C99CFF0AF6D57525E981F88E00B02C0AE4,
	LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_LeanManualRotate_ResetRotation_m90126CB30981A7298DA85A664644C7850E10DC8B,
	LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_LeanManualRotate_SnapToTarget_m0F590A6B43C6D3760E4BEF4A975054D80AE2338F,
	LeanManualRotate_tE964510CAFA1962DFF37577D8C9B226CAD4ED66C_CustomAttributesCacheGenerator_LeanManualRotate_StopRotation_m9E83B607C247214965CD067ACFC18D71F25A75D0,
	LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_LeanManualTranslate_ResetPosition_m96A8915E69D6DCF055E70D2C67B37035BEEE89E4,
	LeanManualTranslate_tB5249FAA3C78817AA9C39539EEF0D344AE4D0444_CustomAttributesCacheGenerator_LeanManualTranslate_SnapToTarget_m1EC30AA794459BDEC88980D4DC737EF87868F7B8,
	LeanPitchYaw_tCA9508FAF72FC4A57EA85CC1BB87472F461EA38E_CustomAttributesCacheGenerator_LeanPitchYaw_ResetRotation_m223BD5FE2B0466FEB5871EBC174A3075CF872B25,
	LeanRandomEvents_t6261D8ADDF7F1F1A8FBF4BF5980BD31E4DAAAFEB_CustomAttributesCacheGenerator_LeanRandomEvents_Invoke_mCEA3D4832552ACC6AAF1895C8DADF995E45CF735,
	LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_LeanRevertTransform_Revert_m5348B2D6D10C01686894612A2DEBA6DE904E53B3,
	LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_LeanRevertTransform_StopRevert_mE0BF7A16895C20C0F4657E03A90EC26975250020,
	LeanRevertTransform_t460AE501E57DDBD411F872DDE1DB97874527D950_CustomAttributesCacheGenerator_LeanRevertTransform_RecordTransform_mDEA71175D5321AFBA577541B88E38284D039D961,
	LeanSelectedRatio_t7EC2D7A7798F11697B26C4ED8322B39A89827BB7_CustomAttributesCacheGenerator_LeanSelectedRatio_UpdateNow_m6CC9B5812A2A7DFC348E8D01A4FB1C1AA5EE53EE,
	LeanSwap_t30422744E12ADFDC5B9BD66AA6A23C21FF1F235E_CustomAttributesCacheGenerator_LeanSwap_UpdateSwap_m3DB8B10CDCF43394B088F0C3AAA33CAE08F1C3BE,
	LeanSwap_t30422744E12ADFDC5B9BD66AA6A23C21FF1F235E_CustomAttributesCacheGenerator_LeanSwap_SwapToPrevious_mB1DB144F6390035AF0AF874F8C5C810FFF496F2B,
	LeanSwap_t30422744E12ADFDC5B9BD66AA6A23C21FF1F235E_CustomAttributesCacheGenerator_LeanSwap_SwapToNext_m1476858973D0B0AAE9F539A24E0F0CD036367F6A,
	LeanCommonPlus_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
