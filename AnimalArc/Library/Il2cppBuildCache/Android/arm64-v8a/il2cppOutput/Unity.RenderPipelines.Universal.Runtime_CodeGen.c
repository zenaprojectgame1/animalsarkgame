﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_m780BE61DB0EBDDD1D1E95E31CA1F37AD542CDA70 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_m8BE7F8142367305750E8134F9E91ACB134AF5361 (void);
// 0x00000003 System.Void UnityEngine.Rendering.Universal.XRSystemData::.ctor()
extern void XRSystemData__ctor_m6E39CCFB35325FC814C589BEE10B04794E187774 (void);
// 0x00000004 System.Void UnityEngine.Rendering.Universal.XRSystemData/ShaderResources::.ctor()
extern void ShaderResources__ctor_m35E93A20CFB6037D4AE810D25EC29BAF506BCD04 (void);
// 0x00000005 System.Void UnityEngine.Rendering.Universal.ScriptableRenderer::.cctor()
extern void ScriptableRenderer__cctor_m0470CA064570E7675DBBF61BFAF1B91AFA8D51C3 (void);
// 0x00000006 System.Void UnityEngine.Rendering.Universal.ShaderPropertyId::.cctor()
extern void ShaderPropertyId__cctor_mA37312BCBED320DA89B602712197A2C596431789 (void);
// 0x00000007 System.Void UnityEngine.Rendering.Universal.XRView::.ctor(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4,UnityEngine.Rect,System.Int32)
extern void XRView__ctor_m26697ACC489FE20A5ABE57491F5266BBEAD8DA61 (void);
// 0x00000008 System.Void UnityEngine.Rendering.Universal.XRView::.ctor(UnityEngine.XR.XRDisplaySubsystem/XRRenderPass,UnityEngine.XR.XRDisplaySubsystem/XRRenderParameter)
extern void XRView__ctor_mBD2BA2F6548FD9780A2EA656E186BE6AB02FA411 (void);
// 0x00000009 System.Boolean UnityEngine.Rendering.Universal.XRPass::get_enabled()
extern void XRPass_get_enabled_m29320181846F74E0DCF77D4D897479362D2273CA (void);
// 0x0000000A System.Boolean UnityEngine.Rendering.Universal.XRPass::get_xrSdkEnabled()
extern void XRPass_get_xrSdkEnabled_m382436041060A68DD4DCC122B48E4F8BEDC1EB64 (void);
// 0x0000000B System.Void UnityEngine.Rendering.Universal.XRPass::set_xrSdkEnabled(System.Boolean)
extern void XRPass_set_xrSdkEnabled_m6C320B4690D3C05225AC8A456AA1DB38CCECDB16 (void);
// 0x0000000C System.Void UnityEngine.Rendering.Universal.XRPass::set_copyDepth(System.Boolean)
extern void XRPass_set_copyDepth_mABAD1DE38CB40CFB7971ED1D5A507FB046BFAECE (void);
// 0x0000000D System.Int32 UnityEngine.Rendering.Universal.XRPass::get_multipassId()
extern void XRPass_get_multipassId_mD4A8E396F5B8E3EA3E5FA642A95948614CB19C1D (void);
// 0x0000000E System.Void UnityEngine.Rendering.Universal.XRPass::set_multipassId(System.Int32)
extern void XRPass_set_multipassId_mBF5A78EF00B1DEB40F3315D446BFDA15B5677F07 (void);
// 0x0000000F System.Void UnityEngine.Rendering.Universal.XRPass::set_cullingPassId(System.Int32)
extern void XRPass_set_cullingPassId_mA32F73AD044A828C56DE57E15537AA05B2698A90 (void);
// 0x00000010 System.Void UnityEngine.Rendering.Universal.XRPass::set_renderTarget(UnityEngine.Rendering.RenderTargetIdentifier)
extern void XRPass_set_renderTarget_m528BDC7E91273283D86C9F1543CCE551E09F0A29 (void);
// 0x00000011 UnityEngine.RenderTextureDescriptor UnityEngine.Rendering.Universal.XRPass::get_renderTargetDesc()
extern void XRPass_get_renderTargetDesc_m0E4F094BC06E1AEBBEA885532FCBD3E088B51DC4 (void);
// 0x00000012 System.Void UnityEngine.Rendering.Universal.XRPass::set_renderTargetDesc(UnityEngine.RenderTextureDescriptor)
extern void XRPass_set_renderTargetDesc_mB21461AD204A490E0568E8E126590F59DF6DB07A (void);
// 0x00000013 System.Void UnityEngine.Rendering.Universal.XRPass::set_renderTargetIsRenderTexture(System.Boolean)
extern void XRPass_set_renderTargetIsRenderTexture_m93DAA821EB43DAC5BEC6E454B5E995B3F2147393 (void);
// 0x00000014 UnityEngine.Rect UnityEngine.Rendering.Universal.XRPass::GetViewport(System.Int32)
extern void XRPass_GetViewport_m86B907AD139BF56872DB6826CF0C981A344BB4D5 (void);
// 0x00000015 System.Void UnityEngine.Rendering.Universal.XRPass::set_cullingParams(UnityEngine.Rendering.ScriptableCullingParameters)
extern void XRPass_set_cullingParams_m22E55420DE3E4F12CCD9E486B6451DE18E44D72D (void);
// 0x00000016 System.Int32 UnityEngine.Rendering.Universal.XRPass::get_viewCount()
extern void XRPass_get_viewCount_m057B8D5B4F9E1945FAAB5E4F02A9966CEE36F297 (void);
// 0x00000017 System.Boolean UnityEngine.Rendering.Universal.XRPass::get_singlePassEnabled()
extern void XRPass_get_singlePassEnabled_mE9467F40CAF6B34B853C1D352FF7256E1F762FC2 (void);
// 0x00000018 System.Boolean UnityEngine.Rendering.Universal.XRPass::get_isOcclusionMeshSupported()
extern void XRPass_get_isOcclusionMeshSupported_m3D4CE92972FA8DB21FD2EBF02CEBF687AE828853 (void);
// 0x00000019 System.Void UnityEngine.Rendering.Universal.XRPass::UpdateView(System.Int32,UnityEngine.XR.XRDisplaySubsystem/XRRenderPass,UnityEngine.XR.XRDisplaySubsystem/XRRenderParameter)
extern void XRPass_UpdateView_m0818F95E7AF7C1763B807C508F7095D3348D7E20 (void);
// 0x0000001A System.Void UnityEngine.Rendering.Universal.XRPass::UpdateView(System.Int32,UnityEngine.Matrix4x4,UnityEngine.Matrix4x4,UnityEngine.Rect,System.Int32)
extern void XRPass_UpdateView_m236A460DEDE22C366FCAF5BFF2EEA4BFD6E27EBE (void);
// 0x0000001B System.Void UnityEngine.Rendering.Universal.XRPass::UpdateCullingParams(System.Int32,UnityEngine.Rendering.ScriptableCullingParameters)
extern void XRPass_UpdateCullingParams_mA7D560B027CD211FC86B4419BED46010489DF06A (void);
// 0x0000001C UnityEngine.Rendering.Universal.XRPass UnityEngine.Rendering.Universal.XRPass::Create(UnityEngine.XR.XRDisplaySubsystem/XRRenderPass,System.Int32,UnityEngine.Rendering.ScriptableCullingParameters,UnityEngine.Material)
extern void XRPass_Create_mB49E75C77DF62A1DFD8FAEDB2CDC924739EC802C (void);
// 0x0000001D System.Void UnityEngine.Rendering.Universal.XRPass::AddView(UnityEngine.XR.XRDisplaySubsystem/XRRenderPass,UnityEngine.XR.XRDisplaySubsystem/XRRenderParameter)
extern void XRPass_AddView_m4FB7F1B1148E42FD55AEDBF364FA7600C8BA6DDC (void);
// 0x0000001E System.Void UnityEngine.Rendering.Universal.XRPass::Release(UnityEngine.Rendering.Universal.XRPass)
extern void XRPass_Release_m2B844EE45248EB9AF52CF82EAFC15E15A92D44FB (void);
// 0x0000001F System.Void UnityEngine.Rendering.Universal.XRPass::AddViewInternal(UnityEngine.Rendering.Universal.XRView)
extern void XRPass_AddViewInternal_m61BD6957BF5925AD65CA954BB04385BDF4D42047 (void);
// 0x00000020 System.Void UnityEngine.Rendering.Universal.XRPass::UpdateOcclusionMesh()
extern void XRPass_UpdateOcclusionMesh_m2288932ABD0F00CE1DDE099452CD19540687B433 (void);
// 0x00000021 System.Boolean UnityEngine.Rendering.Universal.XRPass::TryGetOcclusionMeshCombinedHashCode(System.Int32&)
extern void XRPass_TryGetOcclusionMeshCombinedHashCode_m667C57572358A2527469A4986F9EDCC37BFB28DE (void);
// 0x00000022 System.Void UnityEngine.Rendering.Universal.XRPass::CreateOcclusionMeshCombined()
extern void XRPass_CreateOcclusionMeshCombined_mF3DBCA8C5D4A7A6577099DCF5739C62CCC4FF534 (void);
// 0x00000023 System.Void UnityEngine.Rendering.Universal.XRPass::.ctor()
extern void XRPass__ctor_mCA55DA5C03B0479EE54DCA7EA94E5FF36FED7732 (void);
// 0x00000024 System.Void UnityEngine.Rendering.Universal.XRPass::.cctor()
extern void XRPass__cctor_mD947D8A47464164323DF9ADE78C89E2811CBAD9D (void);
// 0x00000025 System.Void UnityEngine.Rendering.Universal.XRPass/CustomMirrorView::.ctor(System.Object,System.IntPtr)
extern void CustomMirrorView__ctor_m475059281857B20C7CE15BAC68A40B79D3AB0230 (void);
// 0x00000026 System.Void UnityEngine.Rendering.Universal.XRPass/CustomMirrorView::Invoke(UnityEngine.Rendering.Universal.XRPass,UnityEngine.Rendering.CommandBuffer,UnityEngine.RenderTexture,UnityEngine.Rect)
extern void CustomMirrorView_Invoke_m3D97B0AEE1612273019D50A80D4C74D6D8CDD09B (void);
// 0x00000027 System.IAsyncResult UnityEngine.Rendering.Universal.XRPass/CustomMirrorView::BeginInvoke(UnityEngine.Rendering.Universal.XRPass,UnityEngine.Rendering.CommandBuffer,UnityEngine.RenderTexture,UnityEngine.Rect,System.AsyncCallback,System.Object)
extern void CustomMirrorView_BeginInvoke_mDD89A15673A2E48EDC69BA2829E7B39F3B703637 (void);
// 0x00000028 System.Void UnityEngine.Rendering.Universal.XRPass/CustomMirrorView::EndInvoke(System.IAsyncResult)
extern void CustomMirrorView_EndInvoke_mD403A4F005650120E0A6D12C44B3BA44E5F38C35 (void);
// 0x00000029 System.Void UnityEngine.Rendering.Universal.XRSystem::.ctor()
extern void XRSystem__ctor_mA584B8E34EDA1F815E22DDB15AEE3D0DF1C27278 (void);
// 0x0000002A System.Void UnityEngine.Rendering.Universal.XRSystem::InitializeXRSystemData(UnityEngine.Rendering.Universal.XRSystemData)
extern void XRSystem_InitializeXRSystemData_m8FF5784CC5608FB7A9CACF6DF2A0DA7CA70E7B18 (void);
// 0x0000002B System.Void UnityEngine.Rendering.Universal.XRSystem::GetDisplaySubsystem()
extern void XRSystem_GetDisplaySubsystem_m1176C00E95F9FCE7674FC344CB2EB7B8942CF91E (void);
// 0x0000002C System.Void UnityEngine.Rendering.Universal.XRSystem::XRSystemInit()
extern void XRSystem_XRSystemInit_m5A01FCB59B5AF7FD207E117C9BEEA5C723685DD4 (void);
// 0x0000002D System.Void UnityEngine.Rendering.Universal.XRSystem::UpdateMSAALevel(System.Int32)
extern void XRSystem_UpdateMSAALevel_m37D6C30265B675A37895D8A4A81F6750AA86C2EC (void);
// 0x0000002E System.Int32 UnityEngine.Rendering.Universal.XRSystem::GetMSAALevel()
extern void XRSystem_GetMSAALevel_m7B1213F7688F34D95D65A5AD8CE2DAA261C9B42D (void);
// 0x0000002F System.Void UnityEngine.Rendering.Universal.XRSystem::UpdateRenderScale(System.Single)
extern void XRSystem_UpdateRenderScale_m4456D89C0E7CD079B68BF564DD8ABFD170F0C6A3 (void);
// 0x00000030 System.Int32 UnityEngine.Rendering.Universal.XRSystem::GetMaxViews()
extern void XRSystem_GetMaxViews_m2F0885AEB08405FB58E24642602B253F7F674B1C (void);
// 0x00000031 System.Collections.Generic.List`1<UnityEngine.Rendering.Universal.XRPass> UnityEngine.Rendering.Universal.XRSystem::SetupFrame(UnityEngine.Rendering.Universal.CameraData)
extern void XRSystem_SetupFrame_m9A10E388C0F65B456AFB4E1E45639F06A835ABB3 (void);
// 0x00000032 System.Void UnityEngine.Rendering.Universal.XRSystem::ReleaseFrame()
extern void XRSystem_ReleaseFrame_mE9046CA23B4BCFDD5DD555748D2ECAC1C30740D8 (void);
// 0x00000033 System.Boolean UnityEngine.Rendering.Universal.XRSystem::RefreshXrSdk()
extern void XRSystem_RefreshXrSdk_m30A7FD07FFC08ED83A1C4D2D59730A79653CFACD (void);
// 0x00000034 System.Void UnityEngine.Rendering.Universal.XRSystem::UpdateCameraData(UnityEngine.Rendering.Universal.CameraData&,UnityEngine.Rendering.Universal.XRPass&)
extern void XRSystem_UpdateCameraData_m31EA9B7F30C04638528251CC518C51063BE43885 (void);
// 0x00000035 System.Void UnityEngine.Rendering.Universal.XRSystem::UpdateFromCamera(UnityEngine.Rendering.Universal.XRPass&,UnityEngine.Rendering.Universal.CameraData)
extern void XRSystem_UpdateFromCamera_m1BEDCEC0DCA1590628665D20BEDB8CB06DD385B8 (void);
// 0x00000036 System.Void UnityEngine.Rendering.Universal.XRSystem::CreateLayoutFromXrSdk(UnityEngine.Camera,System.Boolean)
extern void XRSystem_CreateLayoutFromXrSdk_mD46049C4F7A08CFD6E5C88C5F31058B55F0B4E9B (void);
// 0x00000037 System.Void UnityEngine.Rendering.Universal.XRSystem::Dispose()
extern void XRSystem_Dispose_mEE8DB22C444B1E6F776B834B03647C20DD3AD658 (void);
// 0x00000038 System.Void UnityEngine.Rendering.Universal.XRSystem::AddPassToFrame(UnityEngine.Rendering.Universal.XRPass)
extern void XRSystem_AddPassToFrame_m9E95DC808E61B996871908A4052717D1A665C2D4 (void);
// 0x00000039 System.Void UnityEngine.Rendering.Universal.XRSystem::RenderMirrorView(UnityEngine.Rendering.CommandBuffer,UnityEngine.Camera)
extern void XRSystem_RenderMirrorView_mDDEB09624A85CA2D482CA9960049FD0B54613034 (void);
// 0x0000003A System.Void UnityEngine.Rendering.Universal.XRSystem::.cctor()
extern void XRSystem__cctor_m7B8F02B7D57C203238E42352E4678976010A0BF2 (void);
// 0x0000003B System.Boolean UnityEngine.Rendering.Universal.XRSystem::<CreateLayoutFromXrSdk>g__CanUseSinglePass|24_0(UnityEngine.XR.XRDisplaySubsystem/XRRenderPass,UnityEngine.Rendering.Universal.XRSystem/<>c__DisplayClass24_0&)
extern void XRSystem_U3CCreateLayoutFromXrSdkU3Eg__CanUseSinglePassU7C24_0_mC086FCBD276F4990745BA9168A14F4301F3914A6 (void);
// 0x0000003C System.Void UnityEngine.Rendering.Universal.XRSystem/XRShaderIDs::.cctor()
extern void XRShaderIDs__cctor_mF11B95778345BA7AC7CF34DA2ED6149C2B65CED2 (void);
// 0x0000003D System.Int32 UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::get_assetsPPU()
extern void PixelPerfectCamera_get_assetsPPU_m16C0A1B2A2BB8644A41FC9522A541CBF5A75E445 (void);
// 0x0000003E System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::set_assetsPPU(System.Int32)
extern void PixelPerfectCamera_set_assetsPPU_mE9F840B821EF2C9DC29D86095A4B48D24C02CC55 (void);
// 0x0000003F System.Int32 UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::get_refResolutionX()
extern void PixelPerfectCamera_get_refResolutionX_m0ABC9A98CA6CFDB32E731D1AF178FAD58F6D254E (void);
// 0x00000040 System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::set_refResolutionX(System.Int32)
extern void PixelPerfectCamera_set_refResolutionX_mA639C0DA9757B663124681C51838CEB1F5D1613D (void);
// 0x00000041 System.Int32 UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::get_refResolutionY()
extern void PixelPerfectCamera_get_refResolutionY_m2AE92ADB79340A73D903FC52E047D3FEE8633C0D (void);
// 0x00000042 System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::set_refResolutionY(System.Int32)
extern void PixelPerfectCamera_set_refResolutionY_m5BAC54558D80C76781D0FD6B464005BF164B4C3C (void);
// 0x00000043 System.Boolean UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::get_upscaleRT()
extern void PixelPerfectCamera_get_upscaleRT_mE8C7FC04AE10058C5DD8B8ED681FDAB0DCE01D8C (void);
// 0x00000044 System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::set_upscaleRT(System.Boolean)
extern void PixelPerfectCamera_set_upscaleRT_mF7C93BD41FE1A09A374411FC0DF1ACB0DD1DC4F3 (void);
// 0x00000045 System.Boolean UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::get_pixelSnapping()
extern void PixelPerfectCamera_get_pixelSnapping_mE0D4A93909775D06BFCE02A77B6753E498DA6AFB (void);
// 0x00000046 System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::set_pixelSnapping(System.Boolean)
extern void PixelPerfectCamera_set_pixelSnapping_m0B52DA205E54AA6DB05641997F00F2FBA7C87EB7 (void);
// 0x00000047 System.Boolean UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::get_cropFrameX()
extern void PixelPerfectCamera_get_cropFrameX_mCB0D6314BE8D082BAF0C2AFD7BD8D3EFC067C2A6 (void);
// 0x00000048 System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::set_cropFrameX(System.Boolean)
extern void PixelPerfectCamera_set_cropFrameX_mA275EC21C3AA94B72C15BFF7F807B33D8F5FF65C (void);
// 0x00000049 System.Boolean UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::get_cropFrameY()
extern void PixelPerfectCamera_get_cropFrameY_m99A1B9D17D96011EA6AAFE8442875D3A8B922B25 (void);
// 0x0000004A System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::set_cropFrameY(System.Boolean)
extern void PixelPerfectCamera_set_cropFrameY_mC892DA37BFC3C18E9946447DFD624A2907927F3A (void);
// 0x0000004B System.Boolean UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::get_stretchFill()
extern void PixelPerfectCamera_get_stretchFill_m5A98E0B454DD4201445EC9D8E76E5FB97CFF23FC (void);
// 0x0000004C System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::set_stretchFill(System.Boolean)
extern void PixelPerfectCamera_set_stretchFill_mF0F09EA5081BDFAD029ED0CED016C991B1B81E0F (void);
// 0x0000004D System.Int32 UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::get_pixelRatio()
extern void PixelPerfectCamera_get_pixelRatio_m613231F84624266E0BC7B96EE47992DA8245A70D (void);
// 0x0000004E UnityEngine.Vector3 UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::RoundToPixel(UnityEngine.Vector3)
extern void PixelPerfectCamera_RoundToPixel_mE0918CF4DC93FBDA90676E466850E41B08079D09 (void);
// 0x0000004F System.Single UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::CorrectCinemachineOrthoSize(System.Single)
extern void PixelPerfectCamera_CorrectCinemachineOrthoSize_mBA3786D33A75A187D5024026A8B8270B041FB37E (void);
// 0x00000050 System.Boolean UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::get_isRunning()
extern void PixelPerfectCamera_get_isRunning_mE7966A17634436B98159AE6A5A15F641E923DA9A (void);
// 0x00000051 UnityEngine.FilterMode UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::get_finalBlitFilterMode()
extern void PixelPerfectCamera_get_finalBlitFilterMode_m9A4E8E99011DBD371049362FF4C5839A18119045 (void);
// 0x00000052 UnityEngine.Vector2Int UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::get_offscreenRTSize()
extern void PixelPerfectCamera_get_offscreenRTSize_mCBB9DCC1AAF9BB1DC9BD8E3E5575BACEF01D1BC1 (void);
// 0x00000053 UnityEngine.Vector2Int UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::get_cameraRTSize()
extern void PixelPerfectCamera_get_cameraRTSize_mCABC13C159389FC13C299E63C2801F1DA7DC64D8 (void);
// 0x00000054 System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::PixelSnap()
extern void PixelPerfectCamera_PixelSnap_m8C2227B71DE3F880D3974FCBAAD0FC22D7D602C3 (void);
// 0x00000055 System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::Awake()
extern void PixelPerfectCamera_Awake_mE1F1DEB95B8B2FEF4427A7C1B9CF521D8E540DFB (void);
// 0x00000056 System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::OnBeginFrameRendering(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera[])
extern void PixelPerfectCamera_OnBeginFrameRendering_m59A702D22AA445916C6178BCD3FACF06A058E95A (void);
// 0x00000057 System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::OnBeginCameraRendering(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera)
extern void PixelPerfectCamera_OnBeginCameraRendering_mAE998E99F21F8960177FFA60DBC29A9F973547F4 (void);
// 0x00000058 System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::OnEndCameraRendering(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera)
extern void PixelPerfectCamera_OnEndCameraRendering_m3DAFA45C4CE07FE01E38277D57E1CE96784E6B09 (void);
// 0x00000059 System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::OnEnable()
extern void PixelPerfectCamera_OnEnable_m2DE4422FDA4E57CA6F2AAEB7E696CA2B9D6B7053 (void);
// 0x0000005A System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::OnDisable()
extern void PixelPerfectCamera_OnDisable_mA7B1D6E90B7EE81BD0A1C1E734D34EB510BEDC3D (void);
// 0x0000005B System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera::.ctor()
extern void PixelPerfectCamera__ctor_m52297333EC215F64759295CBBF7FF9E163AA7DA6 (void);
// 0x0000005C System.Int32 UnityEngine.Experimental.Rendering.Universal.IPixelPerfectCamera::get_assetsPPU()
// 0x0000005D System.Int32 UnityEngine.Experimental.Rendering.Universal.IPixelPerfectCamera::get_refResolutionX()
// 0x0000005E System.Int32 UnityEngine.Experimental.Rendering.Universal.IPixelPerfectCamera::get_refResolutionY()
// 0x0000005F System.Boolean UnityEngine.Experimental.Rendering.Universal.IPixelPerfectCamera::get_upscaleRT()
// 0x00000060 System.Boolean UnityEngine.Experimental.Rendering.Universal.IPixelPerfectCamera::get_pixelSnapping()
// 0x00000061 System.Boolean UnityEngine.Experimental.Rendering.Universal.IPixelPerfectCamera::get_cropFrameX()
// 0x00000062 System.Boolean UnityEngine.Experimental.Rendering.Universal.IPixelPerfectCamera::get_cropFrameY()
// 0x00000063 System.Boolean UnityEngine.Experimental.Rendering.Universal.IPixelPerfectCamera::get_stretchFill()
// 0x00000064 System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCameraInternal::.ctor(UnityEngine.Experimental.Rendering.Universal.IPixelPerfectCamera)
extern void PixelPerfectCameraInternal__ctor_mFC5B46BD1C0AC5D040F3EA2C52E8F13BC4F07876 (void);
// 0x00000065 System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCameraInternal::OnBeforeSerialize()
extern void PixelPerfectCameraInternal_OnBeforeSerialize_m68623E75795993705991F4EF5ECD4C8835E74DE3 (void);
// 0x00000066 System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCameraInternal::OnAfterDeserialize()
extern void PixelPerfectCameraInternal_OnAfterDeserialize_m51F70F2D26BCDC3DE9C30E5407F6E62402B2DFED (void);
// 0x00000067 System.Void UnityEngine.Experimental.Rendering.Universal.PixelPerfectCameraInternal::CalculateCameraProperties(System.Int32,System.Int32)
extern void PixelPerfectCameraInternal_CalculateCameraProperties_m104A1F1B72FF6DAE609C8ABB2639AACEBB8F1BCE (void);
// 0x00000068 UnityEngine.Rect UnityEngine.Experimental.Rendering.Universal.PixelPerfectCameraInternal::CalculateFinalBlitPixelRect(System.Int32,System.Int32)
extern void PixelPerfectCameraInternal_CalculateFinalBlitPixelRect_mBDA39EE35E9414AA9058A58D6C40E37283114F1C (void);
// 0x00000069 System.Single UnityEngine.Experimental.Rendering.Universal.PixelPerfectCameraInternal::CorrectCinemachineOrthoSize(System.Single)
extern void PixelPerfectCameraInternal_CorrectCinemachineOrthoSize_mD48B7CA2CA6948DF9BFDC0C1A45A97FEC8E71AF5 (void);
static Il2CppMethodPointer s_methodPointers[105] = 
{
	EmbeddedAttribute__ctor_m780BE61DB0EBDDD1D1E95E31CA1F37AD542CDA70,
	IsReadOnlyAttribute__ctor_m8BE7F8142367305750E8134F9E91ACB134AF5361,
	XRSystemData__ctor_m6E39CCFB35325FC814C589BEE10B04794E187774,
	ShaderResources__ctor_m35E93A20CFB6037D4AE810D25EC29BAF506BCD04,
	ScriptableRenderer__cctor_m0470CA064570E7675DBBF61BFAF1B91AFA8D51C3,
	ShaderPropertyId__cctor_mA37312BCBED320DA89B602712197A2C596431789,
	XRView__ctor_m26697ACC489FE20A5ABE57491F5266BBEAD8DA61,
	XRView__ctor_mBD2BA2F6548FD9780A2EA656E186BE6AB02FA411,
	XRPass_get_enabled_m29320181846F74E0DCF77D4D897479362D2273CA,
	XRPass_get_xrSdkEnabled_m382436041060A68DD4DCC122B48E4F8BEDC1EB64,
	XRPass_set_xrSdkEnabled_m6C320B4690D3C05225AC8A456AA1DB38CCECDB16,
	XRPass_set_copyDepth_mABAD1DE38CB40CFB7971ED1D5A507FB046BFAECE,
	XRPass_get_multipassId_mD4A8E396F5B8E3EA3E5FA642A95948614CB19C1D,
	XRPass_set_multipassId_mBF5A78EF00B1DEB40F3315D446BFDA15B5677F07,
	XRPass_set_cullingPassId_mA32F73AD044A828C56DE57E15537AA05B2698A90,
	XRPass_set_renderTarget_m528BDC7E91273283D86C9F1543CCE551E09F0A29,
	XRPass_get_renderTargetDesc_m0E4F094BC06E1AEBBEA885532FCBD3E088B51DC4,
	XRPass_set_renderTargetDesc_mB21461AD204A490E0568E8E126590F59DF6DB07A,
	XRPass_set_renderTargetIsRenderTexture_m93DAA821EB43DAC5BEC6E454B5E995B3F2147393,
	XRPass_GetViewport_m86B907AD139BF56872DB6826CF0C981A344BB4D5,
	XRPass_set_cullingParams_m22E55420DE3E4F12CCD9E486B6451DE18E44D72D,
	XRPass_get_viewCount_m057B8D5B4F9E1945FAAB5E4F02A9966CEE36F297,
	XRPass_get_singlePassEnabled_mE9467F40CAF6B34B853C1D352FF7256E1F762FC2,
	XRPass_get_isOcclusionMeshSupported_m3D4CE92972FA8DB21FD2EBF02CEBF687AE828853,
	XRPass_UpdateView_m0818F95E7AF7C1763B807C508F7095D3348D7E20,
	XRPass_UpdateView_m236A460DEDE22C366FCAF5BFF2EEA4BFD6E27EBE,
	XRPass_UpdateCullingParams_mA7D560B027CD211FC86B4419BED46010489DF06A,
	XRPass_Create_mB49E75C77DF62A1DFD8FAEDB2CDC924739EC802C,
	XRPass_AddView_m4FB7F1B1148E42FD55AEDBF364FA7600C8BA6DDC,
	XRPass_Release_m2B844EE45248EB9AF52CF82EAFC15E15A92D44FB,
	XRPass_AddViewInternal_m61BD6957BF5925AD65CA954BB04385BDF4D42047,
	XRPass_UpdateOcclusionMesh_m2288932ABD0F00CE1DDE099452CD19540687B433,
	XRPass_TryGetOcclusionMeshCombinedHashCode_m667C57572358A2527469A4986F9EDCC37BFB28DE,
	XRPass_CreateOcclusionMeshCombined_mF3DBCA8C5D4A7A6577099DCF5739C62CCC4FF534,
	XRPass__ctor_mCA55DA5C03B0479EE54DCA7EA94E5FF36FED7732,
	XRPass__cctor_mD947D8A47464164323DF9ADE78C89E2811CBAD9D,
	CustomMirrorView__ctor_m475059281857B20C7CE15BAC68A40B79D3AB0230,
	CustomMirrorView_Invoke_m3D97B0AEE1612273019D50A80D4C74D6D8CDD09B,
	CustomMirrorView_BeginInvoke_mDD89A15673A2E48EDC69BA2829E7B39F3B703637,
	CustomMirrorView_EndInvoke_mD403A4F005650120E0A6D12C44B3BA44E5F38C35,
	XRSystem__ctor_mA584B8E34EDA1F815E22DDB15AEE3D0DF1C27278,
	XRSystem_InitializeXRSystemData_m8FF5784CC5608FB7A9CACF6DF2A0DA7CA70E7B18,
	XRSystem_GetDisplaySubsystem_m1176C00E95F9FCE7674FC344CB2EB7B8942CF91E,
	XRSystem_XRSystemInit_m5A01FCB59B5AF7FD207E117C9BEEA5C723685DD4,
	XRSystem_UpdateMSAALevel_m37D6C30265B675A37895D8A4A81F6750AA86C2EC,
	XRSystem_GetMSAALevel_m7B1213F7688F34D95D65A5AD8CE2DAA261C9B42D,
	XRSystem_UpdateRenderScale_m4456D89C0E7CD079B68BF564DD8ABFD170F0C6A3,
	XRSystem_GetMaxViews_m2F0885AEB08405FB58E24642602B253F7F674B1C,
	XRSystem_SetupFrame_m9A10E388C0F65B456AFB4E1E45639F06A835ABB3,
	XRSystem_ReleaseFrame_mE9046CA23B4BCFDD5DD555748D2ECAC1C30740D8,
	XRSystem_RefreshXrSdk_m30A7FD07FFC08ED83A1C4D2D59730A79653CFACD,
	XRSystem_UpdateCameraData_m31EA9B7F30C04638528251CC518C51063BE43885,
	XRSystem_UpdateFromCamera_m1BEDCEC0DCA1590628665D20BEDB8CB06DD385B8,
	XRSystem_CreateLayoutFromXrSdk_mD46049C4F7A08CFD6E5C88C5F31058B55F0B4E9B,
	XRSystem_Dispose_mEE8DB22C444B1E6F776B834B03647C20DD3AD658,
	XRSystem_AddPassToFrame_m9E95DC808E61B996871908A4052717D1A665C2D4,
	XRSystem_RenderMirrorView_mDDEB09624A85CA2D482CA9960049FD0B54613034,
	XRSystem__cctor_m7B8F02B7D57C203238E42352E4678976010A0BF2,
	XRSystem_U3CCreateLayoutFromXrSdkU3Eg__CanUseSinglePassU7C24_0_mC086FCBD276F4990745BA9168A14F4301F3914A6,
	XRShaderIDs__cctor_mF11B95778345BA7AC7CF34DA2ED6149C2B65CED2,
	PixelPerfectCamera_get_assetsPPU_m16C0A1B2A2BB8644A41FC9522A541CBF5A75E445,
	PixelPerfectCamera_set_assetsPPU_mE9F840B821EF2C9DC29D86095A4B48D24C02CC55,
	PixelPerfectCamera_get_refResolutionX_m0ABC9A98CA6CFDB32E731D1AF178FAD58F6D254E,
	PixelPerfectCamera_set_refResolutionX_mA639C0DA9757B663124681C51838CEB1F5D1613D,
	PixelPerfectCamera_get_refResolutionY_m2AE92ADB79340A73D903FC52E047D3FEE8633C0D,
	PixelPerfectCamera_set_refResolutionY_m5BAC54558D80C76781D0FD6B464005BF164B4C3C,
	PixelPerfectCamera_get_upscaleRT_mE8C7FC04AE10058C5DD8B8ED681FDAB0DCE01D8C,
	PixelPerfectCamera_set_upscaleRT_mF7C93BD41FE1A09A374411FC0DF1ACB0DD1DC4F3,
	PixelPerfectCamera_get_pixelSnapping_mE0D4A93909775D06BFCE02A77B6753E498DA6AFB,
	PixelPerfectCamera_set_pixelSnapping_m0B52DA205E54AA6DB05641997F00F2FBA7C87EB7,
	PixelPerfectCamera_get_cropFrameX_mCB0D6314BE8D082BAF0C2AFD7BD8D3EFC067C2A6,
	PixelPerfectCamera_set_cropFrameX_mA275EC21C3AA94B72C15BFF7F807B33D8F5FF65C,
	PixelPerfectCamera_get_cropFrameY_m99A1B9D17D96011EA6AAFE8442875D3A8B922B25,
	PixelPerfectCamera_set_cropFrameY_mC892DA37BFC3C18E9946447DFD624A2907927F3A,
	PixelPerfectCamera_get_stretchFill_m5A98E0B454DD4201445EC9D8E76E5FB97CFF23FC,
	PixelPerfectCamera_set_stretchFill_mF0F09EA5081BDFAD029ED0CED016C991B1B81E0F,
	PixelPerfectCamera_get_pixelRatio_m613231F84624266E0BC7B96EE47992DA8245A70D,
	PixelPerfectCamera_RoundToPixel_mE0918CF4DC93FBDA90676E466850E41B08079D09,
	PixelPerfectCamera_CorrectCinemachineOrthoSize_mBA3786D33A75A187D5024026A8B8270B041FB37E,
	PixelPerfectCamera_get_isRunning_mE7966A17634436B98159AE6A5A15F641E923DA9A,
	PixelPerfectCamera_get_finalBlitFilterMode_m9A4E8E99011DBD371049362FF4C5839A18119045,
	PixelPerfectCamera_get_offscreenRTSize_mCBB9DCC1AAF9BB1DC9BD8E3E5575BACEF01D1BC1,
	PixelPerfectCamera_get_cameraRTSize_mCABC13C159389FC13C299E63C2801F1DA7DC64D8,
	PixelPerfectCamera_PixelSnap_m8C2227B71DE3F880D3974FCBAAD0FC22D7D602C3,
	PixelPerfectCamera_Awake_mE1F1DEB95B8B2FEF4427A7C1B9CF521D8E540DFB,
	PixelPerfectCamera_OnBeginFrameRendering_m59A702D22AA445916C6178BCD3FACF06A058E95A,
	PixelPerfectCamera_OnBeginCameraRendering_mAE998E99F21F8960177FFA60DBC29A9F973547F4,
	PixelPerfectCamera_OnEndCameraRendering_m3DAFA45C4CE07FE01E38277D57E1CE96784E6B09,
	PixelPerfectCamera_OnEnable_m2DE4422FDA4E57CA6F2AAEB7E696CA2B9D6B7053,
	PixelPerfectCamera_OnDisable_mA7B1D6E90B7EE81BD0A1C1E734D34EB510BEDC3D,
	PixelPerfectCamera__ctor_m52297333EC215F64759295CBBF7FF9E163AA7DA6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PixelPerfectCameraInternal__ctor_mFC5B46BD1C0AC5D040F3EA2C52E8F13BC4F07876,
	PixelPerfectCameraInternal_OnBeforeSerialize_m68623E75795993705991F4EF5ECD4C8835E74DE3,
	PixelPerfectCameraInternal_OnAfterDeserialize_m51F70F2D26BCDC3DE9C30E5407F6E62402B2DFED,
	PixelPerfectCameraInternal_CalculateCameraProperties_m104A1F1B72FF6DAE609C8ABB2639AACEBB8F1BCE,
	PixelPerfectCameraInternal_CalculateFinalBlitPixelRect_mBDA39EE35E9414AA9058A58D6C40E37283114F1C,
	PixelPerfectCameraInternal_CorrectCinemachineOrthoSize_mD48B7CA2CA6948DF9BFDC0C1A45A97FEC8E71AF5,
};
extern void XRView__ctor_m26697ACC489FE20A5ABE57491F5266BBEAD8DA61_AdjustorThunk (void);
extern void XRView__ctor_mBD2BA2F6548FD9780A2EA656E186BE6AB02FA411_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[2] = 
{
	{ 0x06000007, XRView__ctor_m26697ACC489FE20A5ABE57491F5266BBEAD8DA61_AdjustorThunk },
	{ 0x06000008, XRView__ctor_mBD2BA2F6548FD9780A2EA656E186BE6AB02FA411_AdjustorThunk },
};
static const int32_t s_InvokerIndices[105] = 
{
	3856,
	3856,
	3856,
	3856,
	5813,
	5813,
	784,
	2041,
	3812,
	3812,
	3220,
	3220,
	3756,
	3172,
	3172,
	3214,
	3803,
	3215,
	3220,
	2589,
	3224,
	3756,
	3812,
	3812,
	1220,
	385,
	1871,
	4574,
	2041,
	5731,
	3272,
	3856,
	2714,
	3856,
	3856,
	5813,
	1984,
	825,
	208,
	3191,
	3856,
	3191,
	5813,
	5813,
	5727,
	5777,
	5738,
	3756,
	2542,
	3856,
	3812,
	1684,
	1685,
	1990,
	3856,
	3191,
	1986,
	5813,
	5313,
	5813,
	3756,
	3172,
	3756,
	3172,
	3756,
	3172,
	3812,
	3220,
	3812,
	3220,
	3812,
	3220,
	3812,
	3220,
	3812,
	3220,
	3756,
	3011,
	2964,
	3812,
	3756,
	3850,
	3850,
	3856,
	3856,
	2013,
	2013,
	2013,
	3856,
	3856,
	3856,
	3756,
	3756,
	3756,
	3812,
	3812,
	3812,
	3812,
	3812,
	3191,
	3856,
	3856,
	1831,
	1510,
	2964,
};
extern const CustomAttributesCacheGenerator g_Unity_RenderPipelines_Universal_Runtime_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Unity_RenderPipelines_Universal_Runtime_CodeGenModule;
const Il2CppCodeGenModule g_Unity_RenderPipelines_Universal_Runtime_CodeGenModule = 
{
	"Unity.RenderPipelines.Universal.Runtime.dll",
	105,
	s_methodPointers,
	2,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Unity_RenderPipelines_Universal_Runtime_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
