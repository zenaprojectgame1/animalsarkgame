﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Lean.Touch.IDropHandler::HandleDrop(UnityEngine.GameObject,Lean.Touch.LeanFinger)
// 0x00000002 System.Void Lean.Touch.LeanDragColorMesh::set_PaintColor(UnityEngine.Color)
extern void LeanDragColorMesh_set_PaintColor_m02D13638676B0607283C53A0AFB99934AD4B009C (void);
// 0x00000003 UnityEngine.Color Lean.Touch.LeanDragColorMesh::get_PaintColor()
extern void LeanDragColorMesh_get_PaintColor_m9FE51EF569DE905D08F16FDFDBEF4913BD5D74C9 (void);
// 0x00000004 System.Void Lean.Touch.LeanDragColorMesh::set_Camera(UnityEngine.Camera)
extern void LeanDragColorMesh_set_Camera_m34DA00494E9154A3FA5C08C8C82E43852E172693 (void);
// 0x00000005 UnityEngine.Camera Lean.Touch.LeanDragColorMesh::get_Camera()
extern void LeanDragColorMesh_get_Camera_m3CDF4522DDCB652CDDA654F17C1B35622BBC97C2 (void);
// 0x00000006 System.Void Lean.Touch.LeanDragColorMesh::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragColorMesh_AddFinger_mCE3752EB2F987C3895277F6F54EE6C50839C2382 (void);
// 0x00000007 System.Void Lean.Touch.LeanDragColorMesh::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragColorMesh_RemoveFinger_mBA269A0BDFD0572FB9D1248865D3C79FBA90BEBF (void);
// 0x00000008 System.Void Lean.Touch.LeanDragColorMesh::RemoveAllFingers()
extern void LeanDragColorMesh_RemoveAllFingers_m056FF92363F0DC13FB54115D298063A7C5C3FC46 (void);
// 0x00000009 System.Void Lean.Touch.LeanDragColorMesh::Awake()
extern void LeanDragColorMesh_Awake_mF2F1017D508ED95F56D541954C2AF2E82CEFA692 (void);
// 0x0000000A System.Void Lean.Touch.LeanDragColorMesh::Update()
extern void LeanDragColorMesh_Update_mA3487A2816B38A91197822AEA0B1915E0DC0D95A (void);
// 0x0000000B System.Void Lean.Touch.LeanDragColorMesh::Paint(Lean.Touch.LeanFinger)
extern void LeanDragColorMesh_Paint_mAF2EDACE873FE09F231E4ADD201CD67C0255AA6B (void);
// 0x0000000C System.Void Lean.Touch.LeanDragColorMesh::.ctor()
extern void LeanDragColorMesh__ctor_mC8D00698009CAF615ADD90AA0E520A96BC3E6A3D (void);
// 0x0000000D System.Void Lean.Touch.LeanDragDeformMesh::set_ScaledRadius(System.Single)
extern void LeanDragDeformMesh_set_ScaledRadius_m4C00C9076AB014B88456D036B2806A4B6646BE35 (void);
// 0x0000000E System.Single Lean.Touch.LeanDragDeformMesh::get_ScaledRadius()
extern void LeanDragDeformMesh_get_ScaledRadius_mA485FB24D6A3A78D38D2E9065F9B098417CD7ECF (void);
// 0x0000000F System.Void Lean.Touch.LeanDragDeformMesh::set_ApplyToMeshCollider(System.Boolean)
extern void LeanDragDeformMesh_set_ApplyToMeshCollider_mCD0CC78EC0B68ACF07B0BFE312AAF4B519FCE616 (void);
// 0x00000010 System.Boolean Lean.Touch.LeanDragDeformMesh::get_ApplyToMeshCollider()
extern void LeanDragDeformMesh_get_ApplyToMeshCollider_mF79306E2D5D920FAA04DF31DADD60A474CC2C90D (void);
// 0x00000011 System.Void Lean.Touch.LeanDragDeformMesh::set_Camera(UnityEngine.Camera)
extern void LeanDragDeformMesh_set_Camera_m96083927ADE481A8DD2CEB9CE1443020FD3BE307 (void);
// 0x00000012 UnityEngine.Camera Lean.Touch.LeanDragDeformMesh::get_Camera()
extern void LeanDragDeformMesh_get_Camera_m65BD1DF992D516584E5E4BE7AACC0F475310E220 (void);
// 0x00000013 System.Void Lean.Touch.LeanDragDeformMesh::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragDeformMesh_AddFinger_m729599252F8FB05C68A83C1C37473E2F40A10C8B (void);
// 0x00000014 System.Void Lean.Touch.LeanDragDeformMesh::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragDeformMesh_RemoveFinger_mB1626BD417902694DB0DDBD1C0D1D98D6ADB1259 (void);
// 0x00000015 System.Void Lean.Touch.LeanDragDeformMesh::RemoveAllFingers()
extern void LeanDragDeformMesh_RemoveAllFingers_mF5931C346425CD44A7CA76176787791848707ACA (void);
// 0x00000016 System.Void Lean.Touch.LeanDragDeformMesh::Awake()
extern void LeanDragDeformMesh_Awake_mF9861C5E79FBE527C0E31C1C17DC426AA7F24CE8 (void);
// 0x00000017 System.Void Lean.Touch.LeanDragDeformMesh::Update()
extern void LeanDragDeformMesh_Update_mC53946E490C37C4C5F58316E9E9D9D55587F857D (void);
// 0x00000018 System.Void Lean.Touch.LeanDragDeformMesh::.ctor()
extern void LeanDragDeformMesh__ctor_m13B2E40A6F236FD4552A1A13FF5DCA498B1ED8C3 (void);
// 0x00000019 System.Void Lean.Touch.LeanDragLine::set_WidthScale(System.Single)
extern void LeanDragLine_set_WidthScale_m91EFF29BF3BBF189C38C2A1786C6F3231ED7CB2E (void);
// 0x0000001A System.Single Lean.Touch.LeanDragLine::get_WidthScale()
extern void LeanDragLine_get_WidthScale_m36C36A8E922E3C4FEAA20AC075AABEFD0A8B9348 (void);
// 0x0000001B System.Void Lean.Touch.LeanDragLine::set_LengthMin(System.Single)
extern void LeanDragLine_set_LengthMin_m84C99D85C226FA489E922D4B83ED46AE62DC892C (void);
// 0x0000001C System.Single Lean.Touch.LeanDragLine::get_LengthMin()
extern void LeanDragLine_get_LengthMin_mB66FCA8E8F5054227FA8F70388E20551F9D8840F (void);
// 0x0000001D System.Void Lean.Touch.LeanDragLine::set_LengthMax(System.Single)
extern void LeanDragLine_set_LengthMax_m9453E8EC817154FAF558DC1ECF2EFD6DA3251A2A (void);
// 0x0000001E System.Single Lean.Touch.LeanDragLine::get_LengthMax()
extern void LeanDragLine_get_LengthMax_m2ED1FB4D850FBF08B79E13B8701C176D9A21FF2D (void);
// 0x0000001F System.Void Lean.Touch.LeanDragLine::set_StartAtOrigin(System.Boolean)
extern void LeanDragLine_set_StartAtOrigin_mF9780895E82EC172370B952C4DA2F33562180FB2 (void);
// 0x00000020 System.Boolean Lean.Touch.LeanDragLine::get_StartAtOrigin()
extern void LeanDragLine_get_StartAtOrigin_mC1B7D85C0EE8E20158B45A5A8FD503D0CFD47DFF (void);
// 0x00000021 System.Void Lean.Touch.LeanDragLine::set_Invert(System.Boolean)
extern void LeanDragLine_set_Invert_mB7C00EA8A1BEE4804367A0EFB5D49A2D726E9B08 (void);
// 0x00000022 System.Boolean Lean.Touch.LeanDragLine::get_Invert()
extern void LeanDragLine_get_Invert_mE165FD8FF65858616334E1851574B8BE0A348F42 (void);
// 0x00000023 Lean.Touch.LeanDragLine/Vector3Event Lean.Touch.LeanDragLine::get_OnReleasedFrom()
extern void LeanDragLine_get_OnReleasedFrom_m7D2977721BB0154A3161E9E87852918AC9003582 (void);
// 0x00000024 Lean.Touch.LeanDragLine/Vector3Event Lean.Touch.LeanDragLine::get_OnReleasedTo()
extern void LeanDragLine_get_OnReleasedTo_m5776BF2BFF2EA317A556443ED0F7E0EB241FFB72 (void);
// 0x00000025 Lean.Touch.LeanDragLine/Vector3Event Lean.Touch.LeanDragLine::get_OnReleasedDelta()
extern void LeanDragLine_get_OnReleasedDelta_m7A0207313E71BDE68E7887A0D16C373FC9EB8965 (void);
// 0x00000026 Lean.Touch.LeanDragLine/Vector3Vector3Event Lean.Touch.LeanDragLine::get_OnReleasedFromTo()
extern void LeanDragLine_get_OnReleasedFromTo_mF2226F3C557C4DF908AFC403EAD2BB107C0FF175 (void);
// 0x00000027 System.Void Lean.Touch.LeanDragLine::UpdateLine(Lean.Touch.LeanDragTrail/FingerData,Lean.Touch.LeanFinger,UnityEngine.LineRenderer)
extern void LeanDragLine_UpdateLine_m8C179AE30FF9F3D2B71555D703F2035C8CFBBB72 (void);
// 0x00000028 System.Void Lean.Touch.LeanDragLine::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanDragLine_HandleFingerUp_mC8F1846770D07B143BC15A9A7C46482FC3F96F37 (void);
// 0x00000029 System.Void Lean.Touch.LeanDragLine::.ctor()
extern void LeanDragLine__ctor_mF6D28C925183A9FFA1B8D920214A06CC72D3815C (void);
// 0x0000002A System.Void Lean.Touch.LeanDragLine/Vector3Vector3Event::.ctor()
extern void Vector3Vector3Event__ctor_mDB239AB300DF27B79AE1AC01E9DEEF2700441CE6 (void);
// 0x0000002B System.Void Lean.Touch.LeanDragLine/Vector3Event::.ctor()
extern void Vector3Event__ctor_m7E521216C5D428050D6D85A8B1596596EC786B53 (void);
// 0x0000002C System.Void Lean.Touch.LeanDragSelect::set_Select(Lean.Touch.LeanSelectByFinger)
extern void LeanDragSelect_set_Select_mACAAA29E4B0B647BF488D8FC3A4F1987EC1A21A5 (void);
// 0x0000002D Lean.Touch.LeanSelectByFinger Lean.Touch.LeanDragSelect::get_Select()
extern void LeanDragSelect_get_Select_m321C47B883CC417834F0CA4BFFF7A730ED665B82 (void);
// 0x0000002E System.Void Lean.Touch.LeanDragSelect::set_RequireNoSelectables(System.Boolean)
extern void LeanDragSelect_set_RequireNoSelectables_mEE8B99B4288AFB192C20D536D328DBE3CE5E63EE (void);
// 0x0000002F System.Boolean Lean.Touch.LeanDragSelect::get_RequireNoSelectables()
extern void LeanDragSelect_get_RequireNoSelectables_m52330B97D85ADEE8150D1DC4C2F8D52EB9FB951D (void);
// 0x00000030 System.Void Lean.Touch.LeanDragSelect::set_RequireInitialSelection(System.Boolean)
extern void LeanDragSelect_set_RequireInitialSelection_mA2E5B69DC1DE08031E8AC84D25DF565180C042AD (void);
// 0x00000031 System.Boolean Lean.Touch.LeanDragSelect::get_RequireInitialSelection()
extern void LeanDragSelect_get_RequireInitialSelection_m937A1C3036360159EC640225EEE85A24AC0C0FE4 (void);
// 0x00000032 System.Void Lean.Touch.LeanDragSelect::set_DeselectAllAtStart(System.Boolean)
extern void LeanDragSelect_set_DeselectAllAtStart_mED1B2185188965D2FA81549ABC34768E4A94F708 (void);
// 0x00000033 System.Boolean Lean.Touch.LeanDragSelect::get_DeselectAllAtStart()
extern void LeanDragSelect_get_DeselectAllAtStart_mEDEEA6B31FE1118E22116CF688E596F6C59440D9 (void);
// 0x00000034 System.Void Lean.Touch.LeanDragSelect::set_MaximumSeparation(System.Single)
extern void LeanDragSelect_set_MaximumSeparation_m2E2C18F767E7E2134DEBAFBD4888F29E4529A703 (void);
// 0x00000035 System.Single Lean.Touch.LeanDragSelect::get_MaximumSeparation()
extern void LeanDragSelect_get_MaximumSeparation_mE437816B4CE036005F403A1556561CD2F750C42D (void);
// 0x00000036 System.Void Lean.Touch.LeanDragSelect::OnEnable()
extern void LeanDragSelect_OnEnable_m64CA4EA5D33B3593E9DE69C90C9728858110BDDD (void);
// 0x00000037 System.Void Lean.Touch.LeanDragSelect::OnDisable()
extern void LeanDragSelect_OnDisable_m14B1552A809CC0C73A9908816DEF848B7A9A652D (void);
// 0x00000038 System.Void Lean.Touch.LeanDragSelect::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanDragSelect_HandleFingerDown_mB30A4C5ACA0293C237324D8882AB4B63780E1E94 (void);
// 0x00000039 System.Void Lean.Touch.LeanDragSelect::HandleFingerUpdate(Lean.Touch.LeanFinger)
extern void LeanDragSelect_HandleFingerUpdate_m0B1BAD45EEB893C42A1E4043D882A116351F498E (void);
// 0x0000003A System.Void Lean.Touch.LeanDragSelect::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanDragSelect_HandleFingerUp_mBFDA712FBC048D13CB59E18B1211B31DC0560BAC (void);
// 0x0000003B System.Void Lean.Touch.LeanDragSelect::HandleAnySelectedFinger(Lean.Touch.LeanSelectByFinger,Lean.Touch.LeanSelectableByFinger,Lean.Touch.LeanFinger)
extern void LeanDragSelect_HandleAnySelectedFinger_mD5DDF7918194B324C1917621173DD8CFCC738834 (void);
// 0x0000003C System.Void Lean.Touch.LeanDragSelect::.ctor()
extern void LeanDragSelect__ctor_m1D99C807B88F8AF5F5F54428DC1E1BB7B29B8AAE (void);
// 0x0000003D System.Void Lean.Touch.LeanDragSelect/FingerData::.ctor()
extern void FingerData__ctor_m9F627641F99D2D60FA6A205D6B12460F18220F31 (void);
// 0x0000003E System.Void Lean.Touch.LeanDragTranslateAlong::set_Target(UnityEngine.Transform)
extern void LeanDragTranslateAlong_set_Target_m765D444CF39F04C6DF0D2C95967AF1D4520C56AA (void);
// 0x0000003F UnityEngine.Transform Lean.Touch.LeanDragTranslateAlong::get_Target()
extern void LeanDragTranslateAlong_get_Target_mF2A02A31842BA368E4E54DF08BB594F6FE691973 (void);
// 0x00000040 System.Void Lean.Touch.LeanDragTranslateAlong::set_TrackScreenPosition(System.Boolean)
extern void LeanDragTranslateAlong_set_TrackScreenPosition_mB077CF510F51C35A77080F114A6B6721D69C8C3C (void);
// 0x00000041 System.Boolean Lean.Touch.LeanDragTranslateAlong::get_TrackScreenPosition()
extern void LeanDragTranslateAlong_get_TrackScreenPosition_mA2050F03AE7FB0F1EC4DDEEEF5811C7CC3507C92 (void);
// 0x00000042 System.Void Lean.Touch.LeanDragTranslateAlong::set_Damping(System.Single)
extern void LeanDragTranslateAlong_set_Damping_m6B8376A59578475D058252842BB114F6507EED37 (void);
// 0x00000043 System.Single Lean.Touch.LeanDragTranslateAlong::get_Damping()
extern void LeanDragTranslateAlong_get_Damping_m951AFE786C5987E3769A2C3A3BBD9D2A76CDE867 (void);
// 0x00000044 System.Void Lean.Touch.LeanDragTranslateAlong::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslateAlong_AddFinger_m8495B62F73B8E0BD8E7A2E5D251B75AB9F4AC417 (void);
// 0x00000045 System.Void Lean.Touch.LeanDragTranslateAlong::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslateAlong_RemoveFinger_m1F16764BC10DC5CC951F1DD9B0853D33F02A949A (void);
// 0x00000046 System.Void Lean.Touch.LeanDragTranslateAlong::RemoveAllFingers()
extern void LeanDragTranslateAlong_RemoveAllFingers_mC7CCD24E1C199ED24769EBB038A427CB15F62C83 (void);
// 0x00000047 System.Void Lean.Touch.LeanDragTranslateAlong::Awake()
extern void LeanDragTranslateAlong_Awake_m50D7810B93398AAB993CD0B73B9E84566D0F00E1 (void);
// 0x00000048 System.Void Lean.Touch.LeanDragTranslateAlong::Update()
extern void LeanDragTranslateAlong_Update_mDB782356A7F05D6782AA3A43AB50A1EC2D6B24A0 (void);
// 0x00000049 System.Void Lean.Touch.LeanDragTranslateAlong::UpdateTranslation()
extern void LeanDragTranslateAlong_UpdateTranslation_m79D54167B5E83D27B92815E351D209D137F849A9 (void);
// 0x0000004A System.Void Lean.Touch.LeanDragTranslateAlong::.ctor()
extern void LeanDragTranslateAlong__ctor_m034724420F305D01E98E011BF309D935D50E0224 (void);
// 0x0000004B System.Void Lean.Touch.LeanDragTranslateRigidbody::set_Camera(UnityEngine.Camera)
extern void LeanDragTranslateRigidbody_set_Camera_m50B9CC6D413F23ACB996A8C82E3FD7EB6C98E93C (void);
// 0x0000004C UnityEngine.Camera Lean.Touch.LeanDragTranslateRigidbody::get_Camera()
extern void LeanDragTranslateRigidbody_get_Camera_m994DEE17BECF93B946AE321526AF8C84380B907D (void);
// 0x0000004D System.Void Lean.Touch.LeanDragTranslateRigidbody::set_Damping(System.Single)
extern void LeanDragTranslateRigidbody_set_Damping_m73D923AA2259298F3663C0937C4D77C3ACE88959 (void);
// 0x0000004E System.Single Lean.Touch.LeanDragTranslateRigidbody::get_Damping()
extern void LeanDragTranslateRigidbody_get_Damping_mB2A0D8F4CEB71857F1C7D9DEF46EC69F637453D5 (void);
// 0x0000004F System.Void Lean.Touch.LeanDragTranslateRigidbody::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslateRigidbody_AddFinger_m8A398753A7A5B501E55FF7C77FA786E9CF521CF3 (void);
// 0x00000050 System.Void Lean.Touch.LeanDragTranslateRigidbody::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslateRigidbody_RemoveFinger_mFE733D164C9BCA228E1862BF3525EEB44D9CC1C9 (void);
// 0x00000051 System.Void Lean.Touch.LeanDragTranslateRigidbody::RemoveAllFingers()
extern void LeanDragTranslateRigidbody_RemoveAllFingers_m625D805575FBF5D967A28491F243B3891DAA6A6A (void);
// 0x00000052 System.Void Lean.Touch.LeanDragTranslateRigidbody::Awake()
extern void LeanDragTranslateRigidbody_Awake_m3D9EBEDB11F627DDAA9F020413F2DF5BB9A4DFAD (void);
// 0x00000053 System.Void Lean.Touch.LeanDragTranslateRigidbody::OnEnable()
extern void LeanDragTranslateRigidbody_OnEnable_m32B08AF6D8860D1F5828C73CD3520D4D975ADEF5 (void);
// 0x00000054 System.Void Lean.Touch.LeanDragTranslateRigidbody::FixedUpdate()
extern void LeanDragTranslateRigidbody_FixedUpdate_mCF42FBDE001F55F790AE320A3422F1A7CDD7E504 (void);
// 0x00000055 System.Void Lean.Touch.LeanDragTranslateRigidbody::Update()
extern void LeanDragTranslateRigidbody_Update_m7A0BBA3232B254A03194F6DB14C4FED2D1DBBDBC (void);
// 0x00000056 System.Void Lean.Touch.LeanDragTranslateRigidbody::.ctor()
extern void LeanDragTranslateRigidbody__ctor_m96DFB46042E8A38344525963A5092FB7E47FC194 (void);
// 0x00000057 System.Void Lean.Touch.LeanDragTranslateRigidbody2D::set_Camera(UnityEngine.Camera)
extern void LeanDragTranslateRigidbody2D_set_Camera_mB67459350160E6A6850A16796B9BA044722FD19F (void);
// 0x00000058 UnityEngine.Camera Lean.Touch.LeanDragTranslateRigidbody2D::get_Camera()
extern void LeanDragTranslateRigidbody2D_get_Camera_m80E0A12AFA755D941A4195756785B3CEA4D073FC (void);
// 0x00000059 System.Void Lean.Touch.LeanDragTranslateRigidbody2D::set_Damping(System.Single)
extern void LeanDragTranslateRigidbody2D_set_Damping_m0B87F2DC486DD614C00F9E7FF1029653AAD0E903 (void);
// 0x0000005A System.Single Lean.Touch.LeanDragTranslateRigidbody2D::get_Damping()
extern void LeanDragTranslateRigidbody2D_get_Damping_m05A6D526C02616B8B0010571EAF168CF22071187 (void);
// 0x0000005B System.Void Lean.Touch.LeanDragTranslateRigidbody2D::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslateRigidbody2D_AddFinger_m36EAACBF6B464DEFC5F32861EA9E158B930AF7D1 (void);
// 0x0000005C System.Void Lean.Touch.LeanDragTranslateRigidbody2D::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslateRigidbody2D_RemoveFinger_m0E6E0219C41DD426AF13DFB57245D6E54E2A42C8 (void);
// 0x0000005D System.Void Lean.Touch.LeanDragTranslateRigidbody2D::RemoveAllFingers()
extern void LeanDragTranslateRigidbody2D_RemoveAllFingers_m0C9ED3D7DED7CD4899AF6EAEFA275D8471C9C490 (void);
// 0x0000005E System.Void Lean.Touch.LeanDragTranslateRigidbody2D::Awake()
extern void LeanDragTranslateRigidbody2D_Awake_m455C61A0D4F15CBAAA2466AC0BB703C24D4DCD2F (void);
// 0x0000005F System.Void Lean.Touch.LeanDragTranslateRigidbody2D::OnEnable()
extern void LeanDragTranslateRigidbody2D_OnEnable_m2D4CDD14154793E3627BD95A9776547611EA6134 (void);
// 0x00000060 System.Void Lean.Touch.LeanDragTranslateRigidbody2D::FixedUpdate()
extern void LeanDragTranslateRigidbody2D_FixedUpdate_mB37366992B33992938C8E1BD1C7FC802B2E07200 (void);
// 0x00000061 System.Void Lean.Touch.LeanDragTranslateRigidbody2D::Update()
extern void LeanDragTranslateRigidbody2D_Update_m34285E056A04D655DB7E48C5C0B09766884E3517 (void);
// 0x00000062 System.Void Lean.Touch.LeanDragTranslateRigidbody2D::.ctor()
extern void LeanDragTranslateRigidbody2D__ctor_mE3656B3FA5A1105A52DA495CF933AC230CFD8DE8 (void);
// 0x00000063 Lean.Touch.LeanDrop/GameObjectLeanFingerEvent Lean.Touch.LeanDrop::get_OnDropped()
extern void LeanDrop_get_OnDropped_m4F06F9A5CC364DFD7081AC1D70F8E9229C809FD5 (void);
// 0x00000064 System.Void Lean.Touch.LeanDrop::HandleDrop(UnityEngine.GameObject,Lean.Touch.LeanFinger)
extern void LeanDrop_HandleDrop_m8253952841F50B2EE004B57598E8B8A707F5BDFD (void);
// 0x00000065 System.Void Lean.Touch.LeanDrop::.ctor()
extern void LeanDrop__ctor_m99BA7F4387E396A2215C7ED46843B920E9678871 (void);
// 0x00000066 System.Void Lean.Touch.LeanDrop/GameObjectLeanFingerEvent::.ctor()
extern void GameObjectLeanFingerEvent__ctor_mA40063A518CD483175835A564250AD2B72CC572F (void);
// 0x00000067 System.Void Lean.Touch.LeanDropCount::set_Count(System.Int32)
extern void LeanDropCount_set_Count_m210F76848344FCB788161DAC47ADB0DF7074F22B (void);
// 0x00000068 System.Int32 Lean.Touch.LeanDropCount::get_Count()
extern void LeanDropCount_get_Count_mF85D0A590A0078C9295CC37C2DC5AF39A36D0AFC (void);
// 0x00000069 Lean.Touch.LeanDropCount/IntEvent Lean.Touch.LeanDropCount::get_OnCount()
extern void LeanDropCount_get_OnCount_m9CCEB99337B12778223A50C120D1E3366F112204 (void);
// 0x0000006A System.Void Lean.Touch.LeanDropCount::set_MatchMin(System.Int32)
extern void LeanDropCount_set_MatchMin_mD25BD79AAEA193A6A615A0C7A330E96EC182AAD4 (void);
// 0x0000006B System.Int32 Lean.Touch.LeanDropCount::get_MatchMin()
extern void LeanDropCount_get_MatchMin_m68CF493BE54F99DBC4F7186920C782FAFB5AE566 (void);
// 0x0000006C System.Void Lean.Touch.LeanDropCount::set_MatchMax(System.Int32)
extern void LeanDropCount_set_MatchMax_mDAE2098079FF29EB484B8985A09C377552ECA28C (void);
// 0x0000006D System.Int32 Lean.Touch.LeanDropCount::get_MatchMax()
extern void LeanDropCount_get_MatchMax_mCE6DFF9E3A06DE892F9ED1D816F0FD087EF2D59A (void);
// 0x0000006E UnityEngine.Events.UnityEvent Lean.Touch.LeanDropCount::get_OnMatch()
extern void LeanDropCount_get_OnMatch_m3D90B31ABBA10B18FC285DCBDDDFE6CD45599354 (void);
// 0x0000006F UnityEngine.Events.UnityEvent Lean.Touch.LeanDropCount::get_OnUnmatch()
extern void LeanDropCount_get_OnUnmatch_m69A16C85097C11FAE4036CBB91AAD2FB2E275CAF (void);
// 0x00000070 System.Void Lean.Touch.LeanDropCount::HandleDrop(UnityEngine.GameObject,Lean.Touch.LeanFinger)
extern void LeanDropCount_HandleDrop_m64486CD35E9761D8CC48B6E5D2ECB1F822430C78 (void);
// 0x00000071 System.Void Lean.Touch.LeanDropCount::UpdateState(System.Boolean)
extern void LeanDropCount_UpdateState_m8598040D8B70D261E2808047956585EB2C179E40 (void);
// 0x00000072 System.Void Lean.Touch.LeanDropCount::.ctor()
extern void LeanDropCount__ctor_m60F395B5058E243CD5800228ED459692DAFFBBE7 (void);
// 0x00000073 System.Void Lean.Touch.LeanDropCount/IntEvent::.ctor()
extern void IntEvent__ctor_m4E41C37BA6F0D9828A3FE2BB7917A8AC8887964D (void);
// 0x00000074 System.Void Lean.Touch.LeanFingerDownCanvas::set_IgnoreStartedOverGui(System.Boolean)
extern void LeanFingerDownCanvas_set_IgnoreStartedOverGui_m1A9E9E0B373B07DCD7ADCBC00272F839BA092A4D (void);
// 0x00000075 System.Boolean Lean.Touch.LeanFingerDownCanvas::get_IgnoreStartedOverGui()
extern void LeanFingerDownCanvas_get_IgnoreStartedOverGui_mA9F96E1D0296D68AC2A3C402459BAAAB684FBE81 (void);
// 0x00000076 System.Void Lean.Touch.LeanFingerDownCanvas::set_RequiredSelectable(Lean.Common.LeanSelectable)
extern void LeanFingerDownCanvas_set_RequiredSelectable_m8749886E8F129ED10F91EA818D60BDC38C848B8C (void);
// 0x00000077 Lean.Common.LeanSelectable Lean.Touch.LeanFingerDownCanvas::get_RequiredSelectable()
extern void LeanFingerDownCanvas_get_RequiredSelectable_m609BD1F64807B87EB6BDF55C816948838C5D50E7 (void);
// 0x00000078 Lean.Touch.LeanFingerDownCanvas/LeanFingerEvent Lean.Touch.LeanFingerDownCanvas::get_OnFinger()
extern void LeanFingerDownCanvas_get_OnFinger_m6649AF91419EEAD2854EAA50305671B94F45954C (void);
// 0x00000079 Lean.Touch.LeanFingerDownCanvas/Vector3Event Lean.Touch.LeanFingerDownCanvas::get_OnWorld()
extern void LeanFingerDownCanvas_get_OnWorld_m24327B58EB56CD9FD815E81B4CC210315E662C05 (void);
// 0x0000007A System.Boolean Lean.Touch.LeanFingerDownCanvas::ElementOverlapped(Lean.Touch.LeanFinger)
extern void LeanFingerDownCanvas_ElementOverlapped_m2FD8AC4FE82EFB7F5901A19D5FBA75C2D787A530 (void);
// 0x0000007B System.Void Lean.Touch.LeanFingerDownCanvas::OnEnable()
extern void LeanFingerDownCanvas_OnEnable_m71A65239AE993B4FDF080607B34459D96BD7AA60 (void);
// 0x0000007C System.Void Lean.Touch.LeanFingerDownCanvas::OnDisable()
extern void LeanFingerDownCanvas_OnDisable_mEB59B21C0647FFEB1B82CEC9C1A962719DF3806D (void);
// 0x0000007D System.Void Lean.Touch.LeanFingerDownCanvas::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanFingerDownCanvas_HandleFingerDown_mD90CA19B0FC9BCC85F39C3FB575EE5F3876A7338 (void);
// 0x0000007E System.Void Lean.Touch.LeanFingerDownCanvas::.ctor()
extern void LeanFingerDownCanvas__ctor_mCD3AFCE798DF7D43F690E86F21D3D4BD2CF11A68 (void);
// 0x0000007F System.Void Lean.Touch.LeanFingerDownCanvas/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m753ADD15A19B6C85455076C77E1F877D166EEBF3 (void);
// 0x00000080 System.Void Lean.Touch.LeanFingerDownCanvas/Vector3Event::.ctor()
extern void Vector3Event__ctor_mC2AD74C37DBB9DE6353EB174A0CB7C67AC1C17AD (void);
// 0x00000081 System.Void Lean.Touch.LeanFingerFlick::set_IgnoreStartedOverGui(System.Boolean)
extern void LeanFingerFlick_set_IgnoreStartedOverGui_m3B46DC63B350CC1246F2195CB433FA40A9C19B0E (void);
// 0x00000082 System.Boolean Lean.Touch.LeanFingerFlick::get_IgnoreStartedOverGui()
extern void LeanFingerFlick_get_IgnoreStartedOverGui_mEB7B21D6EA2A12A67B03F6FDB21680F2FCFCDF24 (void);
// 0x00000083 System.Void Lean.Touch.LeanFingerFlick::set_IgnoreIsOverGui(System.Boolean)
extern void LeanFingerFlick_set_IgnoreIsOverGui_m7F481652C587E4056AEC6C24BE0FA6846A532F09 (void);
// 0x00000084 System.Boolean Lean.Touch.LeanFingerFlick::get_IgnoreIsOverGui()
extern void LeanFingerFlick_get_IgnoreIsOverGui_m904B9AF491C02582FA3EB00C08E30BA645E42DC8 (void);
// 0x00000085 System.Void Lean.Touch.LeanFingerFlick::set_RequiredSelectable(Lean.Common.LeanSelectable)
extern void LeanFingerFlick_set_RequiredSelectable_m6B07DB8CD364A62A814BC974E75029957A2369A3 (void);
// 0x00000086 Lean.Common.LeanSelectable Lean.Touch.LeanFingerFlick::get_RequiredSelectable()
extern void LeanFingerFlick_get_RequiredSelectable_m4ADBF1B274059F42C5E1D516CF87E9995CBD5668 (void);
// 0x00000087 System.Void Lean.Touch.LeanFingerFlick::set_Check(Lean.Touch.LeanFingerFlick/CheckType)
extern void LeanFingerFlick_set_Check_mB6561448E549D9424010C31E19CDF571F0514454 (void);
// 0x00000088 Lean.Touch.LeanFingerFlick/CheckType Lean.Touch.LeanFingerFlick::get_Check()
extern void LeanFingerFlick_get_Check_mDA7A19CCF28C6035F217334C37E76DECA0496D49 (void);
// 0x00000089 System.Void Lean.Touch.LeanFingerFlick::Awake()
extern void LeanFingerFlick_Awake_m351CEFBE0D0ABD2DD7FADFA08BED62EE831D8386 (void);
// 0x0000008A System.Void Lean.Touch.LeanFingerFlick::OnEnable()
extern void LeanFingerFlick_OnEnable_m42DC4BD9027E2F2B9788F7BCF51C705F56A5B0C1 (void);
// 0x0000008B System.Void Lean.Touch.LeanFingerFlick::OnDisable()
extern void LeanFingerFlick_OnDisable_m25A8DBCE28B1EE47830013FA678743512ED70C6D (void);
// 0x0000008C System.Void Lean.Touch.LeanFingerFlick::Update()
extern void LeanFingerFlick_Update_mF6818F9396E449DD6744CD74FD642BA26AC84FCC (void);
// 0x0000008D System.Void Lean.Touch.LeanFingerFlick::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanFingerFlick_HandleFingerDown_m06FF1D7E0F17ED5518A0322EB6A4FD2CC78CA670 (void);
// 0x0000008E System.Void Lean.Touch.LeanFingerFlick::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanFingerFlick_HandleFingerUp_m16D2F4190EE9C0B5B6033A3391AD864A3897B745 (void);
// 0x0000008F System.Boolean Lean.Touch.LeanFingerFlick::TestFinger(Lean.Touch.LeanFinger,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanFingerFlick_TestFinger_m653BBC29E5850DB9D1077747620C2A47B5F073F5 (void);
// 0x00000090 System.Void Lean.Touch.LeanFingerFlick::.ctor()
extern void LeanFingerFlick__ctor_mE0606001112CCD657573FE0262E57D7EFE30719B (void);
// 0x00000091 System.Void Lean.Touch.LeanFingerFlick/FingerData::.ctor()
extern void FingerData__ctor_mEE7E8F41F34F9733325C39871FAD87B071084A00 (void);
// 0x00000092 System.Void Lean.Touch.LeanFingerHeld::set_IgnoreStartedOverGui(System.Boolean)
extern void LeanFingerHeld_set_IgnoreStartedOverGui_mE34A996A793CB90E60CA0D454B2522522AD23463 (void);
// 0x00000093 System.Boolean Lean.Touch.LeanFingerHeld::get_IgnoreStartedOverGui()
extern void LeanFingerHeld_get_IgnoreStartedOverGui_mDFBACC0AAC5330FBFE08E4A5C95AB29B94A7BE79 (void);
// 0x00000094 System.Void Lean.Touch.LeanFingerHeld::set_IgnoreIsOverGui(System.Boolean)
extern void LeanFingerHeld_set_IgnoreIsOverGui_m9BC3C7CCDCBE77CBDB3C99C1BE27FC52175A159F (void);
// 0x00000095 System.Boolean Lean.Touch.LeanFingerHeld::get_IgnoreIsOverGui()
extern void LeanFingerHeld_get_IgnoreIsOverGui_mC65997E95B6F9A61A084AD5048AEB932551ABC23 (void);
// 0x00000096 System.Void Lean.Touch.LeanFingerHeld::set_RequiredSelectable(Lean.Common.LeanSelectable)
extern void LeanFingerHeld_set_RequiredSelectable_m3519E86B5D603E0936A66A611BCA27D2B42525DF (void);
// 0x00000097 Lean.Common.LeanSelectable Lean.Touch.LeanFingerHeld::get_RequiredSelectable()
extern void LeanFingerHeld_get_RequiredSelectable_mDC042DF34094F3645508EB7396F5FD270B69C953 (void);
// 0x00000098 System.Void Lean.Touch.LeanFingerHeld::set_MinimumAge(System.Single)
extern void LeanFingerHeld_set_MinimumAge_m46357CA5E4D621CC9B0D61D5B885233FDC5F42CF (void);
// 0x00000099 System.Single Lean.Touch.LeanFingerHeld::get_MinimumAge()
extern void LeanFingerHeld_get_MinimumAge_mFEC7ED3346A6F2A02E4E0A3A877E01F7A993CFC8 (void);
// 0x0000009A System.Void Lean.Touch.LeanFingerHeld::set_MaximumMovement(System.Single)
extern void LeanFingerHeld_set_MaximumMovement_m6D1738D75D46C3888E6D525AF5196D162ADF88C1 (void);
// 0x0000009B System.Single Lean.Touch.LeanFingerHeld::get_MaximumMovement()
extern void LeanFingerHeld_get_MaximumMovement_mA77EBE402F1CE2C78B6FB4784F3B7348F7D521E8 (void);
// 0x0000009C Lean.Touch.LeanFingerHeld/LeanFingerEvent Lean.Touch.LeanFingerHeld::get_OnFingerDown()
extern void LeanFingerHeld_get_OnFingerDown_m08642BFBDEBD79D670FA1AD48FD04959C4B3DECD (void);
// 0x0000009D Lean.Touch.LeanFingerHeld/LeanFingerEvent Lean.Touch.LeanFingerHeld::get_OnFingerUpdate()
extern void LeanFingerHeld_get_OnFingerUpdate_m8700FEBBEE2EB5E5DABDB02B4C1C14EEA7EE3221 (void);
// 0x0000009E Lean.Touch.LeanFingerHeld/LeanFingerEvent Lean.Touch.LeanFingerHeld::get_OnFingerUp()
extern void LeanFingerHeld_get_OnFingerUp_mCB8F70B6FCF127E5377EA9D9E6B3B70023CAD459 (void);
// 0x0000009F Lean.Touch.LeanFingerHeld/Vector3Event Lean.Touch.LeanFingerHeld::get_OnWorldDown()
extern void LeanFingerHeld_get_OnWorldDown_mC114BFBCE0DA29CA833D38F86418F909681C9092 (void);
// 0x000000A0 Lean.Touch.LeanFingerHeld/Vector3Event Lean.Touch.LeanFingerHeld::get_OnWorldUpdate()
extern void LeanFingerHeld_get_OnWorldUpdate_m8C6128622B21357D648C0559B64D79E65E528816 (void);
// 0x000000A1 Lean.Touch.LeanFingerHeld/Vector3Event Lean.Touch.LeanFingerHeld::get_OnWorldUp()
extern void LeanFingerHeld_get_OnWorldUp_m0C365EEF611BAE619EC36482ACDEE10712A5CE1A (void);
// 0x000000A2 System.Void Lean.Touch.LeanFingerHeld::Awake()
extern void LeanFingerHeld_Awake_mCF2576FA394B2B0DA1EA340845DF47CADC80F75D (void);
// 0x000000A3 System.Void Lean.Touch.LeanFingerHeld::OnEnable()
extern void LeanFingerHeld_OnEnable_m3D2EC019567A8E8DF2265E97E4221A3DD0E5DC1D (void);
// 0x000000A4 System.Void Lean.Touch.LeanFingerHeld::OnDisable()
extern void LeanFingerHeld_OnDisable_mB73FAF935D54DB0877E993A8B1960D378AF04527 (void);
// 0x000000A5 System.Void Lean.Touch.LeanFingerHeld::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanFingerHeld_HandleFingerDown_mCE5EF045FCA687570A76E951B02D06814B0BADC7 (void);
// 0x000000A6 System.Void Lean.Touch.LeanFingerHeld::HandleFingerUpdate(Lean.Touch.LeanFinger)
extern void LeanFingerHeld_HandleFingerUpdate_m074005AECD62AC9AD013D1F30C265B2A4DF73495 (void);
// 0x000000A7 System.Boolean Lean.Touch.LeanFingerHeld::IsHeld(Lean.Touch.LeanFinger,Lean.Touch.LeanFingerHeld/FingerData)
extern void LeanFingerHeld_IsHeld_m32B437B0B7F33D7458430CF59FC54899AC091757 (void);
// 0x000000A8 System.Void Lean.Touch.LeanFingerHeld::InvokeDown(Lean.Touch.LeanFinger)
extern void LeanFingerHeld_InvokeDown_m5B3575CF7B62E00F33E4AFC037A59CF4F60AF1F0 (void);
// 0x000000A9 System.Void Lean.Touch.LeanFingerHeld::InvokeUpdate(Lean.Touch.LeanFinger)
extern void LeanFingerHeld_InvokeUpdate_mF9B59C7B743384BA70BCC45DD49BCDBAEECCD1BE (void);
// 0x000000AA System.Void Lean.Touch.LeanFingerHeld::InvokeUp(Lean.Touch.LeanFinger)
extern void LeanFingerHeld_InvokeUp_m4B336FFC0D042D8D5C86FC6237D2588C7DB920D1 (void);
// 0x000000AB System.Void Lean.Touch.LeanFingerHeld::.ctor()
extern void LeanFingerHeld__ctor_m9420E40BAA5C8DE7DCBFD87D503393E6B710FFA5 (void);
// 0x000000AC System.Void Lean.Touch.LeanFingerHeld/FingerData::.ctor()
extern void FingerData__ctor_m979E55D184CEDDFB9D37C1E069A43F9436A3F4E8 (void);
// 0x000000AD System.Void Lean.Touch.LeanFingerHeld/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m1FB3EA30283FE042069D2843E97ABEA86B2DEDEA (void);
// 0x000000AE System.Void Lean.Touch.LeanFingerHeld/Vector3Event::.ctor()
extern void Vector3Event__ctor_mD8C1E656CDF3DD7C715EE7B27136FA63348A5755 (void);
// 0x000000AF System.Void Lean.Touch.LeanFingerTapExpired::set_IgnoreStartedOverGui(System.Boolean)
extern void LeanFingerTapExpired_set_IgnoreStartedOverGui_mFB324216D3601F1DDC309A9EC17D35C0DF078502 (void);
// 0x000000B0 System.Boolean Lean.Touch.LeanFingerTapExpired::get_IgnoreStartedOverGui()
extern void LeanFingerTapExpired_get_IgnoreStartedOverGui_m23F4CB9134F04C77802B3C3E40162A5647805943 (void);
// 0x000000B1 System.Void Lean.Touch.LeanFingerTapExpired::set_IgnoreIsOverGui(System.Boolean)
extern void LeanFingerTapExpired_set_IgnoreIsOverGui_m6C7062215853AA09FFC9F275C9308590C703A7BB (void);
// 0x000000B2 System.Boolean Lean.Touch.LeanFingerTapExpired::get_IgnoreIsOverGui()
extern void LeanFingerTapExpired_get_IgnoreIsOverGui_mB3AD23E1F7FBF3DDA8C04934EEB3AB845940B108 (void);
// 0x000000B3 System.Void Lean.Touch.LeanFingerTapExpired::set_RequiredSelectable(Lean.Common.LeanSelectable)
extern void LeanFingerTapExpired_set_RequiredSelectable_m24AC5CF708637DF3858E269841BFE9A6B79E431D (void);
// 0x000000B4 Lean.Common.LeanSelectable Lean.Touch.LeanFingerTapExpired::get_RequiredSelectable()
extern void LeanFingerTapExpired_get_RequiredSelectable_m9AC4A56C987B929083A5F32E3301DD7D70B4A1D6 (void);
// 0x000000B5 System.Void Lean.Touch.LeanFingerTapExpired::set_RequiredTapCount(System.Int32)
extern void LeanFingerTapExpired_set_RequiredTapCount_m84BB71FCAF0CE32FB6408CCD2FF7F42D0BDF2698 (void);
// 0x000000B6 System.Int32 Lean.Touch.LeanFingerTapExpired::get_RequiredTapCount()
extern void LeanFingerTapExpired_get_RequiredTapCount_mF414E71B568E1DA81A87237EE488CD6DFC96030E (void);
// 0x000000B7 System.Void Lean.Touch.LeanFingerTapExpired::set_RequiredTapInterval(System.Int32)
extern void LeanFingerTapExpired_set_RequiredTapInterval_mE22CE57B3EE4FDD1AD7BEA3E71D0024CF811E49C (void);
// 0x000000B8 System.Int32 Lean.Touch.LeanFingerTapExpired::get_RequiredTapInterval()
extern void LeanFingerTapExpired_get_RequiredTapInterval_m52F84188E9C25DE6FECD9C8567C792BCAC2F7D49 (void);
// 0x000000B9 Lean.Touch.LeanFingerTapExpired/LeanFingerEvent Lean.Touch.LeanFingerTapExpired::get_OnFinger()
extern void LeanFingerTapExpired_get_OnFinger_m7E68A9F6C2806B9F84141DE28ECCCA741BCC57B2 (void);
// 0x000000BA Lean.Touch.LeanFingerTapExpired/IntEvent Lean.Touch.LeanFingerTapExpired::get_OnCount()
extern void LeanFingerTapExpired_get_OnCount_m24FF56E818C213BCAB4D78A8355941C83DC029A7 (void);
// 0x000000BB Lean.Touch.LeanFingerTapExpired/Vector3Event Lean.Touch.LeanFingerTapExpired::get_OnWorld()
extern void LeanFingerTapExpired_get_OnWorld_m335F14F6D0065BEE88251804E4F258DB047A1774 (void);
// 0x000000BC System.Void Lean.Touch.LeanFingerTapExpired::Awake()
extern void LeanFingerTapExpired_Awake_m08B89C10BDC85FDDB2B1EB636F725CF94AF78D50 (void);
// 0x000000BD System.Void Lean.Touch.LeanFingerTapExpired::OnEnable()
extern void LeanFingerTapExpired_OnEnable_m3FCFFF48CC014A9A40E766FE31F9DE52E26B7692 (void);
// 0x000000BE System.Void Lean.Touch.LeanFingerTapExpired::OnDisable()
extern void LeanFingerTapExpired_OnDisable_m6132018A80C67AFBA501E2A996FDB88D7C8C5507 (void);
// 0x000000BF System.Void Lean.Touch.LeanFingerTapExpired::HandleFingerTap(Lean.Touch.LeanFinger)
extern void LeanFingerTapExpired_HandleFingerTap_mAE2B1932950601CEDCF62541653228F97D78E1A5 (void);
// 0x000000C0 System.Void Lean.Touch.LeanFingerTapExpired::HandleFingerExpired(Lean.Touch.LeanFinger)
extern void LeanFingerTapExpired_HandleFingerExpired_mB42774F0FBD16E392D27CDAB422D074D1A4C1408 (void);
// 0x000000C1 System.Void Lean.Touch.LeanFingerTapExpired::.ctor()
extern void LeanFingerTapExpired__ctor_m0F653DF15C817A9A485702B1BCADD72F37015B94 (void);
// 0x000000C2 System.Void Lean.Touch.LeanFingerTapExpired/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m814037E4D65E793481CA5D5176F712BA36217148 (void);
// 0x000000C3 System.Void Lean.Touch.LeanFingerTapExpired/Vector3Event::.ctor()
extern void Vector3Event__ctor_m228AD371356A0E7A10804F7FC1DFE5D43A8EA5FD (void);
// 0x000000C4 System.Void Lean.Touch.LeanFingerTapExpired/IntEvent::.ctor()
extern void IntEvent__ctor_m5DDCF08ECEC10CFCB5258408DA7329A040B8D9E6 (void);
// 0x000000C5 System.Void Lean.Touch.LeanFingerTapQuick::set_IgnoreStartedOverGui(System.Boolean)
extern void LeanFingerTapQuick_set_IgnoreStartedOverGui_mC07FB0C926095A63188EA27C36C2775A45BA1D7B (void);
// 0x000000C6 System.Boolean Lean.Touch.LeanFingerTapQuick::get_IgnoreStartedOverGui()
extern void LeanFingerTapQuick_get_IgnoreStartedOverGui_m9E465B4F2B120DD6DF2420BB6C3D61F1E1C14FD3 (void);
// 0x000000C7 System.Void Lean.Touch.LeanFingerTapQuick::set_RequiredSelectable(Lean.Common.LeanSelectable)
extern void LeanFingerTapQuick_set_RequiredSelectable_m7F8093FC981BE2017495B1B958B7EEC5F2BC635F (void);
// 0x000000C8 Lean.Common.LeanSelectable Lean.Touch.LeanFingerTapQuick::get_RequiredSelectable()
extern void LeanFingerTapQuick_get_RequiredSelectable_m9CC09C74E08F553A37513BD8F738651AFDBECD43 (void);
// 0x000000C9 System.Void Lean.Touch.LeanFingerTapQuick::set_RequiredTapCount(System.Int32)
extern void LeanFingerTapQuick_set_RequiredTapCount_m31944E2F89D9CB9A25EA43D0EFCEE648935227FB (void);
// 0x000000CA System.Int32 Lean.Touch.LeanFingerTapQuick::get_RequiredTapCount()
extern void LeanFingerTapQuick_get_RequiredTapCount_m8A3A350425AD2F4679DB260427E2BFE3EDB4DE61 (void);
// 0x000000CB Lean.Touch.LeanFingerTapQuick/LeanFingerEvent Lean.Touch.LeanFingerTapQuick::get_OnFinger()
extern void LeanFingerTapQuick_get_OnFinger_mCF3C2D6E97E0A8130E954D15FAE05C4806CC8DC5 (void);
// 0x000000CC Lean.Touch.LeanFingerTapQuick/Vector3Event Lean.Touch.LeanFingerTapQuick::get_OnWorld()
extern void LeanFingerTapQuick_get_OnWorld_mC25D5BCA6F619559789168BAD30934054EC3DE74 (void);
// 0x000000CD Lean.Touch.LeanFingerTapQuick/Vector2Event Lean.Touch.LeanFingerTapQuick::get_OnScreen()
extern void LeanFingerTapQuick_get_OnScreen_mEA636B51383AA6059E9340FF9B53EDF94521451B (void);
// 0x000000CE System.Void Lean.Touch.LeanFingerTapQuick::Awake()
extern void LeanFingerTapQuick_Awake_m5F4D634C104BF2593481B1CC341876A3DC2EA760 (void);
// 0x000000CF System.Void Lean.Touch.LeanFingerTapQuick::OnEnable()
extern void LeanFingerTapQuick_OnEnable_m11441ADA8732B86B5ECFD65DB99A5D8A0A78081A (void);
// 0x000000D0 System.Void Lean.Touch.LeanFingerTapQuick::OnDisable()
extern void LeanFingerTapQuick_OnDisable_m856FE39C9B447BC32A21065902022C503118AA7A (void);
// 0x000000D1 System.Void Lean.Touch.LeanFingerTapQuick::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanFingerTapQuick_HandleFingerDown_m14CBA593FD32D6CD51525A115E1350FD008E0A2E (void);
// 0x000000D2 System.Void Lean.Touch.LeanFingerTapQuick::.ctor()
extern void LeanFingerTapQuick__ctor_mAC89C4230A7B22D084660BDD651284958BAD3533 (void);
// 0x000000D3 System.Void Lean.Touch.LeanFingerTapQuick/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m6D030CEFDEA2DCE692952F8820A5C2C702E3D88E (void);
// 0x000000D4 System.Void Lean.Touch.LeanFingerTapQuick/Vector3Event::.ctor()
extern void Vector3Event__ctor_m66122C895FC69BB14480E9516F3C4D9EA7F73D41 (void);
// 0x000000D5 System.Void Lean.Touch.LeanFingerTapQuick/Vector2Event::.ctor()
extern void Vector2Event__ctor_m6F991A152F883D39DE3A6D7B5438FCFF21369BB7 (void);
// 0x000000D6 System.Void Lean.Touch.LeanFirstDown::set_IgnoreStartedOverGui(System.Boolean)
extern void LeanFirstDown_set_IgnoreStartedOverGui_m7C8EC54C2EFFDEDAD53695B9D45ACF24692EFC74 (void);
// 0x000000D7 System.Boolean Lean.Touch.LeanFirstDown::get_IgnoreStartedOverGui()
extern void LeanFirstDown_get_IgnoreStartedOverGui_mED5CE02D53A7A4EECCB242E1494280D1F0030740 (void);
// 0x000000D8 System.Void Lean.Touch.LeanFirstDown::set_RequiredSelectable(Lean.Common.LeanSelectable)
extern void LeanFirstDown_set_RequiredSelectable_mD98A0C4DF7BAC53FFD0A67E5C25C8270CDA0FA5B (void);
// 0x000000D9 Lean.Common.LeanSelectable Lean.Touch.LeanFirstDown::get_RequiredSelectable()
extern void LeanFirstDown_get_RequiredSelectable_m6A9796844A68E970F31BE18BA69749B561855D77 (void);
// 0x000000DA Lean.Touch.LeanFirstDown/LeanFingerEvent Lean.Touch.LeanFirstDown::get_OnFinger()
extern void LeanFirstDown_get_OnFinger_m051EAD15CC2AC0E6426F49306B054754E4B02CA0 (void);
// 0x000000DB Lean.Touch.LeanFirstDown/Vector3Event Lean.Touch.LeanFirstDown::get_OnWorld()
extern void LeanFirstDown_get_OnWorld_m24414674ADF62E3B9699EC69377D4235118D7A55 (void);
// 0x000000DC Lean.Touch.LeanFirstDown/Vector2Event Lean.Touch.LeanFirstDown::get_OnScreen()
extern void LeanFirstDown_get_OnScreen_m1323611C7E6A77AAF05991B9CC90319CC28770C8 (void);
// 0x000000DD System.Void Lean.Touch.LeanFirstDown::Awake()
extern void LeanFirstDown_Awake_mFB919E441C8C717A68F0C1A8963FA8B255C148BB (void);
// 0x000000DE System.Void Lean.Touch.LeanFirstDown::OnEnable()
extern void LeanFirstDown_OnEnable_mE86D09FAFE641D0A6FBE5309FC56F0A5F449558F (void);
// 0x000000DF System.Void Lean.Touch.LeanFirstDown::OnDisable()
extern void LeanFirstDown_OnDisable_m19C7178B7AA870B63901F5B6093BC60584C0E92F (void);
// 0x000000E0 System.Void Lean.Touch.LeanFirstDown::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanFirstDown_HandleFingerDown_m6051CAE179EFBA89ED7504E71EB55CF367364AB9 (void);
// 0x000000E1 System.Void Lean.Touch.LeanFirstDown::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanFirstDown_HandleFingerUp_m43F90A126F4657EC7D71421C0B3C1B15F2842A32 (void);
// 0x000000E2 System.Void Lean.Touch.LeanFirstDown::.ctor()
extern void LeanFirstDown__ctor_mED57438C802CD29ADD69C982328C6DFB95175E66 (void);
// 0x000000E3 System.Void Lean.Touch.LeanFirstDown/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mD30A004EAA0CD84F9199C5EE30F4B7ED0FFAB7B5 (void);
// 0x000000E4 System.Void Lean.Touch.LeanFirstDown/Vector3Event::.ctor()
extern void Vector3Event__ctor_mEA09FA2966DC0F1AACFC10353DC133FD04255969 (void);
// 0x000000E5 System.Void Lean.Touch.LeanFirstDown/Vector2Event::.ctor()
extern void Vector2Event__ctor_m5FA03C9147AAC7045CBBE1AEFD9849BEAFCBA18C (void);
// 0x000000E6 System.Boolean Lean.Touch.LeanFirstDownCanvas::ElementOverlapped(Lean.Touch.LeanFinger)
extern void LeanFirstDownCanvas_ElementOverlapped_m0435BA2CE8F3B2363A8877D7801AB8EAE950B25C (void);
// 0x000000E7 System.Void Lean.Touch.LeanFirstDownCanvas::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanFirstDownCanvas_HandleFingerDown_m1FD9ECE0F953C0FE2DAB492D4A5E3C7C3583B51F (void);
// 0x000000E8 System.Void Lean.Touch.LeanFirstDownCanvas::.ctor()
extern void LeanFirstDownCanvas__ctor_mB780E502B8012EA1E0E2E08969A9A8232D0A982A (void);
// 0x000000E9 System.Void Lean.Touch.LeanGestureToggle::set_EnableWithoutIsolation(System.Boolean)
extern void LeanGestureToggle_set_EnableWithoutIsolation_m96A1A42CE752981F01AD452405BBAA2C6F2AB1C3 (void);
// 0x000000EA System.Boolean Lean.Touch.LeanGestureToggle::get_EnableWithoutIsolation()
extern void LeanGestureToggle_get_EnableWithoutIsolation_m33BDDE803A49DF0A1B62AFC6DB27D186F4FD0ED2 (void);
// 0x000000EB System.Void Lean.Touch.LeanGestureToggle::set_DragComponent(UnityEngine.MonoBehaviour)
extern void LeanGestureToggle_set_DragComponent_m6F0C5370EE07F1EB68A9FFD33EC9055A9C1FEDB4 (void);
// 0x000000EC UnityEngine.MonoBehaviour Lean.Touch.LeanGestureToggle::get_DragComponent()
extern void LeanGestureToggle_get_DragComponent_m1B3FE04A84CA57BB06C9C7021018DF8438035329 (void);
// 0x000000ED System.Void Lean.Touch.LeanGestureToggle::set_DragThreshold(System.Single)
extern void LeanGestureToggle_set_DragThreshold_m34C94194BB51CA10B0C89F963ACBA2227BDBFE46 (void);
// 0x000000EE System.Single Lean.Touch.LeanGestureToggle::get_DragThreshold()
extern void LeanGestureToggle_get_DragThreshold_m023F1B800BC0C034E24FB2163F2C12A1F8EA2FDB (void);
// 0x000000EF System.Void Lean.Touch.LeanGestureToggle::set_PinchComponent(UnityEngine.MonoBehaviour)
extern void LeanGestureToggle_set_PinchComponent_m58E341B38074E9ED707999E3801C3F1FE372CC3C (void);
// 0x000000F0 UnityEngine.MonoBehaviour Lean.Touch.LeanGestureToggle::get_PinchComponent()
extern void LeanGestureToggle_get_PinchComponent_m062917DF0E30259981338718E1DA3135ED2C1C29 (void);
// 0x000000F1 System.Void Lean.Touch.LeanGestureToggle::set_PinchThreshold(System.Single)
extern void LeanGestureToggle_set_PinchThreshold_mFBD7C9E662FB3A4F27DF7F285A0683C0BF624CB2 (void);
// 0x000000F2 System.Single Lean.Touch.LeanGestureToggle::get_PinchThreshold()
extern void LeanGestureToggle_get_PinchThreshold_m518D3E23F590FACEA24843F131475D28F41B65B1 (void);
// 0x000000F3 System.Void Lean.Touch.LeanGestureToggle::set_TwistComponent(UnityEngine.MonoBehaviour)
extern void LeanGestureToggle_set_TwistComponent_m244558A7F76814734A5D4CBD8F40380966CF11E8 (void);
// 0x000000F4 UnityEngine.MonoBehaviour Lean.Touch.LeanGestureToggle::get_TwistComponent()
extern void LeanGestureToggle_get_TwistComponent_m57B38E78F63A640DB9435552BDF8F2AD1A551FD6 (void);
// 0x000000F5 System.Void Lean.Touch.LeanGestureToggle::set_TwistThreshold(System.Single)
extern void LeanGestureToggle_set_TwistThreshold_mBF80AA39C3D6596F2F6A45F6A8CD675109250733 (void);
// 0x000000F6 System.Single Lean.Touch.LeanGestureToggle::get_TwistThreshold()
extern void LeanGestureToggle_get_TwistThreshold_m1E7C67B0FBC59C6E2AACD852E98568210DF94640 (void);
// 0x000000F7 System.Void Lean.Touch.LeanGestureToggle::set_TwistWithPinch(System.Boolean)
extern void LeanGestureToggle_set_TwistWithPinch_m13FB40EECFDE6F57A8D0529CFE3B8607D5CD39E5 (void);
// 0x000000F8 System.Boolean Lean.Touch.LeanGestureToggle::get_TwistWithPinch()
extern void LeanGestureToggle_get_TwistWithPinch_m0C25B76B52A3A83212E42B4E69C7C35302889A50 (void);
// 0x000000F9 System.Void Lean.Touch.LeanGestureToggle::AddFinger(Lean.Touch.LeanFinger)
extern void LeanGestureToggle_AddFinger_m830F5DD2EC12E79B36A0C8CED1BEA053D82031D4 (void);
// 0x000000FA System.Void Lean.Touch.LeanGestureToggle::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanGestureToggle_RemoveFinger_m42E89FF05EDF674024151F190A15BF3C2F3FFCC8 (void);
// 0x000000FB System.Void Lean.Touch.LeanGestureToggle::RemoveAllFingers()
extern void LeanGestureToggle_RemoveAllFingers_mC36B8421C9BEAA99756549E713D3EBF6C0FE80FF (void);
// 0x000000FC System.Void Lean.Touch.LeanGestureToggle::Awake()
extern void LeanGestureToggle_Awake_m1D18D54E5CFF8E3A244E0BA653E6D71F4BA40E2D (void);
// 0x000000FD System.Void Lean.Touch.LeanGestureToggle::Update()
extern void LeanGestureToggle_Update_mC508D596CC7DAFE29369C9E8EE063043114EBC26 (void);
// 0x000000FE System.Void Lean.Touch.LeanGestureToggle::.ctor()
extern void LeanGestureToggle__ctor_m0B8B98120FFB3593B1B5B7FE3492B5B5D39744D5 (void);
// 0x000000FF System.Void Lean.Touch.LeanLastUp::set_IgnoreStartedOverGui(System.Boolean)
extern void LeanLastUp_set_IgnoreStartedOverGui_m4835FE1AD2474F8DD177A65333F89DF296463D63 (void);
// 0x00000100 System.Boolean Lean.Touch.LeanLastUp::get_IgnoreStartedOverGui()
extern void LeanLastUp_get_IgnoreStartedOverGui_m0679DA95D2369CD8ACD165431036CE17815A6E17 (void);
// 0x00000101 System.Void Lean.Touch.LeanLastUp::set_RequiredSelectable(Lean.Common.LeanSelectable)
extern void LeanLastUp_set_RequiredSelectable_m1362EC581A086D5B3B8C435B19B338C9E2463EB5 (void);
// 0x00000102 Lean.Common.LeanSelectable Lean.Touch.LeanLastUp::get_RequiredSelectable()
extern void LeanLastUp_get_RequiredSelectable_mBF444DB2242D1216FCC6A71C8F4CCECB1A80ECB9 (void);
// 0x00000103 Lean.Touch.LeanLastUp/LeanFingerEvent Lean.Touch.LeanLastUp::get_OnFinger()
extern void LeanLastUp_get_OnFinger_m5B89DAFC1652AE3FB0069475E7B2827F88F60250 (void);
// 0x00000104 Lean.Touch.LeanLastUp/Vector3Event Lean.Touch.LeanLastUp::get_OnWorld()
extern void LeanLastUp_get_OnWorld_m1834851441EBD7E3C2A6861E5BB48DE4D2EA111B (void);
// 0x00000105 Lean.Touch.LeanLastUp/Vector2Event Lean.Touch.LeanLastUp::get_OnScreen()
extern void LeanLastUp_get_OnScreen_mB4F20403B6F89A51EBF7388FA22B51C685F1AE29 (void);
// 0x00000106 System.Void Lean.Touch.LeanLastUp::Awake()
extern void LeanLastUp_Awake_mEDD249DA964FBB611D9136026975041E5FCD8825 (void);
// 0x00000107 System.Void Lean.Touch.LeanLastUp::OnEnable()
extern void LeanLastUp_OnEnable_m66E969F41F15BD4B60F984D4501A107624DD5F93 (void);
// 0x00000108 System.Void Lean.Touch.LeanLastUp::OnDisable()
extern void LeanLastUp_OnDisable_mB0508F8CBF91B694B2E4AFC98B90B3D2D6C8F06C (void);
// 0x00000109 System.Void Lean.Touch.LeanLastUp::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanLastUp_HandleFingerDown_m48FAD54E463CD0DED1BCFD74719EA8ADEA969BF8 (void);
// 0x0000010A System.Void Lean.Touch.LeanLastUp::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanLastUp_HandleFingerUp_m5D5E75D9872F951BED018919AF9C26E97794ADE9 (void);
// 0x0000010B System.Void Lean.Touch.LeanLastUp::.ctor()
extern void LeanLastUp__ctor_m87497828079F3D3EEEC08C0BE281F93B4424D9C9 (void);
// 0x0000010C System.Void Lean.Touch.LeanLastUp/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m787E15476A7706A45021B2A01FF64997FBD6F1F2 (void);
// 0x0000010D System.Void Lean.Touch.LeanLastUp/Vector3Event::.ctor()
extern void Vector3Event__ctor_m90633122E5BEAFDDC897F1F574A2DC9DFA50970F (void);
// 0x0000010E System.Void Lean.Touch.LeanLastUp/Vector2Event::.ctor()
extern void Vector2Event__ctor_mE437ACD2C37A47415016CB9E683E6F0B8076CDE0 (void);
// 0x0000010F System.Boolean Lean.Touch.LeanLastUpCanvas::ElementOverlapped(Lean.Touch.LeanFinger)
extern void LeanLastUpCanvas_ElementOverlapped_mB4286D8D275FAD6EBD3364A1C918517FD6332F8E (void);
// 0x00000110 System.Void Lean.Touch.LeanLastUpCanvas::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanLastUpCanvas_HandleFingerDown_m95F5ADD02D7CA42F61C9937A7BEDD96B614E8ED3 (void);
// 0x00000111 System.Void Lean.Touch.LeanLastUpCanvas::.ctor()
extern void LeanLastUpCanvas__ctor_m7C634C63EEFD8D75B11A62814A0383697CA3B4DE (void);
// 0x00000112 System.Void Lean.Touch.LeanManualFlick::set_IgnoreIsOverGui(System.Boolean)
extern void LeanManualFlick_set_IgnoreIsOverGui_m8FA2B5123D80791CF33D3C2C57C8C83EE439F379 (void);
// 0x00000113 System.Boolean Lean.Touch.LeanManualFlick::get_IgnoreIsOverGui()
extern void LeanManualFlick_get_IgnoreIsOverGui_mB44881312C841880A829D74282D8F37B23B192BD (void);
// 0x00000114 System.Void Lean.Touch.LeanManualFlick::set_RequiredSelectable(Lean.Common.LeanSelectable)
extern void LeanManualFlick_set_RequiredSelectable_m8EBBF1E87EC06104273880F994B43DD7568E0AE1 (void);
// 0x00000115 Lean.Common.LeanSelectable Lean.Touch.LeanManualFlick::get_RequiredSelectable()
extern void LeanManualFlick_get_RequiredSelectable_m29C6F055BA08318A6402C1ED05389ECA9FC6645C (void);
// 0x00000116 System.Void Lean.Touch.LeanManualFlick::set_Check(Lean.Touch.LeanManualFlick/CheckType)
extern void LeanManualFlick_set_Check_m0C94181A73379AD29E55B8B6EDAB9C879E75B737 (void);
// 0x00000117 Lean.Touch.LeanManualFlick/CheckType Lean.Touch.LeanManualFlick::get_Check()
extern void LeanManualFlick_get_Check_mA8F9D077E89C7D3D4BBC0670497705EBE42D6AAB (void);
// 0x00000118 System.Void Lean.Touch.LeanManualFlick::AddFinger(Lean.Touch.LeanFinger)
extern void LeanManualFlick_AddFinger_m3CA41A7EC420DB84DFA4E0A217AC535E3C36B79D (void);
// 0x00000119 System.Void Lean.Touch.LeanManualFlick::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanManualFlick_RemoveFinger_m3996B361236FC36277078851A63C835A3CAF9506 (void);
// 0x0000011A System.Void Lean.Touch.LeanManualFlick::Start()
extern void LeanManualFlick_Start_m0E69E016B72FD897A2AA66CDC116CF188784860E (void);
// 0x0000011B System.Void Lean.Touch.LeanManualFlick::OnEnable()
extern void LeanManualFlick_OnEnable_m5279687BF636A690A812B79DB52979C24E8A1F5A (void);
// 0x0000011C System.Void Lean.Touch.LeanManualFlick::OnDisable()
extern void LeanManualFlick_OnDisable_mFA452322421DE5C0598B054609A284021D2F7DA2 (void);
// 0x0000011D System.Void Lean.Touch.LeanManualFlick::Update()
extern void LeanManualFlick_Update_m939F716064EBF40361E3835B14498E9EB9E5A48A (void);
// 0x0000011E System.Void Lean.Touch.LeanManualFlick::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanManualFlick_HandleFingerUp_m6AF6863F4EBBCFDBE454A1D68F4B7AC70DA6D1DB (void);
// 0x0000011F System.Boolean Lean.Touch.LeanManualFlick::TestFinger(Lean.Touch.LeanFinger,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanManualFlick_TestFinger_mA5F3E39030EACACF7CA879DA1135D6B09FC900C6 (void);
// 0x00000120 System.Void Lean.Touch.LeanManualFlick::.ctor()
extern void LeanManualFlick__ctor_m9B141318A6A6071B09CE1090D16969D965E22C26 (void);
// 0x00000121 System.Void Lean.Touch.LeanManualFlick/FingerData::.ctor()
extern void FingerData__ctor_mD311A6891E83D6137A088835C47A50BE643C3CED (void);
// 0x00000122 System.Void Lean.Touch.LeanManualSwipe::set_IgnoreIsOverGui(System.Boolean)
extern void LeanManualSwipe_set_IgnoreIsOverGui_mF432DD384DC0760CA6F1812F009533FDC13F4670 (void);
// 0x00000123 System.Boolean Lean.Touch.LeanManualSwipe::get_IgnoreIsOverGui()
extern void LeanManualSwipe_get_IgnoreIsOverGui_mE8F2D2A41BEA9207EC9A8C8000C8E8E69928EFC5 (void);
// 0x00000124 System.Void Lean.Touch.LeanManualSwipe::set_RequiredSelectable(Lean.Common.LeanSelectable)
extern void LeanManualSwipe_set_RequiredSelectable_m5E7071585BF9F1017E53076E0CA5E97865785EF6 (void);
// 0x00000125 Lean.Common.LeanSelectable Lean.Touch.LeanManualSwipe::get_RequiredSelectable()
extern void LeanManualSwipe_get_RequiredSelectable_mF815F8104289D6458D203ACC23139A6D83826E6F (void);
// 0x00000126 System.Void Lean.Touch.LeanManualSwipe::AddFinger(Lean.Touch.LeanFinger)
extern void LeanManualSwipe_AddFinger_m9AB1712843D8220D610E44F84A864676B5E5E5DA (void);
// 0x00000127 System.Void Lean.Touch.LeanManualSwipe::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanManualSwipe_RemoveFinger_m0204875930D2808E64CE073671A8C36B5CEECC95 (void);
// 0x00000128 System.Void Lean.Touch.LeanManualSwipe::Start()
extern void LeanManualSwipe_Start_mE9FA39FA8EF54D19CF64F20B8790CC4A8C7B8F20 (void);
// 0x00000129 System.Void Lean.Touch.LeanManualSwipe::OnEnable()
extern void LeanManualSwipe_OnEnable_mC2A45DA6A7E6D7F8ED791186171F013B5C26197A (void);
// 0x0000012A System.Void Lean.Touch.LeanManualSwipe::OnDisable()
extern void LeanManualSwipe_OnDisable_m8C2F01870A09A59E414C1921EC07F1C77140449F (void);
// 0x0000012B System.Void Lean.Touch.LeanManualSwipe::HandleFingerSwipe(Lean.Touch.LeanFinger)
extern void LeanManualSwipe_HandleFingerSwipe_mCE292CCB3DB42CC7F6DB9AC62970654DEC69913E (void);
// 0x0000012C System.Void Lean.Touch.LeanManualSwipe::.ctor()
extern void LeanManualSwipe__ctor_mDA40C9C2210B768E3639394D209F98441B11FC3B (void);
// 0x0000012D System.Void Lean.Touch.LeanMouseWheel::set_RequiredSelectable(Lean.Common.LeanSelectable)
extern void LeanMouseWheel_set_RequiredSelectable_mAE09646F2E803C530695897889D652F35B910432 (void);
// 0x0000012E Lean.Common.LeanSelectable Lean.Touch.LeanMouseWheel::get_RequiredSelectable()
extern void LeanMouseWheel_get_RequiredSelectable_mBB68AFEC7FB18F7122AC61117E924C840D528D95 (void);
// 0x0000012F System.Void Lean.Touch.LeanMouseWheel::set_RequiredMouseButton(System.Int32)
extern void LeanMouseWheel_set_RequiredMouseButton_m7B4B4F0040F474CD2F495277A5122FA5D412C40C (void);
// 0x00000130 System.Int32 Lean.Touch.LeanMouseWheel::get_RequiredMouseButton()
extern void LeanMouseWheel_get_RequiredMouseButton_m04040A1387F64C078454EDAE34088978870306EA (void);
// 0x00000131 System.Void Lean.Touch.LeanMouseWheel::set_Modify(Lean.Touch.LeanMouseWheel/ModifyType)
extern void LeanMouseWheel_set_Modify_m525B60117EFD28F2F5CDC3F7D907B710351EF2D4 (void);
// 0x00000132 Lean.Touch.LeanMouseWheel/ModifyType Lean.Touch.LeanMouseWheel::get_Modify()
extern void LeanMouseWheel_get_Modify_m2F74E38130BCB22AF4238EDCED6821E9B3FF90A5 (void);
// 0x00000133 System.Void Lean.Touch.LeanMouseWheel::set_Multiplier(System.Single)
extern void LeanMouseWheel_set_Multiplier_mA2B26982F2A5DEB91134205F78F8CA95CD4FA6D2 (void);
// 0x00000134 System.Single Lean.Touch.LeanMouseWheel::get_Multiplier()
extern void LeanMouseWheel_get_Multiplier_m61B4AD68B9D70CD75B1DCA68B1057AC62222D9B7 (void);
// 0x00000135 System.Void Lean.Touch.LeanMouseWheel::set_Coordinate(Lean.Touch.LeanMouseWheel/CoordinateType)
extern void LeanMouseWheel_set_Coordinate_m02AAB4D9D2933DDBE0105641635829644F9A74EB (void);
// 0x00000136 Lean.Touch.LeanMouseWheel/CoordinateType Lean.Touch.LeanMouseWheel::get_Coordinate()
extern void LeanMouseWheel_get_Coordinate_m9E78BDD05E4A982EE21D049FD46DA084531006BB (void);
// 0x00000137 Lean.Touch.LeanMouseWheel/FloatEvent Lean.Touch.LeanMouseWheel::get_OnDelta()
extern void LeanMouseWheel_get_OnDelta_m6FB6D3D3107A0E8BC081F6D976BE6348C114DEAC (void);
// 0x00000138 System.Void Lean.Touch.LeanMouseWheel::Awake()
extern void LeanMouseWheel_Awake_mE7CC0EAD14E1F783A4C125B3DC51436252CFC3F7 (void);
// 0x00000139 System.Void Lean.Touch.LeanMouseWheel::Update()
extern void LeanMouseWheel_Update_m7AEC66D31603FA29C8004585B7D1BB240B134EFA (void);
// 0x0000013A System.Void Lean.Touch.LeanMouseWheel::.ctor()
extern void LeanMouseWheel__ctor_m2B7C94435EC9C29045A8CEDB19CB5916163690E7 (void);
// 0x0000013B System.Void Lean.Touch.LeanMouseWheel/FloatEvent::.ctor()
extern void FloatEvent__ctor_m4045C9F75FE81B33436A051E04419DEB1BA0FC33 (void);
// 0x0000013C System.Void Lean.Touch.LeanMultiDirection::set_IgnoreIfStatic(System.Boolean)
extern void LeanMultiDirection_set_IgnoreIfStatic_m59F66088AE16777D8A980A9B273B73E7A590DC1D (void);
// 0x0000013D System.Boolean Lean.Touch.LeanMultiDirection::get_IgnoreIfStatic()
extern void LeanMultiDirection_get_IgnoreIfStatic_m8BDF807282F0F6A7CE1B3711DD57041107EAE5C0 (void);
// 0x0000013E System.Void Lean.Touch.LeanMultiDirection::set_Angle(System.Single)
extern void LeanMultiDirection_set_Angle_mB1B0E0C1C32293F9ED9B40E205E961E0ADA9DA66 (void);
// 0x0000013F System.Single Lean.Touch.LeanMultiDirection::get_Angle()
extern void LeanMultiDirection_get_Angle_m421C718CE152DB516B37B2C2996FE768732783DB (void);
// 0x00000140 System.Void Lean.Touch.LeanMultiDirection::set_OneWay(System.Boolean)
extern void LeanMultiDirection_set_OneWay_m42889C65CB17DFF7C5DBC6C2F11CF4343F655620 (void);
// 0x00000141 System.Boolean Lean.Touch.LeanMultiDirection::get_OneWay()
extern void LeanMultiDirection_get_OneWay_m061167E0AE4E3A49DB9A470862B1AE13F8DA76DC (void);
// 0x00000142 System.Void Lean.Touch.LeanMultiDirection::set_Coordinate(Lean.Touch.LeanMultiDirection/CoordinateType)
extern void LeanMultiDirection_set_Coordinate_m55BAA7434A371DBCE72E750B7909C685FC63B798 (void);
// 0x00000143 Lean.Touch.LeanMultiDirection/CoordinateType Lean.Touch.LeanMultiDirection::get_Coordinate()
extern void LeanMultiDirection_get_Coordinate_m203D60604F845796196BD607E73D5E95EB215151 (void);
// 0x00000144 System.Void Lean.Touch.LeanMultiDirection::set_Multiplier(System.Single)
extern void LeanMultiDirection_set_Multiplier_m7574B35F1BDF8B8E6B85C0EA587DC36C23626BBF (void);
// 0x00000145 System.Single Lean.Touch.LeanMultiDirection::get_Multiplier()
extern void LeanMultiDirection_get_Multiplier_m83CDB138108A8A86F8D554071BE61446E2089699 (void);
// 0x00000146 Lean.Touch.LeanMultiDirection/FloatEvent Lean.Touch.LeanMultiDirection::get_OnDelta()
extern void LeanMultiDirection_get_OnDelta_m00E27763A19E6C5479B4260BB1F0FD570E133455 (void);
// 0x00000147 System.Void Lean.Touch.LeanMultiDirection::AddFinger(Lean.Touch.LeanFinger)
extern void LeanMultiDirection_AddFinger_mD2EC33C1379E7CD106B3D5E63BB85D8FDF4BBA4B (void);
// 0x00000148 System.Void Lean.Touch.LeanMultiDirection::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanMultiDirection_RemoveFinger_mD49C8188E41C1F5E499573E4EEEF78511F76F7E0 (void);
// 0x00000149 System.Void Lean.Touch.LeanMultiDirection::RemoveAllFingers()
extern void LeanMultiDirection_RemoveAllFingers_m1DA8D6FB78AC0BE8973030009351C0DD7E1D73AA (void);
// 0x0000014A System.Void Lean.Touch.LeanMultiDirection::Awake()
extern void LeanMultiDirection_Awake_m915F3F474CB2792EC14FEE125D1AF1C2EF5D7028 (void);
// 0x0000014B System.Void Lean.Touch.LeanMultiDirection::Update()
extern void LeanMultiDirection_Update_m29DE695087FEC1DA3A27F75FF5080E45747B4B4B (void);
// 0x0000014C System.Void Lean.Touch.LeanMultiDirection::.ctor()
extern void LeanMultiDirection__ctor_m4301C82CEA2635B1CC54B42F1902DE4E573758AA (void);
// 0x0000014D System.Void Lean.Touch.LeanMultiDirection/FloatEvent::.ctor()
extern void FloatEvent__ctor_m9B3015223FB628FA6DF8DECDAC5919598322174F (void);
// 0x0000014E System.Void Lean.Touch.LeanMultiDown::set_IgnoreStartedOverGui(System.Boolean)
extern void LeanMultiDown_set_IgnoreStartedOverGui_m8824187750178D2E7DA23EF11EDE49A75DD27EE6 (void);
// 0x0000014F System.Boolean Lean.Touch.LeanMultiDown::get_IgnoreStartedOverGui()
extern void LeanMultiDown_get_IgnoreStartedOverGui_m2EA8BBCAE7E3414FC32218CA45DF6FA3A27824C8 (void);
// 0x00000150 System.Void Lean.Touch.LeanMultiDown::set_RequiredSelectable(Lean.Common.LeanSelectable)
extern void LeanMultiDown_set_RequiredSelectable_mCDA39E15058FB71A4A079362096E196D11C6CDDC (void);
// 0x00000151 Lean.Common.LeanSelectable Lean.Touch.LeanMultiDown::get_RequiredSelectable()
extern void LeanMultiDown_get_RequiredSelectable_m2B96DF2D525069831FC1CF81145C03AF95662B53 (void);
// 0x00000152 System.Void Lean.Touch.LeanMultiDown::set_RequiredCount(System.Int32)
extern void LeanMultiDown_set_RequiredCount_m73695852E2F14FE7E83031E6435186FF763A1791 (void);
// 0x00000153 System.Int32 Lean.Touch.LeanMultiDown::get_RequiredCount()
extern void LeanMultiDown_get_RequiredCount_m59636B58D498CEDCD88DB28713D010381C63AFA5 (void);
// 0x00000154 Lean.Touch.LeanMultiDown/LeanFingerListEvent Lean.Touch.LeanMultiDown::get_OnFingers()
extern void LeanMultiDown_get_OnFingers_m75BB92BB267C16D9E3684E8C3EB34F6A95053BCF (void);
// 0x00000155 Lean.Touch.LeanMultiDown/Vector3Event Lean.Touch.LeanMultiDown::get_OnWorld()
extern void LeanMultiDown_get_OnWorld_m9C260D61BAB42358BA925E793334E4C487796CE2 (void);
// 0x00000156 Lean.Touch.LeanMultiDown/Vector2Event Lean.Touch.LeanMultiDown::get_OnScreen()
extern void LeanMultiDown_get_OnScreen_m31EA600570A38E529FDDF8F9AE37FCBDE6CA6CCC (void);
// 0x00000157 System.Void Lean.Touch.LeanMultiDown::Awake()
extern void LeanMultiDown_Awake_m22EA823902235773F9F0C0AA818A179B2B78A41C (void);
// 0x00000158 System.Void Lean.Touch.LeanMultiDown::OnEnable()
extern void LeanMultiDown_OnEnable_mCD263348B633C7A9ED38F923FAD137C9E09BD91F (void);
// 0x00000159 System.Void Lean.Touch.LeanMultiDown::OnDisable()
extern void LeanMultiDown_OnDisable_m98DC029884DB2A0DE3D159B46CDF10D7B943486C (void);
// 0x0000015A System.Void Lean.Touch.LeanMultiDown::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanMultiDown_HandleFingerDown_m785DF29737757D63B0A821611959166984A24695 (void);
// 0x0000015B System.Void Lean.Touch.LeanMultiDown::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanMultiDown_HandleFingerUp_m3530C86D55BF3FB28C48D77B8DB730320A9A8D33 (void);
// 0x0000015C System.Void Lean.Touch.LeanMultiDown::.ctor()
extern void LeanMultiDown__ctor_m103E0479CCCAB35E81DBBF171B5FCE0ED159D9DB (void);
// 0x0000015D System.Void Lean.Touch.LeanMultiDown/LeanFingerListEvent::.ctor()
extern void LeanFingerListEvent__ctor_m8204585BF37E50623E14FA4A39E55DFB457ECF98 (void);
// 0x0000015E System.Void Lean.Touch.LeanMultiDown/Vector3Event::.ctor()
extern void Vector3Event__ctor_m97468BA6E523A6D1536FC38520D78F8C3F30EB07 (void);
// 0x0000015F System.Void Lean.Touch.LeanMultiDown/Vector2Event::.ctor()
extern void Vector2Event__ctor_m177A0E2F3260A2AE9E9B6208F4634680DB518CEC (void);
// 0x00000160 System.Void Lean.Touch.LeanMultiHeld::set_IgnoreStartedOverGui(System.Boolean)
extern void LeanMultiHeld_set_IgnoreStartedOverGui_m74607D368ED9AA5A3382F52B12AACC82983B82A4 (void);
// 0x00000161 System.Boolean Lean.Touch.LeanMultiHeld::get_IgnoreStartedOverGui()
extern void LeanMultiHeld_get_IgnoreStartedOverGui_m71B27D3C8D645D4F1AD74E5DF509582CAE3D4889 (void);
// 0x00000162 System.Void Lean.Touch.LeanMultiHeld::set_IgnoreIsOverGui(System.Boolean)
extern void LeanMultiHeld_set_IgnoreIsOverGui_m7E030EC9EB4F7724CC25E96A506E1496384C0FE8 (void);
// 0x00000163 System.Boolean Lean.Touch.LeanMultiHeld::get_IgnoreIsOverGui()
extern void LeanMultiHeld_get_IgnoreIsOverGui_m2631C18BC5A5B99DB9838F3D72BA7C89E805F04A (void);
// 0x00000164 System.Void Lean.Touch.LeanMultiHeld::set_RequiredSelectable(Lean.Common.LeanSelectable)
extern void LeanMultiHeld_set_RequiredSelectable_m026F5E53228389CA7F4037C751B1036314B6DB32 (void);
// 0x00000165 Lean.Common.LeanSelectable Lean.Touch.LeanMultiHeld::get_RequiredSelectable()
extern void LeanMultiHeld_get_RequiredSelectable_m891F15086C0B0E5F5E43F1C2C771D10C0B7AE043 (void);
// 0x00000166 System.Void Lean.Touch.LeanMultiHeld::set_RequiredCount(System.Int32)
extern void LeanMultiHeld_set_RequiredCount_m18FD5B0E2C1187D96275CB21FB1794D3473F95A2 (void);
// 0x00000167 System.Int32 Lean.Touch.LeanMultiHeld::get_RequiredCount()
extern void LeanMultiHeld_get_RequiredCount_mDE1182A00978C2DF399B79E81D8887F5A5DCA7C4 (void);
// 0x00000168 System.Void Lean.Touch.LeanMultiHeld::set_MinimumAge(System.Single)
extern void LeanMultiHeld_set_MinimumAge_mFCAE881D3CFFC8BB34DA2971900312A488BF95DF (void);
// 0x00000169 System.Single Lean.Touch.LeanMultiHeld::get_MinimumAge()
extern void LeanMultiHeld_get_MinimumAge_mEA9CB90959925A29AB48E362EA9EFF0F66962D93 (void);
// 0x0000016A System.Void Lean.Touch.LeanMultiHeld::set_MaximumMovement(System.Single)
extern void LeanMultiHeld_set_MaximumMovement_mCEA09B6D267ED2378235DBE30EBF8FD827BC159D (void);
// 0x0000016B System.Single Lean.Touch.LeanMultiHeld::get_MaximumMovement()
extern void LeanMultiHeld_get_MaximumMovement_mCD27F6CD4EBF031DFCCCDB27B3F472B338ECCF6C (void);
// 0x0000016C Lean.Touch.LeanMultiHeld/LeanFingerListEvent Lean.Touch.LeanMultiHeld::get_OnFingersDown()
extern void LeanMultiHeld_get_OnFingersDown_mBB758E0610750FEE5CF7D3F6A1ED69A43CFE021C (void);
// 0x0000016D Lean.Touch.LeanMultiHeld/LeanFingerListEvent Lean.Touch.LeanMultiHeld::get_OnFingersUpdate()
extern void LeanMultiHeld_get_OnFingersUpdate_m2AAEB2874CB61CFE6A7436B1F5595FE637720585 (void);
// 0x0000016E Lean.Touch.LeanMultiHeld/LeanFingerListEvent Lean.Touch.LeanMultiHeld::get_OnFingersUp()
extern void LeanMultiHeld_get_OnFingersUp_mF8B35640DE32FF8AB4CD57C5E9BD12B278B837E3 (void);
// 0x0000016F Lean.Touch.LeanMultiHeld/Vector3Event Lean.Touch.LeanMultiHeld::get_OnWorldDown()
extern void LeanMultiHeld_get_OnWorldDown_mF4332D257CB4FC2723DC529F84EAB99EF4F444F6 (void);
// 0x00000170 Lean.Touch.LeanMultiHeld/Vector3Event Lean.Touch.LeanMultiHeld::get_OnWorldUpdate()
extern void LeanMultiHeld_get_OnWorldUpdate_mE1BB4471F34DA52F509E3FC258E922E04D472E4B (void);
// 0x00000171 Lean.Touch.LeanMultiHeld/Vector3Event Lean.Touch.LeanMultiHeld::get_OnWorldUp()
extern void LeanMultiHeld_get_OnWorldUp_mC61FF5555F8A7438673952D6C045CD8E0452F9EB (void);
// 0x00000172 Lean.Touch.LeanMultiHeld/Vector2Event Lean.Touch.LeanMultiHeld::get_OnScreenDown()
extern void LeanMultiHeld_get_OnScreenDown_mE48E160CD161E689A48F2EA4462908CEA402CB95 (void);
// 0x00000173 Lean.Touch.LeanMultiHeld/Vector2Event Lean.Touch.LeanMultiHeld::get_OnScreenUpdate()
extern void LeanMultiHeld_get_OnScreenUpdate_m0D8DA94C135954800ECAE2199B14593250C96451 (void);
// 0x00000174 Lean.Touch.LeanMultiHeld/Vector2Event Lean.Touch.LeanMultiHeld::get_OnScreenUp()
extern void LeanMultiHeld_get_OnScreenUp_m810BFFDB8040D255E1F11891411176E8FD459756 (void);
// 0x00000175 System.Void Lean.Touch.LeanMultiHeld::Awake()
extern void LeanMultiHeld_Awake_mD74A7974661C78D3B32BE0CFC737237C24A6D4B6 (void);
// 0x00000176 System.Void Lean.Touch.LeanMultiHeld::OnEnable()
extern void LeanMultiHeld_OnEnable_m3D590BFEE575B3D47E80727D317D959738DF7840 (void);
// 0x00000177 System.Void Lean.Touch.LeanMultiHeld::OnDisable()
extern void LeanMultiHeld_OnDisable_m0F14878D8762CC1493005597B97F6920F8C56049 (void);
// 0x00000178 System.Void Lean.Touch.LeanMultiHeld::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanMultiHeld_HandleFingerDown_m5D4A0B729B4EE24F2931913937C0F5405603519D (void);
// 0x00000179 System.Void Lean.Touch.LeanMultiHeld::HandleGesture(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanMultiHeld_HandleGesture_m19023B8707709192A26919D3CCD902D4453CC9FA (void);
// 0x0000017A System.Boolean Lean.Touch.LeanMultiHeld::get_IsHeld()
extern void LeanMultiHeld_get_IsHeld_mFE0E1928CD2CD72F69A42FBDE3B2CA0BB3FB1545 (void);
// 0x0000017B System.Void Lean.Touch.LeanMultiHeld::InvokeDown()
extern void LeanMultiHeld_InvokeDown_mDD2C43175C2B8C5B14E22FFA2D5264FB76646ED7 (void);
// 0x0000017C System.Void Lean.Touch.LeanMultiHeld::InvokeUpdate()
extern void LeanMultiHeld_InvokeUpdate_m04D0B6FB3422C6A75D1E0869A7569A51C18200DE (void);
// 0x0000017D System.Void Lean.Touch.LeanMultiHeld::InvokeUp()
extern void LeanMultiHeld_InvokeUp_m0D8373A4280F8141D1FF0F1B823EDE97A1EE02C6 (void);
// 0x0000017E System.Void Lean.Touch.LeanMultiHeld::.ctor()
extern void LeanMultiHeld__ctor_m7DCD0E18904B460CFC4E927E2DE295DBA1A20717 (void);
// 0x0000017F System.Void Lean.Touch.LeanMultiHeld/FingerData::.ctor()
extern void FingerData__ctor_mA9DD5E40BE6AAF8BFF0BDD76CDAB91C5E8AC0985 (void);
// 0x00000180 System.Void Lean.Touch.LeanMultiHeld/LeanFingerListEvent::.ctor()
extern void LeanFingerListEvent__ctor_m94FBECE94E0BB95C1E226E908CDCE0AEF5A541A1 (void);
// 0x00000181 System.Void Lean.Touch.LeanMultiHeld/Vector3Event::.ctor()
extern void Vector3Event__ctor_mAEE74001980C33AD99AA240E073A0F12F2B5CCA1 (void);
// 0x00000182 System.Void Lean.Touch.LeanMultiHeld/Vector2Event::.ctor()
extern void Vector2Event__ctor_mBC7EA484E139C93839A4C1655F0CE7BAB2389BB3 (void);
// 0x00000183 System.Void Lean.Touch.LeanMultiPinch::set_IgnoreIfStatic(System.Boolean)
extern void LeanMultiPinch_set_IgnoreIfStatic_m19ACA6415DC8DA3F71400E0612AE2C6906DAACFF (void);
// 0x00000184 System.Boolean Lean.Touch.LeanMultiPinch::get_IgnoreIfStatic()
extern void LeanMultiPinch_get_IgnoreIfStatic_m4117EA83A506626FBDC560C0E620C0430FA1F519 (void);
// 0x00000185 System.Void Lean.Touch.LeanMultiPinch::set_Coordinate(Lean.Touch.LeanMultiPinch/CoordinateType)
extern void LeanMultiPinch_set_Coordinate_m6B77117835B5452DC27AE307CFB1EEC4727952EC (void);
// 0x00000186 Lean.Touch.LeanMultiPinch/CoordinateType Lean.Touch.LeanMultiPinch::get_Coordinate()
extern void LeanMultiPinch_get_Coordinate_m8A68F7FA1A1DDC10309429AE0DFB5352A1684293 (void);
// 0x00000187 System.Void Lean.Touch.LeanMultiPinch::set_Multiplier(System.Single)
extern void LeanMultiPinch_set_Multiplier_m7EC11D3D000DF92360805FF1C9787A2D753BCE96 (void);
// 0x00000188 System.Single Lean.Touch.LeanMultiPinch::get_Multiplier()
extern void LeanMultiPinch_get_Multiplier_m7363109E14F67B734457F5B2F4DE2F569BFC31CB (void);
// 0x00000189 Lean.Touch.LeanMultiPinch/FloatEvent Lean.Touch.LeanMultiPinch::get_OnPinch()
extern void LeanMultiPinch_get_OnPinch_m097281DBFD5EA8687B3CE2C0A600B901F80B8B73 (void);
// 0x0000018A System.Void Lean.Touch.LeanMultiPinch::AddFinger(Lean.Touch.LeanFinger)
extern void LeanMultiPinch_AddFinger_m82847C33174311E48D084D8CE145891B96F45C85 (void);
// 0x0000018B System.Void Lean.Touch.LeanMultiPinch::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanMultiPinch_RemoveFinger_m47EEF9D6C541025E3203B169873DC685981BBDAC (void);
// 0x0000018C System.Void Lean.Touch.LeanMultiPinch::RemoveAllFingers()
extern void LeanMultiPinch_RemoveAllFingers_m177C5C7997CB03DCD8BE5245E9C143B4D4D60C58 (void);
// 0x0000018D System.Void Lean.Touch.LeanMultiPinch::Awake()
extern void LeanMultiPinch_Awake_m04005867994A365BE7F4F9A873D875A3D9660664 (void);
// 0x0000018E System.Void Lean.Touch.LeanMultiPinch::Update()
extern void LeanMultiPinch_Update_mC238C5C776D3B4CF1B6DE5BFABEE1ED8AA6947CA (void);
// 0x0000018F System.Void Lean.Touch.LeanMultiPinch::.ctor()
extern void LeanMultiPinch__ctor_m07EFB01364F69ABD52AF2FC4D84F8A83A1BD7834 (void);
// 0x00000190 System.Void Lean.Touch.LeanMultiPinch/FloatEvent::.ctor()
extern void FloatEvent__ctor_m910CE5D3BE2A3DCEFA6CE1D703284732E615E656 (void);
// 0x00000191 System.Void Lean.Touch.LeanMultiPull::set_Coordinate(Lean.Touch.LeanMultiPull/CoordinateType)
extern void LeanMultiPull_set_Coordinate_m6F3B7FFDF9BF1DA5D0C272F1417CC8DEBE569677 (void);
// 0x00000192 Lean.Touch.LeanMultiPull/CoordinateType Lean.Touch.LeanMultiPull::get_Coordinate()
extern void LeanMultiPull_get_Coordinate_m7426C239A87B0862135417C3AEA6A8B1120C5B7C (void);
// 0x00000193 System.Void Lean.Touch.LeanMultiPull::set_Multiplier(System.Single)
extern void LeanMultiPull_set_Multiplier_mA2FFA9EB98BA39F30E772CD1C2E8E1B5FA2AC5C2 (void);
// 0x00000194 System.Single Lean.Touch.LeanMultiPull::get_Multiplier()
extern void LeanMultiPull_get_Multiplier_m446D29E8E16D102B423C47F52543294F7AEF9FB3 (void);
// 0x00000195 System.Void Lean.Touch.LeanMultiPull::set_ScaleByTime(System.Boolean)
extern void LeanMultiPull_set_ScaleByTime_m31C14AB6EBABB8BF2922E54CFF1B728ABB6D3AE9 (void);
// 0x00000196 System.Boolean Lean.Touch.LeanMultiPull::get_ScaleByTime()
extern void LeanMultiPull_get_ScaleByTime_mAD22E7A54852A51CBDBDCDA08D32423D7EB8C975 (void);
// 0x00000197 Lean.Touch.LeanMultiPull/Vector2Event Lean.Touch.LeanMultiPull::get_OnVector()
extern void LeanMultiPull_get_OnVector_m7AED6D30E4B11F3E5585F541AFA76E1764BEB329 (void);
// 0x00000198 Lean.Touch.LeanMultiPull/FloatEvent Lean.Touch.LeanMultiPull::get_OnDistance()
extern void LeanMultiPull_get_OnDistance_m983EE6BB3CD2F025E0869DA7F540D48DAE853B50 (void);
// 0x00000199 Lean.Touch.LeanMultiPull/Vector3Event Lean.Touch.LeanMultiPull::get_OnWorldFrom()
extern void LeanMultiPull_get_OnWorldFrom_m4D1F21BC1B0BDE96B412323B977852F02B81833D (void);
// 0x0000019A Lean.Touch.LeanMultiPull/Vector3Event Lean.Touch.LeanMultiPull::get_OnWorldTo()
extern void LeanMultiPull_get_OnWorldTo_m1AD2508BD14D0F12CA944E5D3832D4987BD3FAB4 (void);
// 0x0000019B Lean.Touch.LeanMultiPull/Vector3Event Lean.Touch.LeanMultiPull::get_OnWorldDelta()
extern void LeanMultiPull_get_OnWorldDelta_mC04F22EDBB977AFED747DE4C88A0779696892CD2 (void);
// 0x0000019C Lean.Touch.LeanMultiPull/Vector3Vector3Event Lean.Touch.LeanMultiPull::get_OnWorldFromTo()
extern void LeanMultiPull_get_OnWorldFromTo_mF0CB88EC446B0F9E9709F648A73B72B854147AA1 (void);
// 0x0000019D System.Void Lean.Touch.LeanMultiPull::AddFinger(Lean.Touch.LeanFinger)
extern void LeanMultiPull_AddFinger_mDB8B17400C14A9C321AEFC615BCB9D23125B1CEF (void);
// 0x0000019E System.Void Lean.Touch.LeanMultiPull::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanMultiPull_RemoveFinger_m1BE6D3744733E343D8C46068554B1C34D07C3ED3 (void);
// 0x0000019F System.Void Lean.Touch.LeanMultiPull::RemoveAllFingers()
extern void LeanMultiPull_RemoveAllFingers_m985DB24046F6053BBF68950AF2267C00BCCE9146 (void);
// 0x000001A0 System.Void Lean.Touch.LeanMultiPull::Awake()
extern void LeanMultiPull_Awake_m9ACEBBC454E3AF7F8314D0D7961C8AE2D65EC021 (void);
// 0x000001A1 System.Void Lean.Touch.LeanMultiPull::Update()
extern void LeanMultiPull_Update_m33E10DF675FA43EB94EA28047E78BEEB8D33A4B5 (void);
// 0x000001A2 System.Void Lean.Touch.LeanMultiPull::.ctor()
extern void LeanMultiPull__ctor_mB074B7810C30600D277073CBA7000A936B553B1D (void);
// 0x000001A3 System.Void Lean.Touch.LeanMultiPull/FloatEvent::.ctor()
extern void FloatEvent__ctor_mF15EDE985E0976EDB7D781B74792093901C0C68D (void);
// 0x000001A4 System.Void Lean.Touch.LeanMultiPull/Vector2Event::.ctor()
extern void Vector2Event__ctor_mBCDC9F6B503D0E4B630539787D357D423FD4A87A (void);
// 0x000001A5 System.Void Lean.Touch.LeanMultiPull/Vector3Event::.ctor()
extern void Vector3Event__ctor_m59E022854A40CE20ACF20D90A21A9BE4FD19DAC8 (void);
// 0x000001A6 System.Void Lean.Touch.LeanMultiPull/Vector3Vector3Event::.ctor()
extern void Vector3Vector3Event__ctor_m411E7090968D0F493BD715BBEE3AE1F1D0D6B1BD (void);
// 0x000001A7 System.Void Lean.Touch.LeanMultiSwipe::set_ScaledDistanceThreshold(System.Single)
extern void LeanMultiSwipe_set_ScaledDistanceThreshold_m61BC006B77FFFA4010AE869A270BD2F6842A647D (void);
// 0x000001A8 System.Single Lean.Touch.LeanMultiSwipe::get_ScaledDistanceThreshold()
extern void LeanMultiSwipe_get_ScaledDistanceThreshold_mF9D74A6431C6AEA27E5940DDC77EA688E0F42F0F (void);
// 0x000001A9 System.Void Lean.Touch.LeanMultiSwipe::set_ParallelAngleThreshold(System.Single)
extern void LeanMultiSwipe_set_ParallelAngleThreshold_m6C1F1B70840D054A7F030CBCD15BCAB03963463E (void);
// 0x000001AA System.Single Lean.Touch.LeanMultiSwipe::get_ParallelAngleThreshold()
extern void LeanMultiSwipe_get_ParallelAngleThreshold_m205A378E2117A2476F3D3C458D5B25E6E0B22D70 (void);
// 0x000001AB System.Void Lean.Touch.LeanMultiSwipe::set_PinchScaledDistanceThreshold(System.Single)
extern void LeanMultiSwipe_set_PinchScaledDistanceThreshold_m6EAB1F05001B73339C78B6FE46938CF242667FB3 (void);
// 0x000001AC System.Single Lean.Touch.LeanMultiSwipe::get_PinchScaledDistanceThreshold()
extern void LeanMultiSwipe_get_PinchScaledDistanceThreshold_mC366D8FCB4402B3C4C7FCFB76FEC1EECEC6E609F (void);
// 0x000001AD Lean.Touch.LeanMultiSwipe/FingerListEvent Lean.Touch.LeanMultiSwipe::get_OnFingers()
extern void LeanMultiSwipe_get_OnFingers_m98EAA402E18319C464601E7671DDCBDC9ADE30FB (void);
// 0x000001AE Lean.Touch.LeanMultiSwipe/Vector2Event Lean.Touch.LeanMultiSwipe::get_OnSwipeParallel()
extern void LeanMultiSwipe_get_OnSwipeParallel_m30326C970914744D3CA3EB5820CCA83CE95F6C46 (void);
// 0x000001AF Lean.Touch.LeanMultiSwipe/FloatEvent Lean.Touch.LeanMultiSwipe::get_OnSwipeIn()
extern void LeanMultiSwipe_get_OnSwipeIn_m299E6A298754F7FA126829C866C2A142C7561BCB (void);
// 0x000001B0 Lean.Touch.LeanMultiSwipe/FloatEvent Lean.Touch.LeanMultiSwipe::get_OnSwipeOut()
extern void LeanMultiSwipe_get_OnSwipeOut_mDC1B37FEE98827C117BC0F7A1D03FE0A59D5AF80 (void);
// 0x000001B1 System.Void Lean.Touch.LeanMultiSwipe::AddFinger(Lean.Touch.LeanFinger)
extern void LeanMultiSwipe_AddFinger_m5FA130A4012E6B7542EB6782C1E70DB968EE6873 (void);
// 0x000001B2 System.Void Lean.Touch.LeanMultiSwipe::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanMultiSwipe_RemoveFinger_mE82E74E095BEA73B1818DE197E0705908D57EA19 (void);
// 0x000001B3 System.Void Lean.Touch.LeanMultiSwipe::RemoveAllFingers()
extern void LeanMultiSwipe_RemoveAllFingers_m5F181D47CDA6CF36D8E0194B4EE38A9CEBF79EEB (void);
// 0x000001B4 System.Void Lean.Touch.LeanMultiSwipe::Awake()
extern void LeanMultiSwipe_Awake_mBADCB16DC7CC114BF5718EE486511CAB33343B25 (void);
// 0x000001B5 System.Void Lean.Touch.LeanMultiSwipe::Update()
extern void LeanMultiSwipe_Update_m6678BD6A9088FD3C2A6367BF126578E56054B498 (void);
// 0x000001B6 System.Void Lean.Touch.LeanMultiSwipe::FingerSwipe(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,Lean.Touch.LeanFinger)
extern void LeanMultiSwipe_FingerSwipe_m72D385EDEEEAE29FA82FC90570AE42E1B28652F2 (void);
// 0x000001B7 System.Void Lean.Touch.LeanMultiSwipe::.ctor()
extern void LeanMultiSwipe__ctor_m9E4B99EC67A5BC35AAC109DF6CF6BC3EBCB1DD07 (void);
// 0x000001B8 System.Void Lean.Touch.LeanMultiSwipe/FingerListEvent::.ctor()
extern void FingerListEvent__ctor_m557EF0EE2E4A43FFD59E9EB9E2D43C6762751643 (void);
// 0x000001B9 System.Void Lean.Touch.LeanMultiSwipe/Vector2Event::.ctor()
extern void Vector2Event__ctor_m61CDFD9F8992A503193ACC868E0A55C207711BFF (void);
// 0x000001BA System.Void Lean.Touch.LeanMultiSwipe/FloatEvent::.ctor()
extern void FloatEvent__ctor_m398D1FEF3DF599A239130BFDEBD8611EAAE056B4 (void);
// 0x000001BB UnityEngine.Events.UnityEvent Lean.Touch.LeanMultiTap::get_OnTap()
extern void LeanMultiTap_get_OnTap_mB3A816449C8E6DA0371F736CA77E537A793A41E8 (void);
// 0x000001BC Lean.Touch.LeanMultiTap/IntEvent Lean.Touch.LeanMultiTap::get_OnCount()
extern void LeanMultiTap_get_OnCount_m7FD8A7F5799F79449B47F6E2236BA04F4CF4C19B (void);
// 0x000001BD Lean.Touch.LeanMultiTap/IntEvent Lean.Touch.LeanMultiTap::get_OnHighest()
extern void LeanMultiTap_get_OnHighest_m1125CBFDDBB88ED8D3614A674C19B7107B8087EF (void);
// 0x000001BE Lean.Touch.LeanMultiTap/IntIntEvent Lean.Touch.LeanMultiTap::get_OnCountHighest()
extern void LeanMultiTap_get_OnCountHighest_m3CFFC1F131FD0B77FEFA8FEB6F4F2C3100B536B8 (void);
// 0x000001BF System.Void Lean.Touch.LeanMultiTap::AddFinger(Lean.Touch.LeanFinger)
extern void LeanMultiTap_AddFinger_mEDD9100A13C9FF0CD1A50AE8211DF8C170EF0015 (void);
// 0x000001C0 System.Void Lean.Touch.LeanMultiTap::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanMultiTap_RemoveFinger_m7666D571A74C9931FD198A529986C34682D17D46 (void);
// 0x000001C1 System.Void Lean.Touch.LeanMultiTap::RemoveAllFingers()
extern void LeanMultiTap_RemoveAllFingers_mD5C5015785D6C6A007EF9536B5F0CA0429DE1A48 (void);
// 0x000001C2 System.Void Lean.Touch.LeanMultiTap::Awake()
extern void LeanMultiTap_Awake_mCB72166953A9B727EED85C28085818140C9EADBD (void);
// 0x000001C3 System.Void Lean.Touch.LeanMultiTap::Update()
extern void LeanMultiTap_Update_m23FB55F502B1EB17EB7D392240030138E595612D (void);
// 0x000001C4 System.Int32 Lean.Touch.LeanMultiTap::GetFingerCount(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanMultiTap_GetFingerCount_m7EBE1E5895ACC80860D208F859094681D777EB59 (void);
// 0x000001C5 System.Void Lean.Touch.LeanMultiTap::.ctor()
extern void LeanMultiTap__ctor_mCCAFAA3A4FDC08D4BC906281E39F893757BD60CD (void);
// 0x000001C6 System.Void Lean.Touch.LeanMultiTap/IntEvent::.ctor()
extern void IntEvent__ctor_m5F9F1EE32595F5E2929C17087688143669160515 (void);
// 0x000001C7 System.Void Lean.Touch.LeanMultiTap/IntIntEvent::.ctor()
extern void IntIntEvent__ctor_mE2AB6A560FAF5F5BF13F7DE7290A0ED41C812729 (void);
// 0x000001C8 System.Void Lean.Touch.LeanMultiTwist::set_IgnoreIfStatic(System.Boolean)
extern void LeanMultiTwist_set_IgnoreIfStatic_m3D554AF99E789FEF09BD645ED574F90DD292B2C5 (void);
// 0x000001C9 System.Boolean Lean.Touch.LeanMultiTwist::get_IgnoreIfStatic()
extern void LeanMultiTwist_get_IgnoreIfStatic_m2DDBBA3E98ABFB947FA52544CCD67C8A8582EE20 (void);
// 0x000001CA System.Void Lean.Touch.LeanMultiTwist::set_OneFinger(Lean.Touch.LeanMultiTwist/OneFingerType)
extern void LeanMultiTwist_set_OneFinger_mF3DCC06F3E5E518EDC2B92370624B95E381D8591 (void);
// 0x000001CB Lean.Touch.LeanMultiTwist/OneFingerType Lean.Touch.LeanMultiTwist::get_OneFinger()
extern void LeanMultiTwist_get_OneFinger_mE0A8C4BD9083A1F7BC610FA4D6ADA358215442F1 (void);
// 0x000001CC Lean.Touch.LeanMultiTwist/FloatEvent Lean.Touch.LeanMultiTwist::get_OnTwistDegrees()
extern void LeanMultiTwist_get_OnTwistDegrees_m862255AB28EFB4C6F8DD31636F8ECAB4B383EE61 (void);
// 0x000001CD System.Void Lean.Touch.LeanMultiTwist::AddFinger(Lean.Touch.LeanFinger)
extern void LeanMultiTwist_AddFinger_mC76E5AF6A55DD8A48D7B444E0F3F1E4ECC9EA9E5 (void);
// 0x000001CE System.Void Lean.Touch.LeanMultiTwist::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanMultiTwist_RemoveFinger_m32A6CD6FB5EB3F8448FBF808ED022A6194ED75C6 (void);
// 0x000001CF System.Void Lean.Touch.LeanMultiTwist::RemoveAllFingers()
extern void LeanMultiTwist_RemoveAllFingers_mBC5DFF913600381C6F907FC686534587ABE6EA12 (void);
// 0x000001D0 System.Void Lean.Touch.LeanMultiTwist::Awake()
extern void LeanMultiTwist_Awake_m908736A1BF6EEF30F0B643FF14AFAD67F9ED7AEC (void);
// 0x000001D1 System.Void Lean.Touch.LeanMultiTwist::Update()
extern void LeanMultiTwist_Update_mAF20709CED57EA19321B0ED86BFC1ADB9C9333C6 (void);
// 0x000001D2 System.Void Lean.Touch.LeanMultiTwist::.ctor()
extern void LeanMultiTwist__ctor_mB91E2DBE6247515567D81AEA7C6749DAF2305D64 (void);
// 0x000001D3 System.Void Lean.Touch.LeanMultiTwist/FloatEvent::.ctor()
extern void FloatEvent__ctor_m9BF1D544D19B7F60F6E74C18B806C3CE953CF6AA (void);
// 0x000001D4 Lean.Touch.LeanMultiUp/LeanFingerEvent Lean.Touch.LeanMultiUp::get_OnFinger()
extern void LeanMultiUp_get_OnFinger_m32F9094ABAD0F7AD6D15499CFA99A1BB80081915 (void);
// 0x000001D5 Lean.Touch.LeanMultiUp/Vector3Event Lean.Touch.LeanMultiUp::get_OnWorld()
extern void LeanMultiUp_get_OnWorld_m48A6B51981ADBA7740EAC3517047742DD9E88628 (void);
// 0x000001D6 Lean.Touch.LeanMultiUp/Vector2Event Lean.Touch.LeanMultiUp::get_OnScreen()
extern void LeanMultiUp_get_OnScreen_m31963CC274725AD3D95CEB101A588D0331899E2F (void);
// 0x000001D7 System.Void Lean.Touch.LeanMultiUp::AddFinger(Lean.Touch.LeanFinger)
extern void LeanMultiUp_AddFinger_mD60110FD923F033B80635AA8FBB66BCB466746E9 (void);
// 0x000001D8 System.Void Lean.Touch.LeanMultiUp::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanMultiUp_RemoveFinger_m799F33F2E9E7123D377A065FED3B47ADEADA6B46 (void);
// 0x000001D9 System.Void Lean.Touch.LeanMultiUp::RemoveAllFingers()
extern void LeanMultiUp_RemoveAllFingers_m668BF5A33E24566EC9A24628B83B5143CB04378E (void);
// 0x000001DA System.Void Lean.Touch.LeanMultiUp::Awake()
extern void LeanMultiUp_Awake_m26E66DDBF2958D9895A38DCC3AC5F0AFD9862FC2 (void);
// 0x000001DB System.Void Lean.Touch.LeanMultiUp::Update()
extern void LeanMultiUp_Update_m8E30968004A357C03D3DC89FA5DAC0771A79428A (void);
// 0x000001DC System.Void Lean.Touch.LeanMultiUp::.ctor()
extern void LeanMultiUp__ctor_m817B3CE7602B16D18D5A46FAD04BC3DBB982C523 (void);
// 0x000001DD System.Void Lean.Touch.LeanMultiUp/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mA74984CF8AC970A9B777BC34547A463D734FD7CC (void);
// 0x000001DE System.Void Lean.Touch.LeanMultiUp/Vector3Event::.ctor()
extern void Vector3Event__ctor_m40E805E5E6B2F7CABFE63CF18942D60D59795DEB (void);
// 0x000001DF System.Void Lean.Touch.LeanMultiUp/Vector2Event::.ctor()
extern void Vector2Event__ctor_m358ED9AC5DC1A59BED43B0874A47E01FEBB68494 (void);
// 0x000001E0 System.Void Lean.Touch.LeanMultiUpdate::set_IgnoreIfStatic(System.Boolean)
extern void LeanMultiUpdate_set_IgnoreIfStatic_m0A37AF08D12FD42E899B705E37D62A6AB6E4B570 (void);
// 0x000001E1 System.Boolean Lean.Touch.LeanMultiUpdate::get_IgnoreIfStatic()
extern void LeanMultiUpdate_get_IgnoreIfStatic_m9D6C56B622F00A2B1D7011154490D492B5A97CBC (void);
// 0x000001E2 Lean.Touch.LeanMultiUpdate/LeanFingerListEvent Lean.Touch.LeanMultiUpdate::get_OnFingers()
extern void LeanMultiUpdate_get_OnFingers_mA4BAAEBDB3CD83A42BDB9205FF6603C54A430267 (void);
// 0x000001E3 System.Void Lean.Touch.LeanMultiUpdate::set_Coordinate(Lean.Touch.LeanMultiUpdate/CoordinateType)
extern void LeanMultiUpdate_set_Coordinate_mB83ECE5C25C571517B164D10F3EB11DAC7F4DA1B (void);
// 0x000001E4 Lean.Touch.LeanMultiUpdate/CoordinateType Lean.Touch.LeanMultiUpdate::get_Coordinate()
extern void LeanMultiUpdate_get_Coordinate_mC349D0B825C786B8E17858AEFA645DA2DA959704 (void);
// 0x000001E5 System.Void Lean.Touch.LeanMultiUpdate::set_Multiplier(System.Single)
extern void LeanMultiUpdate_set_Multiplier_m62503A1E386259AC155C2A576478EF6B5DFBE65E (void);
// 0x000001E6 System.Single Lean.Touch.LeanMultiUpdate::get_Multiplier()
extern void LeanMultiUpdate_get_Multiplier_mFD72881AE01F706E844E3D5C77DAA941176746F1 (void);
// 0x000001E7 Lean.Touch.LeanMultiUpdate/Vector2Event Lean.Touch.LeanMultiUpdate::get_OnDelta()
extern void LeanMultiUpdate_get_OnDelta_m48FCA1A889B55B7DC390675E92EE4B7078352429 (void);
// 0x000001E8 Lean.Touch.LeanMultiUpdate/FloatEvent Lean.Touch.LeanMultiUpdate::get_OnDistance()
extern void LeanMultiUpdate_get_OnDistance_mC35A7F6F31BD8E9982CC745EC29669F45DC78ACF (void);
// 0x000001E9 Lean.Touch.LeanMultiUpdate/Vector3Event Lean.Touch.LeanMultiUpdate::get_OnWorldFrom()
extern void LeanMultiUpdate_get_OnWorldFrom_m1B09F779876A4440F77CFB23AB1177197A0370E1 (void);
// 0x000001EA Lean.Touch.LeanMultiUpdate/Vector3Event Lean.Touch.LeanMultiUpdate::get_OnWorldTo()
extern void LeanMultiUpdate_get_OnWorldTo_m4D8A2B28AAB42A963E13D420A6B86199B56ADA51 (void);
// 0x000001EB Lean.Touch.LeanMultiUpdate/Vector3Event Lean.Touch.LeanMultiUpdate::get_OnWorldDelta()
extern void LeanMultiUpdate_get_OnWorldDelta_mC66FB68B3F27F50C61709172AAE2F13E1BED1996 (void);
// 0x000001EC Lean.Touch.LeanMultiUpdate/Vector3Vector3Event Lean.Touch.LeanMultiUpdate::get_OnWorldFromTo()
extern void LeanMultiUpdate_get_OnWorldFromTo_mB30C30C40079C84FDA46E1EFF7CB51D0A5D2BA3E (void);
// 0x000001ED System.Void Lean.Touch.LeanMultiUpdate::AddFinger(Lean.Touch.LeanFinger)
extern void LeanMultiUpdate_AddFinger_m93484E2DEB8DAD508C56200FEF383C503FA6D56D (void);
// 0x000001EE System.Void Lean.Touch.LeanMultiUpdate::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanMultiUpdate_RemoveFinger_m2889B50A84E4EB46E78831017B4E6177F6240AD8 (void);
// 0x000001EF System.Void Lean.Touch.LeanMultiUpdate::RemoveAllFingers()
extern void LeanMultiUpdate_RemoveAllFingers_mCCA1CBC56594044660957D4C58D51FAA7B4DD636 (void);
// 0x000001F0 System.Void Lean.Touch.LeanMultiUpdate::Awake()
extern void LeanMultiUpdate_Awake_m93B50D5751A65587C375A28712C4198E33890B9B (void);
// 0x000001F1 System.Void Lean.Touch.LeanMultiUpdate::Update()
extern void LeanMultiUpdate_Update_m2390B25EE46BF85B013EB909A33DAEEAC8966D31 (void);
// 0x000001F2 System.Void Lean.Touch.LeanMultiUpdate::.ctor()
extern void LeanMultiUpdate__ctor_m4AEFE368F6FDB31E67E9C059CBF15EA5ECEE6598 (void);
// 0x000001F3 System.Void Lean.Touch.LeanMultiUpdate/LeanFingerListEvent::.ctor()
extern void LeanFingerListEvent__ctor_mC23F299CA296A2DD3031D2F76C6BD4A62AA4A55E (void);
// 0x000001F4 System.Void Lean.Touch.LeanMultiUpdate/FloatEvent::.ctor()
extern void FloatEvent__ctor_m249A50B149BB51315E91B12065DEB77246EC037B (void);
// 0x000001F5 System.Void Lean.Touch.LeanMultiUpdate/Vector2Event::.ctor()
extern void Vector2Event__ctor_mFB25A50F057AB4F0FDB4EC5DF853E38A47B542F8 (void);
// 0x000001F6 System.Void Lean.Touch.LeanMultiUpdate/Vector3Event::.ctor()
extern void Vector3Event__ctor_mFA76270CFBD1A2DFB78DD89ED0F34FA8DE833951 (void);
// 0x000001F7 System.Void Lean.Touch.LeanMultiUpdate/Vector3Vector3Event::.ctor()
extern void Vector3Vector3Event__ctor_m03B321D0D6C1099D51B6FD8A33EF905DC54396DC (void);
// 0x000001F8 System.Void Lean.Touch.LeanMultiUpdateCanvas::set_IgnoreIfOff(System.Boolean)
extern void LeanMultiUpdateCanvas_set_IgnoreIfOff_mC7E37811BC51F8A8E8E3070D5865A9CECF74A00A (void);
// 0x000001F9 System.Boolean Lean.Touch.LeanMultiUpdateCanvas::get_IgnoreIfOff()
extern void LeanMultiUpdateCanvas_get_IgnoreIfOff_m263F8EAA205553CB0A22AF9798E2F555E672E933 (void);
// 0x000001FA Lean.Touch.LeanMultiUpdateCanvas/LeanFingerListEvent Lean.Touch.LeanMultiUpdateCanvas::get_OnFingers()
extern void LeanMultiUpdateCanvas_get_OnFingers_mD05C25D09381A39E86107419A2C6259EEFC82ED2 (void);
// 0x000001FB Lean.Touch.LeanMultiUpdateCanvas/Vector3Event Lean.Touch.LeanMultiUpdateCanvas::get_OnWorld()
extern void LeanMultiUpdateCanvas_get_OnWorld_m37DE2074F3C4AE5FA44F504592767595AA52CDB2 (void);
// 0x000001FC System.Void Lean.Touch.LeanMultiUpdateCanvas::AddFinger(Lean.Touch.LeanFinger)
extern void LeanMultiUpdateCanvas_AddFinger_m15DF653C5A8C498E45E665D16EB5FB1CAC64BE99 (void);
// 0x000001FD System.Void Lean.Touch.LeanMultiUpdateCanvas::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanMultiUpdateCanvas_RemoveFinger_m890BF391F97516D2605E649D001F95356E62C12C (void);
// 0x000001FE System.Void Lean.Touch.LeanMultiUpdateCanvas::RemoveAllFingers()
extern void LeanMultiUpdateCanvas_RemoveAllFingers_m1CBAAECE834309C5EDC1E9DBC9F1512DC61C1715 (void);
// 0x000001FF System.Boolean Lean.Touch.LeanMultiUpdateCanvas::ElementOverlapped(Lean.Touch.LeanFinger)
extern void LeanMultiUpdateCanvas_ElementOverlapped_m51754DE60EF8ABC031056AA0B0BDE9132E706CB6 (void);
// 0x00000200 System.Void Lean.Touch.LeanMultiUpdateCanvas::Awake()
extern void LeanMultiUpdateCanvas_Awake_m2489CF7734AF93203D53535011B0C9BB1D48CA7E (void);
// 0x00000201 System.Void Lean.Touch.LeanMultiUpdateCanvas::OnEnable()
extern void LeanMultiUpdateCanvas_OnEnable_m8373073CF7D6C76DA3CBF5355EB8A3ECBC56F1EA (void);
// 0x00000202 System.Void Lean.Touch.LeanMultiUpdateCanvas::OnDisable()
extern void LeanMultiUpdateCanvas_OnDisable_mF0D7244003C055024E1A4C09AB8501C2A6CB33C4 (void);
// 0x00000203 System.Void Lean.Touch.LeanMultiUpdateCanvas::Update()
extern void LeanMultiUpdateCanvas_Update_m797A25E41CADD789DB6E4963E20702CD769F22BD (void);
// 0x00000204 System.Void Lean.Touch.LeanMultiUpdateCanvas::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanMultiUpdateCanvas_HandleFingerDown_m77133BB83FD5E9AC313465130ECE0BC6A6DFF1F3 (void);
// 0x00000205 System.Void Lean.Touch.LeanMultiUpdateCanvas::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanMultiUpdateCanvas_HandleFingerUp_mD851C876AFF72681DF37704AEE3EA7D91AB45AD2 (void);
// 0x00000206 System.Void Lean.Touch.LeanMultiUpdateCanvas::.ctor()
extern void LeanMultiUpdateCanvas__ctor_mFB2516C8E62CD4E1AF376885FAE26080D7C727C5 (void);
// 0x00000207 System.Void Lean.Touch.LeanMultiUpdateCanvas/LeanFingerListEvent::.ctor()
extern void LeanFingerListEvent__ctor_m07768BB1E8A58FE1284BFFFE5C2EAFA8D1E6ED77 (void);
// 0x00000208 System.Void Lean.Touch.LeanMultiUpdateCanvas/Vector3Event::.ctor()
extern void Vector3Event__ctor_mAD5A428D75F7A17D8C2D5856835D75580CEF2B39 (void);
// 0x00000209 System.Void Lean.Touch.LeanPick::set_RequiredTag(System.String)
extern void LeanPick_set_RequiredTag_m4712C69D410577710EDADBC44A28BB11196402C9 (void);
// 0x0000020A System.String Lean.Touch.LeanPick::get_RequiredTag()
extern void LeanPick_get_RequiredTag_m2F2EFF858D9A6814556176B8478FEC369DF26036 (void);
// 0x0000020B Lean.Touch.LeanPick/LeanPickableEvent Lean.Touch.LeanPick::get_OnPickable()
extern void LeanPick_get_OnPickable_m2542F31E7F744C17A126447A9E64DE4E00238BB9 (void);
// 0x0000020C System.Void Lean.Touch.LeanPick::PickStartScreenPosition(Lean.Touch.LeanFinger)
extern void LeanPick_PickStartScreenPosition_m557C600DC7D404335449F3479B120CEAA5549C5A (void);
// 0x0000020D System.Void Lean.Touch.LeanPick::PickScreenPosition(Lean.Touch.LeanFinger)
extern void LeanPick_PickScreenPosition_mED8B7C7DBF6F784F2B7AD014801F08AEA286C21C (void);
// 0x0000020E System.Void Lean.Touch.LeanPick::SelectScreenPosition(Lean.Touch.LeanFinger,UnityEngine.Vector2)
extern void LeanPick_SelectScreenPosition_mAFCA2DF630D4BAE9119D0A8D3954FC4637AFD79F (void);
// 0x0000020F System.Void Lean.Touch.LeanPick::.ctor()
extern void LeanPick__ctor_m3CBC103D88CDC5B36433C348CA23B68654B0471F (void);
// 0x00000210 System.Void Lean.Touch.LeanPick/LeanPickableEvent::.ctor()
extern void LeanPickableEvent__ctor_m0B53AEC2E4426AE30F4D3B78ABBBDAB4FEFF2976 (void);
// 0x00000211 Lean.Touch.LeanPickable/LeanFingerEvent Lean.Touch.LeanPickable::get_OnFinger()
extern void LeanPickable_get_OnFinger_mA16865CA338CD1546AEEBA3DA97A1ABAA209A321 (void);
// 0x00000212 Lean.Touch.LeanPickable/Vector3Event Lean.Touch.LeanPickable::get_OnWorld()
extern void LeanPickable_get_OnWorld_m8692A0FBA3D35A523BA8236D091449356CB082E2 (void);
// 0x00000213 Lean.Touch.LeanPickable/Vector2Event Lean.Touch.LeanPickable::get_OnScreen()
extern void LeanPickable_get_OnScreen_m79FA67FBC0931CAFF2703A1607C755C8DFF20848 (void);
// 0x00000214 System.Void Lean.Touch.LeanPickable::InvokePick(Lean.Touch.LeanFinger,UnityEngine.Vector2)
extern void LeanPickable_InvokePick_m67841B00B5A0AC722ADFE2F597A0F6C8ECA1E6B9 (void);
// 0x00000215 System.Void Lean.Touch.LeanPickable::.ctor()
extern void LeanPickable__ctor_mC89D16BA6906C985E8CB7A3955D625ED9F534085 (void);
// 0x00000216 System.Void Lean.Touch.LeanPickable/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m5C900BFC8AC2FAE876F7887922CE0FAA17529EFC (void);
// 0x00000217 System.Void Lean.Touch.LeanPickable/Vector3Event::.ctor()
extern void Vector3Event__ctor_m0227F10D52B05CADFB936D78E4E597097B7D0585 (void);
// 0x00000218 System.Void Lean.Touch.LeanPickable/Vector2Event::.ctor()
extern void Vector2Event__ctor_mC83B1971D763150A0A7927A3E8C231C3E867B28C (void);
// 0x00000219 System.Void Lean.Touch.LeanPinchCamera::set_Camera(UnityEngine.Camera)
extern void LeanPinchCamera_set_Camera_m43FB3EB9DA501523A7EB305B9D84DE672CA8385B (void);
// 0x0000021A UnityEngine.Camera Lean.Touch.LeanPinchCamera::get_Camera()
extern void LeanPinchCamera_get_Camera_mC7A53207B98FF8E1190A50AB525B86A8A5B126F2 (void);
// 0x0000021B System.Void Lean.Touch.LeanPinchCamera::set_Zoom(System.Single)
extern void LeanPinchCamera_set_Zoom_mC05333EAE3BB092E95573CB32D87877A0892B663 (void);
// 0x0000021C System.Single Lean.Touch.LeanPinchCamera::get_Zoom()
extern void LeanPinchCamera_get_Zoom_m928E991D35F0F34CBFD78FE904D3E0AB30FBA151 (void);
// 0x0000021D System.Void Lean.Touch.LeanPinchCamera::set_Damping(System.Single)
extern void LeanPinchCamera_set_Damping_m51DA1118DDFDF19A34F6667F2A1C48964AB15FDC (void);
// 0x0000021E System.Single Lean.Touch.LeanPinchCamera::get_Damping()
extern void LeanPinchCamera_get_Damping_m063C055BDED3FB3C289489DC6B9EABBF792F56E2 (void);
// 0x0000021F System.Void Lean.Touch.LeanPinchCamera::set_Clamp(System.Boolean)
extern void LeanPinchCamera_set_Clamp_mBC0DDF5141D9BCB7648EDBD06F8DF8DD21A8BE05 (void);
// 0x00000220 System.Boolean Lean.Touch.LeanPinchCamera::get_Clamp()
extern void LeanPinchCamera_get_Clamp_m6F4323A67995F90635CFA0B0AB605FCF0ACE2A05 (void);
// 0x00000221 System.Void Lean.Touch.LeanPinchCamera::set_ClampMin(System.Single)
extern void LeanPinchCamera_set_ClampMin_mB8285FC688D3997649BCF2860382F5AC92AF769E (void);
// 0x00000222 System.Single Lean.Touch.LeanPinchCamera::get_ClampMin()
extern void LeanPinchCamera_get_ClampMin_mE7FFDEE79E6B286A6A26CB6B539752969C94DD14 (void);
// 0x00000223 System.Void Lean.Touch.LeanPinchCamera::set_ClampMax(System.Single)
extern void LeanPinchCamera_set_ClampMax_m26D5E396FA6C2C2951485658F9F9AAE95E3F61D1 (void);
// 0x00000224 System.Single Lean.Touch.LeanPinchCamera::get_ClampMax()
extern void LeanPinchCamera_get_ClampMax_m1205CD08FBD779B27C5307BF3A6E3D9FFAE19F69 (void);
// 0x00000225 System.Void Lean.Touch.LeanPinchCamera::set_Relative(System.Boolean)
extern void LeanPinchCamera_set_Relative_m72BFEC0FB396378D2DD95D9F57F11C700ED4554E (void);
// 0x00000226 System.Boolean Lean.Touch.LeanPinchCamera::get_Relative()
extern void LeanPinchCamera_get_Relative_m1E802B163874D94FDE2D2A64E534878AF827093C (void);
// 0x00000227 System.Void Lean.Touch.LeanPinchCamera::set_IgnoreZ(System.Boolean)
extern void LeanPinchCamera_set_IgnoreZ_mE8FC62137CE9B6BEE79ABA44A374462439560305 (void);
// 0x00000228 System.Boolean Lean.Touch.LeanPinchCamera::get_IgnoreZ()
extern void LeanPinchCamera_get_IgnoreZ_mDBE386DF62A910B86CB1497DABBF116460FDA5F5 (void);
// 0x00000229 System.Void Lean.Touch.LeanPinchCamera::ContinuouslyZoom(System.Single)
extern void LeanPinchCamera_ContinuouslyZoom_mD78DD2117C2AAED168F8067EE30C91640A767DEF (void);
// 0x0000022A System.Void Lean.Touch.LeanPinchCamera::MultiplyZoom(System.Single)
extern void LeanPinchCamera_MultiplyZoom_m9B05C0878263C39BBAB53986D99354EFBE77047E (void);
// 0x0000022B System.Void Lean.Touch.LeanPinchCamera::IncrementZoom(System.Single)
extern void LeanPinchCamera_IncrementZoom_m12BFED79BEF90702B44F9CBF3F374C43766EADDE (void);
// 0x0000022C System.Void Lean.Touch.LeanPinchCamera::AddFinger(Lean.Touch.LeanFinger)
extern void LeanPinchCamera_AddFinger_mCC7CBB984819E5716FE1FCC1DC19F4A508D250C2 (void);
// 0x0000022D System.Void Lean.Touch.LeanPinchCamera::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanPinchCamera_RemoveFinger_m03998556079691F10011025BB31711977A93B8D5 (void);
// 0x0000022E System.Void Lean.Touch.LeanPinchCamera::RemoveAllFingers()
extern void LeanPinchCamera_RemoveAllFingers_mAB3DF1D2C79C3743EB637A86B12B64B620FD8D07 (void);
// 0x0000022F System.Void Lean.Touch.LeanPinchCamera::Awake()
extern void LeanPinchCamera_Awake_mAD870BC5A75F5FEEA15089021E5496FBBE09A413 (void);
// 0x00000230 System.Void Lean.Touch.LeanPinchCamera::Start()
extern void LeanPinchCamera_Start_m897D20680A1E50270506530F982CDC45BD31EED0 (void);
// 0x00000231 System.Void Lean.Touch.LeanPinchCamera::LateUpdate()
extern void LeanPinchCamera_LateUpdate_mE4AC6F9081C806639A55A1977926BA5992845C8B (void);
// 0x00000232 System.Void Lean.Touch.LeanPinchCamera::SetZoom(System.Single)
extern void LeanPinchCamera_SetZoom_mEE6FCD91376C7FE5ACB45C1687672B13ACD43AB0 (void);
// 0x00000233 System.Single Lean.Touch.LeanPinchCamera::TryClamp(System.Single)
extern void LeanPinchCamera_TryClamp_mAE7F4A09736F5731274BF592EEDE019D49BD724F (void);
// 0x00000234 System.Void Lean.Touch.LeanPinchCamera::.ctor()
extern void LeanPinchCamera__ctor_mD067BCAE9B905FD20B6A6247EF2D6E986B3C9C29 (void);
// 0x00000235 System.Void Lean.Touch.LeanReplayFinger::set_Cursor(UnityEngine.Transform)
extern void LeanReplayFinger_set_Cursor_mD468F0DA29A524A2D3E30F35E1684B1597E2D561 (void);
// 0x00000236 UnityEngine.Transform Lean.Touch.LeanReplayFinger::get_Cursor()
extern void LeanReplayFinger_get_Cursor_m09AB81DBF8E3D3306CE243FE3885997E82E6051D (void);
// 0x00000237 System.Void Lean.Touch.LeanReplayFinger::set_Playing(System.Boolean)
extern void LeanReplayFinger_set_Playing_m77BF285F70A051B1A86C0EDC42FC45BACE7D4901 (void);
// 0x00000238 System.Boolean Lean.Touch.LeanReplayFinger::get_Playing()
extern void LeanReplayFinger_get_Playing_mD06DE6F4E14A3024DD984C9EB4DAE6FE2E96DEC7 (void);
// 0x00000239 System.Void Lean.Touch.LeanReplayFinger::set_PlayTime(System.Single)
extern void LeanReplayFinger_set_PlayTime_m379DDB7277AB39C603A272C437AA652F78198B55 (void);
// 0x0000023A System.Single Lean.Touch.LeanReplayFinger::get_PlayTime()
extern void LeanReplayFinger_get_PlayTime_m50BC8C3E01DF5D72E14F4AF898DC26AB6A0BD3F8 (void);
// 0x0000023B System.Void Lean.Touch.LeanReplayFinger::Replay()
extern void LeanReplayFinger_Replay_m7DE9573F4A3DDB8F84A0915EA7643634E842D81C (void);
// 0x0000023C System.Void Lean.Touch.LeanReplayFinger::StopReplay()
extern void LeanReplayFinger_StopReplay_m08CEC132782E2D5F3726719615B65FC17C02534B (void);
// 0x0000023D System.Void Lean.Touch.LeanReplayFinger::OnEnable()
extern void LeanReplayFinger_OnEnable_mFA892D9C9C0BD51F8FF48DD902F7826B17349620 (void);
// 0x0000023E System.Void Lean.Touch.LeanReplayFinger::OnDisable()
extern void LeanReplayFinger_OnDisable_m25EE3C1264318AB1669D72CBF96AAC4E8A5ED4C8 (void);
// 0x0000023F System.Void Lean.Touch.LeanReplayFinger::Update()
extern void LeanReplayFinger_Update_mEAC2CBE6894BAA391F3B1BF1B6C6719B97DEBB35 (void);
// 0x00000240 System.Void Lean.Touch.LeanReplayFinger::HandleFingerUpdate(Lean.Touch.LeanFinger)
extern void LeanReplayFinger_HandleFingerUpdate_m0BF22B7A382CB3CAD1F30AFE5B2A58CA9744E553 (void);
// 0x00000241 System.Void Lean.Touch.LeanReplayFinger::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanReplayFinger_HandleFingerUp_m694A09DC512ACAB31B54366C5E464EBF9D40BF02 (void);
// 0x00000242 System.Void Lean.Touch.LeanReplayFinger::CopySnapshots(Lean.Touch.LeanFinger)
extern void LeanReplayFinger_CopySnapshots_mC6B8A010ADF19786B0B4A7E0DE97FF57542880B9 (void);
// 0x00000243 System.Void Lean.Touch.LeanReplayFinger::.ctor()
extern void LeanReplayFinger__ctor_m3FF6BFBFFEAB71574C8A34E6C4F6BE462F9F993B (void);
// 0x00000244 System.Void Lean.Touch.LeanSelectableBlock::set_X(System.Int32)
extern void LeanSelectableBlock_set_X_m3CCE4AA2D0C1B0BD74080A855AE6481B48C9B9D5 (void);
// 0x00000245 System.Int32 Lean.Touch.LeanSelectableBlock::get_X()
extern void LeanSelectableBlock_get_X_m6D3E4E02F93C0AE8CD93A8BAE0934F1478D6D77A (void);
// 0x00000246 System.Void Lean.Touch.LeanSelectableBlock::set_Y(System.Int32)
extern void LeanSelectableBlock_set_Y_mD360C29236B3E5AD269A1EAAA2867FE3B33DCB66 (void);
// 0x00000247 System.Int32 Lean.Touch.LeanSelectableBlock::get_Y()
extern void LeanSelectableBlock_get_Y_m39AD2956985F2F6C46425A44E6AAFAAC08331E67 (void);
// 0x00000248 System.Void Lean.Touch.LeanSelectableBlock::set_BlockSize(System.Single)
extern void LeanSelectableBlock_set_BlockSize_m1D0349E94643CAAC69704EA42B5CC61CC9A67514 (void);
// 0x00000249 System.Single Lean.Touch.LeanSelectableBlock::get_BlockSize()
extern void LeanSelectableBlock_get_BlockSize_mEECBA78BCE127FDA782686A76B488D6933B84A9F (void);
// 0x0000024A System.Void Lean.Touch.LeanSelectableBlock::set_DeselectOnSwap(System.Boolean)
extern void LeanSelectableBlock_set_DeselectOnSwap_mD01D7E9F6DDAF09211D5468E587FF92B40B99EFB (void);
// 0x0000024B System.Boolean Lean.Touch.LeanSelectableBlock::get_DeselectOnSwap()
extern void LeanSelectableBlock_get_DeselectOnSwap_m5BF8CC23A23D89CBE4A0F210CA694A4248CC8290 (void);
// 0x0000024C System.Void Lean.Touch.LeanSelectableBlock::set_Damping(System.Single)
extern void LeanSelectableBlock_set_Damping_m9651F365CD91B9351D83E5FF7964714D19D5FECE (void);
// 0x0000024D System.Single Lean.Touch.LeanSelectableBlock::get_Damping()
extern void LeanSelectableBlock_get_Damping_m680EA34A053C14F9D148C290A894155AB8910B32 (void);
// 0x0000024E Lean.Touch.LeanSelectableBlock Lean.Touch.LeanSelectableBlock::FindBlock(System.Int32,System.Int32)
extern void LeanSelectableBlock_FindBlock_m10DC9177D54B93BFA231D098634FF1331988E01B (void);
// 0x0000024F System.Void Lean.Touch.LeanSelectableBlock::OnEnable()
extern void LeanSelectableBlock_OnEnable_m2B221370A4FA0DF06537ECBF1B99E0D96699C06F (void);
// 0x00000250 System.Void Lean.Touch.LeanSelectableBlock::OnDisable()
extern void LeanSelectableBlock_OnDisable_m9BE5B6FD65EAC8B89C3F3ECD9E532FFC69CF7700 (void);
// 0x00000251 System.Void Lean.Touch.LeanSelectableBlock::Swap(Lean.Touch.LeanSelectableBlock,Lean.Touch.LeanSelectableBlock)
extern void LeanSelectableBlock_Swap_m40231CD576BE06E4B45C7172097250AC23214ADE (void);
// 0x00000252 System.Void Lean.Touch.LeanSelectableBlock::Update()
extern void LeanSelectableBlock_Update_mE67F8B503842E570FC6A2A76E03D20265A776DE4 (void);
// 0x00000253 System.Void Lean.Touch.LeanSelectableBlock::.ctor()
extern void LeanSelectableBlock__ctor_m5528493EA9E96168268AE0421F251447774D09FE (void);
// 0x00000254 System.Void Lean.Touch.LeanSelectableBlock::.cctor()
extern void LeanSelectableBlock__cctor_m8F15A364E870B49F4BA01EFBAAD20B33E1919097 (void);
// 0x00000255 Lean.Touch.LeanSelectableCenter/Vector3Event Lean.Touch.LeanSelectableCenter::get_OnPosition()
extern void LeanSelectableCenter_get_OnPosition_m6236B9F326A8A8668609B8126A490845E7751C58 (void);
// 0x00000256 System.Void Lean.Touch.LeanSelectableCenter::Calculate()
extern void LeanSelectableCenter_Calculate_m30CCA82E5C0E2B6C838A6DBA5C43DDD627F0AF4C (void);
// 0x00000257 System.Void Lean.Touch.LeanSelectableCenter::.ctor()
extern void LeanSelectableCenter__ctor_m22110B79E053F7DB840C04F2F3FD39F09BB866B2 (void);
// 0x00000258 System.Void Lean.Touch.LeanSelectableCenter/Vector3Event::.ctor()
extern void Vector3Event__ctor_mBD048EDE9910FBCE2F98C291B11F6B656FF850DD (void);
// 0x00000259 System.Void Lean.Touch.LeanSelectableCount::set_Count(System.Int32)
extern void LeanSelectableCount_set_Count_mC3709AE3869AAE35C423DA68DEAA2E954BD36E94 (void);
// 0x0000025A System.Int32 Lean.Touch.LeanSelectableCount::get_Count()
extern void LeanSelectableCount_get_Count_m67E3F0039EA06449B281CA4AF424CC55B7267D82 (void);
// 0x0000025B Lean.Touch.LeanSelectableCount/IntEvent Lean.Touch.LeanSelectableCount::get_OnCount()
extern void LeanSelectableCount_get_OnCount_mB1C6901F134F52F7A535ACB36837DE90DC827284 (void);
// 0x0000025C System.Void Lean.Touch.LeanSelectableCount::set_MatchMin(System.Int32)
extern void LeanSelectableCount_set_MatchMin_mBB7374A1126B36C2DFD5F5C4670F54A2970CE335 (void);
// 0x0000025D System.Int32 Lean.Touch.LeanSelectableCount::get_MatchMin()
extern void LeanSelectableCount_get_MatchMin_m3C787FF82C642EC5855E63B77CDDB833D46D02DF (void);
// 0x0000025E System.Void Lean.Touch.LeanSelectableCount::set_MatchMax(System.Int32)
extern void LeanSelectableCount_set_MatchMax_m304E39BC288196020CA2956CC1D68802EBDA91F0 (void);
// 0x0000025F System.Int32 Lean.Touch.LeanSelectableCount::get_MatchMax()
extern void LeanSelectableCount_get_MatchMax_mEA2C84E3C505FD9804215421616556A27633511A (void);
// 0x00000260 UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectableCount::get_OnMatch()
extern void LeanSelectableCount_get_OnMatch_mB9A46426AD3A7212B58F6D0482BB21326870FC4E (void);
// 0x00000261 UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectableCount::get_OnUnmatch()
extern void LeanSelectableCount_get_OnUnmatch_mCBEB3FDFE5E139B3566D2E295F3FD4B62150226F (void);
// 0x00000262 System.Void Lean.Touch.LeanSelectableCount::OnSelected()
extern void LeanSelectableCount_OnSelected_mCFBBD435C209E0B73C8AAA3FB77520D230BD6A72 (void);
// 0x00000263 System.Void Lean.Touch.LeanSelectableCount::OnDeselected()
extern void LeanSelectableCount_OnDeselected_m0084321F8BED84CF24FABB649694C6A66360FD1F (void);
// 0x00000264 System.Void Lean.Touch.LeanSelectableCount::UpdateState(System.Boolean)
extern void LeanSelectableCount_UpdateState_m09348F832578C7F265A53636D86EE6C31DE956F1 (void);
// 0x00000265 System.Void Lean.Touch.LeanSelectableCount::.ctor()
extern void LeanSelectableCount__ctor_m557ED514F5073C5C07EB2B54A6EB3DF57EE0DF92 (void);
// 0x00000266 System.Void Lean.Touch.LeanSelectableCount/IntEvent::.ctor()
extern void IntEvent__ctor_mFFD43B9FFEE825DE8A6D10174B91CCC4448726B8 (void);
// 0x00000267 System.Void Lean.Touch.LeanSelectableDial::set_Camera(UnityEngine.Camera)
extern void LeanSelectableDial_set_Camera_m44641853FD37C8741CE1A30196C00B4DCDD1F155 (void);
// 0x00000268 UnityEngine.Camera Lean.Touch.LeanSelectableDial::get_Camera()
extern void LeanSelectableDial_get_Camera_m135AF4E0CC201CCC8EDE52173689F297D1628E36 (void);
// 0x00000269 System.Void Lean.Touch.LeanSelectableDial::set_Tilt(UnityEngine.Vector3)
extern void LeanSelectableDial_set_Tilt_m4E6FF9871EA813BCD9C3DA474F0108AECA21080A (void);
// 0x0000026A UnityEngine.Vector3 Lean.Touch.LeanSelectableDial::get_Tilt()
extern void LeanSelectableDial_get_Tilt_mBDDB81F76A7488A7F399A4DECC3E23541428A6EC (void);
// 0x0000026B System.Void Lean.Touch.LeanSelectableDial::set_Axis(UnityEngine.Vector3)
extern void LeanSelectableDial_set_Axis_m0C7D383D04069522274EE3B8C46DA3B763F00FCB (void);
// 0x0000026C UnityEngine.Vector3 Lean.Touch.LeanSelectableDial::get_Axis()
extern void LeanSelectableDial_get_Axis_m66DD832FFA69B6042B42F29814AFAEF19A51BBA2 (void);
// 0x0000026D System.Void Lean.Touch.LeanSelectableDial::set_Angle(System.Single)
extern void LeanSelectableDial_set_Angle_mFA0904BC72B9A7617CA9CE3384B3C48B99C3C0DC (void);
// 0x0000026E System.Single Lean.Touch.LeanSelectableDial::get_Angle()
extern void LeanSelectableDial_get_Angle_mA90C87DFFC42FAF24A4F5C4C866E14D28881B480 (void);
// 0x0000026F System.Void Lean.Touch.LeanSelectableDial::set_Clamp(System.Boolean)
extern void LeanSelectableDial_set_Clamp_m3177E69298DBF4D18389A6B93C4679EBD3ABADFF (void);
// 0x00000270 System.Boolean Lean.Touch.LeanSelectableDial::get_Clamp()
extern void LeanSelectableDial_get_Clamp_m79A7AFD1EF340D85D0FFE3AEE72E8F7279B9B6C0 (void);
// 0x00000271 System.Void Lean.Touch.LeanSelectableDial::set_ClampMin(System.Single)
extern void LeanSelectableDial_set_ClampMin_mDB93CB7EAEBBE23588EA7BE58926A5E2218EA1DD (void);
// 0x00000272 System.Single Lean.Touch.LeanSelectableDial::get_ClampMin()
extern void LeanSelectableDial_get_ClampMin_m54CDC368D8FCCD923FE9586929167C9772002ACE (void);
// 0x00000273 System.Void Lean.Touch.LeanSelectableDial::set_ClampMax(System.Single)
extern void LeanSelectableDial_set_ClampMax_mF3E2A3A9432CF39D9CAABE818A76074D2AD25B2E (void);
// 0x00000274 System.Single Lean.Touch.LeanSelectableDial::get_ClampMax()
extern void LeanSelectableDial_get_ClampMax_m594A1D974F1EA90CEB8C39DB5159444FA418081C (void);
// 0x00000275 System.Collections.Generic.List`1<Lean.Touch.LeanSelectableDial/Trigger> Lean.Touch.LeanSelectableDial::get_Triggers()
extern void LeanSelectableDial_get_Triggers_mBA14561BC72DAB7CE1DE5081FE46CB97B152C0A7 (void);
// 0x00000276 Lean.Touch.LeanSelectableDial/FloatEvent Lean.Touch.LeanSelectableDial::get_OnAngleChanged()
extern void LeanSelectableDial_get_OnAngleChanged_mC0EF22987A662677DEB1C55C0CC7D9DDF29EDA83 (void);
// 0x00000277 System.Void Lean.Touch.LeanSelectableDial::IncrementAngle(System.Single)
extern void LeanSelectableDial_IncrementAngle_mF76760B5BFF5914BD0392BC5BC6899D727E9606E (void);
// 0x00000278 System.Void Lean.Touch.LeanSelectableDial::Update()
extern void LeanSelectableDial_Update_m6803E7859005874696BD4EB4B223375EB7838989 (void);
// 0x00000279 UnityEngine.Vector2 Lean.Touch.LeanSelectableDial::GetPoint(UnityEngine.Vector2)
extern void LeanSelectableDial_GetPoint_mE0874F4F4A21ED7CF6CF6D283F32265DF3E89486 (void);
// 0x0000027A System.Void Lean.Touch.LeanSelectableDial::.ctor()
extern void LeanSelectableDial__ctor_m8CBAADC09F60AEC120A02F09445DA991ACE66789 (void);
// 0x0000027B UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectableDial/Trigger::get_OnEnter()
extern void Trigger_get_OnEnter_mB078BDAB6F8EDCD7C32E031BEA126D8E8344C3C5 (void);
// 0x0000027C UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectableDial/Trigger::get_OnExit()
extern void Trigger_get_OnExit_mA51B9884CADD3C8285FFA3B80A427FAB0B042E0C (void);
// 0x0000027D System.Boolean Lean.Touch.LeanSelectableDial/Trigger::IsInside(System.Single,System.Boolean)
extern void Trigger_IsInside_m5907DCDCAB9FC3F07A2539547C80A13E9ADE96F8 (void);
// 0x0000027E System.Void Lean.Touch.LeanSelectableDial/Trigger::.ctor()
extern void Trigger__ctor_m96E85FA706A948FC40388F7C242453E6D16B4E71 (void);
// 0x0000027F System.Void Lean.Touch.LeanSelectableDial/FloatEvent::.ctor()
extern void FloatEvent__ctor_m81F86D5D9FB25AB91E75CBDDEA5D955D2A8673E6 (void);
// 0x00000280 System.Void Lean.Touch.LeanSelectableDragTorque::set_Camera(UnityEngine.Camera)
extern void LeanSelectableDragTorque_set_Camera_m39E44ABD608B24967D49350EC6B212E0E2DFFBF6 (void);
// 0x00000281 UnityEngine.Camera Lean.Touch.LeanSelectableDragTorque::get_Camera()
extern void LeanSelectableDragTorque_get_Camera_m83DA2E568DD8FB796E9815D4A45FD8E135088AF7 (void);
// 0x00000282 System.Void Lean.Touch.LeanSelectableDragTorque::set_Force(System.Single)
extern void LeanSelectableDragTorque_set_Force_m724B4F194862568ED7EADF0794D6FC81B1D4A131 (void);
// 0x00000283 System.Single Lean.Touch.LeanSelectableDragTorque::get_Force()
extern void LeanSelectableDragTorque_get_Force_m507B8F1A77741C32ADCCDABAF6C00D874D93DA35 (void);
// 0x00000284 System.Void Lean.Touch.LeanSelectableDragTorque::OnSelected()
extern void LeanSelectableDragTorque_OnSelected_m5B56B09BE53C001014F42F6587FCC4EE029492FE (void);
// 0x00000285 System.Void Lean.Touch.LeanSelectableDragTorque::Update()
extern void LeanSelectableDragTorque_Update_m2452C2312A12A737501671FBFF9FADAC55088324 (void);
// 0x00000286 System.Void Lean.Touch.LeanSelectableDragTorque::.ctor()
extern void LeanSelectableDragTorque__ctor_m0D6DD6177894ED077AA3050263C301E5E8BA2214 (void);
// 0x00000287 System.Void Lean.Touch.LeanSelectableDrop::set_Ignore(Lean.Touch.LeanSelectableDrop/IgnoreType)
extern void LeanSelectableDrop_set_Ignore_mAF78943058631464D76B40E3AA5D667FFB54C982 (void);
// 0x00000288 Lean.Touch.LeanSelectableDrop/IgnoreType Lean.Touch.LeanSelectableDrop::get_Ignore()
extern void LeanSelectableDrop_get_Ignore_m0DAD5DE2F8D47E632EBEF38B46A6B4A3A0379BA9 (void);
// 0x00000289 Lean.Touch.LeanSelectableDrop/GameObjectEvent Lean.Touch.LeanSelectableDrop::get_OnGameObject()
extern void LeanSelectableDrop_get_OnGameObject_mE3BF2B1AE9118D2822D91A27B4ED92B25738CE98 (void);
// 0x0000028A Lean.Touch.LeanSelectableDrop/IDropHandlerEvent Lean.Touch.LeanSelectableDrop::get_OnDropHandler()
extern void LeanSelectableDrop_get_OnDropHandler_m9454C0FAB58FDCBB414B8CD43E365832EBBF66BB (void);
// 0x0000028B System.Void Lean.Touch.LeanSelectableDrop::OnSelectedFingerUp(Lean.Touch.LeanFinger)
extern void LeanSelectableDrop_OnSelectedFingerUp_m7BAC9F477B8D93E8F11CE8BFB928B91910DAE42A (void);
// 0x0000028C System.Void Lean.Touch.LeanSelectableDrop::.ctor()
extern void LeanSelectableDrop__ctor_m0C0C48914F5BF48305C8B61520060795BE992141 (void);
// 0x0000028D System.Void Lean.Touch.LeanSelectableDrop/GameObjectEvent::.ctor()
extern void GameObjectEvent__ctor_m5DF4A4A01416DFFDDA8B9326E1CF822DCD96F46B (void);
// 0x0000028E System.Void Lean.Touch.LeanSelectableDrop/IDropHandlerEvent::.ctor()
extern void IDropHandlerEvent__ctor_m9E6C867F445ADBAF24F5CE713712795D48140653 (void);
// 0x0000028F System.Void Lean.Touch.LeanSelectablePressureScale::set_BaseScale(UnityEngine.Vector3)
extern void LeanSelectablePressureScale_set_BaseScale_m97CC53E7EAE851915F0638D9075B268D67525400 (void);
// 0x00000290 UnityEngine.Vector3 Lean.Touch.LeanSelectablePressureScale::get_BaseScale()
extern void LeanSelectablePressureScale_get_BaseScale_m2D353FDECCF96B709D9524A1F38AED736FB0598C (void);
// 0x00000291 System.Void Lean.Touch.LeanSelectablePressureScale::set_PressureMultiplier(System.Single)
extern void LeanSelectablePressureScale_set_PressureMultiplier_m1774968FF650122D1B4084D6D17CAF51EC76C18D (void);
// 0x00000292 System.Single Lean.Touch.LeanSelectablePressureScale::get_PressureMultiplier()
extern void LeanSelectablePressureScale_get_PressureMultiplier_m537EEE78A10C67AAF637FF3D931D2D6ED5520CB4 (void);
// 0x00000293 System.Void Lean.Touch.LeanSelectablePressureScale::set_PressureClamp(System.Boolean)
extern void LeanSelectablePressureScale_set_PressureClamp_mCCD73A1F65CA1FDF49343007E81D915AE1A7B4A9 (void);
// 0x00000294 System.Boolean Lean.Touch.LeanSelectablePressureScale::get_PressureClamp()
extern void LeanSelectablePressureScale_get_PressureClamp_m93564474798CC07D790C1398F347FFB19AC16E63 (void);
// 0x00000295 System.Void Lean.Touch.LeanSelectablePressureScale::Update()
extern void LeanSelectablePressureScale_Update_m5145C0A58F1E9D857401D95DBCFE568ADAFB6B9A (void);
// 0x00000296 System.Void Lean.Touch.LeanSelectablePressureScale::.ctor()
extern void LeanSelectablePressureScale__ctor_mDA2BC9C7AC65DB2B0BBAB8E1FFA5C8D12ADA10A7 (void);
// 0x00000297 System.Void Lean.Touch.LeanSelectableSelected::set_Threshold(System.Single)
extern void LeanSelectableSelected_set_Threshold_m7D09AC8C7078507865C167D8B094D5B570E6A1C8 (void);
// 0x00000298 System.Single Lean.Touch.LeanSelectableSelected::get_Threshold()
extern void LeanSelectableSelected_get_Threshold_m2E1A7386F80AA0B1C4F2C70A28D44BB9DA52AD5A (void);
// 0x00000299 System.Void Lean.Touch.LeanSelectableSelected::set_Reset(Lean.Touch.LeanSelectableSelected/ResetType)
extern void LeanSelectableSelected_set_Reset_m207C28724AD3697B081EFF969658DEE3B9EC0433 (void);
// 0x0000029A Lean.Touch.LeanSelectableSelected/ResetType Lean.Touch.LeanSelectableSelected::get_Reset()
extern void LeanSelectableSelected_get_Reset_m162ED6E1F355425A91E30FD9B8E7624DAB86FC2A (void);
// 0x0000029B System.Void Lean.Touch.LeanSelectableSelected::set_RawSelection(System.Boolean)
extern void LeanSelectableSelected_set_RawSelection_m48DE22BD7FB3A4BE3EC1C647789263B96F0DCCF1 (void);
// 0x0000029C System.Boolean Lean.Touch.LeanSelectableSelected::get_RawSelection()
extern void LeanSelectableSelected_get_RawSelection_m42C076D5FE28A6AAB09DD91AAAF0EFD97247000C (void);
// 0x0000029D System.Void Lean.Touch.LeanSelectableSelected::set_RequireFinger(System.Boolean)
extern void LeanSelectableSelected_set_RequireFinger_mBFA43462C4D1F0CE21EE8F227E615D7A6F1BA960 (void);
// 0x0000029E System.Boolean Lean.Touch.LeanSelectableSelected::get_RequireFinger()
extern void LeanSelectableSelected_get_RequireFinger_mBA8A44064EDCB55050229EBD735BD82E0BCB33BF (void);
// 0x0000029F Lean.Touch.LeanSelectableSelected/SelectableEvent Lean.Touch.LeanSelectableSelected::get_OnSelectableDown()
extern void LeanSelectableSelected_get_OnSelectableDown_mF2AE8A9395E3FB8626247ED255189B4A5104DBA9 (void);
// 0x000002A0 Lean.Touch.LeanSelectableSelected/SelectableEvent Lean.Touch.LeanSelectableSelected::get_OnSelectableUpdate()
extern void LeanSelectableSelected_get_OnSelectableUpdate_m60B5E18108023F15F3073ECAEAD3A63905861474 (void);
// 0x000002A1 Lean.Touch.LeanSelectableSelected/SelectableEvent Lean.Touch.LeanSelectableSelected::get_OnSelectableUp()
extern void LeanSelectableSelected_get_OnSelectableUp_m15D05E03489DD3793FC409A69CAB0CB06FE11155 (void);
// 0x000002A2 System.Void Lean.Touch.LeanSelectableSelected::Update()
extern void LeanSelectableSelected_Update_m4E3C80B96D751D0212E469A88D380954589EAD13 (void);
// 0x000002A3 System.Void Lean.Touch.LeanSelectableSelected::OnSelected()
extern void LeanSelectableSelected_OnSelected_m840FDA83E226D64A7C1DA34CF9A8F98CDA18CB7E (void);
// 0x000002A4 System.Void Lean.Touch.LeanSelectableSelected::OnDeselected()
extern void LeanSelectableSelected_OnDeselected_mEABF5DFB4050FE2045F06937FBAAA8CFD2E88515 (void);
// 0x000002A5 System.Void Lean.Touch.LeanSelectableSelected::.ctor()
extern void LeanSelectableSelected__ctor_m5ED3B1C77CA40BDB03628C746DB6F0EC051A0C0A (void);
// 0x000002A6 System.Void Lean.Touch.LeanSelectableSelected/SelectableEvent::.ctor()
extern void SelectableEvent__ctor_m919E20AC6C5B01AB3571D1EC0D3306215703BBCE (void);
// 0x000002A7 System.Void Lean.Touch.LeanSelectableTime::set_Send(Lean.Touch.LeanSelectableTime/SendType)
extern void LeanSelectableTime_set_Send_m6B8C842777A9019AC96E3E2E9D457BA558C26430 (void);
// 0x000002A8 Lean.Touch.LeanSelectableTime/SendType Lean.Touch.LeanSelectableTime::get_Send()
extern void LeanSelectableTime_get_Send_m2DEA934FE7700C3B7FDA92CC4D8B761240DFF296 (void);
// 0x000002A9 Lean.Touch.LeanSelectableTime/FloatEvent Lean.Touch.LeanSelectableTime::get_OnSeconds()
extern void LeanSelectableTime_get_OnSeconds_m29513A7E7F5F9C4B3BB4859741FFABF42B29ACB7 (void);
// 0x000002AA System.Void Lean.Touch.LeanSelectableTime::Update()
extern void LeanSelectableTime_Update_mD1B3E3CE029B00E45341EE43EE947A9023823478 (void);
// 0x000002AB System.Void Lean.Touch.LeanSelectableTime::.ctor()
extern void LeanSelectableTime__ctor_m85A76A4DEE9639059EA0D8F40418A104BEE29875 (void);
// 0x000002AC System.Void Lean.Touch.LeanSelectableTime/FloatEvent::.ctor()
extern void FloatEvent__ctor_m9EADF2017C4D485563D1029979BC5A633594F5F9 (void);
// 0x000002AD System.Void Lean.Touch.LeanSelectionBox::set_Camera(UnityEngine.Camera)
extern void LeanSelectionBox_set_Camera_m943D8D5AB5ECD99269E189F6D13CB76255792A80 (void);
// 0x000002AE UnityEngine.Camera Lean.Touch.LeanSelectionBox::get_Camera()
extern void LeanSelectionBox_get_Camera_m560BEA20B498C82662801AA76DF89E3944CF03D4 (void);
// 0x000002AF System.Void Lean.Touch.LeanSelectionBox::set_IgnoreIfStartedOverGui(System.Boolean)
extern void LeanSelectionBox_set_IgnoreIfStartedOverGui_m6BBD118D11B026D21654AD7B279C3F98C05B04BE (void);
// 0x000002B0 System.Boolean Lean.Touch.LeanSelectionBox::get_IgnoreIfStartedOverGui()
extern void LeanSelectionBox_get_IgnoreIfStartedOverGui_m4146ADC45FF79D0F24CA4200C9C714B54160458C (void);
// 0x000002B1 System.Void Lean.Touch.LeanSelectionBox::set_Prefab(UnityEngine.RectTransform)
extern void LeanSelectionBox_set_Prefab_mCED74ED19C7CD52AFE2AA0F617360AAC24E5078C (void);
// 0x000002B2 UnityEngine.RectTransform Lean.Touch.LeanSelectionBox::get_Prefab()
extern void LeanSelectionBox_get_Prefab_m51337DEF4548C71CD4D342E9F399FE2EEDA418DF (void);
// 0x000002B3 System.Void Lean.Touch.LeanSelectionBox::set_Root(UnityEngine.RectTransform)
extern void LeanSelectionBox_set_Root_m58D6C9F901982DB603030A9E966FCED97BB68BCB (void);
// 0x000002B4 UnityEngine.RectTransform Lean.Touch.LeanSelectionBox::get_Root()
extern void LeanSelectionBox_get_Root_mB67EC4FC876421256D242D7B19532DE450B7364B (void);
// 0x000002B5 System.Void Lean.Touch.LeanSelectionBox::set_Select(Lean.Touch.LeanSelectByFinger)
extern void LeanSelectionBox_set_Select_mBF08B346300D60318753512B42A09F21883FB582 (void);
// 0x000002B6 Lean.Touch.LeanSelectByFinger Lean.Touch.LeanSelectionBox::get_Select()
extern void LeanSelectionBox_get_Select_mFFA405A7AB050F4AE22D0B8314FD3CEBA2322B6F (void);
// 0x000002B7 System.Void Lean.Touch.LeanSelectionBox::set_RequiredLayers(UnityEngine.LayerMask)
extern void LeanSelectionBox_set_RequiredLayers_mBC54A3B917A282D6D91D9BB7A8F59A7A37289784 (void);
// 0x000002B8 UnityEngine.LayerMask Lean.Touch.LeanSelectionBox::get_RequiredLayers()
extern void LeanSelectionBox_get_RequiredLayers_m26CABC0777DF3C2D317F46A9E92D6B4BE50440A3 (void);
// 0x000002B9 System.Collections.Generic.List`1<System.String> Lean.Touch.LeanSelectionBox::get_RequiredTags()
extern void LeanSelectionBox_get_RequiredTags_mFD50533F78AF9726C11A9E2A860BE23B0F4BD354 (void);
// 0x000002BA System.Void Lean.Touch.LeanSelectionBox::OnEnable()
extern void LeanSelectionBox_OnEnable_m6560444F6E3D64F5BD0C44ED38B5E4661D82EA43 (void);
// 0x000002BB System.Void Lean.Touch.LeanSelectionBox::OnDisable()
extern void LeanSelectionBox_OnDisable_mB7D448D301AD7A7A5243CCE48D38815DBA2BDBFE (void);
// 0x000002BC System.Void Lean.Touch.LeanSelectionBox::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanSelectionBox_HandleFingerDown_m18A35A2416071AC81C951AAED04C260A9C56B98B (void);
// 0x000002BD System.Void Lean.Touch.LeanSelectionBox::HandleFingerSet(Lean.Touch.LeanFinger)
extern void LeanSelectionBox_HandleFingerSet_m0F76325DC5CA3704131AB17D9057C8D9A00CD75B (void);
// 0x000002BE System.Void Lean.Touch.LeanSelectionBox::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanSelectionBox_HandleFingerUp_m5DEA0F68FDFE957FCB6F7A09A5577A9F99EAF166 (void);
// 0x000002BF System.Void Lean.Touch.LeanSelectionBox::WriteTransform(UnityEngine.RectTransform,Lean.Touch.LeanFinger)
extern void LeanSelectionBox_WriteTransform_m10172607008E039CC69F1D7B0F00B31DA0BBDAF1 (void);
// 0x000002C0 System.Boolean Lean.Touch.LeanSelectionBox::HasRequiredTag(System.String)
extern void LeanSelectionBox_HasRequiredTag_m712F1BE85B6D3FB395E506A375F752749479358D (void);
// 0x000002C1 System.Void Lean.Touch.LeanSelectionBox::.ctor()
extern void LeanSelectionBox__ctor_mCADDADEDE381D79F6F2D6E78D42E555358728378 (void);
// 0x000002C2 System.Void Lean.Touch.LeanSelectionBox::.cctor()
extern void LeanSelectionBox__cctor_m58DA7605A6337DC364102AF29D3AD15FB724CCDB (void);
// 0x000002C3 System.Void Lean.Touch.LeanSelectionBox/FingerData::.ctor()
extern void FingerData__ctor_m66A10889E5B66034D798A491359A21164C3FEC77 (void);
// 0x000002C4 System.Void Lean.Touch.LeanShape::set_ConnectEnds(System.Boolean)
extern void LeanShape_set_ConnectEnds_m8803D34387B4CD9F6C21457C3E43F9A66F27CFA0 (void);
// 0x000002C5 System.Boolean Lean.Touch.LeanShape::get_ConnectEnds()
extern void LeanShape_get_ConnectEnds_m06FC33CF5EA7538DC2DCA61A9CCCDD86C8A989FE (void);
// 0x000002C6 System.Void Lean.Touch.LeanShape::set_Visual(UnityEngine.LineRenderer)
extern void LeanShape_set_Visual_m62441FD3A3CFF46A907C45905320716C5775C8AA (void);
// 0x000002C7 UnityEngine.LineRenderer Lean.Touch.LeanShape::get_Visual()
extern void LeanShape_get_Visual_mB7421CF4625C94634A111B268A4A134638934775 (void);
// 0x000002C8 System.Collections.Generic.List`1<UnityEngine.Vector2> Lean.Touch.LeanShape::get_Points()
extern void LeanShape_get_Points_m023F85C336F1CB4BBE2CBEE768B8CE7D707F14FF (void);
// 0x000002C9 System.Int32 Lean.Touch.LeanShape::Mod(System.Int32,System.Int32)
extern void LeanShape_Mod_mF2AAB88C66376FA47A2936C2A244F8C8C336983F (void);
// 0x000002CA UnityEngine.Vector2 Lean.Touch.LeanShape::GetPoint(System.Int32,System.Boolean)
extern void LeanShape_GetPoint_m6609D5AEC3203B4EB977568511E26F102F7545AD (void);
// 0x000002CB System.Void Lean.Touch.LeanShape::UpdateVisual()
extern void LeanShape_UpdateVisual_m8C615F030DEAB5A017FF6F05DCF0A3D0C8CBA305 (void);
// 0x000002CC System.Void Lean.Touch.LeanShape::.ctor()
extern void LeanShape__ctor_m91232CE7E03E116899B2AAB71AC7E66ABE74F306 (void);
// 0x000002CD System.Void Lean.Touch.LeanShapeDetector::set_Shape(Lean.Touch.LeanShape)
extern void LeanShapeDetector_set_Shape_m70FAA8EE19ED6E60E4FF6BA11F24A802796482F7 (void);
// 0x000002CE Lean.Touch.LeanShape Lean.Touch.LeanShapeDetector::get_Shape()
extern void LeanShapeDetector_get_Shape_mEF115C09786532116198A59C312EC75DF2ED2D3F (void);
// 0x000002CF System.Void Lean.Touch.LeanShapeDetector::set_StepThreshold(System.Single)
extern void LeanShapeDetector_set_StepThreshold_m9F3EA78AFE1230923555731FEB2830007534D2C5 (void);
// 0x000002D0 System.Single Lean.Touch.LeanShapeDetector::get_StepThreshold()
extern void LeanShapeDetector_get_StepThreshold_m60C18390E6981E60794F3025A7C6E962E4D67972 (void);
// 0x000002D1 System.Void Lean.Touch.LeanShapeDetector::set_DistanceThreshold(System.Single)
extern void LeanShapeDetector_set_DistanceThreshold_mD956E6A87B6F531FBAED17D75A9221277EC7F9BE (void);
// 0x000002D2 System.Single Lean.Touch.LeanShapeDetector::get_DistanceThreshold()
extern void LeanShapeDetector_get_DistanceThreshold_m46C227911B234152D916F72B3F0E02349C65C97B (void);
// 0x000002D3 System.Void Lean.Touch.LeanShapeDetector::set_ErrorThreshold(System.Single)
extern void LeanShapeDetector_set_ErrorThreshold_m612CAA2BA37AE32F5442E59B6FEE7A4917E964FF (void);
// 0x000002D4 System.Single Lean.Touch.LeanShapeDetector::get_ErrorThreshold()
extern void LeanShapeDetector_get_ErrorThreshold_m196E7F8B41A102A7743DBF7A9F81C2AE0D856C62 (void);
// 0x000002D5 System.Void Lean.Touch.LeanShapeDetector::set_MinimumPoints(System.Int32)
extern void LeanShapeDetector_set_MinimumPoints_m52C48EFA39DD3DC124981FF3F1390ADD96B23B89 (void);
// 0x000002D6 System.Int32 Lean.Touch.LeanShapeDetector::get_MinimumPoints()
extern void LeanShapeDetector_get_MinimumPoints_mEA4BAE9768F9A38B7190397C2FC07DFBE64A72D3 (void);
// 0x000002D7 System.Void Lean.Touch.LeanShapeDetector::set_Direction(Lean.Touch.LeanShapeDetector/DirectionType)
extern void LeanShapeDetector_set_Direction_m0B83036DDABFF35F0B36D0239A60127D2C55DBD7 (void);
// 0x000002D8 Lean.Touch.LeanShapeDetector/DirectionType Lean.Touch.LeanShapeDetector::get_Direction()
extern void LeanShapeDetector_get_Direction_mD0DD6F122A68258B61D6CA6B1854F57EF77CDF53 (void);
// 0x000002D9 Lean.Touch.LeanShapeDetector/LeanFingerEvent Lean.Touch.LeanShapeDetector::get_OnDetected()
extern void LeanShapeDetector_get_OnDetected_mEC708697BD4CDEA0ED8A0D164C6B03F9E88346E5 (void);
// 0x000002DA System.Void Lean.Touch.LeanShapeDetector::AddFinger(Lean.Touch.LeanFinger)
extern void LeanShapeDetector_AddFinger_m776C3EF35A9F6262E1B0F5B88D84ACC52840B225 (void);
// 0x000002DB System.Void Lean.Touch.LeanShapeDetector::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanShapeDetector_RemoveFinger_mDE85C3B0FB482870F749AFCA103DC56B9DA39B5C (void);
// 0x000002DC System.Void Lean.Touch.LeanShapeDetector::RemoveAllFingers()
extern void LeanShapeDetector_RemoveAllFingers_m3745A940941909D91099F4ADDC4039F376B28D2B (void);
// 0x000002DD System.Void Lean.Touch.LeanShapeDetector::OnEnable()
extern void LeanShapeDetector_OnEnable_mC1B549D87DEA472ED29329B44E4D081DD9B18EA3 (void);
// 0x000002DE System.Void Lean.Touch.LeanShapeDetector::OnDisable()
extern void LeanShapeDetector_OnDisable_m88F9C1BD02AD7DE79FB189EDD97A2C690EC93359 (void);
// 0x000002DF System.Void Lean.Touch.LeanShapeDetector::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanShapeDetector_HandleFingerDown_mBB6AC90C7051A1457EF67004348CC33D463144E5 (void);
// 0x000002E0 System.Void Lean.Touch.LeanShapeDetector::HandleFingerUpdate(Lean.Touch.LeanFinger)
extern void LeanShapeDetector_HandleFingerUpdate_mC7F9C4785B13C1C55391ECE2B89F1E9743379221 (void);
// 0x000002E1 System.Void Lean.Touch.LeanShapeDetector::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanShapeDetector_HandleFingerUp_m3AB300B53299442FEF895D5B4C8BFB682FE21F09 (void);
// 0x000002E2 System.Void Lean.Touch.LeanShapeDetector::AddRange(System.Int32,System.Int32)
extern void LeanShapeDetector_AddRange_m1A04BD5DC2CD3C9D9D80E0708117C3EDE61ED856 (void);
// 0x000002E3 System.Boolean Lean.Touch.LeanShapeDetector::CalculateMatch(System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Single,System.Single,System.Int32,System.Int32)
extern void LeanShapeDetector_CalculateMatch_m339F24DAEBD4698AA57BE767FF3107072E7EE458 (void);
// 0x000002E4 System.Void Lean.Touch.LeanShapeDetector::FitShape(System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern void LeanShapeDetector_FitShape_m5F02DE1C89A494497FA7145A7FBCE3CC3AC36BCA (void);
// 0x000002E5 System.Void Lean.Touch.LeanShapeDetector::ConvertPoints(System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Int32,System.Int32)
extern void LeanShapeDetector_ConvertPoints_mC03F016479397BCB04C4518EA348D2C5F0FEEE00 (void);
// 0x000002E6 UnityEngine.Vector2 Lean.Touch.LeanShapeDetector::Read(System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Int32)
extern void LeanShapeDetector_Read_m3D68642B63BAD145329A0CA2DD56D5CD868CFBA9 (void);
// 0x000002E7 UnityEngine.Rect Lean.Touch.LeanShapeDetector::GetRect(System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern void LeanShapeDetector_GetRect_m9A1DE679D79C530287665826BA8E149337823EA8 (void);
// 0x000002E8 System.Void Lean.Touch.LeanShapeDetector::.ctor()
extern void LeanShapeDetector__ctor_m9923BF69329BF0ECC66F3A81E3EB3CF3B408698E (void);
// 0x000002E9 System.Void Lean.Touch.LeanShapeDetector::.cctor()
extern void LeanShapeDetector__cctor_mFD7F4A05D0577CFF0550C0FD9AD8D289BCD8C179 (void);
// 0x000002EA System.Void Lean.Touch.LeanShapeDetector/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m464DB7B86BADBC50488DD963D2666D5CE8B4E4DA (void);
// 0x000002EB UnityEngine.Vector2 Lean.Touch.LeanShapeDetector/FingerData::get_EndPoint()
extern void FingerData_get_EndPoint_mD863A5CE572F621AEDB7C431E4A01C6C0837ED22 (void);
// 0x000002EC System.Void Lean.Touch.LeanShapeDetector/FingerData::.ctor()
extern void FingerData__ctor_mFDCE2D7B0EE44BE6B628A4DF5D81B1A5F15F35B3 (void);
// 0x000002ED System.Single Lean.Touch.LeanShapeDetector/Line::GetFirstDistance(UnityEngine.Vector2)
extern void Line_GetFirstDistance_m9890016A67A214E24E791787A493B349D61B3D47 (void);
// 0x000002EE System.Single Lean.Touch.LeanShapeDetector/Line::GetDistance(UnityEngine.Vector2)
extern void Line_GetDistance_mC5BE3BE21A97308070621AD7FE6210404D551731 (void);
// 0x000002EF System.Void Lean.Touch.LeanSpawnWithFinger::set_Prefab(UnityEngine.Transform)
extern void LeanSpawnWithFinger_set_Prefab_m817F51523DBA598406CC7029EBE7F45310CEC543 (void);
// 0x000002F0 UnityEngine.Transform Lean.Touch.LeanSpawnWithFinger::get_Prefab()
extern void LeanSpawnWithFinger_get_Prefab_m8B650147CC3C227078E740C70A3A5871CDD1B8B1 (void);
// 0x000002F1 System.Void Lean.Touch.LeanSpawnWithFinger::set_RotateTo(Lean.Touch.LeanSpawnWithFinger/RotateType)
extern void LeanSpawnWithFinger_set_RotateTo_m123CDA98BF3601BCD808640CB28657A5862EC05A (void);
// 0x000002F2 Lean.Touch.LeanSpawnWithFinger/RotateType Lean.Touch.LeanSpawnWithFinger::get_RotateTo()
extern void LeanSpawnWithFinger_get_RotateTo_mA206E34CFF36BDF477158F01775941D912A12A22 (void);
// 0x000002F3 System.Void Lean.Touch.LeanSpawnWithFinger::set_DragAfterSpawn(System.Boolean)
extern void LeanSpawnWithFinger_set_DragAfterSpawn_mA405D74C831C7455DDF3CC3AFAC346815DDF9CBA (void);
// 0x000002F4 System.Boolean Lean.Touch.LeanSpawnWithFinger::get_DragAfterSpawn()
extern void LeanSpawnWithFinger_get_DragAfterSpawn_mF2425ACA6B966CBDDF9FB31DB9D8B7FDE01ED667 (void);
// 0x000002F5 System.Void Lean.Touch.LeanSpawnWithFinger::set_DestroyIfBlocked(System.Boolean)
extern void LeanSpawnWithFinger_set_DestroyIfBlocked_mD3CD73B50EF30FD74A6F8FC93F9D74E9AF381685 (void);
// 0x000002F6 System.Boolean Lean.Touch.LeanSpawnWithFinger::get_DestroyIfBlocked()
extern void LeanSpawnWithFinger_get_DestroyIfBlocked_mFCD874283D7FCCC6CA27412A591B236388EC5E4C (void);
// 0x000002F7 System.Void Lean.Touch.LeanSpawnWithFinger::set_SelectOnSpawn(System.Boolean)
extern void LeanSpawnWithFinger_set_SelectOnSpawn_mFDDC5C8BC9DD3AF452EA2BF9788A312D4FA4AB4D (void);
// 0x000002F8 System.Boolean Lean.Touch.LeanSpawnWithFinger::get_SelectOnSpawn()
extern void LeanSpawnWithFinger_get_SelectOnSpawn_m089AB5E1C46306C75D7DE84CA51E5D93B9A8865C (void);
// 0x000002F9 System.Void Lean.Touch.LeanSpawnWithFinger::set_SelectWith(Lean.Common.LeanSelect)
extern void LeanSpawnWithFinger_set_SelectWith_mB7794291E1876D9132709086CB34B55AA166A976 (void);
// 0x000002FA Lean.Common.LeanSelect Lean.Touch.LeanSpawnWithFinger::get_SelectWith()
extern void LeanSpawnWithFinger_get_SelectWith_m03E5CBB854A225395D714AEE4330301AF93B3D88 (void);
// 0x000002FB System.Void Lean.Touch.LeanSpawnWithFinger::set_DeselectOnUp(System.Boolean)
extern void LeanSpawnWithFinger_set_DeselectOnUp_m49CDBA27ECB9419959CA6B8A656683D8233776A9 (void);
// 0x000002FC System.Boolean Lean.Touch.LeanSpawnWithFinger::get_DeselectOnUp()
extern void LeanSpawnWithFinger_get_DeselectOnUp_m60584A1972DAFAC10805CEF5A5E4990B42AA89A1 (void);
// 0x000002FD System.Void Lean.Touch.LeanSpawnWithFinger::set_PixelOffset(UnityEngine.Vector2)
extern void LeanSpawnWithFinger_set_PixelOffset_mDA63EF079E799FDF1CEB2AB501202A5E40130141 (void);
// 0x000002FE UnityEngine.Vector2 Lean.Touch.LeanSpawnWithFinger::get_PixelOffset()
extern void LeanSpawnWithFinger_get_PixelOffset_m44FAF422501370635CDCFD3046D04E6711BF0C70 (void);
// 0x000002FF System.Void Lean.Touch.LeanSpawnWithFinger::set_PixelScale(UnityEngine.Canvas)
extern void LeanSpawnWithFinger_set_PixelScale_m5007FDA6003573EC4BEF453D6BB201FA8F927D07 (void);
// 0x00000300 UnityEngine.Canvas Lean.Touch.LeanSpawnWithFinger::get_PixelScale()
extern void LeanSpawnWithFinger_get_PixelScale_m9376BA7AF29DDA7C933D4C2245078CC6222A47DA (void);
// 0x00000301 System.Void Lean.Touch.LeanSpawnWithFinger::set_WorldOffset(UnityEngine.Vector3)
extern void LeanSpawnWithFinger_set_WorldOffset_mC8AE70C28B458C2FBFC74308E15213839A8F1A72 (void);
// 0x00000302 UnityEngine.Vector3 Lean.Touch.LeanSpawnWithFinger::get_WorldOffset()
extern void LeanSpawnWithFinger_get_WorldOffset_m30935963A64D9AB6863F84D21982A4C3D54A8868 (void);
// 0x00000303 System.Void Lean.Touch.LeanSpawnWithFinger::set_WorldRelativeTo(UnityEngine.Transform)
extern void LeanSpawnWithFinger_set_WorldRelativeTo_m4A5BDC9C7C70EE92F26C9AE788F44D10964A21C9 (void);
// 0x00000304 UnityEngine.Transform Lean.Touch.LeanSpawnWithFinger::get_WorldRelativeTo()
extern void LeanSpawnWithFinger_get_WorldRelativeTo_mE1F7C130B014EFD76C831A47CFBA6A7AD8530A1C (void);
// 0x00000305 System.Void Lean.Touch.LeanSpawnWithFinger::Spawn(Lean.Touch.LeanFinger)
extern void LeanSpawnWithFinger_Spawn_mB735ECE956C9C8900EEFCCBE0DA6400548F4474F (void);
// 0x00000306 System.Void Lean.Touch.LeanSpawnWithFinger::OnEnable()
extern void LeanSpawnWithFinger_OnEnable_mA1A4E475C02D8310DC122ABFB989F2B785CD0455 (void);
// 0x00000307 System.Void Lean.Touch.LeanSpawnWithFinger::OnDisable()
extern void LeanSpawnWithFinger_OnDisable_mCF01CEA19EF6E7FB97ADA8C0CCEF17D3FF1BFD5F (void);
// 0x00000308 System.Void Lean.Touch.LeanSpawnWithFinger::Update()
extern void LeanSpawnWithFinger_Update_m3375C1444256BCF2FD504D957FB049D22A08E911 (void);
// 0x00000309 System.Void Lean.Touch.LeanSpawnWithFinger::UpdateSpawnedTransform(Lean.Touch.LeanFinger,UnityEngine.Transform)
extern void LeanSpawnWithFinger_UpdateSpawnedTransform_mCECF8DF74A7E33345E5791F3F90C0FF94E2191DC (void);
// 0x0000030A System.Void Lean.Touch.LeanSpawnWithFinger::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanSpawnWithFinger_HandleFingerUp_mF30BE86F48384C6A244711C1348A64563FB317F0 (void);
// 0x0000030B System.Void Lean.Touch.LeanSpawnWithFinger::.ctor()
extern void LeanSpawnWithFinger__ctor_m4103E35960EA82BE7A4E08939912D1F59E5D5C1B (void);
// 0x0000030C System.Void Lean.Touch.LeanSpawnWithFinger::.cctor()
extern void LeanSpawnWithFinger__cctor_mC0F391C07861AACEA3872A0DDCC5FC43F77942E3 (void);
// 0x0000030D System.Void Lean.Touch.LeanSpawnWithFinger/FingerData::.ctor()
extern void FingerData__ctor_m16F99113B9EF9F522810BE3FFC0FD2F0F8B23982 (void);
// 0x0000030E System.Void Lean.Touch.LeanSwipeEdge::set_Left(System.Boolean)
extern void LeanSwipeEdge_set_Left_m5DD0E423DF2FE933CF1C1C38E9765EF0F43A07B8 (void);
// 0x0000030F System.Boolean Lean.Touch.LeanSwipeEdge::get_Left()
extern void LeanSwipeEdge_get_Left_m3D650F87AB5BFC922CA362D6DCAB66D7D1D49295 (void);
// 0x00000310 System.Void Lean.Touch.LeanSwipeEdge::set_Right(System.Boolean)
extern void LeanSwipeEdge_set_Right_m05FA1EB733D4050EC5B00AB27CD7BA942085F2DA (void);
// 0x00000311 System.Boolean Lean.Touch.LeanSwipeEdge::get_Right()
extern void LeanSwipeEdge_get_Right_mC816C84BFB7E4554AACF8C62497EBD53ED43C327 (void);
// 0x00000312 System.Void Lean.Touch.LeanSwipeEdge::set_Bottom(System.Boolean)
extern void LeanSwipeEdge_set_Bottom_mE1EE1EF713F1AD7274A8E90C8F025654F4FFC93B (void);
// 0x00000313 System.Boolean Lean.Touch.LeanSwipeEdge::get_Bottom()
extern void LeanSwipeEdge_get_Bottom_m10EC5487DD456E4379DA95436C7FD8BEB8DB8848 (void);
// 0x00000314 System.Void Lean.Touch.LeanSwipeEdge::set_Top(System.Boolean)
extern void LeanSwipeEdge_set_Top_mD9C51CCD6C5F267A4B7723F22E175F0047FAD430 (void);
// 0x00000315 System.Boolean Lean.Touch.LeanSwipeEdge::get_Top()
extern void LeanSwipeEdge_get_Top_m3D73458213C68D77381A6B6100911152B3E25880 (void);
// 0x00000316 System.Void Lean.Touch.LeanSwipeEdge::set_AngleThreshold(System.Single)
extern void LeanSwipeEdge_set_AngleThreshold_m870F700221A1B3E2031405EBEF2C1872529F5088 (void);
// 0x00000317 System.Single Lean.Touch.LeanSwipeEdge::get_AngleThreshold()
extern void LeanSwipeEdge_get_AngleThreshold_m3B5424C4FD11CD9F52A66B116CAEB187091D5236 (void);
// 0x00000318 System.Void Lean.Touch.LeanSwipeEdge::set_EdgeThreshold(System.Single)
extern void LeanSwipeEdge_set_EdgeThreshold_m0016ED8CC52B61B48E6E92103292E362FC256AF2 (void);
// 0x00000319 System.Single Lean.Touch.LeanSwipeEdge::get_EdgeThreshold()
extern void LeanSwipeEdge_get_EdgeThreshold_mB32D91C82BFEEA4871AA0B287CD929D86AD8A995 (void);
// 0x0000031A UnityEngine.Events.UnityEvent Lean.Touch.LeanSwipeEdge::get_OnEdge()
extern void LeanSwipeEdge_get_OnEdge_mCD0A96E445B4584B08CC19EC2206A684312AAC4C (void);
// 0x0000031B System.Void Lean.Touch.LeanSwipeEdge::CheckBetween(UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanSwipeEdge_CheckBetween_m569242B4189617703249032C57A23B902E307C67 (void);
// 0x0000031C System.Void Lean.Touch.LeanSwipeEdge::AddFinger(Lean.Touch.LeanFinger)
extern void LeanSwipeEdge_AddFinger_mA7B3C04B5AD739C4198F66D42161A21604ED2814 (void);
// 0x0000031D System.Void Lean.Touch.LeanSwipeEdge::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanSwipeEdge_RemoveFinger_m77463614BF44E25C1D2F372D8A6B5F7436E795CE (void);
// 0x0000031E System.Void Lean.Touch.LeanSwipeEdge::RemoveAllFingers()
extern void LeanSwipeEdge_RemoveAllFingers_mAC1A4D096D1C03227DBC2611754A36706567B7FA (void);
// 0x0000031F System.Void Lean.Touch.LeanSwipeEdge::Awake()
extern void LeanSwipeEdge_Awake_m211B1BF010FC32D456F3DBE421554042AB38E5AA (void);
// 0x00000320 System.Void Lean.Touch.LeanSwipeEdge::Update()
extern void LeanSwipeEdge_Update_mD129304BD898F94F1E342D81423B3C7855B4B2CD (void);
// 0x00000321 System.Void Lean.Touch.LeanSwipeEdge::InvokeEdge()
extern void LeanSwipeEdge_InvokeEdge_m7648B6DE9697228BE50609600DAFEF68CDAB7C8B (void);
// 0x00000322 System.Boolean Lean.Touch.LeanSwipeEdge::CheckAngle(UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanSwipeEdge_CheckAngle_m99F242FA56B49C8101EA10205513DFDF5A399D6C (void);
// 0x00000323 System.Boolean Lean.Touch.LeanSwipeEdge::CheckEdge(System.Single)
extern void LeanSwipeEdge_CheckEdge_mCC72892056F45EF2EEE6B498168F0088E8E2FD4B (void);
// 0x00000324 System.Void Lean.Touch.LeanSwipeEdge::.ctor()
extern void LeanSwipeEdge__ctor_m560586ECB36E4865DD6112ACDBCC22E1220925D8 (void);
// 0x00000325 System.Void Lean.Touch.LeanTwistCamera::set_Damping(System.Single)
extern void LeanTwistCamera_set_Damping_mBBE1CC64EE418113F2BCD85E67117CEF0AC9A0C8 (void);
// 0x00000326 System.Single Lean.Touch.LeanTwistCamera::get_Damping()
extern void LeanTwistCamera_get_Damping_m5D1D25E62813F77A1BBE52FE4FCB5DC45AD38903 (void);
// 0x00000327 System.Void Lean.Touch.LeanTwistCamera::set_Relative(System.Boolean)
extern void LeanTwistCamera_set_Relative_m208817B328CC70DEC55988B050AC948072624D81 (void);
// 0x00000328 System.Boolean Lean.Touch.LeanTwistCamera::get_Relative()
extern void LeanTwistCamera_get_Relative_m2D0BE1C14B3D40C2F287EF826EA2CFF445581E46 (void);
// 0x00000329 System.Void Lean.Touch.LeanTwistCamera::AddFinger(Lean.Touch.LeanFinger)
extern void LeanTwistCamera_AddFinger_m74B8153A15E2BD1C8867E9119C5A54CCE83D2F2D (void);
// 0x0000032A System.Void Lean.Touch.LeanTwistCamera::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanTwistCamera_RemoveFinger_m00D617B1ECB2CFE6DEB1349EC795EB0556CF7AD7 (void);
// 0x0000032B System.Void Lean.Touch.LeanTwistCamera::RemoveAllFingers()
extern void LeanTwistCamera_RemoveAllFingers_mCEAE188CD407C235B26943A42221F7ABCB555E82 (void);
// 0x0000032C System.Void Lean.Touch.LeanTwistCamera::Awake()
extern void LeanTwistCamera_Awake_m2ACB883383D28F0F9DD2F284D84E6C3364753A7D (void);
// 0x0000032D System.Void Lean.Touch.LeanTwistCamera::Update()
extern void LeanTwistCamera_Update_mFECBA8481FFA1B34872EAAD705A24C3764AB7943 (void);
// 0x0000032E System.Void Lean.Touch.LeanTwistCamera::.ctor()
extern void LeanTwistCamera__ctor_m3B0A9026425C27900933223D22C39C9B4E1F3DC1 (void);
static Il2CppMethodPointer s_methodPointers[814] = 
{
	NULL,
	LeanDragColorMesh_set_PaintColor_m02D13638676B0607283C53A0AFB99934AD4B009C,
	LeanDragColorMesh_get_PaintColor_m9FE51EF569DE905D08F16FDFDBEF4913BD5D74C9,
	LeanDragColorMesh_set_Camera_m34DA00494E9154A3FA5C08C8C82E43852E172693,
	LeanDragColorMesh_get_Camera_m3CDF4522DDCB652CDDA654F17C1B35622BBC97C2,
	LeanDragColorMesh_AddFinger_mCE3752EB2F987C3895277F6F54EE6C50839C2382,
	LeanDragColorMesh_RemoveFinger_mBA269A0BDFD0572FB9D1248865D3C79FBA90BEBF,
	LeanDragColorMesh_RemoveAllFingers_m056FF92363F0DC13FB54115D298063A7C5C3FC46,
	LeanDragColorMesh_Awake_mF2F1017D508ED95F56D541954C2AF2E82CEFA692,
	LeanDragColorMesh_Update_mA3487A2816B38A91197822AEA0B1915E0DC0D95A,
	LeanDragColorMesh_Paint_mAF2EDACE873FE09F231E4ADD201CD67C0255AA6B,
	LeanDragColorMesh__ctor_mC8D00698009CAF615ADD90AA0E520A96BC3E6A3D,
	LeanDragDeformMesh_set_ScaledRadius_m4C00C9076AB014B88456D036B2806A4B6646BE35,
	LeanDragDeformMesh_get_ScaledRadius_mA485FB24D6A3A78D38D2E9065F9B098417CD7ECF,
	LeanDragDeformMesh_set_ApplyToMeshCollider_mCD0CC78EC0B68ACF07B0BFE312AAF4B519FCE616,
	LeanDragDeformMesh_get_ApplyToMeshCollider_mF79306E2D5D920FAA04DF31DADD60A474CC2C90D,
	LeanDragDeformMesh_set_Camera_m96083927ADE481A8DD2CEB9CE1443020FD3BE307,
	LeanDragDeformMesh_get_Camera_m65BD1DF992D516584E5E4BE7AACC0F475310E220,
	LeanDragDeformMesh_AddFinger_m729599252F8FB05C68A83C1C37473E2F40A10C8B,
	LeanDragDeformMesh_RemoveFinger_mB1626BD417902694DB0DDBD1C0D1D98D6ADB1259,
	LeanDragDeformMesh_RemoveAllFingers_mF5931C346425CD44A7CA76176787791848707ACA,
	LeanDragDeformMesh_Awake_mF9861C5E79FBE527C0E31C1C17DC426AA7F24CE8,
	LeanDragDeformMesh_Update_mC53946E490C37C4C5F58316E9E9D9D55587F857D,
	LeanDragDeformMesh__ctor_m13B2E40A6F236FD4552A1A13FF5DCA498B1ED8C3,
	LeanDragLine_set_WidthScale_m91EFF29BF3BBF189C38C2A1786C6F3231ED7CB2E,
	LeanDragLine_get_WidthScale_m36C36A8E922E3C4FEAA20AC075AABEFD0A8B9348,
	LeanDragLine_set_LengthMin_m84C99D85C226FA489E922D4B83ED46AE62DC892C,
	LeanDragLine_get_LengthMin_mB66FCA8E8F5054227FA8F70388E20551F9D8840F,
	LeanDragLine_set_LengthMax_m9453E8EC817154FAF558DC1ECF2EFD6DA3251A2A,
	LeanDragLine_get_LengthMax_m2ED1FB4D850FBF08B79E13B8701C176D9A21FF2D,
	LeanDragLine_set_StartAtOrigin_mF9780895E82EC172370B952C4DA2F33562180FB2,
	LeanDragLine_get_StartAtOrigin_mC1B7D85C0EE8E20158B45A5A8FD503D0CFD47DFF,
	LeanDragLine_set_Invert_mB7C00EA8A1BEE4804367A0EFB5D49A2D726E9B08,
	LeanDragLine_get_Invert_mE165FD8FF65858616334E1851574B8BE0A348F42,
	LeanDragLine_get_OnReleasedFrom_m7D2977721BB0154A3161E9E87852918AC9003582,
	LeanDragLine_get_OnReleasedTo_m5776BF2BFF2EA317A556443ED0F7E0EB241FFB72,
	LeanDragLine_get_OnReleasedDelta_m7A0207313E71BDE68E7887A0D16C373FC9EB8965,
	LeanDragLine_get_OnReleasedFromTo_mF2226F3C557C4DF908AFC403EAD2BB107C0FF175,
	LeanDragLine_UpdateLine_m8C179AE30FF9F3D2B71555D703F2035C8CFBBB72,
	LeanDragLine_HandleFingerUp_mC8F1846770D07B143BC15A9A7C46482FC3F96F37,
	LeanDragLine__ctor_mF6D28C925183A9FFA1B8D920214A06CC72D3815C,
	Vector3Vector3Event__ctor_mDB239AB300DF27B79AE1AC01E9DEEF2700441CE6,
	Vector3Event__ctor_m7E521216C5D428050D6D85A8B1596596EC786B53,
	LeanDragSelect_set_Select_mACAAA29E4B0B647BF488D8FC3A4F1987EC1A21A5,
	LeanDragSelect_get_Select_m321C47B883CC417834F0CA4BFFF7A730ED665B82,
	LeanDragSelect_set_RequireNoSelectables_mEE8B99B4288AFB192C20D536D328DBE3CE5E63EE,
	LeanDragSelect_get_RequireNoSelectables_m52330B97D85ADEE8150D1DC4C2F8D52EB9FB951D,
	LeanDragSelect_set_RequireInitialSelection_mA2E5B69DC1DE08031E8AC84D25DF565180C042AD,
	LeanDragSelect_get_RequireInitialSelection_m937A1C3036360159EC640225EEE85A24AC0C0FE4,
	LeanDragSelect_set_DeselectAllAtStart_mED1B2185188965D2FA81549ABC34768E4A94F708,
	LeanDragSelect_get_DeselectAllAtStart_mEDEEA6B31FE1118E22116CF688E596F6C59440D9,
	LeanDragSelect_set_MaximumSeparation_m2E2C18F767E7E2134DEBAFBD4888F29E4529A703,
	LeanDragSelect_get_MaximumSeparation_mE437816B4CE036005F403A1556561CD2F750C42D,
	LeanDragSelect_OnEnable_m64CA4EA5D33B3593E9DE69C90C9728858110BDDD,
	LeanDragSelect_OnDisable_m14B1552A809CC0C73A9908816DEF848B7A9A652D,
	LeanDragSelect_HandleFingerDown_mB30A4C5ACA0293C237324D8882AB4B63780E1E94,
	LeanDragSelect_HandleFingerUpdate_m0B1BAD45EEB893C42A1E4043D882A116351F498E,
	LeanDragSelect_HandleFingerUp_mBFDA712FBC048D13CB59E18B1211B31DC0560BAC,
	LeanDragSelect_HandleAnySelectedFinger_mD5DDF7918194B324C1917621173DD8CFCC738834,
	LeanDragSelect__ctor_m1D99C807B88F8AF5F5F54428DC1E1BB7B29B8AAE,
	FingerData__ctor_m9F627641F99D2D60FA6A205D6B12460F18220F31,
	LeanDragTranslateAlong_set_Target_m765D444CF39F04C6DF0D2C95967AF1D4520C56AA,
	LeanDragTranslateAlong_get_Target_mF2A02A31842BA368E4E54DF08BB594F6FE691973,
	LeanDragTranslateAlong_set_TrackScreenPosition_mB077CF510F51C35A77080F114A6B6721D69C8C3C,
	LeanDragTranslateAlong_get_TrackScreenPosition_mA2050F03AE7FB0F1EC4DDEEEF5811C7CC3507C92,
	LeanDragTranslateAlong_set_Damping_m6B8376A59578475D058252842BB114F6507EED37,
	LeanDragTranslateAlong_get_Damping_m951AFE786C5987E3769A2C3A3BBD9D2A76CDE867,
	LeanDragTranslateAlong_AddFinger_m8495B62F73B8E0BD8E7A2E5D251B75AB9F4AC417,
	LeanDragTranslateAlong_RemoveFinger_m1F16764BC10DC5CC951F1DD9B0853D33F02A949A,
	LeanDragTranslateAlong_RemoveAllFingers_mC7CCD24E1C199ED24769EBB038A427CB15F62C83,
	LeanDragTranslateAlong_Awake_m50D7810B93398AAB993CD0B73B9E84566D0F00E1,
	LeanDragTranslateAlong_Update_mDB782356A7F05D6782AA3A43AB50A1EC2D6B24A0,
	LeanDragTranslateAlong_UpdateTranslation_m79D54167B5E83D27B92815E351D209D137F849A9,
	LeanDragTranslateAlong__ctor_m034724420F305D01E98E011BF309D935D50E0224,
	LeanDragTranslateRigidbody_set_Camera_m50B9CC6D413F23ACB996A8C82E3FD7EB6C98E93C,
	LeanDragTranslateRigidbody_get_Camera_m994DEE17BECF93B946AE321526AF8C84380B907D,
	LeanDragTranslateRigidbody_set_Damping_m73D923AA2259298F3663C0937C4D77C3ACE88959,
	LeanDragTranslateRigidbody_get_Damping_mB2A0D8F4CEB71857F1C7D9DEF46EC69F637453D5,
	LeanDragTranslateRigidbody_AddFinger_m8A398753A7A5B501E55FF7C77FA786E9CF521CF3,
	LeanDragTranslateRigidbody_RemoveFinger_mFE733D164C9BCA228E1862BF3525EEB44D9CC1C9,
	LeanDragTranslateRigidbody_RemoveAllFingers_m625D805575FBF5D967A28491F243B3891DAA6A6A,
	LeanDragTranslateRigidbody_Awake_m3D9EBEDB11F627DDAA9F020413F2DF5BB9A4DFAD,
	LeanDragTranslateRigidbody_OnEnable_m32B08AF6D8860D1F5828C73CD3520D4D975ADEF5,
	LeanDragTranslateRigidbody_FixedUpdate_mCF42FBDE001F55F790AE320A3422F1A7CDD7E504,
	LeanDragTranslateRigidbody_Update_m7A0BBA3232B254A03194F6DB14C4FED2D1DBBDBC,
	LeanDragTranslateRigidbody__ctor_m96DFB46042E8A38344525963A5092FB7E47FC194,
	LeanDragTranslateRigidbody2D_set_Camera_mB67459350160E6A6850A16796B9BA044722FD19F,
	LeanDragTranslateRigidbody2D_get_Camera_m80E0A12AFA755D941A4195756785B3CEA4D073FC,
	LeanDragTranslateRigidbody2D_set_Damping_m0B87F2DC486DD614C00F9E7FF1029653AAD0E903,
	LeanDragTranslateRigidbody2D_get_Damping_m05A6D526C02616B8B0010571EAF168CF22071187,
	LeanDragTranslateRigidbody2D_AddFinger_m36EAACBF6B464DEFC5F32861EA9E158B930AF7D1,
	LeanDragTranslateRigidbody2D_RemoveFinger_m0E6E0219C41DD426AF13DFB57245D6E54E2A42C8,
	LeanDragTranslateRigidbody2D_RemoveAllFingers_m0C9ED3D7DED7CD4899AF6EAEFA275D8471C9C490,
	LeanDragTranslateRigidbody2D_Awake_m455C61A0D4F15CBAAA2466AC0BB703C24D4DCD2F,
	LeanDragTranslateRigidbody2D_OnEnable_m2D4CDD14154793E3627BD95A9776547611EA6134,
	LeanDragTranslateRigidbody2D_FixedUpdate_mB37366992B33992938C8E1BD1C7FC802B2E07200,
	LeanDragTranslateRigidbody2D_Update_m34285E056A04D655DB7E48C5C0B09766884E3517,
	LeanDragTranslateRigidbody2D__ctor_mE3656B3FA5A1105A52DA495CF933AC230CFD8DE8,
	LeanDrop_get_OnDropped_m4F06F9A5CC364DFD7081AC1D70F8E9229C809FD5,
	LeanDrop_HandleDrop_m8253952841F50B2EE004B57598E8B8A707F5BDFD,
	LeanDrop__ctor_m99BA7F4387E396A2215C7ED46843B920E9678871,
	GameObjectLeanFingerEvent__ctor_mA40063A518CD483175835A564250AD2B72CC572F,
	LeanDropCount_set_Count_m210F76848344FCB788161DAC47ADB0DF7074F22B,
	LeanDropCount_get_Count_mF85D0A590A0078C9295CC37C2DC5AF39A36D0AFC,
	LeanDropCount_get_OnCount_m9CCEB99337B12778223A50C120D1E3366F112204,
	LeanDropCount_set_MatchMin_mD25BD79AAEA193A6A615A0C7A330E96EC182AAD4,
	LeanDropCount_get_MatchMin_m68CF493BE54F99DBC4F7186920C782FAFB5AE566,
	LeanDropCount_set_MatchMax_mDAE2098079FF29EB484B8985A09C377552ECA28C,
	LeanDropCount_get_MatchMax_mCE6DFF9E3A06DE892F9ED1D816F0FD087EF2D59A,
	LeanDropCount_get_OnMatch_m3D90B31ABBA10B18FC285DCBDDDFE6CD45599354,
	LeanDropCount_get_OnUnmatch_m69A16C85097C11FAE4036CBB91AAD2FB2E275CAF,
	LeanDropCount_HandleDrop_m64486CD35E9761D8CC48B6E5D2ECB1F822430C78,
	LeanDropCount_UpdateState_m8598040D8B70D261E2808047956585EB2C179E40,
	LeanDropCount__ctor_m60F395B5058E243CD5800228ED459692DAFFBBE7,
	IntEvent__ctor_m4E41C37BA6F0D9828A3FE2BB7917A8AC8887964D,
	LeanFingerDownCanvas_set_IgnoreStartedOverGui_m1A9E9E0B373B07DCD7ADCBC00272F839BA092A4D,
	LeanFingerDownCanvas_get_IgnoreStartedOverGui_mA9F96E1D0296D68AC2A3C402459BAAAB684FBE81,
	LeanFingerDownCanvas_set_RequiredSelectable_m8749886E8F129ED10F91EA818D60BDC38C848B8C,
	LeanFingerDownCanvas_get_RequiredSelectable_m609BD1F64807B87EB6BDF55C816948838C5D50E7,
	LeanFingerDownCanvas_get_OnFinger_m6649AF91419EEAD2854EAA50305671B94F45954C,
	LeanFingerDownCanvas_get_OnWorld_m24327B58EB56CD9FD815E81B4CC210315E662C05,
	LeanFingerDownCanvas_ElementOverlapped_m2FD8AC4FE82EFB7F5901A19D5FBA75C2D787A530,
	LeanFingerDownCanvas_OnEnable_m71A65239AE993B4FDF080607B34459D96BD7AA60,
	LeanFingerDownCanvas_OnDisable_mEB59B21C0647FFEB1B82CEC9C1A962719DF3806D,
	LeanFingerDownCanvas_HandleFingerDown_mD90CA19B0FC9BCC85F39C3FB575EE5F3876A7338,
	LeanFingerDownCanvas__ctor_mCD3AFCE798DF7D43F690E86F21D3D4BD2CF11A68,
	LeanFingerEvent__ctor_m753ADD15A19B6C85455076C77E1F877D166EEBF3,
	Vector3Event__ctor_mC2AD74C37DBB9DE6353EB174A0CB7C67AC1C17AD,
	LeanFingerFlick_set_IgnoreStartedOverGui_m3B46DC63B350CC1246F2195CB433FA40A9C19B0E,
	LeanFingerFlick_get_IgnoreStartedOverGui_mEB7B21D6EA2A12A67B03F6FDB21680F2FCFCDF24,
	LeanFingerFlick_set_IgnoreIsOverGui_m7F481652C587E4056AEC6C24BE0FA6846A532F09,
	LeanFingerFlick_get_IgnoreIsOverGui_m904B9AF491C02582FA3EB00C08E30BA645E42DC8,
	LeanFingerFlick_set_RequiredSelectable_m6B07DB8CD364A62A814BC974E75029957A2369A3,
	LeanFingerFlick_get_RequiredSelectable_m4ADBF1B274059F42C5E1D516CF87E9995CBD5668,
	LeanFingerFlick_set_Check_mB6561448E549D9424010C31E19CDF571F0514454,
	LeanFingerFlick_get_Check_mDA7A19CCF28C6035F217334C37E76DECA0496D49,
	LeanFingerFlick_Awake_m351CEFBE0D0ABD2DD7FADFA08BED62EE831D8386,
	LeanFingerFlick_OnEnable_m42DC4BD9027E2F2B9788F7BCF51C705F56A5B0C1,
	LeanFingerFlick_OnDisable_m25A8DBCE28B1EE47830013FA678743512ED70C6D,
	LeanFingerFlick_Update_mF6818F9396E449DD6744CD74FD642BA26AC84FCC,
	LeanFingerFlick_HandleFingerDown_m06FF1D7E0F17ED5518A0322EB6A4FD2CC78CA670,
	LeanFingerFlick_HandleFingerUp_m16D2F4190EE9C0B5B6033A3391AD864A3897B745,
	LeanFingerFlick_TestFinger_m653BBC29E5850DB9D1077747620C2A47B5F073F5,
	LeanFingerFlick__ctor_mE0606001112CCD657573FE0262E57D7EFE30719B,
	FingerData__ctor_mEE7E8F41F34F9733325C39871FAD87B071084A00,
	LeanFingerHeld_set_IgnoreStartedOverGui_mE34A996A793CB90E60CA0D454B2522522AD23463,
	LeanFingerHeld_get_IgnoreStartedOverGui_mDFBACC0AAC5330FBFE08E4A5C95AB29B94A7BE79,
	LeanFingerHeld_set_IgnoreIsOverGui_m9BC3C7CCDCBE77CBDB3C99C1BE27FC52175A159F,
	LeanFingerHeld_get_IgnoreIsOverGui_mC65997E95B6F9A61A084AD5048AEB932551ABC23,
	LeanFingerHeld_set_RequiredSelectable_m3519E86B5D603E0936A66A611BCA27D2B42525DF,
	LeanFingerHeld_get_RequiredSelectable_mDC042DF34094F3645508EB7396F5FD270B69C953,
	LeanFingerHeld_set_MinimumAge_m46357CA5E4D621CC9B0D61D5B885233FDC5F42CF,
	LeanFingerHeld_get_MinimumAge_mFEC7ED3346A6F2A02E4E0A3A877E01F7A993CFC8,
	LeanFingerHeld_set_MaximumMovement_m6D1738D75D46C3888E6D525AF5196D162ADF88C1,
	LeanFingerHeld_get_MaximumMovement_mA77EBE402F1CE2C78B6FB4784F3B7348F7D521E8,
	LeanFingerHeld_get_OnFingerDown_m08642BFBDEBD79D670FA1AD48FD04959C4B3DECD,
	LeanFingerHeld_get_OnFingerUpdate_m8700FEBBEE2EB5E5DABDB02B4C1C14EEA7EE3221,
	LeanFingerHeld_get_OnFingerUp_mCB8F70B6FCF127E5377EA9D9E6B3B70023CAD459,
	LeanFingerHeld_get_OnWorldDown_mC114BFBCE0DA29CA833D38F86418F909681C9092,
	LeanFingerHeld_get_OnWorldUpdate_m8C6128622B21357D648C0559B64D79E65E528816,
	LeanFingerHeld_get_OnWorldUp_m0C365EEF611BAE619EC36482ACDEE10712A5CE1A,
	LeanFingerHeld_Awake_mCF2576FA394B2B0DA1EA340845DF47CADC80F75D,
	LeanFingerHeld_OnEnable_m3D2EC019567A8E8DF2265E97E4221A3DD0E5DC1D,
	LeanFingerHeld_OnDisable_mB73FAF935D54DB0877E993A8B1960D378AF04527,
	LeanFingerHeld_HandleFingerDown_mCE5EF045FCA687570A76E951B02D06814B0BADC7,
	LeanFingerHeld_HandleFingerUpdate_m074005AECD62AC9AD013D1F30C265B2A4DF73495,
	LeanFingerHeld_IsHeld_m32B437B0B7F33D7458430CF59FC54899AC091757,
	LeanFingerHeld_InvokeDown_m5B3575CF7B62E00F33E4AFC037A59CF4F60AF1F0,
	LeanFingerHeld_InvokeUpdate_mF9B59C7B743384BA70BCC45DD49BCDBAEECCD1BE,
	LeanFingerHeld_InvokeUp_m4B336FFC0D042D8D5C86FC6237D2588C7DB920D1,
	LeanFingerHeld__ctor_m9420E40BAA5C8DE7DCBFD87D503393E6B710FFA5,
	FingerData__ctor_m979E55D184CEDDFB9D37C1E069A43F9436A3F4E8,
	LeanFingerEvent__ctor_m1FB3EA30283FE042069D2843E97ABEA86B2DEDEA,
	Vector3Event__ctor_mD8C1E656CDF3DD7C715EE7B27136FA63348A5755,
	LeanFingerTapExpired_set_IgnoreStartedOverGui_mFB324216D3601F1DDC309A9EC17D35C0DF078502,
	LeanFingerTapExpired_get_IgnoreStartedOverGui_m23F4CB9134F04C77802B3C3E40162A5647805943,
	LeanFingerTapExpired_set_IgnoreIsOverGui_m6C7062215853AA09FFC9F275C9308590C703A7BB,
	LeanFingerTapExpired_get_IgnoreIsOverGui_mB3AD23E1F7FBF3DDA8C04934EEB3AB845940B108,
	LeanFingerTapExpired_set_RequiredSelectable_m24AC5CF708637DF3858E269841BFE9A6B79E431D,
	LeanFingerTapExpired_get_RequiredSelectable_m9AC4A56C987B929083A5F32E3301DD7D70B4A1D6,
	LeanFingerTapExpired_set_RequiredTapCount_m84BB71FCAF0CE32FB6408CCD2FF7F42D0BDF2698,
	LeanFingerTapExpired_get_RequiredTapCount_mF414E71B568E1DA81A87237EE488CD6DFC96030E,
	LeanFingerTapExpired_set_RequiredTapInterval_mE22CE57B3EE4FDD1AD7BEA3E71D0024CF811E49C,
	LeanFingerTapExpired_get_RequiredTapInterval_m52F84188E9C25DE6FECD9C8567C792BCAC2F7D49,
	LeanFingerTapExpired_get_OnFinger_m7E68A9F6C2806B9F84141DE28ECCCA741BCC57B2,
	LeanFingerTapExpired_get_OnCount_m24FF56E818C213BCAB4D78A8355941C83DC029A7,
	LeanFingerTapExpired_get_OnWorld_m335F14F6D0065BEE88251804E4F258DB047A1774,
	LeanFingerTapExpired_Awake_m08B89C10BDC85FDDB2B1EB636F725CF94AF78D50,
	LeanFingerTapExpired_OnEnable_m3FCFFF48CC014A9A40E766FE31F9DE52E26B7692,
	LeanFingerTapExpired_OnDisable_m6132018A80C67AFBA501E2A996FDB88D7C8C5507,
	LeanFingerTapExpired_HandleFingerTap_mAE2B1932950601CEDCF62541653228F97D78E1A5,
	LeanFingerTapExpired_HandleFingerExpired_mB42774F0FBD16E392D27CDAB422D074D1A4C1408,
	LeanFingerTapExpired__ctor_m0F653DF15C817A9A485702B1BCADD72F37015B94,
	LeanFingerEvent__ctor_m814037E4D65E793481CA5D5176F712BA36217148,
	Vector3Event__ctor_m228AD371356A0E7A10804F7FC1DFE5D43A8EA5FD,
	IntEvent__ctor_m5DDCF08ECEC10CFCB5258408DA7329A040B8D9E6,
	LeanFingerTapQuick_set_IgnoreStartedOverGui_mC07FB0C926095A63188EA27C36C2775A45BA1D7B,
	LeanFingerTapQuick_get_IgnoreStartedOverGui_m9E465B4F2B120DD6DF2420BB6C3D61F1E1C14FD3,
	LeanFingerTapQuick_set_RequiredSelectable_m7F8093FC981BE2017495B1B958B7EEC5F2BC635F,
	LeanFingerTapQuick_get_RequiredSelectable_m9CC09C74E08F553A37513BD8F738651AFDBECD43,
	LeanFingerTapQuick_set_RequiredTapCount_m31944E2F89D9CB9A25EA43D0EFCEE648935227FB,
	LeanFingerTapQuick_get_RequiredTapCount_m8A3A350425AD2F4679DB260427E2BFE3EDB4DE61,
	LeanFingerTapQuick_get_OnFinger_mCF3C2D6E97E0A8130E954D15FAE05C4806CC8DC5,
	LeanFingerTapQuick_get_OnWorld_mC25D5BCA6F619559789168BAD30934054EC3DE74,
	LeanFingerTapQuick_get_OnScreen_mEA636B51383AA6059E9340FF9B53EDF94521451B,
	LeanFingerTapQuick_Awake_m5F4D634C104BF2593481B1CC341876A3DC2EA760,
	LeanFingerTapQuick_OnEnable_m11441ADA8732B86B5ECFD65DB99A5D8A0A78081A,
	LeanFingerTapQuick_OnDisable_m856FE39C9B447BC32A21065902022C503118AA7A,
	LeanFingerTapQuick_HandleFingerDown_m14CBA593FD32D6CD51525A115E1350FD008E0A2E,
	LeanFingerTapQuick__ctor_mAC89C4230A7B22D084660BDD651284958BAD3533,
	LeanFingerEvent__ctor_m6D030CEFDEA2DCE692952F8820A5C2C702E3D88E,
	Vector3Event__ctor_m66122C895FC69BB14480E9516F3C4D9EA7F73D41,
	Vector2Event__ctor_m6F991A152F883D39DE3A6D7B5438FCFF21369BB7,
	LeanFirstDown_set_IgnoreStartedOverGui_m7C8EC54C2EFFDEDAD53695B9D45ACF24692EFC74,
	LeanFirstDown_get_IgnoreStartedOverGui_mED5CE02D53A7A4EECCB242E1494280D1F0030740,
	LeanFirstDown_set_RequiredSelectable_mD98A0C4DF7BAC53FFD0A67E5C25C8270CDA0FA5B,
	LeanFirstDown_get_RequiredSelectable_m6A9796844A68E970F31BE18BA69749B561855D77,
	LeanFirstDown_get_OnFinger_m051EAD15CC2AC0E6426F49306B054754E4B02CA0,
	LeanFirstDown_get_OnWorld_m24414674ADF62E3B9699EC69377D4235118D7A55,
	LeanFirstDown_get_OnScreen_m1323611C7E6A77AAF05991B9CC90319CC28770C8,
	LeanFirstDown_Awake_mFB919E441C8C717A68F0C1A8963FA8B255C148BB,
	LeanFirstDown_OnEnable_mE86D09FAFE641D0A6FBE5309FC56F0A5F449558F,
	LeanFirstDown_OnDisable_m19C7178B7AA870B63901F5B6093BC60584C0E92F,
	LeanFirstDown_HandleFingerDown_m6051CAE179EFBA89ED7504E71EB55CF367364AB9,
	LeanFirstDown_HandleFingerUp_m43F90A126F4657EC7D71421C0B3C1B15F2842A32,
	LeanFirstDown__ctor_mED57438C802CD29ADD69C982328C6DFB95175E66,
	LeanFingerEvent__ctor_mD30A004EAA0CD84F9199C5EE30F4B7ED0FFAB7B5,
	Vector3Event__ctor_mEA09FA2966DC0F1AACFC10353DC133FD04255969,
	Vector2Event__ctor_m5FA03C9147AAC7045CBBE1AEFD9849BEAFCBA18C,
	LeanFirstDownCanvas_ElementOverlapped_m0435BA2CE8F3B2363A8877D7801AB8EAE950B25C,
	LeanFirstDownCanvas_HandleFingerDown_m1FD9ECE0F953C0FE2DAB492D4A5E3C7C3583B51F,
	LeanFirstDownCanvas__ctor_mB780E502B8012EA1E0E2E08969A9A8232D0A982A,
	LeanGestureToggle_set_EnableWithoutIsolation_m96A1A42CE752981F01AD452405BBAA2C6F2AB1C3,
	LeanGestureToggle_get_EnableWithoutIsolation_m33BDDE803A49DF0A1B62AFC6DB27D186F4FD0ED2,
	LeanGestureToggle_set_DragComponent_m6F0C5370EE07F1EB68A9FFD33EC9055A9C1FEDB4,
	LeanGestureToggle_get_DragComponent_m1B3FE04A84CA57BB06C9C7021018DF8438035329,
	LeanGestureToggle_set_DragThreshold_m34C94194BB51CA10B0C89F963ACBA2227BDBFE46,
	LeanGestureToggle_get_DragThreshold_m023F1B800BC0C034E24FB2163F2C12A1F8EA2FDB,
	LeanGestureToggle_set_PinchComponent_m58E341B38074E9ED707999E3801C3F1FE372CC3C,
	LeanGestureToggle_get_PinchComponent_m062917DF0E30259981338718E1DA3135ED2C1C29,
	LeanGestureToggle_set_PinchThreshold_mFBD7C9E662FB3A4F27DF7F285A0683C0BF624CB2,
	LeanGestureToggle_get_PinchThreshold_m518D3E23F590FACEA24843F131475D28F41B65B1,
	LeanGestureToggle_set_TwistComponent_m244558A7F76814734A5D4CBD8F40380966CF11E8,
	LeanGestureToggle_get_TwistComponent_m57B38E78F63A640DB9435552BDF8F2AD1A551FD6,
	LeanGestureToggle_set_TwistThreshold_mBF80AA39C3D6596F2F6A45F6A8CD675109250733,
	LeanGestureToggle_get_TwistThreshold_m1E7C67B0FBC59C6E2AACD852E98568210DF94640,
	LeanGestureToggle_set_TwistWithPinch_m13FB40EECFDE6F57A8D0529CFE3B8607D5CD39E5,
	LeanGestureToggle_get_TwistWithPinch_m0C25B76B52A3A83212E42B4E69C7C35302889A50,
	LeanGestureToggle_AddFinger_m830F5DD2EC12E79B36A0C8CED1BEA053D82031D4,
	LeanGestureToggle_RemoveFinger_m42E89FF05EDF674024151F190A15BF3C2F3FFCC8,
	LeanGestureToggle_RemoveAllFingers_mC36B8421C9BEAA99756549E713D3EBF6C0FE80FF,
	LeanGestureToggle_Awake_m1D18D54E5CFF8E3A244E0BA653E6D71F4BA40E2D,
	LeanGestureToggle_Update_mC508D596CC7DAFE29369C9E8EE063043114EBC26,
	LeanGestureToggle__ctor_m0B8B98120FFB3593B1B5B7FE3492B5B5D39744D5,
	LeanLastUp_set_IgnoreStartedOverGui_m4835FE1AD2474F8DD177A65333F89DF296463D63,
	LeanLastUp_get_IgnoreStartedOverGui_m0679DA95D2369CD8ACD165431036CE17815A6E17,
	LeanLastUp_set_RequiredSelectable_m1362EC581A086D5B3B8C435B19B338C9E2463EB5,
	LeanLastUp_get_RequiredSelectable_mBF444DB2242D1216FCC6A71C8F4CCECB1A80ECB9,
	LeanLastUp_get_OnFinger_m5B89DAFC1652AE3FB0069475E7B2827F88F60250,
	LeanLastUp_get_OnWorld_m1834851441EBD7E3C2A6861E5BB48DE4D2EA111B,
	LeanLastUp_get_OnScreen_mB4F20403B6F89A51EBF7388FA22B51C685F1AE29,
	LeanLastUp_Awake_mEDD249DA964FBB611D9136026975041E5FCD8825,
	LeanLastUp_OnEnable_m66E969F41F15BD4B60F984D4501A107624DD5F93,
	LeanLastUp_OnDisable_mB0508F8CBF91B694B2E4AFC98B90B3D2D6C8F06C,
	LeanLastUp_HandleFingerDown_m48FAD54E463CD0DED1BCFD74719EA8ADEA969BF8,
	LeanLastUp_HandleFingerUp_m5D5E75D9872F951BED018919AF9C26E97794ADE9,
	LeanLastUp__ctor_m87497828079F3D3EEEC08C0BE281F93B4424D9C9,
	LeanFingerEvent__ctor_m787E15476A7706A45021B2A01FF64997FBD6F1F2,
	Vector3Event__ctor_m90633122E5BEAFDDC897F1F574A2DC9DFA50970F,
	Vector2Event__ctor_mE437ACD2C37A47415016CB9E683E6F0B8076CDE0,
	LeanLastUpCanvas_ElementOverlapped_mB4286D8D275FAD6EBD3364A1C918517FD6332F8E,
	LeanLastUpCanvas_HandleFingerDown_m95F5ADD02D7CA42F61C9937A7BEDD96B614E8ED3,
	LeanLastUpCanvas__ctor_m7C634C63EEFD8D75B11A62814A0383697CA3B4DE,
	LeanManualFlick_set_IgnoreIsOverGui_m8FA2B5123D80791CF33D3C2C57C8C83EE439F379,
	LeanManualFlick_get_IgnoreIsOverGui_mB44881312C841880A829D74282D8F37B23B192BD,
	LeanManualFlick_set_RequiredSelectable_m8EBBF1E87EC06104273880F994B43DD7568E0AE1,
	LeanManualFlick_get_RequiredSelectable_m29C6F055BA08318A6402C1ED05389ECA9FC6645C,
	LeanManualFlick_set_Check_m0C94181A73379AD29E55B8B6EDAB9C879E75B737,
	LeanManualFlick_get_Check_mA8F9D077E89C7D3D4BBC0670497705EBE42D6AAB,
	LeanManualFlick_AddFinger_m3CA41A7EC420DB84DFA4E0A217AC535E3C36B79D,
	LeanManualFlick_RemoveFinger_m3996B361236FC36277078851A63C835A3CAF9506,
	LeanManualFlick_Start_m0E69E016B72FD897A2AA66CDC116CF188784860E,
	LeanManualFlick_OnEnable_m5279687BF636A690A812B79DB52979C24E8A1F5A,
	LeanManualFlick_OnDisable_mFA452322421DE5C0598B054609A284021D2F7DA2,
	LeanManualFlick_Update_m939F716064EBF40361E3835B14498E9EB9E5A48A,
	LeanManualFlick_HandleFingerUp_m6AF6863F4EBBCFDBE454A1D68F4B7AC70DA6D1DB,
	LeanManualFlick_TestFinger_mA5F3E39030EACACF7CA879DA1135D6B09FC900C6,
	LeanManualFlick__ctor_m9B141318A6A6071B09CE1090D16969D965E22C26,
	FingerData__ctor_mD311A6891E83D6137A088835C47A50BE643C3CED,
	LeanManualSwipe_set_IgnoreIsOverGui_mF432DD384DC0760CA6F1812F009533FDC13F4670,
	LeanManualSwipe_get_IgnoreIsOverGui_mE8F2D2A41BEA9207EC9A8C8000C8E8E69928EFC5,
	LeanManualSwipe_set_RequiredSelectable_m5E7071585BF9F1017E53076E0CA5E97865785EF6,
	LeanManualSwipe_get_RequiredSelectable_mF815F8104289D6458D203ACC23139A6D83826E6F,
	LeanManualSwipe_AddFinger_m9AB1712843D8220D610E44F84A864676B5E5E5DA,
	LeanManualSwipe_RemoveFinger_m0204875930D2808E64CE073671A8C36B5CEECC95,
	LeanManualSwipe_Start_mE9FA39FA8EF54D19CF64F20B8790CC4A8C7B8F20,
	LeanManualSwipe_OnEnable_mC2A45DA6A7E6D7F8ED791186171F013B5C26197A,
	LeanManualSwipe_OnDisable_m8C2F01870A09A59E414C1921EC07F1C77140449F,
	LeanManualSwipe_HandleFingerSwipe_mCE292CCB3DB42CC7F6DB9AC62970654DEC69913E,
	LeanManualSwipe__ctor_mDA40C9C2210B768E3639394D209F98441B11FC3B,
	LeanMouseWheel_set_RequiredSelectable_mAE09646F2E803C530695897889D652F35B910432,
	LeanMouseWheel_get_RequiredSelectable_mBB68AFEC7FB18F7122AC61117E924C840D528D95,
	LeanMouseWheel_set_RequiredMouseButton_m7B4B4F0040F474CD2F495277A5122FA5D412C40C,
	LeanMouseWheel_get_RequiredMouseButton_m04040A1387F64C078454EDAE34088978870306EA,
	LeanMouseWheel_set_Modify_m525B60117EFD28F2F5CDC3F7D907B710351EF2D4,
	LeanMouseWheel_get_Modify_m2F74E38130BCB22AF4238EDCED6821E9B3FF90A5,
	LeanMouseWheel_set_Multiplier_mA2B26982F2A5DEB91134205F78F8CA95CD4FA6D2,
	LeanMouseWheel_get_Multiplier_m61B4AD68B9D70CD75B1DCA68B1057AC62222D9B7,
	LeanMouseWheel_set_Coordinate_m02AAB4D9D2933DDBE0105641635829644F9A74EB,
	LeanMouseWheel_get_Coordinate_m9E78BDD05E4A982EE21D049FD46DA084531006BB,
	LeanMouseWheel_get_OnDelta_m6FB6D3D3107A0E8BC081F6D976BE6348C114DEAC,
	LeanMouseWheel_Awake_mE7CC0EAD14E1F783A4C125B3DC51436252CFC3F7,
	LeanMouseWheel_Update_m7AEC66D31603FA29C8004585B7D1BB240B134EFA,
	LeanMouseWheel__ctor_m2B7C94435EC9C29045A8CEDB19CB5916163690E7,
	FloatEvent__ctor_m4045C9F75FE81B33436A051E04419DEB1BA0FC33,
	LeanMultiDirection_set_IgnoreIfStatic_m59F66088AE16777D8A980A9B273B73E7A590DC1D,
	LeanMultiDirection_get_IgnoreIfStatic_m8BDF807282F0F6A7CE1B3711DD57041107EAE5C0,
	LeanMultiDirection_set_Angle_mB1B0E0C1C32293F9ED9B40E205E961E0ADA9DA66,
	LeanMultiDirection_get_Angle_m421C718CE152DB516B37B2C2996FE768732783DB,
	LeanMultiDirection_set_OneWay_m42889C65CB17DFF7C5DBC6C2F11CF4343F655620,
	LeanMultiDirection_get_OneWay_m061167E0AE4E3A49DB9A470862B1AE13F8DA76DC,
	LeanMultiDirection_set_Coordinate_m55BAA7434A371DBCE72E750B7909C685FC63B798,
	LeanMultiDirection_get_Coordinate_m203D60604F845796196BD607E73D5E95EB215151,
	LeanMultiDirection_set_Multiplier_m7574B35F1BDF8B8E6B85C0EA587DC36C23626BBF,
	LeanMultiDirection_get_Multiplier_m83CDB138108A8A86F8D554071BE61446E2089699,
	LeanMultiDirection_get_OnDelta_m00E27763A19E6C5479B4260BB1F0FD570E133455,
	LeanMultiDirection_AddFinger_mD2EC33C1379E7CD106B3D5E63BB85D8FDF4BBA4B,
	LeanMultiDirection_RemoveFinger_mD49C8188E41C1F5E499573E4EEEF78511F76F7E0,
	LeanMultiDirection_RemoveAllFingers_m1DA8D6FB78AC0BE8973030009351C0DD7E1D73AA,
	LeanMultiDirection_Awake_m915F3F474CB2792EC14FEE125D1AF1C2EF5D7028,
	LeanMultiDirection_Update_m29DE695087FEC1DA3A27F75FF5080E45747B4B4B,
	LeanMultiDirection__ctor_m4301C82CEA2635B1CC54B42F1902DE4E573758AA,
	FloatEvent__ctor_m9B3015223FB628FA6DF8DECDAC5919598322174F,
	LeanMultiDown_set_IgnoreStartedOverGui_m8824187750178D2E7DA23EF11EDE49A75DD27EE6,
	LeanMultiDown_get_IgnoreStartedOverGui_m2EA8BBCAE7E3414FC32218CA45DF6FA3A27824C8,
	LeanMultiDown_set_RequiredSelectable_mCDA39E15058FB71A4A079362096E196D11C6CDDC,
	LeanMultiDown_get_RequiredSelectable_m2B96DF2D525069831FC1CF81145C03AF95662B53,
	LeanMultiDown_set_RequiredCount_m73695852E2F14FE7E83031E6435186FF763A1791,
	LeanMultiDown_get_RequiredCount_m59636B58D498CEDCD88DB28713D010381C63AFA5,
	LeanMultiDown_get_OnFingers_m75BB92BB267C16D9E3684E8C3EB34F6A95053BCF,
	LeanMultiDown_get_OnWorld_m9C260D61BAB42358BA925E793334E4C487796CE2,
	LeanMultiDown_get_OnScreen_m31EA600570A38E529FDDF8F9AE37FCBDE6CA6CCC,
	LeanMultiDown_Awake_m22EA823902235773F9F0C0AA818A179B2B78A41C,
	LeanMultiDown_OnEnable_mCD263348B633C7A9ED38F923FAD137C9E09BD91F,
	LeanMultiDown_OnDisable_m98DC029884DB2A0DE3D159B46CDF10D7B943486C,
	LeanMultiDown_HandleFingerDown_m785DF29737757D63B0A821611959166984A24695,
	LeanMultiDown_HandleFingerUp_m3530C86D55BF3FB28C48D77B8DB730320A9A8D33,
	LeanMultiDown__ctor_m103E0479CCCAB35E81DBBF171B5FCE0ED159D9DB,
	LeanFingerListEvent__ctor_m8204585BF37E50623E14FA4A39E55DFB457ECF98,
	Vector3Event__ctor_m97468BA6E523A6D1536FC38520D78F8C3F30EB07,
	Vector2Event__ctor_m177A0E2F3260A2AE9E9B6208F4634680DB518CEC,
	LeanMultiHeld_set_IgnoreStartedOverGui_m74607D368ED9AA5A3382F52B12AACC82983B82A4,
	LeanMultiHeld_get_IgnoreStartedOverGui_m71B27D3C8D645D4F1AD74E5DF509582CAE3D4889,
	LeanMultiHeld_set_IgnoreIsOverGui_m7E030EC9EB4F7724CC25E96A506E1496384C0FE8,
	LeanMultiHeld_get_IgnoreIsOverGui_m2631C18BC5A5B99DB9838F3D72BA7C89E805F04A,
	LeanMultiHeld_set_RequiredSelectable_m026F5E53228389CA7F4037C751B1036314B6DB32,
	LeanMultiHeld_get_RequiredSelectable_m891F15086C0B0E5F5E43F1C2C771D10C0B7AE043,
	LeanMultiHeld_set_RequiredCount_m18FD5B0E2C1187D96275CB21FB1794D3473F95A2,
	LeanMultiHeld_get_RequiredCount_mDE1182A00978C2DF399B79E81D8887F5A5DCA7C4,
	LeanMultiHeld_set_MinimumAge_mFCAE881D3CFFC8BB34DA2971900312A488BF95DF,
	LeanMultiHeld_get_MinimumAge_mEA9CB90959925A29AB48E362EA9EFF0F66962D93,
	LeanMultiHeld_set_MaximumMovement_mCEA09B6D267ED2378235DBE30EBF8FD827BC159D,
	LeanMultiHeld_get_MaximumMovement_mCD27F6CD4EBF031DFCCCDB27B3F472B338ECCF6C,
	LeanMultiHeld_get_OnFingersDown_mBB758E0610750FEE5CF7D3F6A1ED69A43CFE021C,
	LeanMultiHeld_get_OnFingersUpdate_m2AAEB2874CB61CFE6A7436B1F5595FE637720585,
	LeanMultiHeld_get_OnFingersUp_mF8B35640DE32FF8AB4CD57C5E9BD12B278B837E3,
	LeanMultiHeld_get_OnWorldDown_mF4332D257CB4FC2723DC529F84EAB99EF4F444F6,
	LeanMultiHeld_get_OnWorldUpdate_mE1BB4471F34DA52F509E3FC258E922E04D472E4B,
	LeanMultiHeld_get_OnWorldUp_mC61FF5555F8A7438673952D6C045CD8E0452F9EB,
	LeanMultiHeld_get_OnScreenDown_mE48E160CD161E689A48F2EA4462908CEA402CB95,
	LeanMultiHeld_get_OnScreenUpdate_m0D8DA94C135954800ECAE2199B14593250C96451,
	LeanMultiHeld_get_OnScreenUp_m810BFFDB8040D255E1F11891411176E8FD459756,
	LeanMultiHeld_Awake_mD74A7974661C78D3B32BE0CFC737237C24A6D4B6,
	LeanMultiHeld_OnEnable_m3D590BFEE575B3D47E80727D317D959738DF7840,
	LeanMultiHeld_OnDisable_m0F14878D8762CC1493005597B97F6920F8C56049,
	LeanMultiHeld_HandleFingerDown_m5D4A0B729B4EE24F2931913937C0F5405603519D,
	LeanMultiHeld_HandleGesture_m19023B8707709192A26919D3CCD902D4453CC9FA,
	LeanMultiHeld_get_IsHeld_mFE0E1928CD2CD72F69A42FBDE3B2CA0BB3FB1545,
	LeanMultiHeld_InvokeDown_mDD2C43175C2B8C5B14E22FFA2D5264FB76646ED7,
	LeanMultiHeld_InvokeUpdate_m04D0B6FB3422C6A75D1E0869A7569A51C18200DE,
	LeanMultiHeld_InvokeUp_m0D8373A4280F8141D1FF0F1B823EDE97A1EE02C6,
	LeanMultiHeld__ctor_m7DCD0E18904B460CFC4E927E2DE295DBA1A20717,
	FingerData__ctor_mA9DD5E40BE6AAF8BFF0BDD76CDAB91C5E8AC0985,
	LeanFingerListEvent__ctor_m94FBECE94E0BB95C1E226E908CDCE0AEF5A541A1,
	Vector3Event__ctor_mAEE74001980C33AD99AA240E073A0F12F2B5CCA1,
	Vector2Event__ctor_mBC7EA484E139C93839A4C1655F0CE7BAB2389BB3,
	LeanMultiPinch_set_IgnoreIfStatic_m19ACA6415DC8DA3F71400E0612AE2C6906DAACFF,
	LeanMultiPinch_get_IgnoreIfStatic_m4117EA83A506626FBDC560C0E620C0430FA1F519,
	LeanMultiPinch_set_Coordinate_m6B77117835B5452DC27AE307CFB1EEC4727952EC,
	LeanMultiPinch_get_Coordinate_m8A68F7FA1A1DDC10309429AE0DFB5352A1684293,
	LeanMultiPinch_set_Multiplier_m7EC11D3D000DF92360805FF1C9787A2D753BCE96,
	LeanMultiPinch_get_Multiplier_m7363109E14F67B734457F5B2F4DE2F569BFC31CB,
	LeanMultiPinch_get_OnPinch_m097281DBFD5EA8687B3CE2C0A600B901F80B8B73,
	LeanMultiPinch_AddFinger_m82847C33174311E48D084D8CE145891B96F45C85,
	LeanMultiPinch_RemoveFinger_m47EEF9D6C541025E3203B169873DC685981BBDAC,
	LeanMultiPinch_RemoveAllFingers_m177C5C7997CB03DCD8BE5245E9C143B4D4D60C58,
	LeanMultiPinch_Awake_m04005867994A365BE7F4F9A873D875A3D9660664,
	LeanMultiPinch_Update_mC238C5C776D3B4CF1B6DE5BFABEE1ED8AA6947CA,
	LeanMultiPinch__ctor_m07EFB01364F69ABD52AF2FC4D84F8A83A1BD7834,
	FloatEvent__ctor_m910CE5D3BE2A3DCEFA6CE1D703284732E615E656,
	LeanMultiPull_set_Coordinate_m6F3B7FFDF9BF1DA5D0C272F1417CC8DEBE569677,
	LeanMultiPull_get_Coordinate_m7426C239A87B0862135417C3AEA6A8B1120C5B7C,
	LeanMultiPull_set_Multiplier_mA2FFA9EB98BA39F30E772CD1C2E8E1B5FA2AC5C2,
	LeanMultiPull_get_Multiplier_m446D29E8E16D102B423C47F52543294F7AEF9FB3,
	LeanMultiPull_set_ScaleByTime_m31C14AB6EBABB8BF2922E54CFF1B728ABB6D3AE9,
	LeanMultiPull_get_ScaleByTime_mAD22E7A54852A51CBDBDCDA08D32423D7EB8C975,
	LeanMultiPull_get_OnVector_m7AED6D30E4B11F3E5585F541AFA76E1764BEB329,
	LeanMultiPull_get_OnDistance_m983EE6BB3CD2F025E0869DA7F540D48DAE853B50,
	LeanMultiPull_get_OnWorldFrom_m4D1F21BC1B0BDE96B412323B977852F02B81833D,
	LeanMultiPull_get_OnWorldTo_m1AD2508BD14D0F12CA944E5D3832D4987BD3FAB4,
	LeanMultiPull_get_OnWorldDelta_mC04F22EDBB977AFED747DE4C88A0779696892CD2,
	LeanMultiPull_get_OnWorldFromTo_mF0CB88EC446B0F9E9709F648A73B72B854147AA1,
	LeanMultiPull_AddFinger_mDB8B17400C14A9C321AEFC615BCB9D23125B1CEF,
	LeanMultiPull_RemoveFinger_m1BE6D3744733E343D8C46068554B1C34D07C3ED3,
	LeanMultiPull_RemoveAllFingers_m985DB24046F6053BBF68950AF2267C00BCCE9146,
	LeanMultiPull_Awake_m9ACEBBC454E3AF7F8314D0D7961C8AE2D65EC021,
	LeanMultiPull_Update_m33E10DF675FA43EB94EA28047E78BEEB8D33A4B5,
	LeanMultiPull__ctor_mB074B7810C30600D277073CBA7000A936B553B1D,
	FloatEvent__ctor_mF15EDE985E0976EDB7D781B74792093901C0C68D,
	Vector2Event__ctor_mBCDC9F6B503D0E4B630539787D357D423FD4A87A,
	Vector3Event__ctor_m59E022854A40CE20ACF20D90A21A9BE4FD19DAC8,
	Vector3Vector3Event__ctor_m411E7090968D0F493BD715BBEE3AE1F1D0D6B1BD,
	LeanMultiSwipe_set_ScaledDistanceThreshold_m61BC006B77FFFA4010AE869A270BD2F6842A647D,
	LeanMultiSwipe_get_ScaledDistanceThreshold_mF9D74A6431C6AEA27E5940DDC77EA688E0F42F0F,
	LeanMultiSwipe_set_ParallelAngleThreshold_m6C1F1B70840D054A7F030CBCD15BCAB03963463E,
	LeanMultiSwipe_get_ParallelAngleThreshold_m205A378E2117A2476F3D3C458D5B25E6E0B22D70,
	LeanMultiSwipe_set_PinchScaledDistanceThreshold_m6EAB1F05001B73339C78B6FE46938CF242667FB3,
	LeanMultiSwipe_get_PinchScaledDistanceThreshold_mC366D8FCB4402B3C4C7FCFB76FEC1EECEC6E609F,
	LeanMultiSwipe_get_OnFingers_m98EAA402E18319C464601E7671DDCBDC9ADE30FB,
	LeanMultiSwipe_get_OnSwipeParallel_m30326C970914744D3CA3EB5820CCA83CE95F6C46,
	LeanMultiSwipe_get_OnSwipeIn_m299E6A298754F7FA126829C866C2A142C7561BCB,
	LeanMultiSwipe_get_OnSwipeOut_mDC1B37FEE98827C117BC0F7A1D03FE0A59D5AF80,
	LeanMultiSwipe_AddFinger_m5FA130A4012E6B7542EB6782C1E70DB968EE6873,
	LeanMultiSwipe_RemoveFinger_mE82E74E095BEA73B1818DE197E0705908D57EA19,
	LeanMultiSwipe_RemoveAllFingers_m5F181D47CDA6CF36D8E0194B4EE38A9CEBF79EEB,
	LeanMultiSwipe_Awake_mBADCB16DC7CC114BF5718EE486511CAB33343B25,
	LeanMultiSwipe_Update_m6678BD6A9088FD3C2A6367BF126578E56054B498,
	LeanMultiSwipe_FingerSwipe_m72D385EDEEEAE29FA82FC90570AE42E1B28652F2,
	LeanMultiSwipe__ctor_m9E4B99EC67A5BC35AAC109DF6CF6BC3EBCB1DD07,
	FingerListEvent__ctor_m557EF0EE2E4A43FFD59E9EB9E2D43C6762751643,
	Vector2Event__ctor_m61CDFD9F8992A503193ACC868E0A55C207711BFF,
	FloatEvent__ctor_m398D1FEF3DF599A239130BFDEBD8611EAAE056B4,
	LeanMultiTap_get_OnTap_mB3A816449C8E6DA0371F736CA77E537A793A41E8,
	LeanMultiTap_get_OnCount_m7FD8A7F5799F79449B47F6E2236BA04F4CF4C19B,
	LeanMultiTap_get_OnHighest_m1125CBFDDBB88ED8D3614A674C19B7107B8087EF,
	LeanMultiTap_get_OnCountHighest_m3CFFC1F131FD0B77FEFA8FEB6F4F2C3100B536B8,
	LeanMultiTap_AddFinger_mEDD9100A13C9FF0CD1A50AE8211DF8C170EF0015,
	LeanMultiTap_RemoveFinger_m7666D571A74C9931FD198A529986C34682D17D46,
	LeanMultiTap_RemoveAllFingers_mD5C5015785D6C6A007EF9536B5F0CA0429DE1A48,
	LeanMultiTap_Awake_mCB72166953A9B727EED85C28085818140C9EADBD,
	LeanMultiTap_Update_m23FB55F502B1EB17EB7D392240030138E595612D,
	LeanMultiTap_GetFingerCount_m7EBE1E5895ACC80860D208F859094681D777EB59,
	LeanMultiTap__ctor_mCCAFAA3A4FDC08D4BC906281E39F893757BD60CD,
	IntEvent__ctor_m5F9F1EE32595F5E2929C17087688143669160515,
	IntIntEvent__ctor_mE2AB6A560FAF5F5BF13F7DE7290A0ED41C812729,
	LeanMultiTwist_set_IgnoreIfStatic_m3D554AF99E789FEF09BD645ED574F90DD292B2C5,
	LeanMultiTwist_get_IgnoreIfStatic_m2DDBBA3E98ABFB947FA52544CCD67C8A8582EE20,
	LeanMultiTwist_set_OneFinger_mF3DCC06F3E5E518EDC2B92370624B95E381D8591,
	LeanMultiTwist_get_OneFinger_mE0A8C4BD9083A1F7BC610FA4D6ADA358215442F1,
	LeanMultiTwist_get_OnTwistDegrees_m862255AB28EFB4C6F8DD31636F8ECAB4B383EE61,
	LeanMultiTwist_AddFinger_mC76E5AF6A55DD8A48D7B444E0F3F1E4ECC9EA9E5,
	LeanMultiTwist_RemoveFinger_m32A6CD6FB5EB3F8448FBF808ED022A6194ED75C6,
	LeanMultiTwist_RemoveAllFingers_mBC5DFF913600381C6F907FC686534587ABE6EA12,
	LeanMultiTwist_Awake_m908736A1BF6EEF30F0B643FF14AFAD67F9ED7AEC,
	LeanMultiTwist_Update_mAF20709CED57EA19321B0ED86BFC1ADB9C9333C6,
	LeanMultiTwist__ctor_mB91E2DBE6247515567D81AEA7C6749DAF2305D64,
	FloatEvent__ctor_m9BF1D544D19B7F60F6E74C18B806C3CE953CF6AA,
	LeanMultiUp_get_OnFinger_m32F9094ABAD0F7AD6D15499CFA99A1BB80081915,
	LeanMultiUp_get_OnWorld_m48A6B51981ADBA7740EAC3517047742DD9E88628,
	LeanMultiUp_get_OnScreen_m31963CC274725AD3D95CEB101A588D0331899E2F,
	LeanMultiUp_AddFinger_mD60110FD923F033B80635AA8FBB66BCB466746E9,
	LeanMultiUp_RemoveFinger_m799F33F2E9E7123D377A065FED3B47ADEADA6B46,
	LeanMultiUp_RemoveAllFingers_m668BF5A33E24566EC9A24628B83B5143CB04378E,
	LeanMultiUp_Awake_m26E66DDBF2958D9895A38DCC3AC5F0AFD9862FC2,
	LeanMultiUp_Update_m8E30968004A357C03D3DC89FA5DAC0771A79428A,
	LeanMultiUp__ctor_m817B3CE7602B16D18D5A46FAD04BC3DBB982C523,
	LeanFingerEvent__ctor_mA74984CF8AC970A9B777BC34547A463D734FD7CC,
	Vector3Event__ctor_m40E805E5E6B2F7CABFE63CF18942D60D59795DEB,
	Vector2Event__ctor_m358ED9AC5DC1A59BED43B0874A47E01FEBB68494,
	LeanMultiUpdate_set_IgnoreIfStatic_m0A37AF08D12FD42E899B705E37D62A6AB6E4B570,
	LeanMultiUpdate_get_IgnoreIfStatic_m9D6C56B622F00A2B1D7011154490D492B5A97CBC,
	LeanMultiUpdate_get_OnFingers_mA4BAAEBDB3CD83A42BDB9205FF6603C54A430267,
	LeanMultiUpdate_set_Coordinate_mB83ECE5C25C571517B164D10F3EB11DAC7F4DA1B,
	LeanMultiUpdate_get_Coordinate_mC349D0B825C786B8E17858AEFA645DA2DA959704,
	LeanMultiUpdate_set_Multiplier_m62503A1E386259AC155C2A576478EF6B5DFBE65E,
	LeanMultiUpdate_get_Multiplier_mFD72881AE01F706E844E3D5C77DAA941176746F1,
	LeanMultiUpdate_get_OnDelta_m48FCA1A889B55B7DC390675E92EE4B7078352429,
	LeanMultiUpdate_get_OnDistance_mC35A7F6F31BD8E9982CC745EC29669F45DC78ACF,
	LeanMultiUpdate_get_OnWorldFrom_m1B09F779876A4440F77CFB23AB1177197A0370E1,
	LeanMultiUpdate_get_OnWorldTo_m4D8A2B28AAB42A963E13D420A6B86199B56ADA51,
	LeanMultiUpdate_get_OnWorldDelta_mC66FB68B3F27F50C61709172AAE2F13E1BED1996,
	LeanMultiUpdate_get_OnWorldFromTo_mB30C30C40079C84FDA46E1EFF7CB51D0A5D2BA3E,
	LeanMultiUpdate_AddFinger_m93484E2DEB8DAD508C56200FEF383C503FA6D56D,
	LeanMultiUpdate_RemoveFinger_m2889B50A84E4EB46E78831017B4E6177F6240AD8,
	LeanMultiUpdate_RemoveAllFingers_mCCA1CBC56594044660957D4C58D51FAA7B4DD636,
	LeanMultiUpdate_Awake_m93B50D5751A65587C375A28712C4198E33890B9B,
	LeanMultiUpdate_Update_m2390B25EE46BF85B013EB909A33DAEEAC8966D31,
	LeanMultiUpdate__ctor_m4AEFE368F6FDB31E67E9C059CBF15EA5ECEE6598,
	LeanFingerListEvent__ctor_mC23F299CA296A2DD3031D2F76C6BD4A62AA4A55E,
	FloatEvent__ctor_m249A50B149BB51315E91B12065DEB77246EC037B,
	Vector2Event__ctor_mFB25A50F057AB4F0FDB4EC5DF853E38A47B542F8,
	Vector3Event__ctor_mFA76270CFBD1A2DFB78DD89ED0F34FA8DE833951,
	Vector3Vector3Event__ctor_m03B321D0D6C1099D51B6FD8A33EF905DC54396DC,
	LeanMultiUpdateCanvas_set_IgnoreIfOff_mC7E37811BC51F8A8E8E3070D5865A9CECF74A00A,
	LeanMultiUpdateCanvas_get_IgnoreIfOff_m263F8EAA205553CB0A22AF9798E2F555E672E933,
	LeanMultiUpdateCanvas_get_OnFingers_mD05C25D09381A39E86107419A2C6259EEFC82ED2,
	LeanMultiUpdateCanvas_get_OnWorld_m37DE2074F3C4AE5FA44F504592767595AA52CDB2,
	LeanMultiUpdateCanvas_AddFinger_m15DF653C5A8C498E45E665D16EB5FB1CAC64BE99,
	LeanMultiUpdateCanvas_RemoveFinger_m890BF391F97516D2605E649D001F95356E62C12C,
	LeanMultiUpdateCanvas_RemoveAllFingers_m1CBAAECE834309C5EDC1E9DBC9F1512DC61C1715,
	LeanMultiUpdateCanvas_ElementOverlapped_m51754DE60EF8ABC031056AA0B0BDE9132E706CB6,
	LeanMultiUpdateCanvas_Awake_m2489CF7734AF93203D53535011B0C9BB1D48CA7E,
	LeanMultiUpdateCanvas_OnEnable_m8373073CF7D6C76DA3CBF5355EB8A3ECBC56F1EA,
	LeanMultiUpdateCanvas_OnDisable_mF0D7244003C055024E1A4C09AB8501C2A6CB33C4,
	LeanMultiUpdateCanvas_Update_m797A25E41CADD789DB6E4963E20702CD769F22BD,
	LeanMultiUpdateCanvas_HandleFingerDown_m77133BB83FD5E9AC313465130ECE0BC6A6DFF1F3,
	LeanMultiUpdateCanvas_HandleFingerUp_mD851C876AFF72681DF37704AEE3EA7D91AB45AD2,
	LeanMultiUpdateCanvas__ctor_mFB2516C8E62CD4E1AF376885FAE26080D7C727C5,
	LeanFingerListEvent__ctor_m07768BB1E8A58FE1284BFFFE5C2EAFA8D1E6ED77,
	Vector3Event__ctor_mAD5A428D75F7A17D8C2D5856835D75580CEF2B39,
	LeanPick_set_RequiredTag_m4712C69D410577710EDADBC44A28BB11196402C9,
	LeanPick_get_RequiredTag_m2F2EFF858D9A6814556176B8478FEC369DF26036,
	LeanPick_get_OnPickable_m2542F31E7F744C17A126447A9E64DE4E00238BB9,
	LeanPick_PickStartScreenPosition_m557C600DC7D404335449F3479B120CEAA5549C5A,
	LeanPick_PickScreenPosition_mED8B7C7DBF6F784F2B7AD014801F08AEA286C21C,
	LeanPick_SelectScreenPosition_mAFCA2DF630D4BAE9119D0A8D3954FC4637AFD79F,
	LeanPick__ctor_m3CBC103D88CDC5B36433C348CA23B68654B0471F,
	LeanPickableEvent__ctor_m0B53AEC2E4426AE30F4D3B78ABBBDAB4FEFF2976,
	LeanPickable_get_OnFinger_mA16865CA338CD1546AEEBA3DA97A1ABAA209A321,
	LeanPickable_get_OnWorld_m8692A0FBA3D35A523BA8236D091449356CB082E2,
	LeanPickable_get_OnScreen_m79FA67FBC0931CAFF2703A1607C755C8DFF20848,
	LeanPickable_InvokePick_m67841B00B5A0AC722ADFE2F597A0F6C8ECA1E6B9,
	LeanPickable__ctor_mC89D16BA6906C985E8CB7A3955D625ED9F534085,
	LeanFingerEvent__ctor_m5C900BFC8AC2FAE876F7887922CE0FAA17529EFC,
	Vector3Event__ctor_m0227F10D52B05CADFB936D78E4E597097B7D0585,
	Vector2Event__ctor_mC83B1971D763150A0A7927A3E8C231C3E867B28C,
	LeanPinchCamera_set_Camera_m43FB3EB9DA501523A7EB305B9D84DE672CA8385B,
	LeanPinchCamera_get_Camera_mC7A53207B98FF8E1190A50AB525B86A8A5B126F2,
	LeanPinchCamera_set_Zoom_mC05333EAE3BB092E95573CB32D87877A0892B663,
	LeanPinchCamera_get_Zoom_m928E991D35F0F34CBFD78FE904D3E0AB30FBA151,
	LeanPinchCamera_set_Damping_m51DA1118DDFDF19A34F6667F2A1C48964AB15FDC,
	LeanPinchCamera_get_Damping_m063C055BDED3FB3C289489DC6B9EABBF792F56E2,
	LeanPinchCamera_set_Clamp_mBC0DDF5141D9BCB7648EDBD06F8DF8DD21A8BE05,
	LeanPinchCamera_get_Clamp_m6F4323A67995F90635CFA0B0AB605FCF0ACE2A05,
	LeanPinchCamera_set_ClampMin_mB8285FC688D3997649BCF2860382F5AC92AF769E,
	LeanPinchCamera_get_ClampMin_mE7FFDEE79E6B286A6A26CB6B539752969C94DD14,
	LeanPinchCamera_set_ClampMax_m26D5E396FA6C2C2951485658F9F9AAE95E3F61D1,
	LeanPinchCamera_get_ClampMax_m1205CD08FBD779B27C5307BF3A6E3D9FFAE19F69,
	LeanPinchCamera_set_Relative_m72BFEC0FB396378D2DD95D9F57F11C700ED4554E,
	LeanPinchCamera_get_Relative_m1E802B163874D94FDE2D2A64E534878AF827093C,
	LeanPinchCamera_set_IgnoreZ_mE8FC62137CE9B6BEE79ABA44A374462439560305,
	LeanPinchCamera_get_IgnoreZ_mDBE386DF62A910B86CB1497DABBF116460FDA5F5,
	LeanPinchCamera_ContinuouslyZoom_mD78DD2117C2AAED168F8067EE30C91640A767DEF,
	LeanPinchCamera_MultiplyZoom_m9B05C0878263C39BBAB53986D99354EFBE77047E,
	LeanPinchCamera_IncrementZoom_m12BFED79BEF90702B44F9CBF3F374C43766EADDE,
	LeanPinchCamera_AddFinger_mCC7CBB984819E5716FE1FCC1DC19F4A508D250C2,
	LeanPinchCamera_RemoveFinger_m03998556079691F10011025BB31711977A93B8D5,
	LeanPinchCamera_RemoveAllFingers_mAB3DF1D2C79C3743EB637A86B12B64B620FD8D07,
	LeanPinchCamera_Awake_mAD870BC5A75F5FEEA15089021E5496FBBE09A413,
	LeanPinchCamera_Start_m897D20680A1E50270506530F982CDC45BD31EED0,
	LeanPinchCamera_LateUpdate_mE4AC6F9081C806639A55A1977926BA5992845C8B,
	LeanPinchCamera_SetZoom_mEE6FCD91376C7FE5ACB45C1687672B13ACD43AB0,
	LeanPinchCamera_TryClamp_mAE7F4A09736F5731274BF592EEDE019D49BD724F,
	LeanPinchCamera__ctor_mD067BCAE9B905FD20B6A6247EF2D6E986B3C9C29,
	LeanReplayFinger_set_Cursor_mD468F0DA29A524A2D3E30F35E1684B1597E2D561,
	LeanReplayFinger_get_Cursor_m09AB81DBF8E3D3306CE243FE3885997E82E6051D,
	LeanReplayFinger_set_Playing_m77BF285F70A051B1A86C0EDC42FC45BACE7D4901,
	LeanReplayFinger_get_Playing_mD06DE6F4E14A3024DD984C9EB4DAE6FE2E96DEC7,
	LeanReplayFinger_set_PlayTime_m379DDB7277AB39C603A272C437AA652F78198B55,
	LeanReplayFinger_get_PlayTime_m50BC8C3E01DF5D72E14F4AF898DC26AB6A0BD3F8,
	LeanReplayFinger_Replay_m7DE9573F4A3DDB8F84A0915EA7643634E842D81C,
	LeanReplayFinger_StopReplay_m08CEC132782E2D5F3726719615B65FC17C02534B,
	LeanReplayFinger_OnEnable_mFA892D9C9C0BD51F8FF48DD902F7826B17349620,
	LeanReplayFinger_OnDisable_m25EE3C1264318AB1669D72CBF96AAC4E8A5ED4C8,
	LeanReplayFinger_Update_mEAC2CBE6894BAA391F3B1BF1B6C6719B97DEBB35,
	LeanReplayFinger_HandleFingerUpdate_m0BF22B7A382CB3CAD1F30AFE5B2A58CA9744E553,
	LeanReplayFinger_HandleFingerUp_m694A09DC512ACAB31B54366C5E464EBF9D40BF02,
	LeanReplayFinger_CopySnapshots_mC6B8A010ADF19786B0B4A7E0DE97FF57542880B9,
	LeanReplayFinger__ctor_m3FF6BFBFFEAB71574C8A34E6C4F6BE462F9F993B,
	LeanSelectableBlock_set_X_m3CCE4AA2D0C1B0BD74080A855AE6481B48C9B9D5,
	LeanSelectableBlock_get_X_m6D3E4E02F93C0AE8CD93A8BAE0934F1478D6D77A,
	LeanSelectableBlock_set_Y_mD360C29236B3E5AD269A1EAAA2867FE3B33DCB66,
	LeanSelectableBlock_get_Y_m39AD2956985F2F6C46425A44E6AAFAAC08331E67,
	LeanSelectableBlock_set_BlockSize_m1D0349E94643CAAC69704EA42B5CC61CC9A67514,
	LeanSelectableBlock_get_BlockSize_mEECBA78BCE127FDA782686A76B488D6933B84A9F,
	LeanSelectableBlock_set_DeselectOnSwap_mD01D7E9F6DDAF09211D5468E587FF92B40B99EFB,
	LeanSelectableBlock_get_DeselectOnSwap_m5BF8CC23A23D89CBE4A0F210CA694A4248CC8290,
	LeanSelectableBlock_set_Damping_m9651F365CD91B9351D83E5FF7964714D19D5FECE,
	LeanSelectableBlock_get_Damping_m680EA34A053C14F9D148C290A894155AB8910B32,
	LeanSelectableBlock_FindBlock_m10DC9177D54B93BFA231D098634FF1331988E01B,
	LeanSelectableBlock_OnEnable_m2B221370A4FA0DF06537ECBF1B99E0D96699C06F,
	LeanSelectableBlock_OnDisable_m9BE5B6FD65EAC8B89C3F3ECD9E532FFC69CF7700,
	LeanSelectableBlock_Swap_m40231CD576BE06E4B45C7172097250AC23214ADE,
	LeanSelectableBlock_Update_mE67F8B503842E570FC6A2A76E03D20265A776DE4,
	LeanSelectableBlock__ctor_m5528493EA9E96168268AE0421F251447774D09FE,
	LeanSelectableBlock__cctor_m8F15A364E870B49F4BA01EFBAAD20B33E1919097,
	LeanSelectableCenter_get_OnPosition_m6236B9F326A8A8668609B8126A490845E7751C58,
	LeanSelectableCenter_Calculate_m30CCA82E5C0E2B6C838A6DBA5C43DDD627F0AF4C,
	LeanSelectableCenter__ctor_m22110B79E053F7DB840C04F2F3FD39F09BB866B2,
	Vector3Event__ctor_mBD048EDE9910FBCE2F98C291B11F6B656FF850DD,
	LeanSelectableCount_set_Count_mC3709AE3869AAE35C423DA68DEAA2E954BD36E94,
	LeanSelectableCount_get_Count_m67E3F0039EA06449B281CA4AF424CC55B7267D82,
	LeanSelectableCount_get_OnCount_mB1C6901F134F52F7A535ACB36837DE90DC827284,
	LeanSelectableCount_set_MatchMin_mBB7374A1126B36C2DFD5F5C4670F54A2970CE335,
	LeanSelectableCount_get_MatchMin_m3C787FF82C642EC5855E63B77CDDB833D46D02DF,
	LeanSelectableCount_set_MatchMax_m304E39BC288196020CA2956CC1D68802EBDA91F0,
	LeanSelectableCount_get_MatchMax_mEA2C84E3C505FD9804215421616556A27633511A,
	LeanSelectableCount_get_OnMatch_mB9A46426AD3A7212B58F6D0482BB21326870FC4E,
	LeanSelectableCount_get_OnUnmatch_mCBEB3FDFE5E139B3566D2E295F3FD4B62150226F,
	LeanSelectableCount_OnSelected_mCFBBD435C209E0B73C8AAA3FB77520D230BD6A72,
	LeanSelectableCount_OnDeselected_m0084321F8BED84CF24FABB649694C6A66360FD1F,
	LeanSelectableCount_UpdateState_m09348F832578C7F265A53636D86EE6C31DE956F1,
	LeanSelectableCount__ctor_m557ED514F5073C5C07EB2B54A6EB3DF57EE0DF92,
	IntEvent__ctor_mFFD43B9FFEE825DE8A6D10174B91CCC4448726B8,
	LeanSelectableDial_set_Camera_m44641853FD37C8741CE1A30196C00B4DCDD1F155,
	LeanSelectableDial_get_Camera_m135AF4E0CC201CCC8EDE52173689F297D1628E36,
	LeanSelectableDial_set_Tilt_m4E6FF9871EA813BCD9C3DA474F0108AECA21080A,
	LeanSelectableDial_get_Tilt_mBDDB81F76A7488A7F399A4DECC3E23541428A6EC,
	LeanSelectableDial_set_Axis_m0C7D383D04069522274EE3B8C46DA3B763F00FCB,
	LeanSelectableDial_get_Axis_m66DD832FFA69B6042B42F29814AFAEF19A51BBA2,
	LeanSelectableDial_set_Angle_mFA0904BC72B9A7617CA9CE3384B3C48B99C3C0DC,
	LeanSelectableDial_get_Angle_mA90C87DFFC42FAF24A4F5C4C866E14D28881B480,
	LeanSelectableDial_set_Clamp_m3177E69298DBF4D18389A6B93C4679EBD3ABADFF,
	LeanSelectableDial_get_Clamp_m79A7AFD1EF340D85D0FFE3AEE72E8F7279B9B6C0,
	LeanSelectableDial_set_ClampMin_mDB93CB7EAEBBE23588EA7BE58926A5E2218EA1DD,
	LeanSelectableDial_get_ClampMin_m54CDC368D8FCCD923FE9586929167C9772002ACE,
	LeanSelectableDial_set_ClampMax_mF3E2A3A9432CF39D9CAABE818A76074D2AD25B2E,
	LeanSelectableDial_get_ClampMax_m594A1D974F1EA90CEB8C39DB5159444FA418081C,
	LeanSelectableDial_get_Triggers_mBA14561BC72DAB7CE1DE5081FE46CB97B152C0A7,
	LeanSelectableDial_get_OnAngleChanged_mC0EF22987A662677DEB1C55C0CC7D9DDF29EDA83,
	LeanSelectableDial_IncrementAngle_mF76760B5BFF5914BD0392BC5BC6899D727E9606E,
	LeanSelectableDial_Update_m6803E7859005874696BD4EB4B223375EB7838989,
	LeanSelectableDial_GetPoint_mE0874F4F4A21ED7CF6CF6D283F32265DF3E89486,
	LeanSelectableDial__ctor_m8CBAADC09F60AEC120A02F09445DA991ACE66789,
	Trigger_get_OnEnter_mB078BDAB6F8EDCD7C32E031BEA126D8E8344C3C5,
	Trigger_get_OnExit_mA51B9884CADD3C8285FFA3B80A427FAB0B042E0C,
	Trigger_IsInside_m5907DCDCAB9FC3F07A2539547C80A13E9ADE96F8,
	Trigger__ctor_m96E85FA706A948FC40388F7C242453E6D16B4E71,
	FloatEvent__ctor_m81F86D5D9FB25AB91E75CBDDEA5D955D2A8673E6,
	LeanSelectableDragTorque_set_Camera_m39E44ABD608B24967D49350EC6B212E0E2DFFBF6,
	LeanSelectableDragTorque_get_Camera_m83DA2E568DD8FB796E9815D4A45FD8E135088AF7,
	LeanSelectableDragTorque_set_Force_m724B4F194862568ED7EADF0794D6FC81B1D4A131,
	LeanSelectableDragTorque_get_Force_m507B8F1A77741C32ADCCDABAF6C00D874D93DA35,
	LeanSelectableDragTorque_OnSelected_m5B56B09BE53C001014F42F6587FCC4EE029492FE,
	LeanSelectableDragTorque_Update_m2452C2312A12A737501671FBFF9FADAC55088324,
	LeanSelectableDragTorque__ctor_m0D6DD6177894ED077AA3050263C301E5E8BA2214,
	LeanSelectableDrop_set_Ignore_mAF78943058631464D76B40E3AA5D667FFB54C982,
	LeanSelectableDrop_get_Ignore_m0DAD5DE2F8D47E632EBEF38B46A6B4A3A0379BA9,
	LeanSelectableDrop_get_OnGameObject_mE3BF2B1AE9118D2822D91A27B4ED92B25738CE98,
	LeanSelectableDrop_get_OnDropHandler_m9454C0FAB58FDCBB414B8CD43E365832EBBF66BB,
	LeanSelectableDrop_OnSelectedFingerUp_m7BAC9F477B8D93E8F11CE8BFB928B91910DAE42A,
	LeanSelectableDrop__ctor_m0C0C48914F5BF48305C8B61520060795BE992141,
	GameObjectEvent__ctor_m5DF4A4A01416DFFDDA8B9326E1CF822DCD96F46B,
	IDropHandlerEvent__ctor_m9E6C867F445ADBAF24F5CE713712795D48140653,
	LeanSelectablePressureScale_set_BaseScale_m97CC53E7EAE851915F0638D9075B268D67525400,
	LeanSelectablePressureScale_get_BaseScale_m2D353FDECCF96B709D9524A1F38AED736FB0598C,
	LeanSelectablePressureScale_set_PressureMultiplier_m1774968FF650122D1B4084D6D17CAF51EC76C18D,
	LeanSelectablePressureScale_get_PressureMultiplier_m537EEE78A10C67AAF637FF3D931D2D6ED5520CB4,
	LeanSelectablePressureScale_set_PressureClamp_mCCD73A1F65CA1FDF49343007E81D915AE1A7B4A9,
	LeanSelectablePressureScale_get_PressureClamp_m93564474798CC07D790C1398F347FFB19AC16E63,
	LeanSelectablePressureScale_Update_m5145C0A58F1E9D857401D95DBCFE568ADAFB6B9A,
	LeanSelectablePressureScale__ctor_mDA2BC9C7AC65DB2B0BBAB8E1FFA5C8D12ADA10A7,
	LeanSelectableSelected_set_Threshold_m7D09AC8C7078507865C167D8B094D5B570E6A1C8,
	LeanSelectableSelected_get_Threshold_m2E1A7386F80AA0B1C4F2C70A28D44BB9DA52AD5A,
	LeanSelectableSelected_set_Reset_m207C28724AD3697B081EFF969658DEE3B9EC0433,
	LeanSelectableSelected_get_Reset_m162ED6E1F355425A91E30FD9B8E7624DAB86FC2A,
	LeanSelectableSelected_set_RawSelection_m48DE22BD7FB3A4BE3EC1C647789263B96F0DCCF1,
	LeanSelectableSelected_get_RawSelection_m42C076D5FE28A6AAB09DD91AAAF0EFD97247000C,
	LeanSelectableSelected_set_RequireFinger_mBFA43462C4D1F0CE21EE8F227E615D7A6F1BA960,
	LeanSelectableSelected_get_RequireFinger_mBA8A44064EDCB55050229EBD735BD82E0BCB33BF,
	LeanSelectableSelected_get_OnSelectableDown_mF2AE8A9395E3FB8626247ED255189B4A5104DBA9,
	LeanSelectableSelected_get_OnSelectableUpdate_m60B5E18108023F15F3073ECAEAD3A63905861474,
	LeanSelectableSelected_get_OnSelectableUp_m15D05E03489DD3793FC409A69CAB0CB06FE11155,
	LeanSelectableSelected_Update_m4E3C80B96D751D0212E469A88D380954589EAD13,
	LeanSelectableSelected_OnSelected_m840FDA83E226D64A7C1DA34CF9A8F98CDA18CB7E,
	LeanSelectableSelected_OnDeselected_mEABF5DFB4050FE2045F06937FBAAA8CFD2E88515,
	LeanSelectableSelected__ctor_m5ED3B1C77CA40BDB03628C746DB6F0EC051A0C0A,
	SelectableEvent__ctor_m919E20AC6C5B01AB3571D1EC0D3306215703BBCE,
	LeanSelectableTime_set_Send_m6B8C842777A9019AC96E3E2E9D457BA558C26430,
	LeanSelectableTime_get_Send_m2DEA934FE7700C3B7FDA92CC4D8B761240DFF296,
	LeanSelectableTime_get_OnSeconds_m29513A7E7F5F9C4B3BB4859741FFABF42B29ACB7,
	LeanSelectableTime_Update_mD1B3E3CE029B00E45341EE43EE947A9023823478,
	LeanSelectableTime__ctor_m85A76A4DEE9639059EA0D8F40418A104BEE29875,
	FloatEvent__ctor_m9EADF2017C4D485563D1029979BC5A633594F5F9,
	LeanSelectionBox_set_Camera_m943D8D5AB5ECD99269E189F6D13CB76255792A80,
	LeanSelectionBox_get_Camera_m560BEA20B498C82662801AA76DF89E3944CF03D4,
	LeanSelectionBox_set_IgnoreIfStartedOverGui_m6BBD118D11B026D21654AD7B279C3F98C05B04BE,
	LeanSelectionBox_get_IgnoreIfStartedOverGui_m4146ADC45FF79D0F24CA4200C9C714B54160458C,
	LeanSelectionBox_set_Prefab_mCED74ED19C7CD52AFE2AA0F617360AAC24E5078C,
	LeanSelectionBox_get_Prefab_m51337DEF4548C71CD4D342E9F399FE2EEDA418DF,
	LeanSelectionBox_set_Root_m58D6C9F901982DB603030A9E966FCED97BB68BCB,
	LeanSelectionBox_get_Root_mB67EC4FC876421256D242D7B19532DE450B7364B,
	LeanSelectionBox_set_Select_mBF08B346300D60318753512B42A09F21883FB582,
	LeanSelectionBox_get_Select_mFFA405A7AB050F4AE22D0B8314FD3CEBA2322B6F,
	LeanSelectionBox_set_RequiredLayers_mBC54A3B917A282D6D91D9BB7A8F59A7A37289784,
	LeanSelectionBox_get_RequiredLayers_m26CABC0777DF3C2D317F46A9E92D6B4BE50440A3,
	LeanSelectionBox_get_RequiredTags_mFD50533F78AF9726C11A9E2A860BE23B0F4BD354,
	LeanSelectionBox_OnEnable_m6560444F6E3D64F5BD0C44ED38B5E4661D82EA43,
	LeanSelectionBox_OnDisable_mB7D448D301AD7A7A5243CCE48D38815DBA2BDBFE,
	LeanSelectionBox_HandleFingerDown_m18A35A2416071AC81C951AAED04C260A9C56B98B,
	LeanSelectionBox_HandleFingerSet_m0F76325DC5CA3704131AB17D9057C8D9A00CD75B,
	LeanSelectionBox_HandleFingerUp_m5DEA0F68FDFE957FCB6F7A09A5577A9F99EAF166,
	LeanSelectionBox_WriteTransform_m10172607008E039CC69F1D7B0F00B31DA0BBDAF1,
	LeanSelectionBox_HasRequiredTag_m712F1BE85B6D3FB395E506A375F752749479358D,
	LeanSelectionBox__ctor_mCADDADEDE381D79F6F2D6E78D42E555358728378,
	LeanSelectionBox__cctor_m58DA7605A6337DC364102AF29D3AD15FB724CCDB,
	FingerData__ctor_m66A10889E5B66034D798A491359A21164C3FEC77,
	LeanShape_set_ConnectEnds_m8803D34387B4CD9F6C21457C3E43F9A66F27CFA0,
	LeanShape_get_ConnectEnds_m06FC33CF5EA7538DC2DCA61A9CCCDD86C8A989FE,
	LeanShape_set_Visual_m62441FD3A3CFF46A907C45905320716C5775C8AA,
	LeanShape_get_Visual_mB7421CF4625C94634A111B268A4A134638934775,
	LeanShape_get_Points_m023F85C336F1CB4BBE2CBEE768B8CE7D707F14FF,
	LeanShape_Mod_mF2AAB88C66376FA47A2936C2A244F8C8C336983F,
	LeanShape_GetPoint_m6609D5AEC3203B4EB977568511E26F102F7545AD,
	LeanShape_UpdateVisual_m8C615F030DEAB5A017FF6F05DCF0A3D0C8CBA305,
	LeanShape__ctor_m91232CE7E03E116899B2AAB71AC7E66ABE74F306,
	LeanShapeDetector_set_Shape_m70FAA8EE19ED6E60E4FF6BA11F24A802796482F7,
	LeanShapeDetector_get_Shape_mEF115C09786532116198A59C312EC75DF2ED2D3F,
	LeanShapeDetector_set_StepThreshold_m9F3EA78AFE1230923555731FEB2830007534D2C5,
	LeanShapeDetector_get_StepThreshold_m60C18390E6981E60794F3025A7C6E962E4D67972,
	LeanShapeDetector_set_DistanceThreshold_mD956E6A87B6F531FBAED17D75A9221277EC7F9BE,
	LeanShapeDetector_get_DistanceThreshold_m46C227911B234152D916F72B3F0E02349C65C97B,
	LeanShapeDetector_set_ErrorThreshold_m612CAA2BA37AE32F5442E59B6FEE7A4917E964FF,
	LeanShapeDetector_get_ErrorThreshold_m196E7F8B41A102A7743DBF7A9F81C2AE0D856C62,
	LeanShapeDetector_set_MinimumPoints_m52C48EFA39DD3DC124981FF3F1390ADD96B23B89,
	LeanShapeDetector_get_MinimumPoints_mEA4BAE9768F9A38B7190397C2FC07DFBE64A72D3,
	LeanShapeDetector_set_Direction_m0B83036DDABFF35F0B36D0239A60127D2C55DBD7,
	LeanShapeDetector_get_Direction_mD0DD6F122A68258B61D6CA6B1854F57EF77CDF53,
	LeanShapeDetector_get_OnDetected_mEC708697BD4CDEA0ED8A0D164C6B03F9E88346E5,
	LeanShapeDetector_AddFinger_m776C3EF35A9F6262E1B0F5B88D84ACC52840B225,
	LeanShapeDetector_RemoveFinger_mDE85C3B0FB482870F749AFCA103DC56B9DA39B5C,
	LeanShapeDetector_RemoveAllFingers_m3745A940941909D91099F4ADDC4039F376B28D2B,
	LeanShapeDetector_OnEnable_mC1B549D87DEA472ED29329B44E4D081DD9B18EA3,
	LeanShapeDetector_OnDisable_m88F9C1BD02AD7DE79FB189EDD97A2C690EC93359,
	LeanShapeDetector_HandleFingerDown_mBB6AC90C7051A1457EF67004348CC33D463144E5,
	LeanShapeDetector_HandleFingerUpdate_mC7F9C4785B13C1C55391ECE2B89F1E9743379221,
	LeanShapeDetector_HandleFingerUp_m3AB300B53299442FEF895D5B4C8BFB682FE21F09,
	LeanShapeDetector_AddRange_m1A04BD5DC2CD3C9D9D80E0708117C3EDE61ED856,
	LeanShapeDetector_CalculateMatch_m339F24DAEBD4698AA57BE767FF3107072E7EE458,
	LeanShapeDetector_FitShape_m5F02DE1C89A494497FA7145A7FBCE3CC3AC36BCA,
	LeanShapeDetector_ConvertPoints_mC03F016479397BCB04C4518EA348D2C5F0FEEE00,
	LeanShapeDetector_Read_m3D68642B63BAD145329A0CA2DD56D5CD868CFBA9,
	LeanShapeDetector_GetRect_m9A1DE679D79C530287665826BA8E149337823EA8,
	LeanShapeDetector__ctor_m9923BF69329BF0ECC66F3A81E3EB3CF3B408698E,
	LeanShapeDetector__cctor_mFD7F4A05D0577CFF0550C0FD9AD8D289BCD8C179,
	LeanFingerEvent__ctor_m464DB7B86BADBC50488DD963D2666D5CE8B4E4DA,
	FingerData_get_EndPoint_mD863A5CE572F621AEDB7C431E4A01C6C0837ED22,
	FingerData__ctor_mFDCE2D7B0EE44BE6B628A4DF5D81B1A5F15F35B3,
	Line_GetFirstDistance_m9890016A67A214E24E791787A493B349D61B3D47,
	Line_GetDistance_mC5BE3BE21A97308070621AD7FE6210404D551731,
	LeanSpawnWithFinger_set_Prefab_m817F51523DBA598406CC7029EBE7F45310CEC543,
	LeanSpawnWithFinger_get_Prefab_m8B650147CC3C227078E740C70A3A5871CDD1B8B1,
	LeanSpawnWithFinger_set_RotateTo_m123CDA98BF3601BCD808640CB28657A5862EC05A,
	LeanSpawnWithFinger_get_RotateTo_mA206E34CFF36BDF477158F01775941D912A12A22,
	LeanSpawnWithFinger_set_DragAfterSpawn_mA405D74C831C7455DDF3CC3AFAC346815DDF9CBA,
	LeanSpawnWithFinger_get_DragAfterSpawn_mF2425ACA6B966CBDDF9FB31DB9D8B7FDE01ED667,
	LeanSpawnWithFinger_set_DestroyIfBlocked_mD3CD73B50EF30FD74A6F8FC93F9D74E9AF381685,
	LeanSpawnWithFinger_get_DestroyIfBlocked_mFCD874283D7FCCC6CA27412A591B236388EC5E4C,
	LeanSpawnWithFinger_set_SelectOnSpawn_mFDDC5C8BC9DD3AF452EA2BF9788A312D4FA4AB4D,
	LeanSpawnWithFinger_get_SelectOnSpawn_m089AB5E1C46306C75D7DE84CA51E5D93B9A8865C,
	LeanSpawnWithFinger_set_SelectWith_mB7794291E1876D9132709086CB34B55AA166A976,
	LeanSpawnWithFinger_get_SelectWith_m03E5CBB854A225395D714AEE4330301AF93B3D88,
	LeanSpawnWithFinger_set_DeselectOnUp_m49CDBA27ECB9419959CA6B8A656683D8233776A9,
	LeanSpawnWithFinger_get_DeselectOnUp_m60584A1972DAFAC10805CEF5A5E4990B42AA89A1,
	LeanSpawnWithFinger_set_PixelOffset_mDA63EF079E799FDF1CEB2AB501202A5E40130141,
	LeanSpawnWithFinger_get_PixelOffset_m44FAF422501370635CDCFD3046D04E6711BF0C70,
	LeanSpawnWithFinger_set_PixelScale_m5007FDA6003573EC4BEF453D6BB201FA8F927D07,
	LeanSpawnWithFinger_get_PixelScale_m9376BA7AF29DDA7C933D4C2245078CC6222A47DA,
	LeanSpawnWithFinger_set_WorldOffset_mC8AE70C28B458C2FBFC74308E15213839A8F1A72,
	LeanSpawnWithFinger_get_WorldOffset_m30935963A64D9AB6863F84D21982A4C3D54A8868,
	LeanSpawnWithFinger_set_WorldRelativeTo_m4A5BDC9C7C70EE92F26C9AE788F44D10964A21C9,
	LeanSpawnWithFinger_get_WorldRelativeTo_mE1F7C130B014EFD76C831A47CFBA6A7AD8530A1C,
	LeanSpawnWithFinger_Spawn_mB735ECE956C9C8900EEFCCBE0DA6400548F4474F,
	LeanSpawnWithFinger_OnEnable_mA1A4E475C02D8310DC122ABFB989F2B785CD0455,
	LeanSpawnWithFinger_OnDisable_mCF01CEA19EF6E7FB97ADA8C0CCEF17D3FF1BFD5F,
	LeanSpawnWithFinger_Update_m3375C1444256BCF2FD504D957FB049D22A08E911,
	LeanSpawnWithFinger_UpdateSpawnedTransform_mCECF8DF74A7E33345E5791F3F90C0FF94E2191DC,
	LeanSpawnWithFinger_HandleFingerUp_mF30BE86F48384C6A244711C1348A64563FB317F0,
	LeanSpawnWithFinger__ctor_m4103E35960EA82BE7A4E08939912D1F59E5D5C1B,
	LeanSpawnWithFinger__cctor_mC0F391C07861AACEA3872A0DDCC5FC43F77942E3,
	FingerData__ctor_m16F99113B9EF9F522810BE3FFC0FD2F0F8B23982,
	LeanSwipeEdge_set_Left_m5DD0E423DF2FE933CF1C1C38E9765EF0F43A07B8,
	LeanSwipeEdge_get_Left_m3D650F87AB5BFC922CA362D6DCAB66D7D1D49295,
	LeanSwipeEdge_set_Right_m05FA1EB733D4050EC5B00AB27CD7BA942085F2DA,
	LeanSwipeEdge_get_Right_mC816C84BFB7E4554AACF8C62497EBD53ED43C327,
	LeanSwipeEdge_set_Bottom_mE1EE1EF713F1AD7274A8E90C8F025654F4FFC93B,
	LeanSwipeEdge_get_Bottom_m10EC5487DD456E4379DA95436C7FD8BEB8DB8848,
	LeanSwipeEdge_set_Top_mD9C51CCD6C5F267A4B7723F22E175F0047FAD430,
	LeanSwipeEdge_get_Top_m3D73458213C68D77381A6B6100911152B3E25880,
	LeanSwipeEdge_set_AngleThreshold_m870F700221A1B3E2031405EBEF2C1872529F5088,
	LeanSwipeEdge_get_AngleThreshold_m3B5424C4FD11CD9F52A66B116CAEB187091D5236,
	LeanSwipeEdge_set_EdgeThreshold_m0016ED8CC52B61B48E6E92103292E362FC256AF2,
	LeanSwipeEdge_get_EdgeThreshold_mB32D91C82BFEEA4871AA0B287CD929D86AD8A995,
	LeanSwipeEdge_get_OnEdge_mCD0A96E445B4584B08CC19EC2206A684312AAC4C,
	LeanSwipeEdge_CheckBetween_m569242B4189617703249032C57A23B902E307C67,
	LeanSwipeEdge_AddFinger_mA7B3C04B5AD739C4198F66D42161A21604ED2814,
	LeanSwipeEdge_RemoveFinger_m77463614BF44E25C1D2F372D8A6B5F7436E795CE,
	LeanSwipeEdge_RemoveAllFingers_mAC1A4D096D1C03227DBC2611754A36706567B7FA,
	LeanSwipeEdge_Awake_m211B1BF010FC32D456F3DBE421554042AB38E5AA,
	LeanSwipeEdge_Update_mD129304BD898F94F1E342D81423B3C7855B4B2CD,
	LeanSwipeEdge_InvokeEdge_m7648B6DE9697228BE50609600DAFEF68CDAB7C8B,
	LeanSwipeEdge_CheckAngle_m99F242FA56B49C8101EA10205513DFDF5A399D6C,
	LeanSwipeEdge_CheckEdge_mCC72892056F45EF2EEE6B498168F0088E8E2FD4B,
	LeanSwipeEdge__ctor_m560586ECB36E4865DD6112ACDBCC22E1220925D8,
	LeanTwistCamera_set_Damping_mBBE1CC64EE418113F2BCD85E67117CEF0AC9A0C8,
	LeanTwistCamera_get_Damping_m5D1D25E62813F77A1BBE52FE4FCB5DC45AD38903,
	LeanTwistCamera_set_Relative_m208817B328CC70DEC55988B050AC948072624D81,
	LeanTwistCamera_get_Relative_m2D0BE1C14B3D40C2F287EF826EA2CFF445581E46,
	LeanTwistCamera_AddFinger_m74B8153A15E2BD1C8867E9119C5A54CCE83D2F2D,
	LeanTwistCamera_RemoveFinger_m00D617B1ECB2CFE6DEB1349EC795EB0556CF7AD7,
	LeanTwistCamera_RemoveAllFingers_mCEAE188CD407C235B26943A42221F7ABCB555E82,
	LeanTwistCamera_Awake_m2ACB883383D28F0F9DD2F284D84E6C3364753A7D,
	LeanTwistCamera_Update_mFECBA8481FFA1B34872EAAD705A24C3764AB7943,
	LeanTwistCamera__ctor_m3B0A9026425C27900933223D22C39C9B4E1F3DC1,
};
extern void Line_GetFirstDistance_m9890016A67A214E24E791787A493B349D61B3D47_AdjustorThunk (void);
extern void Line_GetDistance_mC5BE3BE21A97308070621AD7FE6210404D551731_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[2] = 
{
	{ 0x060002ED, Line_GetFirstDistance_m9890016A67A214E24E791787A493B349D61B3D47_AdjustorThunk },
	{ 0x060002EE, Line_GetDistance_mC5BE3BE21A97308070621AD7FE6210404D551731_AdjustorThunk },
};
static const int32_t s_InvokerIndices[814] = 
{
	1986,
	3122,
	3711,
	3191,
	3775,
	3191,
	3191,
	3856,
	3856,
	3856,
	3191,
	3856,
	3238,
	3828,
	3220,
	3812,
	3191,
	3775,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	3238,
	3828,
	3238,
	3828,
	3238,
	3828,
	3220,
	3812,
	3220,
	3812,
	3775,
	3775,
	3775,
	3775,
	1256,
	3191,
	3856,
	3856,
	3856,
	3191,
	3775,
	3220,
	3812,
	3220,
	3812,
	3220,
	3812,
	3238,
	3828,
	3856,
	3856,
	3191,
	3191,
	3191,
	1256,
	3856,
	3856,
	3191,
	3775,
	3220,
	3812,
	3238,
	3828,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	3856,
	3191,
	3775,
	3238,
	3828,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3191,
	3775,
	3238,
	3828,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3775,
	1986,
	3856,
	3856,
	3172,
	3756,
	3775,
	3172,
	3756,
	3172,
	3756,
	3775,
	3775,
	1986,
	3220,
	3856,
	3856,
	3220,
	3812,
	3191,
	3775,
	3775,
	3775,
	2805,
	3856,
	3856,
	3191,
	3856,
	3856,
	3856,
	3220,
	3812,
	3220,
	3812,
	3191,
	3775,
	3172,
	3756,
	3856,
	3856,
	3856,
	3856,
	3191,
	3191,
	1131,
	3856,
	3856,
	3220,
	3812,
	3220,
	3812,
	3191,
	3775,
	3238,
	3828,
	3238,
	3828,
	3775,
	3775,
	3775,
	3775,
	3775,
	3775,
	3856,
	3856,
	3856,
	3191,
	3191,
	1576,
	3191,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	3220,
	3812,
	3220,
	3812,
	3191,
	3775,
	3172,
	3756,
	3172,
	3756,
	3775,
	3775,
	3775,
	3856,
	3856,
	3856,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	3220,
	3812,
	3191,
	3775,
	3172,
	3756,
	3775,
	3775,
	3775,
	3856,
	3856,
	3856,
	3191,
	3856,
	3856,
	3856,
	3856,
	3220,
	3812,
	3191,
	3775,
	3775,
	3775,
	3775,
	3856,
	3856,
	3856,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	2805,
	3191,
	3856,
	3220,
	3812,
	3191,
	3775,
	3238,
	3828,
	3191,
	3775,
	3238,
	3828,
	3191,
	3775,
	3238,
	3828,
	3220,
	3812,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	3220,
	3812,
	3191,
	3775,
	3775,
	3775,
	3775,
	3856,
	3856,
	3856,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	2805,
	3191,
	3856,
	3220,
	3812,
	3191,
	3775,
	3172,
	3756,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	3191,
	1131,
	3856,
	3856,
	3220,
	3812,
	3191,
	3775,
	3191,
	3191,
	3856,
	3856,
	3856,
	3191,
	3856,
	3191,
	3775,
	3172,
	3756,
	3172,
	3756,
	3238,
	3828,
	3172,
	3756,
	3775,
	3856,
	3856,
	3856,
	3856,
	3220,
	3812,
	3238,
	3828,
	3220,
	3812,
	3172,
	3756,
	3238,
	3828,
	3775,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	3856,
	3220,
	3812,
	3191,
	3775,
	3172,
	3756,
	3775,
	3775,
	3775,
	3856,
	3856,
	3856,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	3220,
	3812,
	3220,
	3812,
	3191,
	3775,
	3172,
	3756,
	3238,
	3828,
	3238,
	3828,
	3775,
	3775,
	3775,
	3775,
	3775,
	3775,
	3775,
	3775,
	3775,
	3856,
	3856,
	3856,
	3191,
	3191,
	3812,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3220,
	3812,
	3172,
	3756,
	3238,
	3828,
	3775,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	3856,
	3172,
	3756,
	3238,
	3828,
	3220,
	3812,
	3775,
	3775,
	3775,
	3775,
	3775,
	3775,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3238,
	3828,
	3238,
	3828,
	3238,
	3828,
	3775,
	3775,
	3775,
	3775,
	3191,
	3191,
	3856,
	3856,
	3856,
	1986,
	3856,
	3856,
	3856,
	3856,
	3775,
	3775,
	3775,
	3775,
	3191,
	3191,
	3856,
	3856,
	3856,
	2402,
	3856,
	3856,
	3856,
	3220,
	3812,
	3172,
	3756,
	3775,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	3856,
	3775,
	3775,
	3775,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3220,
	3812,
	3775,
	3172,
	3756,
	3238,
	3828,
	3775,
	3775,
	3775,
	3775,
	3775,
	3775,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3220,
	3812,
	3775,
	3775,
	3191,
	3191,
	3856,
	2805,
	3856,
	3856,
	3856,
	3856,
	3191,
	3191,
	3856,
	3856,
	3856,
	3191,
	3775,
	3775,
	3191,
	3191,
	1996,
	3856,
	3856,
	3775,
	3775,
	3775,
	1996,
	3856,
	3856,
	3856,
	3856,
	3191,
	3775,
	3238,
	3828,
	3238,
	3828,
	3220,
	3812,
	3238,
	3828,
	3238,
	3828,
	3220,
	3812,
	3220,
	3812,
	3238,
	3238,
	3238,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	3238,
	2964,
	3856,
	3191,
	3775,
	3220,
	3812,
	3238,
	3828,
	3856,
	3856,
	3856,
	3856,
	3856,
	3191,
	3191,
	3191,
	3856,
	3172,
	3756,
	3172,
	3756,
	3238,
	3828,
	3220,
	3812,
	3238,
	3828,
	5180,
	3856,
	3856,
	5417,
	3856,
	3856,
	5813,
	3775,
	3856,
	3856,
	3856,
	3172,
	3756,
	3775,
	3172,
	3756,
	3172,
	3756,
	3775,
	3775,
	3856,
	3856,
	3220,
	3856,
	3856,
	3191,
	3775,
	3263,
	3851,
	3263,
	3851,
	3238,
	3828,
	3220,
	3812,
	3238,
	3828,
	3238,
	3828,
	3775,
	3775,
	3238,
	3856,
	3003,
	3856,
	3775,
	3775,
	1600,
	3856,
	3856,
	3191,
	3775,
	3238,
	3828,
	3856,
	3856,
	3856,
	3172,
	3756,
	3775,
	3775,
	3191,
	3856,
	3856,
	3856,
	3263,
	3851,
	3238,
	3828,
	3220,
	3812,
	3856,
	3856,
	3238,
	3828,
	3172,
	3756,
	3220,
	3812,
	3220,
	3812,
	3775,
	3775,
	3775,
	3856,
	3856,
	3856,
	3856,
	3856,
	3172,
	3756,
	3775,
	3856,
	3856,
	3856,
	3191,
	3775,
	3220,
	3812,
	3191,
	3775,
	3191,
	3775,
	3191,
	3775,
	3181,
	3764,
	3775,
	3856,
	3856,
	3191,
	3191,
	3191,
	1986,
	2805,
	3856,
	5813,
	3856,
	3220,
	3812,
	3191,
	3775,
	3775,
	5127,
	1657,
	3856,
	3856,
	3191,
	3775,
	3238,
	3828,
	3238,
	3828,
	3238,
	3828,
	3172,
	3756,
	3172,
	3756,
	3775,
	3191,
	3191,
	3856,
	3856,
	3856,
	3191,
	3191,
	3191,
	1831,
	4115,
	5417,
	4984,
	5351,
	5654,
	3856,
	5813,
	3856,
	3849,
	3856,
	2965,
	2965,
	3191,
	3775,
	3172,
	3756,
	3220,
	3812,
	3220,
	3812,
	3220,
	3812,
	3191,
	3775,
	3220,
	3812,
	3261,
	3849,
	3191,
	3775,
	3263,
	3851,
	3191,
	3775,
	3191,
	3856,
	3856,
	3856,
	1986,
	3191,
	3856,
	5813,
	3856,
	3220,
	3812,
	3220,
	3812,
	3220,
	3812,
	3220,
	3812,
	3238,
	3828,
	3238,
	3828,
	3775,
	2027,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	1608,
	2854,
	3856,
	3238,
	3828,
	3220,
	3812,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
};
extern const CustomAttributesCacheGenerator g_LeanTouchPlus_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_LeanTouchPlus_CodeGenModule;
const Il2CppCodeGenModule g_LeanTouchPlus_CodeGenModule = 
{
	"LeanTouchPlus.dll",
	814,
	s_methodPointers,
	2,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_LeanTouchPlus_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
