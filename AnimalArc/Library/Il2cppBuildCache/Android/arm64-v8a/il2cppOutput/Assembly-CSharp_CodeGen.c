﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void DemoController::Update()
extern void DemoController_Update_m65BE6DD3EFE5FD35CDCA82EA64CC5EA05CC39B54 (void);
// 0x00000002 System.Void DemoController::NextAnim()
extern void DemoController_NextAnim_mF4414379F85B18B68EA5206859D0ADFEE320C41A (void);
// 0x00000003 System.Void DemoController::PrevAnim()
extern void DemoController_PrevAnim_m661CCFC17C6A3F0F8A873654B86E71CAFD39A75A (void);
// 0x00000004 System.Void DemoController::PlayAnim()
extern void DemoController_PlayAnim_m88268B9A1C8A18933FA9495C32CF19053265DAF1 (void);
// 0x00000005 System.Void DemoController::GoToWebsite(System.String)
extern void DemoController_GoToWebsite_m6D1775DA170EE55597781D45B7D474338E10D7C8 (void);
// 0x00000006 System.Void DemoController::.ctor()
extern void DemoController__ctor_mDCA85316804A352961C2DA6570E01FF23E7C9853 (void);
// 0x00000007 System.Void CamController::Start()
extern void CamController_Start_mEDF9FE7BEA5DA271CF400F632CCB2AEAFEA7E039 (void);
// 0x00000008 System.Collections.IEnumerator CamController::CamSetting()
extern void CamController_CamSetting_m51B10FA34446CA7A543892B60C360FA24FC672CD (void);
// 0x00000009 System.Void CamController::.ctor()
extern void CamController__ctor_mE9C81B1081FF7B4230FF66A1013698288C4C90C1 (void);
// 0x0000000A System.Void CamController/<CamSetting>d__3::.ctor(System.Int32)
extern void U3CCamSettingU3Ed__3__ctor_m8B0D0AE60BC45D32C578403CA715C618E42A93E0 (void);
// 0x0000000B System.Void CamController/<CamSetting>d__3::System.IDisposable.Dispose()
extern void U3CCamSettingU3Ed__3_System_IDisposable_Dispose_m8D61F6E5004619A2C3A93159F4ECA329F1284C45 (void);
// 0x0000000C System.Boolean CamController/<CamSetting>d__3::MoveNext()
extern void U3CCamSettingU3Ed__3_MoveNext_m87A559BA121C6B5E4755D74ACDB8984F47F4A528 (void);
// 0x0000000D System.Object CamController/<CamSetting>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCamSettingU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m247DD3E72107F6522844A2FAE04922F9B90E7756 (void);
// 0x0000000E System.Void CamController/<CamSetting>d__3::System.Collections.IEnumerator.Reset()
extern void U3CCamSettingU3Ed__3_System_Collections_IEnumerator_Reset_m83570DB6DDBB45080E2D6EDE312014E7B9919F2E (void);
// 0x0000000F System.Object CamController/<CamSetting>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CCamSettingU3Ed__3_System_Collections_IEnumerator_get_Current_m9AAC4E7D2536AD94D4C056662DA0C5661245EAB4 (void);
// 0x00000010 System.Void CheckPoint::Start()
extern void CheckPoint_Start_m9BB59A2CFAB9E1D880D35A3C90C3583C1D3FA954 (void);
// 0x00000011 System.Void CheckPoint::Update()
extern void CheckPoint_Update_m6C4383DA634F30FA674194F900C08158D445EE96 (void);
// 0x00000012 System.Void CheckPoint::.ctor()
extern void CheckPoint__ctor_m567092658676E17DD5CC0FB6FE90689AAED74C47 (void);
// 0x00000013 System.Void ControllerAnimal::Start()
extern void ControllerAnimal_Start_m4071928C042DE4AD4D7C2963DB05799C5B6E382A (void);
// 0x00000014 System.Void ControllerAnimal::SetAngularDrive(UnityEngine.JointDrive,UnityEngine.JointDrive)
extern void ControllerAnimal_SetAngularDrive_m0C70560E814CB6F2B2BAF9864E260E783910FA09 (void);
// 0x00000015 System.Void ControllerAnimal::FixedUpdate()
extern void ControllerAnimal_FixedUpdate_m7BC6BA1FA1F11D0CB18DFF893198E50C31F4DEC8 (void);
// 0x00000016 System.Void ControllerAnimal::.ctor()
extern void ControllerAnimal__ctor_m28C13EFF33C021CC3487D77A8D2295F1FE40F73A (void);
// 0x00000017 System.Void PlayerController::Update()
extern void PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794 (void);
// 0x00000018 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (void);
// 0x00000019 System.Void DataModelHere::Start()
extern void DataModelHere_Start_mA9D1240509FFC87EA0370D5B92875B7D0915E09A (void);
// 0x0000001A System.Void DataModelHere::Update()
extern void DataModelHere_Update_m290A9F7E90291DB342E3B6278CF7636EBB1C49EC (void);
// 0x0000001B System.Void DataModelHere::.ctor()
extern void DataModelHere__ctor_mD5E53C001DB6E74AE4ECFD7CDAFC0F47EA79090C (void);
// 0x0000001C System.String HomeController::SceneName()
extern void HomeController_SceneName_mE4F0B8EB70C0F0C9DF473D5D6AE1F47C4731DD4A (void);
// 0x0000001D System.Void HomeController::.ctor()
extern void HomeController__ctor_mA8077DB22E606DA009CD773F9C00536876A72DF4 (void);
// 0x0000001E System.String techGameController::SceneName()
extern void techGameController_SceneName_mC465BADB84969F3EE807CFF975BF07DB243322F2 (void);
// 0x0000001F System.Void techGameController::.ctor()
extern void techGameController__ctor_mE8A36E60FC426F1AF56F01B249ECF16A885EAB88 (void);
// 0x00000020 System.Void Const::.ctor()
extern void Const__ctor_m0CD639302F73E17069A919829CCE30742348BAB9 (void);
// 0x00000021 System.Void RandomFlyer::Start()
extern void RandomFlyer_Start_mFAE78D640C06D0491ADA60F16815417B14B35044 (void);
// 0x00000022 System.Void RandomFlyer::Update()
extern void RandomFlyer_Update_m782E691C9BF4DB9C9C822126773567CAC61D5193 (void);
// 0x00000023 System.Void RandomFlyer::OnDestroy()
extern void RandomFlyer_OnDestroy_m29AE94FFC785D3F5043E5FE12AE2AF0127E7EFC9 (void);
// 0x00000024 System.Void RandomFlyer::.ctor()
extern void RandomFlyer__ctor_m21045CFD31F13DD36FE7A385B8681582EC3E8045 (void);
// 0x00000025 System.Void RectTransformExtensions::SetLeft(UnityEngine.RectTransform,System.Single)
extern void RectTransformExtensions_SetLeft_mA86C085FF57265E25F8732197C2FA7E4E3377F05 (void);
// 0x00000026 System.Void RectTransformExtensions::SetRight(UnityEngine.RectTransform,System.Single)
extern void RectTransformExtensions_SetRight_m41A7631DD85392C568598C634B73D1A7CA9C3BA8 (void);
// 0x00000027 System.Void RectTransformExtensions::SetTop(UnityEngine.RectTransform,System.Single)
extern void RectTransformExtensions_SetTop_m8E0112409A0B4AE8678D326E04149A7422CC0702 (void);
// 0x00000028 System.Void RectTransformExtensions::SetBottom(UnityEngine.RectTransform,System.Single)
extern void RectTransformExtensions_SetBottom_mD6AC62815DA40BDA0158DDBF0DBC8AFAF8B9C038 (void);
// 0x00000029 System.Void Static::GoHome()
extern void Static_GoHome_m6D69A14CE0782323AF25473BB2C7F8ECDD36F8F8 (void);
// 0x0000002A System.Void TrailController::Start()
extern void TrailController_Start_m5A542BA9759CF1B41A92B8F6F7AE5B6F495F74F3 (void);
// 0x0000002B System.Void TrailController::Update()
extern void TrailController_Update_m88A7B864DFEA4B080C12E83DB5B70D04D2CF485D (void);
// 0x0000002C System.Boolean TrailController::IsPointInPolygon(UnityEngine.Vector3,UnityEngine.Vector3[])
extern void TrailController_IsPointInPolygon_mDAC90F5177B04E2C48390664EAFD93992C7DB579 (void);
// 0x0000002D System.Void TrailController::checkButterfly(UnityEngine.GameObject[],System.Int32)
extern void TrailController_checkButterfly_m1F08704425A38FA76C9236C1272DC7F01481C2BF (void);
// 0x0000002E System.Void TrailController::RemoveButterfly()
extern void TrailController_RemoveButterfly_m80C2EA9777A0511B706FCC5A7633BC4984224BAB (void);
// 0x0000002F System.Boolean TrailController::IsPointInPolygon(UnityEngine.Vector3)
extern void TrailController_IsPointInPolygon_mC8047FCE999E70EDF4659F63AD51F50C2BBB53F4 (void);
// 0x00000030 System.Void TrailController::DestroyButterflies(System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern void TrailController_DestroyButterflies_m452106AF577C36D537DFB1F518B734A16A6E20A2 (void);
// 0x00000031 UnityEngine.Vector2 TrailController::Centroid(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern void TrailController_Centroid_m84A701BA17CBD8BCEC895D1ED6C5EAF2BABCB412 (void);
// 0x00000032 System.Void TrailController::.ctor()
extern void TrailController__ctor_m8976E0A734E926FF10177E49BCCC7B47CF152292 (void);
// 0x00000033 System.Void ZoomCinemachine::Start()
extern void ZoomCinemachine_Start_m4775EB7067403168E726C9D622C168F1551163C6 (void);
// 0x00000034 System.Void ZoomCinemachine::LateUpdate()
extern void ZoomCinemachine_LateUpdate_m7235E42B3C435C0D6195639F7849D48554A7EBC4 (void);
// 0x00000035 System.Void ZoomCinemachine::SetZoom(UnityEngine.Vector3)
extern void ZoomCinemachine_SetZoom_m47AD781064095E1EB1EC7CB2224E9F01FC6F5EF6 (void);
// 0x00000036 System.Void ZoomCinemachine::.ctor()
extern void ZoomCinemachine__ctor_m34444417E601E43B88B1E2B9802CF68BF796F253 (void);
// 0x00000037 System.Single AudioManager::get_BgmVolume()
extern void AudioManager_get_BgmVolume_m8140F51FBD5BBE8710FE4FE1374EE26101A01200 (void);
// 0x00000038 System.Void AudioManager::set_BgmVolume(System.Single)
extern void AudioManager_set_BgmVolume_m74CBC81EC4473DEE8C4967D5AF0F69895BF05F97 (void);
// 0x00000039 System.Single AudioManager::get_SfxVolume()
extern void AudioManager_get_SfxVolume_m501DA9D4F00EA0812B00C346D599E9D04F45E790 (void);
// 0x0000003A System.Void AudioManager::set_SfxVolume(System.Single)
extern void AudioManager_set_SfxVolume_m705E78912C92938CB707CE55A713A6C7019E3699 (void);
// 0x0000003B System.Single AudioManager::get_VoiceVolume()
extern void AudioManager_get_VoiceVolume_mE685AAAE24C1758EA6F12C44E8B8322EFF717A91 (void);
// 0x0000003C System.Void AudioManager::set_VoiceVolume(System.Single)
extern void AudioManager_set_VoiceVolume_mA304FB47D3C1159F82700D6C506CC23DFC96D8AA (void);
// 0x0000003D System.Void AudioManager::Save()
extern void AudioManager_Save_m24B6E6E3D9606F5F08E073D1AB4A35284F25A625 (void);
// 0x0000003E AudioManager AudioManager::get_instance()
extern void AudioManager_get_instance_m36FB14B490ECEFC3EBF150336F7547156DFB1B8D (void);
// 0x0000003F System.Void AudioManager::set_instance(AudioManager)
extern void AudioManager_set_instance_m666A0E6C0CF110F46FA9BF050EFFA22214C7D674 (void);
// 0x00000040 System.String AudioManager::get_sfxPath()
extern void AudioManager_get_sfxPath_mA2DE394D1B52A4A52739E44DFEF6285F56455326 (void);
// 0x00000041 System.String AudioManager::get_bgmPath()
extern void AudioManager_get_bgmPath_m77031744EC0B0B91BB74C8212A52E7EC4038E4F4 (void);
// 0x00000042 System.String AudioManager::get_voicePath()
extern void AudioManager_get_voicePath_m3B37191DC7C27B1EE8439E7125F4B67139707DFF (void);
// 0x00000043 UnityEngine.AudioSource AudioManager::get_voiceSource()
extern void AudioManager_get_voiceSource_m0C77294535A2A6148FAA94018486216874F95C34 (void);
// 0x00000044 System.Void AudioManager::PlayButtonTapSfx()
extern void AudioManager_PlayButtonTapSfx_m8C02B479A0A8EBA87FB7A587DD70857C1E03500B (void);
// 0x00000045 System.Void AudioManager::PlaySfx(System.String[],System.Single)
extern void AudioManager_PlaySfx_mECB229ADAD09D0FC58C66CB44AE86B40E3468910 (void);
// 0x00000046 System.Void AudioManager::PlaySfx(UnityEngine.AudioClip[],System.Single)
extern void AudioManager_PlaySfx_m457CD60AE7C52CD597283013FD8563435C099D11 (void);
// 0x00000047 System.Void AudioManager::PlaySfx(UnityEngine.AudioClip,System.Single)
extern void AudioManager_PlaySfx_mD31CC26EA8BE322EC579B25F93EA1FC0E1CD0C1A (void);
// 0x00000048 System.Void AudioManager::PlaySfx(System.String,System.Single,System.Boolean,System.Boolean)
extern void AudioManager_PlaySfx_m7D9D83919AE72DC8EE98660D89179EEFEE7D0B36 (void);
// 0x00000049 System.Void AudioManager::StopSfx()
extern void AudioManager_StopSfx_m6C2595527290283667844F899753040C255A3F21 (void);
// 0x0000004A System.Single AudioManager::GetLength(System.String)
extern void AudioManager_GetLength_m7B7202D1B105443E204B63B36D974CA4707D0456 (void);
// 0x0000004B System.Single AudioManager::PlayVoice(UnityEngine.AudioClip,System.Single)
extern void AudioManager_PlayVoice_m97C378E0638E4A3CD9D7A51F5F2357E8D7F33EBB (void);
// 0x0000004C System.Void AudioManager::PlayVoice(System.String,System.Single,System.Boolean)
extern void AudioManager_PlayVoice_m467D007A10EA8419C1F9F13F460398FFA404D061 (void);
// 0x0000004D System.Void AudioManager::SetVoicePitch(System.Single)
extern void AudioManager_SetVoicePitch_mD5194CAE37FC8314F0AD1464ACD850A40C4441DD (void);
// 0x0000004E System.Void AudioManager::PlayBgm(System.String,System.Single)
extern void AudioManager_PlayBgm_mEB3CA79BA839F3FEFEE42CE72E4471DF2E942F44 (void);
// 0x0000004F System.Void AudioManager::StopBgm(System.Boolean)
extern void AudioManager_StopBgm_m2C7CE9153A21D7864FAEBC8FEC69618F3477493C (void);
// 0x00000050 System.Void AudioManager::set_pauseBgm(System.Boolean)
extern void AudioManager_set_pauseBgm_mCA4EDC87EAEA46DCC0EAE56DD7BB8C19EF06825F (void);
// 0x00000051 System.Void AudioManager::CacheClip(UnityEngine.AudioClip,System.String)
extern void AudioManager_CacheClip_mF906258F36557F2C3A6656FA892B0FB7549D2717 (void);
// 0x00000052 System.Void AudioManager::CacheClip(System.String,System.String)
extern void AudioManager_CacheClip_mA4668FD6A1590F236D71F7381A3726B5E501AF19 (void);
// 0x00000053 System.Void AudioManager::CacheSfx(System.String[])
extern void AudioManager_CacheSfx_m2A00939A8DF74D286299246D60ADA03016C37069 (void);
// 0x00000054 System.Void AudioManager::CacheSfx(System.String)
extern void AudioManager_CacheSfx_mC5F8662B879D16BACBBB9E6768B1BB70906BCC7F (void);
// 0x00000055 System.Void AudioManager::ClearCacheClip(System.String,System.Boolean)
extern void AudioManager_ClearCacheClip_mDC5540C013E6A5664A484064980E0C68B33A930A (void);
// 0x00000056 System.Void AudioManager::ClearCacheClip()
extern void AudioManager_ClearCacheClip_mEA76D00816CD619A5687F307DA4CECE84021EBFC (void);
// 0x00000057 System.Void AudioManager::Fadeout()
extern void AudioManager_Fadeout_m0A4635CDA9DF5DF7391A540048AAE8B7C2144E74 (void);
// 0x00000058 System.Void AudioManager::Awake()
extern void AudioManager_Awake_mD7873A38A3ED577A313A05D24BF6511E59EDEC01 (void);
// 0x00000059 System.Void AudioManager::.ctor()
extern void AudioManager__ctor_m6C686441D1A1A223E4CF940A8EB0128535D603BD (void);
// 0x0000005A System.Void AudioManager::.cctor()
extern void AudioManager__cctor_mCE2018DCC9F41CB769EDFBC4D1C2221125848DED (void);
// 0x0000005B System.Void ButtonAudio::Awake()
extern void ButtonAudio_Awake_mF58556EAEC0A0472C8EDAEC0F02CD6A6D99009DD (void);
// 0x0000005C System.Void ButtonAudio::Play()
extern void ButtonAudio_Play_mB026D8C95FDBCB680A998A3EBDE1EF66CCAF98C3 (void);
// 0x0000005D System.Void ButtonAudio::.ctor()
extern void ButtonAudio__ctor_m2899BFF7C26853C5C5C617AA9333B53E43B58B69 (void);
// 0x0000005E System.Void ETFXProjectileScript::Start()
extern void ETFXProjectileScript_Start_mE8756011C883DDCF287343C6BF881B36B7ADB59C (void);
// 0x0000005F System.Void ETFXProjectileScript::FixedUpdate()
extern void ETFXProjectileScript_FixedUpdate_m959BFE684CC8CF77305ABF0C54256007023E2FFD (void);
// 0x00000060 System.Void ETFXProjectileScript::.ctor()
extern void ETFXProjectileScript__ctor_mDB3BFAED1E91A0DC99930E80169BF2F1EE26CFB5 (void);
// 0x00000061 System.Void ETFXSceneManager::LoadScene2DDemo()
extern void ETFXSceneManager_LoadScene2DDemo_m11E37C513C423B288C88C336A3F77E846AA6B358 (void);
// 0x00000062 System.Void ETFXSceneManager::LoadSceneCards()
extern void ETFXSceneManager_LoadSceneCards_m2374101144F9C56EB968B3A3B7BEACA75BB21C03 (void);
// 0x00000063 System.Void ETFXSceneManager::LoadSceneCombat()
extern void ETFXSceneManager_LoadSceneCombat_mDA5B85860C1746C93E4D81CE992E7C4887D89E0C (void);
// 0x00000064 System.Void ETFXSceneManager::LoadSceneDecals()
extern void ETFXSceneManager_LoadSceneDecals_m0D919CCFC190AAB40C20D990A8DEBBDF79075F2E (void);
// 0x00000065 System.Void ETFXSceneManager::LoadSceneDecals2()
extern void ETFXSceneManager_LoadSceneDecals2_mEFA6A5777999D5F3D87E1E8226A361880433DCF6 (void);
// 0x00000066 System.Void ETFXSceneManager::LoadSceneEmojis()
extern void ETFXSceneManager_LoadSceneEmojis_m01E3A7533C8A4D8CF6115E0EE1DC687455AB4D4E (void);
// 0x00000067 System.Void ETFXSceneManager::LoadSceneEmojis2()
extern void ETFXSceneManager_LoadSceneEmojis2_m6005349247D781727A558BA01D2085C893AF6438 (void);
// 0x00000068 System.Void ETFXSceneManager::LoadSceneExplosions()
extern void ETFXSceneManager_LoadSceneExplosions_m1984686B3E5CE79E49BDDA11BDD09596F8E7C42E (void);
// 0x00000069 System.Void ETFXSceneManager::LoadSceneExplosions2()
extern void ETFXSceneManager_LoadSceneExplosions2_mB04546364A96740B6CA14B50B2DCC3E80C28CF90 (void);
// 0x0000006A System.Void ETFXSceneManager::LoadSceneFire()
extern void ETFXSceneManager_LoadSceneFire_m9E31C2B57B979A46E2C6F727C858947CF9C0EA19 (void);
// 0x0000006B System.Void ETFXSceneManager::LoadSceneFire2()
extern void ETFXSceneManager_LoadSceneFire2_mEAE89BF11E50292B18F69A327923ED3A8B652256 (void);
// 0x0000006C System.Void ETFXSceneManager::LoadSceneFire3()
extern void ETFXSceneManager_LoadSceneFire3_mD9919F751EC9CFEF0D95691414A042A22B55C831 (void);
// 0x0000006D System.Void ETFXSceneManager::LoadSceneFireworks()
extern void ETFXSceneManager_LoadSceneFireworks_m2827DAD7FCC3996841900263F223F5201E8A3C43 (void);
// 0x0000006E System.Void ETFXSceneManager::LoadSceneFlares()
extern void ETFXSceneManager_LoadSceneFlares_mD740C35453DD10F374658441ED58411126845D38 (void);
// 0x0000006F System.Void ETFXSceneManager::LoadSceneMagic()
extern void ETFXSceneManager_LoadSceneMagic_m225C27B1375FCB1B4C215A3D67BDF33C01BDE04E (void);
// 0x00000070 System.Void ETFXSceneManager::LoadSceneMagic2()
extern void ETFXSceneManager_LoadSceneMagic2_m08E7D2EF64DD9F9534B7525E3905324264DCAAC7 (void);
// 0x00000071 System.Void ETFXSceneManager::LoadSceneMagic3()
extern void ETFXSceneManager_LoadSceneMagic3_m58FC3634AA94E283420B22E080D9E91A2A52A737 (void);
// 0x00000072 System.Void ETFXSceneManager::LoadSceneMainDemo()
extern void ETFXSceneManager_LoadSceneMainDemo_m030D3019FECBBAF948E3FAC03B7229FAAF0839C4 (void);
// 0x00000073 System.Void ETFXSceneManager::LoadSceneMissiles()
extern void ETFXSceneManager_LoadSceneMissiles_mEE0267B462F61EC2B7BD88460F69CEE7139A6E8D (void);
// 0x00000074 System.Void ETFXSceneManager::LoadScenePortals()
extern void ETFXSceneManager_LoadScenePortals_m4932DCDC59F52F6B75F7E00FE384C15CAACD4371 (void);
// 0x00000075 System.Void ETFXSceneManager::LoadScenePortals2()
extern void ETFXSceneManager_LoadScenePortals2_mE5701BAD231A3926A5F3DD95E7FB029E9378C89C (void);
// 0x00000076 System.Void ETFXSceneManager::LoadScenePowerups()
extern void ETFXSceneManager_LoadScenePowerups_m49AE6A1640136507D16DFF611DC78B4CDA28FEC2 (void);
// 0x00000077 System.Void ETFXSceneManager::LoadScenePowerups2()
extern void ETFXSceneManager_LoadScenePowerups2_m548B7BFF09B8A133EF48FE2B130A91D796EC6A33 (void);
// 0x00000078 System.Void ETFXSceneManager::LoadSceneSparkles()
extern void ETFXSceneManager_LoadSceneSparkles_m9463E60A6ED35D9897EEEF18D7B94880359B75A5 (void);
// 0x00000079 System.Void ETFXSceneManager::LoadSceneSwordCombat()
extern void ETFXSceneManager_LoadSceneSwordCombat_m2040FBE4FE83D2256F5208F20D5A0A817472052E (void);
// 0x0000007A System.Void ETFXSceneManager::LoadSceneSwordCombat2()
extern void ETFXSceneManager_LoadSceneSwordCombat2_m3C8D0447A800B6C4616D4CCE669711CC6236C13A (void);
// 0x0000007B System.Void ETFXSceneManager::LoadSceneMoney()
extern void ETFXSceneManager_LoadSceneMoney_mE3B3B3433BE6C2E37B56CCF7CF0ACA0AF274B795 (void);
// 0x0000007C System.Void ETFXSceneManager::LoadSceneHealing()
extern void ETFXSceneManager_LoadSceneHealing_m73F368066D2E63E359E36079EB20A4B51006BA56 (void);
// 0x0000007D System.Void ETFXSceneManager::LoadSceneWind()
extern void ETFXSceneManager_LoadSceneWind_mB2FF2390377FF263B5ACAEE93DD389C9D666BE2E (void);
// 0x0000007E System.Void ETFXSceneManager::Update()
extern void ETFXSceneManager_Update_mB2FB561ADC4764627A10DECF88F6C3F17891B9C1 (void);
// 0x0000007F System.Void ETFXSceneManager::.ctor()
extern void ETFXSceneManager__ctor_mC6363CFDCD6B6646152AA5FEC471088DFC20367A (void);
// 0x00000080 System.Void PEButtonScript::Start()
extern void PEButtonScript_Start_mED60465004404B45D9A5BE8129015C39394C9DC0 (void);
// 0x00000081 System.Void PEButtonScript::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void PEButtonScript_OnPointerEnter_m89340DB88B2D0EF0CDD762F27C1EB455A6876CB6 (void);
// 0x00000082 System.Void PEButtonScript::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void PEButtonScript_OnPointerExit_m8F731E714619F43F056DD2D4E27A24B5B045774C (void);
// 0x00000083 System.Void PEButtonScript::OnButtonClicked()
extern void PEButtonScript_OnButtonClicked_m8D7DB17EB5C69D54E2F95931AB6C90A7490159B2 (void);
// 0x00000084 System.Void PEButtonScript::.ctor()
extern void PEButtonScript__ctor_mBFA614FD383BF4104DE7ADEE7AD88A91AC8083D6 (void);
// 0x00000085 System.Void ParticleEffectsLibrary::Awake()
extern void ParticleEffectsLibrary_Awake_mDC6AFDB7CD4E3DA4C5163BFDDCC04A76EED147CE (void);
// 0x00000086 System.Void ParticleEffectsLibrary::Start()
extern void ParticleEffectsLibrary_Start_m82B0B1F30A8C0E6B1060FEEC21C5280500D7F771 (void);
// 0x00000087 System.String ParticleEffectsLibrary::GetCurrentPENameString()
extern void ParticleEffectsLibrary_GetCurrentPENameString_m067B9C792580A9DFD2DE2B4318AEC99254BF5285 (void);
// 0x00000088 System.Void ParticleEffectsLibrary::PreviousParticleEffect()
extern void ParticleEffectsLibrary_PreviousParticleEffect_m4A6BB63833B9C4D91B2F2801741C731C8174836E (void);
// 0x00000089 System.Void ParticleEffectsLibrary::NextParticleEffect()
extern void ParticleEffectsLibrary_NextParticleEffect_mD095F239D4054E69278390CA3A404D033F8A658E (void);
// 0x0000008A System.Void ParticleEffectsLibrary::SpawnParticleEffect(UnityEngine.Vector3)
extern void ParticleEffectsLibrary_SpawnParticleEffect_m17AE1DA6F799AFF7DE1C64DC0ECF3D716E22355A (void);
// 0x0000008B System.Void ParticleEffectsLibrary::.ctor()
extern void ParticleEffectsLibrary__ctor_m777070C2175441BEB93AFF11748BC74588651C7B (void);
// 0x0000008C System.Void UICanvasManager::Awake()
extern void UICanvasManager_Awake_m8D84AB004D6FEEBB3BDC5FBB9C5101B75ACDC396 (void);
// 0x0000008D System.Void UICanvasManager::Start()
extern void UICanvasManager_Start_m9652DFED3C5A6CD686ABFB2D77A474270D5F7656 (void);
// 0x0000008E System.Void UICanvasManager::Update()
extern void UICanvasManager_Update_mA2771AB72770EC84D9D24FA819E57BB122AD7FC0 (void);
// 0x0000008F System.Void UICanvasManager::UpdateToolTip(ButtonTypes)
extern void UICanvasManager_UpdateToolTip_m13510C3590867DC2E5B2F8D45E0BA522479103D7 (void);
// 0x00000090 System.Void UICanvasManager::ClearToolTip()
extern void UICanvasManager_ClearToolTip_mFA69FDBDB5CADD0F18A31D1AC407D38D81F2B3BF (void);
// 0x00000091 System.Void UICanvasManager::SelectPreviousPE()
extern void UICanvasManager_SelectPreviousPE_m88ABF3CB63D07DBBEFCAE908864103FF09C21E30 (void);
// 0x00000092 System.Void UICanvasManager::SelectNextPE()
extern void UICanvasManager_SelectNextPE_m5838FA28DBEEC3DFF2A84E671842B86C366A2367 (void);
// 0x00000093 System.Void UICanvasManager::SpawnCurrentParticleEffect()
extern void UICanvasManager_SpawnCurrentParticleEffect_m8E4B86D6B40EBB353D2EA0D33B22785FB3A55B1B (void);
// 0x00000094 System.Void UICanvasManager::UIButtonClick(ButtonTypes)
extern void UICanvasManager_UIButtonClick_m0BE04EA2C0BA8E35CE5B22BFF4CBD4E9128B3CE4 (void);
// 0x00000095 System.Void UICanvasManager::.ctor()
extern void UICanvasManager__ctor_m8BDA3D4D6FD64344E6CCBEF6F7B314DEE7180E22 (void);
// 0x00000096 System.Void MgAnimation::Awake()
extern void MgAnimation_Awake_m5B4A5D1CC62CA757F225BDE2638BC2B680E93E5C (void);
// 0x00000097 System.Void MgAnimation::OnDisable()
extern void MgAnimation_OnDisable_m1A85B1EB5716A14A49AAAF1CDE1AFEB64377767D (void);
// 0x00000098 System.Void MgAnimation::Show()
extern void MgAnimation_Show_mD2031973358F61C21D5DD997588BF30A83E6A0B6 (void);
// 0x00000099 System.Void MgAnimation::Hide(System.Boolean)
extern void MgAnimation_Hide_m7FA361F9FBCBEF105C8136DCF5E9B7D6760AF333 (void);
// 0x0000009A System.Void MgAnimation::.ctor()
extern void MgAnimation__ctor_m07038ED8FAC63A68B4606E49DC4C636EDB584001 (void);
// 0x0000009B System.Void MgAnimationFade::Show()
extern void MgAnimationFade_Show_m5F746CDC4D6B21AA1CF31412775E3FA9FA237C88 (void);
// 0x0000009C System.Void MgAnimationFade::Hide(System.Boolean)
extern void MgAnimationFade_Hide_mEAFE957483B4B6B08053263B95D897A2B1BB4149 (void);
// 0x0000009D System.Void MgAnimationFade::.ctor()
extern void MgAnimationFade__ctor_mC758352CB7A96D237717ACD30B4E06B50A85F6E6 (void);
// 0x0000009E System.Void MgAnimationLink::Show()
extern void MgAnimationLink_Show_m99FCD8504556358C90525767D3A3AAC24E73E653 (void);
// 0x0000009F System.Collections.IEnumerator MgAnimationLink::CoShow()
extern void MgAnimationLink_CoShow_mE5861550ECA367E4BACE78F17D99DF226500D8CC (void);
// 0x000000A0 System.Void MgAnimationLink::Hide(System.Boolean)
extern void MgAnimationLink_Hide_m8F3950BA24B56DD4419D01FFE367FC966C822A66 (void);
// 0x000000A1 System.Collections.IEnumerator MgAnimationLink::CoHide()
extern void MgAnimationLink_CoHide_m8A0E7D3AD3E93E482FC47FE0DC472A5A5286538B (void);
// 0x000000A2 System.Void MgAnimationLink::.ctor()
extern void MgAnimationLink__ctor_m9A186DAFC787888B8FBB55DF5D2FC0DDCCF00693 (void);
// 0x000000A3 System.Void MgAnimationLink/<CoShow>d__4::.ctor(System.Int32)
extern void U3CCoShowU3Ed__4__ctor_m554EB8574D3EC74588B8C229FEA1C0B0A291546F (void);
// 0x000000A4 System.Void MgAnimationLink/<CoShow>d__4::System.IDisposable.Dispose()
extern void U3CCoShowU3Ed__4_System_IDisposable_Dispose_m07C82A5E84196EA1B1972E780C6DD1FE46574BC2 (void);
// 0x000000A5 System.Boolean MgAnimationLink/<CoShow>d__4::MoveNext()
extern void U3CCoShowU3Ed__4_MoveNext_m8E62859902229C59605109F4A8D8458C57992FA6 (void);
// 0x000000A6 System.Object MgAnimationLink/<CoShow>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCoShowU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC9030BE2687FCFE6F576F56732CE39EEC045140B (void);
// 0x000000A7 System.Void MgAnimationLink/<CoShow>d__4::System.Collections.IEnumerator.Reset()
extern void U3CCoShowU3Ed__4_System_Collections_IEnumerator_Reset_m964546A98807BDFDDFB6F9B948CFEA111004BC87 (void);
// 0x000000A8 System.Object MgAnimationLink/<CoShow>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CCoShowU3Ed__4_System_Collections_IEnumerator_get_Current_m5F2C6965EC88D18494A8D38BDD4716692853BB18 (void);
// 0x000000A9 System.Void MgAnimationLink/<CoHide>d__6::.ctor(System.Int32)
extern void U3CCoHideU3Ed__6__ctor_m4090DB038FFA544ADA13CD42C5846426F206DC1F (void);
// 0x000000AA System.Void MgAnimationLink/<CoHide>d__6::System.IDisposable.Dispose()
extern void U3CCoHideU3Ed__6_System_IDisposable_Dispose_m6FDB5A58039591E3210957C83AE99716A836B3D8 (void);
// 0x000000AB System.Boolean MgAnimationLink/<CoHide>d__6::MoveNext()
extern void U3CCoHideU3Ed__6_MoveNext_m42BF49D8A3A3801ED73880017FE6277A12528643 (void);
// 0x000000AC System.Object MgAnimationLink/<CoHide>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCoHideU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE3180BCF97A6E0B69FEC3031203639BF46203B01 (void);
// 0x000000AD System.Void MgAnimationLink/<CoHide>d__6::System.Collections.IEnumerator.Reset()
extern void U3CCoHideU3Ed__6_System_Collections_IEnumerator_Reset_m08C249CB2DAB83DFC80C43F0AADBF65BEF26E138 (void);
// 0x000000AE System.Object MgAnimationLink/<CoHide>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CCoHideU3Ed__6_System_Collections_IEnumerator_get_Current_mAC9F050CAA3A19D7A27B8D7BEFC50B0F77039748 (void);
// 0x000000AF System.Void MgAnimationLinkObject::Show()
extern void MgAnimationLinkObject_Show_mFC203AD9ECE02BF200860EA7C732C016BB9FBDE9 (void);
// 0x000000B0 System.Collections.IEnumerator MgAnimationLinkObject::CoShow()
extern void MgAnimationLinkObject_CoShow_mDC6098417478F81E119F7A1ED3CFF28504F10B8C (void);
// 0x000000B1 System.Void MgAnimationLinkObject::Hide(System.Boolean)
extern void MgAnimationLinkObject_Hide_mD7454B93B42F63613CC0D2629A4D7C2D8BBAC380 (void);
// 0x000000B2 System.Collections.IEnumerator MgAnimationLinkObject::CoHide()
extern void MgAnimationLinkObject_CoHide_m343BB45CFA8555A43752D7CC98DBD9AA14F2FF2C (void);
// 0x000000B3 System.Void MgAnimationLinkObject::.ctor()
extern void MgAnimationLinkObject__ctor_m2AFD544FB91CC91D4942793F913F61354B8671A9 (void);
// 0x000000B4 System.Void MgAnimationLinkObject/<CoShow>d__4::.ctor(System.Int32)
extern void U3CCoShowU3Ed__4__ctor_m1726143C8B21533706114E0E90EFA33FAC5D33DA (void);
// 0x000000B5 System.Void MgAnimationLinkObject/<CoShow>d__4::System.IDisposable.Dispose()
extern void U3CCoShowU3Ed__4_System_IDisposable_Dispose_mBD83DECBF938630809CFC6823D1BBF2287B14AF1 (void);
// 0x000000B6 System.Boolean MgAnimationLinkObject/<CoShow>d__4::MoveNext()
extern void U3CCoShowU3Ed__4_MoveNext_m792E5483DA82469F94CE7BD373C9A6EF16E1F899 (void);
// 0x000000B7 System.Object MgAnimationLinkObject/<CoShow>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCoShowU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6AA628E39ECD848EB5417D012CCABAA563FAC3EB (void);
// 0x000000B8 System.Void MgAnimationLinkObject/<CoShow>d__4::System.Collections.IEnumerator.Reset()
extern void U3CCoShowU3Ed__4_System_Collections_IEnumerator_Reset_mA33FD166C110C0853517FB614C738B06DEC9520D (void);
// 0x000000B9 System.Object MgAnimationLinkObject/<CoShow>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CCoShowU3Ed__4_System_Collections_IEnumerator_get_Current_mC02634706ABCCA9E4F9C808BE0475E9042DA01AF (void);
// 0x000000BA System.Void MgAnimationLinkObject/<CoHide>d__6::.ctor(System.Int32)
extern void U3CCoHideU3Ed__6__ctor_m1B9365AD609078A363564D5938D192A89625AF71 (void);
// 0x000000BB System.Void MgAnimationLinkObject/<CoHide>d__6::System.IDisposable.Dispose()
extern void U3CCoHideU3Ed__6_System_IDisposable_Dispose_mBD6FC0B545911FAB141FC358F1C67688DEA32191 (void);
// 0x000000BC System.Boolean MgAnimationLinkObject/<CoHide>d__6::MoveNext()
extern void U3CCoHideU3Ed__6_MoveNext_mB97D9FCB8066EBD7EF130FC72365FB6536C92A36 (void);
// 0x000000BD System.Object MgAnimationLinkObject/<CoHide>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCoHideU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43DF0B545FB7DD89B2572D0705D2F83C4F05CF9D (void);
// 0x000000BE System.Void MgAnimationLinkObject/<CoHide>d__6::System.Collections.IEnumerator.Reset()
extern void U3CCoHideU3Ed__6_System_Collections_IEnumerator_Reset_m304762ABA63837D8B6483A489789E6061CBAA982 (void);
// 0x000000BF System.Object MgAnimationLinkObject/<CoHide>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CCoHideU3Ed__6_System_Collections_IEnumerator_get_Current_m7F143AAD687B3EE3B6309FBF1FAB3E547A681355 (void);
// 0x000000C0 System.Void MgAnimationMove::Show()
extern void MgAnimationMove_Show_m49B01EC48EA1DFE17CF55BEF0712F4DA7E09AA3D (void);
// 0x000000C1 System.Void MgAnimationMove::Hide(System.Boolean)
extern void MgAnimationMove_Hide_m7BD03283D712D327B22BAFB712806F83B7559DA9 (void);
// 0x000000C2 System.Void MgAnimationMove::Move()
extern void MgAnimationMove_Move_m6CBF19B2EDDAE9F2C14E95CB1A43D83F0DF05B65 (void);
// 0x000000C3 System.Void MgAnimationMove::SetStart(UnityEngine.Vector3)
extern void MgAnimationMove_SetStart_m545D1B0A99469A5AF1BCE31349160E941B47BA24 (void);
// 0x000000C4 System.Void MgAnimationMove::SetEnd(UnityEngine.Vector3)
extern void MgAnimationMove_SetEnd_m1465522316BACBC965434DE4DB74CFE7B4431F7A (void);
// 0x000000C5 System.Void MgAnimationMove::.ctor()
extern void MgAnimationMove__ctor_mB525943200881949DDF9515837FDE64AA1ED49B5 (void);
// 0x000000C6 System.Void MgAnimationPScaleLoop::Show()
extern void MgAnimationPScaleLoop_Show_m9B8A5D1E964A0037838C6E16DA6109C368A94435 (void);
// 0x000000C7 System.Void MgAnimationPScaleLoop::Hide(System.Boolean)
extern void MgAnimationPScaleLoop_Hide_mF7991768A2390FCD35B9B63E511151CD8FD1CB75 (void);
// 0x000000C8 System.Void MgAnimationPScaleLoop::LoopAnim()
extern void MgAnimationPScaleLoop_LoopAnim_m0656BED0FED06007A0F94B9CD48AD58DED967C0B (void);
// 0x000000C9 System.Void MgAnimationPScaleLoop::Update()
extern void MgAnimationPScaleLoop_Update_mCAC0979BAFDC2CFC41E99DA3EAF003089CE8DCD1 (void);
// 0x000000CA System.Void MgAnimationPScaleLoop::.ctor()
extern void MgAnimationPScaleLoop__ctor_mEF23296C56380A71AD8F54381553AD604FF45858 (void);
// 0x000000CB System.Void MgAnimationPScaleLoop::<LoopAnim>b__5_0()
extern void MgAnimationPScaleLoop_U3CLoopAnimU3Eb__5_0_m62EB2B13EFD62CF7C44BFEFA5FE44EF1ADAC7292 (void);
// 0x000000CC System.Void MgAnimationScale::Show()
extern void MgAnimationScale_Show_m4F8BE69CF8E06FC561D30B901DC307432B225D65 (void);
// 0x000000CD System.Void MgAnimationScale::Hide(System.Boolean)
extern void MgAnimationScale_Hide_mBBAA096748C05F4C4C447EB6295B52C5B093B643 (void);
// 0x000000CE System.Void MgAnimationScale::.ctor()
extern void MgAnimationScale__ctor_mA80B2D7C14E49B0DC02D05136BC7AB87340CD4F9 (void);
// 0x000000CF System.Void MgAnimationSpine::.ctor()
extern void MgAnimationSpine__ctor_m81118013A294643467B38C06C6AF48C5AC34B52F (void);
// 0x000000D0 System.Void MgAnimationUnity::Awake()
extern void MgAnimationUnity_Awake_m4B08BDF238A2D17273A0A5DC7F0676E8A42642A8 (void);
// 0x000000D1 System.Void MgAnimationUnity::Show()
extern void MgAnimationUnity_Show_m836DA71E2742371AD7F5FC12C8280C771471F202 (void);
// 0x000000D2 System.Collections.IEnumerator MgAnimationUnity::CoShow()
extern void MgAnimationUnity_CoShow_m45D03ACE03ADF8509F5F30851DF9333DE68A0B87 (void);
// 0x000000D3 System.Void MgAnimationUnity::Hide(System.Boolean)
extern void MgAnimationUnity_Hide_m06EBB32C46C5AC20658F3724669EC17DF56DCE1D (void);
// 0x000000D4 System.Void MgAnimationUnity::.ctor()
extern void MgAnimationUnity__ctor_m6F56C44A408DA9255F0B94FAED0B871A6EB85A56 (void);
// 0x000000D5 System.Void MgAnimationUnity/<CoShow>d__6::.ctor(System.Int32)
extern void U3CCoShowU3Ed__6__ctor_mFFA3E76623E10E9F668CA1CBF411F626D53964A1 (void);
// 0x000000D6 System.Void MgAnimationUnity/<CoShow>d__6::System.IDisposable.Dispose()
extern void U3CCoShowU3Ed__6_System_IDisposable_Dispose_m0072088E4B8ED1D71D8EA1F6AEB9FAF65095113B (void);
// 0x000000D7 System.Boolean MgAnimationUnity/<CoShow>d__6::MoveNext()
extern void U3CCoShowU3Ed__6_MoveNext_m55FBF7C7C23CB36B51D4FE92DF4BF6BDD2D5BFCF (void);
// 0x000000D8 System.Object MgAnimationUnity/<CoShow>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCoShowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72E89C7F88FFDBF545B5F398A67CA5515106A0BB (void);
// 0x000000D9 System.Void MgAnimationUnity/<CoShow>d__6::System.Collections.IEnumerator.Reset()
extern void U3CCoShowU3Ed__6_System_Collections_IEnumerator_Reset_m19937A01FE7B527C7B7DE39A1360330701041EC5 (void);
// 0x000000DA System.Object MgAnimationUnity/<CoShow>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CCoShowU3Ed__6_System_Collections_IEnumerator_get_Current_mC04A44C677D01377DA40F515F01E51814858CD9D (void);
// 0x000000DB System.Void MgAnimationWiggle::.ctor()
extern void MgAnimationWiggle__ctor_m209FCF71DD6D763AA0659D123374349081282FFC (void);
// 0x000000DC System.Void MgObject::Awake()
extern void MgObject_Awake_mD07D8FFABEECA4AB456A97C1EEC44E4E8DE43437 (void);
// 0x000000DD System.Void MgObject::OnDisable()
extern void MgObject_OnDisable_m12F36A7F3310E34BA37B299976CE30B951313EC4 (void);
// 0x000000DE System.Void MgObject::FindAnims()
extern void MgObject_FindAnims_m92D66820112ABA64BE1070B401665F22EDBFC4EA (void);
// 0x000000DF System.Void MgObject::FindButtonTargetGraphic()
extern void MgObject_FindButtonTargetGraphic_m7DEDE39EC2D0924828C8DAA39B528C7A3DA741A3 (void);
// 0x000000E0 System.Void MgObject::Show()
extern void MgObject_Show_mF9DEBC79B916777A8AF07B1A44338476EB7575C9 (void);
// 0x000000E1 System.Void MgObject::Hide(System.Boolean)
extern void MgObject_Hide_m62A42590BE451A413B2122C29FF01552F0F65C42 (void);
// 0x000000E2 System.Void MgObject::Move()
extern void MgObject_Move_m2882FA32D74BC45D458E0AC200F43B996E3A9799 (void);
// 0x000000E3 System.Void MgObject::Shake()
extern void MgObject_Shake_m6EBEB8C986F0C6C39D676AC6D5262A9F69F89EC3 (void);
// 0x000000E4 System.Void MgObject::PunchScale()
extern void MgObject_PunchScale_mC8F5951DD64927B38BA0C56203F441C8289557BE (void);
// 0x000000E5 System.Void MgObject::.ctor()
extern void MgObject__ctor_m5E8C46DCCBC211060CF1006165EAE244D28BA71A (void);
// 0x000000E6 System.Void MgObject::<Shake>b__10_0()
extern void MgObject_U3CShakeU3Eb__10_0_mCA287CDA1850D00060EE3FCAF67CAF86CB310910 (void);
// 0x000000E7 System.Void MgObject::<PunchScale>b__11_0()
extern void MgObject_U3CPunchScaleU3Eb__11_0_m523F3F64B37548FC411AF2A549713B8806752E3F (void);
// 0x000000E8 System.Boolean PlayerPrefsUtility::IsEncryptedKey(System.String)
extern void PlayerPrefsUtility_IsEncryptedKey_m153D18311A0A907C56DBD065234CFDDEDF0504DE (void);
// 0x000000E9 System.String PlayerPrefsUtility::DecryptKey(System.String)
extern void PlayerPrefsUtility_DecryptKey_m0C8668431AF36978F9949F007361ACD61A0F5E39 (void);
// 0x000000EA System.Void PlayerPrefsUtility::SetEncryptedFloat(System.String,System.Single)
extern void PlayerPrefsUtility_SetEncryptedFloat_m8262DCCF487E79A7609D5DC6DBB2B2CBD90AEAF4 (void);
// 0x000000EB System.Void PlayerPrefsUtility::SetEncryptedInt(System.String,System.Int32)
extern void PlayerPrefsUtility_SetEncryptedInt_m5D8068FE27C12A16FCC0436786E1D91D12393794 (void);
// 0x000000EC System.Void PlayerPrefsUtility::SetEncryptedString(System.String,System.String)
extern void PlayerPrefsUtility_SetEncryptedString_mC765E9F22085FAD92EA1C987D81374E56A2932E1 (void);
// 0x000000ED System.Void PlayerPrefsUtility::SetEncryptedBool(System.String,System.Boolean)
extern void PlayerPrefsUtility_SetEncryptedBool_m916E9BBF325F84B32F0D190C00D40EA2F4B1EACE (void);
// 0x000000EE System.Object PlayerPrefsUtility::GetEncryptedValue(System.String,System.String)
extern void PlayerPrefsUtility_GetEncryptedValue_m9EEB67190666A392E5AA90BE93C11BB908C73067 (void);
// 0x000000EF System.Single PlayerPrefsUtility::GetEncryptedFloat(System.String,System.Single)
extern void PlayerPrefsUtility_GetEncryptedFloat_mB95681423A11D6E853A8470CA26E4C9922A0103D (void);
// 0x000000F0 System.Int32 PlayerPrefsUtility::GetEncryptedInt(System.String,System.Int32)
extern void PlayerPrefsUtility_GetEncryptedInt_mD36FE4990A860C11AE673EAFE6A4ECDED035A808 (void);
// 0x000000F1 System.String PlayerPrefsUtility::GetEncryptedString(System.String,System.String)
extern void PlayerPrefsUtility_GetEncryptedString_mFA30471895C42226DFE0AF1FBDF5B79F1970BF6D (void);
// 0x000000F2 System.Boolean PlayerPrefsUtility::GetEncryptedBool(System.String,System.Boolean)
extern void PlayerPrefsUtility_GetEncryptedBool_mE7846072D3313D290C8D0CAD167E9913B0D316BF (void);
// 0x000000F3 System.Void PlayerPrefsUtility::SetBool(System.String,System.Boolean)
extern void PlayerPrefsUtility_SetBool_m4A72E1C6139F903A68D36344541E50D033E9A100 (void);
// 0x000000F4 System.Boolean PlayerPrefsUtility::GetBool(System.String,System.Boolean)
extern void PlayerPrefsUtility_GetBool_m08778496D06A242626F7E986555649193CDB6D5A (void);
// 0x000000F5 System.Void PlayerPrefsUtility::SetEnum(System.String,System.Enum)
extern void PlayerPrefsUtility_SetEnum_mF5B172507AC0E48E54B8C0DCD3E1C84F70B0197C (void);
// 0x000000F6 T PlayerPrefsUtility::GetEnum(System.String,T)
// 0x000000F7 System.Object PlayerPrefsUtility::GetEnum(System.String,System.Type,System.Object)
extern void PlayerPrefsUtility_GetEnum_mCDE3318023FDE7ED73B4DA5D27DE75D292BB9AC1 (void);
// 0x000000F8 System.Void PlayerPrefsUtility::SetDateTime(System.String,System.DateTime)
extern void PlayerPrefsUtility_SetDateTime_m6623F3E91B0BA4D31420B3FA6D00ED67F165E903 (void);
// 0x000000F9 System.DateTime PlayerPrefsUtility::GetDateTime(System.String,System.DateTime)
extern void PlayerPrefsUtility_GetDateTime_m35A3CA929E76A58B7567F5FDF408FD9E65E3BA5A (void);
// 0x000000FA System.Void PlayerPrefsUtility::SetTimeSpan(System.String,System.TimeSpan)
extern void PlayerPrefsUtility_SetTimeSpan_mD053352AE057FD02671A5A550D961CF2AB1BA712 (void);
// 0x000000FB System.TimeSpan PlayerPrefsUtility::GetTimeSpan(System.String,System.TimeSpan)
extern void PlayerPrefsUtility_GetTimeSpan_m219D47B173B4B461A396F010218647AA807935C0 (void);
// 0x000000FC System.Void DontChangeCanvasCamera::Start()
extern void DontChangeCanvasCamera_Start_m59AFBE23EB40A50C5FBCBC8727E85BE5E71E6853 (void);
// 0x000000FD System.Void DontChangeCanvasCamera::Update()
extern void DontChangeCanvasCamera_Update_m306EBCF45B877244DA19B7D899B669625F3956A7 (void);
// 0x000000FE System.Void DontChangeCanvasCamera::.ctor()
extern void DontChangeCanvasCamera__ctor_m31C20B947EE72E5AE4F6714C4FC821A044E4B255 (void);
// 0x000000FF System.String DGameController::SceneName()
extern void DGameController_SceneName_mF56A5F407051FBFA507661A9F8E64D07356EA81A (void);
// 0x00000100 System.Void DGameController::OnActive(System.Object)
extern void DGameController_OnActive_mD07AE3534DC097B571C77E09DD7DACB03CAD8C90 (void);
// 0x00000101 System.Void DGameController::OnShown()
extern void DGameController_OnShown_mC14E85EDAC2530D4D97B6999D8C434CBA5C7ABF7 (void);
// 0x00000102 System.Void DGameController::OnHidden()
extern void DGameController_OnHidden_m8DB8802D9EEE965744CB9FBD398F983F79ADCBC7 (void);
// 0x00000103 System.Void DGameController::OnButtonTap()
extern void DGameController_OnButtonTap_m30F012ECDFB8F5ECB3B4E8BC67F40D65BEE79F84 (void);
// 0x00000104 System.Collections.IEnumerator DGameController::LoadingToTop()
extern void DGameController_LoadingToTop_m16D87897ACBBDCDB17EA184E1CD158D8EDF252FC (void);
// 0x00000105 System.Void DGameController::.ctor()
extern void DGameController__ctor_mCBA872D897A7AE21006509EB6DEA313CACE78482 (void);
// 0x00000106 System.Void DGameController/<LoadingToTop>d__6::.ctor(System.Int32)
extern void U3CLoadingToTopU3Ed__6__ctor_m4DF50C93EAB6A04080FF76FE71AFB1D9E2C85569 (void);
// 0x00000107 System.Void DGameController/<LoadingToTop>d__6::System.IDisposable.Dispose()
extern void U3CLoadingToTopU3Ed__6_System_IDisposable_Dispose_m55B486431095FC8199CE1CC08EDC327E83B15BC0 (void);
// 0x00000108 System.Boolean DGameController/<LoadingToTop>d__6::MoveNext()
extern void U3CLoadingToTopU3Ed__6_MoveNext_mBEFD615067F5B44C94300159F5D256A24388B7CC (void);
// 0x00000109 System.Object DGameController/<LoadingToTop>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadingToTopU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m359167E9C986E70CB8901CA63CBE7C418B2F3A7D (void);
// 0x0000010A System.Void DGameController/<LoadingToTop>d__6::System.Collections.IEnumerator.Reset()
extern void U3CLoadingToTopU3Ed__6_System_Collections_IEnumerator_Reset_mFBBF4C2124B15D0F6FF9516AB15AB66D50A53879 (void);
// 0x0000010B System.Object DGameController/<LoadingToTop>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CLoadingToTopU3Ed__6_System_Collections_IEnumerator_get_Current_m0D7A10CDEA3F5BB3B9B90102ED2E62EB48B600EB (void);
// 0x0000010C System.String DLoadingController::SceneName()
extern void DLoadingController_SceneName_mC4CB22DDDB72522096D0A132CB8CFCB5180382F6 (void);
// 0x0000010D System.Void DLoadingController::.ctor()
extern void DLoadingController__ctor_mFC74F6C16562E76956E5969A63ECE80117938F5C (void);
// 0x0000010E System.Collections.IEnumerator DMain::Start()
extern void DMain_Start_mCB88609F3C58BD213EAC9D1203B1A69610179042 (void);
// 0x0000010F System.Void DMain::.ctor()
extern void DMain__ctor_mEEB7DA9FC2BFC12D2FEF488BBB7BDAC65E4D4A3A (void);
// 0x00000110 System.Void DMain/<Start>d__0::.ctor(System.Int32)
extern void U3CStartU3Ed__0__ctor_mE4C50085C2DAE6B9AE876845A98A256A8CFD3E3B (void);
// 0x00000111 System.Void DMain/<Start>d__0::System.IDisposable.Dispose()
extern void U3CStartU3Ed__0_System_IDisposable_Dispose_mCB8CDA3D64F78F8C356E9A9F80DA4E48EE53F5CB (void);
// 0x00000112 System.Boolean DMain/<Start>d__0::MoveNext()
extern void U3CStartU3Ed__0_MoveNext_m5C85067F52F9D9EEE79D8C8419D0B201663E2761 (void);
// 0x00000113 System.Object DMain/<Start>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mACE8A452F4692BC6E0FC38AC9DBB35072A007AC1 (void);
// 0x00000114 System.Void DMain/<Start>d__0::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__0_System_Collections_IEnumerator_Reset_m0CB66531627AC524221E203F6FFBC2A527CDEC74 (void);
// 0x00000115 System.Object DMain/<Start>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__0_System_Collections_IEnumerator_get_Current_mC2698620ADBE0D6372150B44731A486A9F313AA3 (void);
// 0x00000116 System.String DPopupController::SceneName()
extern void DPopupController_SceneName_mCF634B1B49A06D1BF5D11351885DD887C5515C5A (void);
// 0x00000117 System.Void DPopupController::OnActive(System.Object)
extern void DPopupController_OnActive_mF1C1754A56B58DCFF2C89E0E43C9110B201C1305 (void);
// 0x00000118 System.Void DPopupController::OnShown()
extern void DPopupController_OnShown_mA7BF50B405677EA7C64C1BD4E87A9CB02534EDBE (void);
// 0x00000119 System.Void DPopupController::OnHidden()
extern void DPopupController_OnHidden_m2AD1FD24C84B558FBA7819F627D901900C3CD442 (void);
// 0x0000011A System.Void DPopupController::OnReFocus()
extern void DPopupController_OnReFocus_mE524740C938E810959100DFDF4F621EF57485D99 (void);
// 0x0000011B System.Void DPopupController::.ctor()
extern void DPopupController__ctor_m955A2278FC1364CB9163C12A72B4F341217CB264 (void);
// 0x0000011C System.String DSelectController::SceneName()
extern void DSelectController_SceneName_m3CF7851C34C394E12BCB182697F3AF053194D5B4 (void);
// 0x0000011D System.Void DSelectController::OnGameButtonTap()
extern void DSelectController_OnGameButtonTap_mAB9100AAD46320537758EED92239D2717896E148 (void);
// 0x0000011E System.Void DSelectController::OnActive(System.Object)
extern void DSelectController_OnActive_m1DE21C0494CB357C3EA992FBDF3CD06E532F4F8F (void);
// 0x0000011F System.Void DSelectController::OnShown()
extern void DSelectController_OnShown_m90085A878E25392D5C16DBF0F96C82158593D63F (void);
// 0x00000120 System.Void DSelectController::OnHidden()
extern void DSelectController_OnHidden_m6F5BD004A9611E77F14FD2CECCE062C622DF8323 (void);
// 0x00000121 System.Void DSelectController::.ctor()
extern void DSelectController__ctor_mD626C4CEB9E3CCFA8A38B7F0A53F3FDEE910D10E (void);
// 0x00000122 System.String DTopController::SceneName()
extern void DTopController_SceneName_mD3B2A27B9068DC2F810021D037235FEA3841D6C5 (void);
// 0x00000123 System.Void DTopController::OnButtonTap()
extern void DTopController_OnButtonTap_mB3A9F9678B40B512B96A1E312EF3BF84AF65226A (void);
// 0x00000124 System.Void DTopController::OnSelectTap()
extern void DTopController_OnSelectTap_m139C2400CE1D0CC125393CB2FAA09E9E92CDCE51 (void);
// 0x00000125 System.Void DTopController::OnActive(System.Object)
extern void DTopController_OnActive_m0367C90A0D3E5E9A5E46E797D4DE793B110435CF (void);
// 0x00000126 System.Void DTopController::OnShown()
extern void DTopController_OnShown_m1B33B053C5DA2ED8F8F983EDC447918158174A36 (void);
// 0x00000127 System.Void DTopController::OnHidden()
extern void DTopController_OnHidden_mA8BFE616C6C6CA9DB0AF591CA1C4879A68D640C2 (void);
// 0x00000128 System.Void DTopController::OnReFocus()
extern void DTopController_OnReFocus_m1C38998A612764B5C55FAB843AF4C1A89414C0E0 (void);
// 0x00000129 System.Void DTopController::.ctor()
extern void DTopController__ctor_mF089A1A358CBF3C55F4FED67457D22E4EDEF3131 (void);
// 0x0000012A System.String TemplateController::SceneName()
extern void TemplateController_SceneName_m0A78BB5FE2F1DC32E7A205FFD00445BF58812999 (void);
// 0x0000012B System.Void TemplateController::.ctor()
extern void TemplateController__ctor_mB11577D492A599B3EFAF220CAB63FCD779C8E561 (void);
// 0x0000012C System.Void VibrationExample::Start()
extern void VibrationExample_Start_mF0B4CAB3164B468E52B2370FEC0011A00148F5E3 (void);
// 0x0000012D System.Void VibrationExample::Update()
extern void VibrationExample_Update_m939AA1C591B6CB5E7BDF4D6D69323F3FD71E3DEC (void);
// 0x0000012E System.Void VibrationExample::TapVibrate()
extern void VibrationExample_TapVibrate_m63329F9D30D7AE4F1BA5DA0C8DCD6A2C9E6CC1FF (void);
// 0x0000012F System.Void VibrationExample::TapVibrateCustom()
extern void VibrationExample_TapVibrateCustom_m6122E6D3FFE06D1E3724F9FE383051D3D1E03EAF (void);
// 0x00000130 System.Void VibrationExample::TapVibratePattern()
extern void VibrationExample_TapVibratePattern_m48AFB13721C230A9029A43407EB6C5E179F9D01B (void);
// 0x00000131 System.Void VibrationExample::TapCancelVibrate()
extern void VibrationExample_TapCancelVibrate_m6AE952B415A91E6C314F5ADEDF9ED67916EA8871 (void);
// 0x00000132 System.Void VibrationExample::TapPopVibrate()
extern void VibrationExample_TapPopVibrate_m2ED910558E27BB9A7D1C3552BB29054D150F4B39 (void);
// 0x00000133 System.Void VibrationExample::TapPeekVibrate()
extern void VibrationExample_TapPeekVibrate_m7DA1A24E7E8AF899B669DF2D50DEEBEEF0B1A65A (void);
// 0x00000134 System.Void VibrationExample::TapNopeVibrate()
extern void VibrationExample_TapNopeVibrate_mDED3B4EEE392C3B27DEDCE4E4CD95F4D1006F7E1 (void);
// 0x00000135 System.Void VibrationExample::.ctor()
extern void VibrationExample__ctor_mCE008E33FA2C908D34FC1DB9D2B7BC7B04FF3498 (void);
// 0x00000136 System.Void VibrationExample/<>c::.cctor()
extern void U3CU3Ec__cctor_m2C5EE4840E068A5155F09B30ADBCE6FB2DAE7DF8 (void);
// 0x00000137 System.Void VibrationExample/<>c::.ctor()
extern void U3CU3Ec__ctor_m45B1DA3293EDF09FF55AF8D1E564E48677272658 (void);
// 0x00000138 System.Int64 VibrationExample/<>c::<TapVibratePattern>b__7_0(System.Char)
extern void U3CU3Ec_U3CTapVibratePatternU3Eb__7_0_m799184AAB33F6087AEDD03D3F120CBE663943304 (void);
// 0x00000139 System.Void HapticManager::Vibrate()
extern void HapticManager_Vibrate_mF81CCA1381A597C8DB4B1DE44C508D8BD0DD3601 (void);
// 0x0000013A System.Void HapticManager::.ctor()
extern void HapticManager__ctor_m3D2DA2169921098312A9904E078177C446D35D6A (void);
// 0x0000013B System.Void Vibration::VibratePop()
extern void Vibration_VibratePop_m2B90720D71C697422070969ED7105A83D0AF49D8 (void);
// 0x0000013C System.Void Vibration::VibratePeek()
extern void Vibration_VibratePeek_m8079731D7A93AA29A7C7523123CA8FE383705EE5 (void);
// 0x0000013D System.Void Vibration::VibrateNope()
extern void Vibration_VibrateNope_m9671B5FBECD5092A9735C44A510092E81AB13FDD (void);
// 0x0000013E System.Void Vibration::.cctor()
extern void Vibration__cctor_m0427AD99DC33B45FE3ABA9A144D2D64F94BB452A (void);
// 0x0000013F System.Void Vibration::Vibrate(System.Int64)
extern void Vibration_Vibrate_m91817995D0C33973EC132EED8AFEC455EB4E8646 (void);
// 0x00000140 System.Void Vibration::Vibrate(System.Int64[],System.Int32)
extern void Vibration_Vibrate_m63E0CA1B52DD80D1D974DAB43FD2307E02639D54 (void);
// 0x00000141 System.Void Vibration::Cancel()
extern void Vibration_Cancel_mD7FBC56CD7B33E2E13F0E3439F1EC919026C7607 (void);
// 0x00000142 System.Boolean Vibration::HasVibrator()
extern void Vibration_HasVibrator_m42E56D4451A27203FC2B6B6A146D81AB90A39002 (void);
// 0x00000143 System.Void Vibration::Vibrate()
extern void Vibration_Vibrate_mB6A7CC969A4BA2D6F53B2E9E468F90B492C01D64 (void);
// 0x00000144 System.Void SS.View.SceneAnimation::HideBeforeShowing()
extern void SceneAnimation_HideBeforeShowing_mFE8CC43AD11E0CAF48DE5DE811A791E558924077 (void);
// 0x00000145 System.Void SS.View.SceneAnimation::Show()
extern void SceneAnimation_Show_m53C8135BB458AE1732A10C6297ED5C42B14D6F51 (void);
// 0x00000146 System.Void SS.View.SceneAnimation::Hide()
extern void SceneAnimation_Hide_m5EA65AF2E2A2A9C109FF405DAD648B7362B853EF (void);
// 0x00000147 System.Void SS.View.SceneAnimation::StartShow()
extern void SceneAnimation_StartShow_m5ADE2AA4D68C4B4611F00D5592CE8CEEB058D8B5 (void);
// 0x00000148 System.Void SS.View.SceneAnimation::StartHide()
extern void SceneAnimation_StartHide_m25DA528F3099F0B0335D9A247C33585BBF5DF0AA (void);
// 0x00000149 System.Void SS.View.SceneAnimation::OnShown()
extern void SceneAnimation_OnShown_m41E4CE6EEACE40125B1D833CF59C83543AA67FD6 (void);
// 0x0000014A System.Void SS.View.SceneAnimation::OnHidden()
extern void SceneAnimation_OnHidden_m368DCC81FCADEC93B4A8C5E6181BD7974E3A29B0 (void);
// 0x0000014B System.Void SS.View.SceneAnimation::SetImmediate()
extern void SceneAnimation_SetImmediate_m61CA2DB4FAB1C2C199506422A2143774DA9104BD (void);
// 0x0000014C System.Void SS.View.SceneAnimation::Start()
extern void SceneAnimation_Start_mD64193360095C3B6BBBDFB2F16F8D14A7FC51FD6 (void);
// 0x0000014D System.Void SS.View.SceneAnimation::UpdateFrameCounter()
extern void SceneAnimation_UpdateFrameCounter_mABD8E477458782B0CD46EB28102023BE4DFBD452 (void);
// 0x0000014E System.Void SS.View.SceneAnimation::Update()
extern void SceneAnimation_Update_m35E88BB50FA2DDDB27400A6E5018E940D07059F6 (void);
// 0x0000014F System.Void SS.View.SceneAnimation::.ctor()
extern void SceneAnimation__ctor_m71A3993FFFE3C6893A1FFC48D60884B29A7B0DB6 (void);
// 0x00000150 System.Void SS.View.SceneDefaultAnimation::Awake()
extern void SceneDefaultAnimation_Awake_m6087ED6ED823B89D8B1B83BBF06E22734BC74395 (void);
// 0x00000151 UnityEngine.RectTransform SS.View.SceneDefaultAnimation::get_RectTransform()
extern void SceneDefaultAnimation_get_RectTransform_mFB006EF43A9F3FDDD70D08A6CB55B202CAFC9AAF (void);
// 0x00000152 UnityEngine.CanvasGroup SS.View.SceneDefaultAnimation::get_CanvasGroup()
extern void SceneDefaultAnimation_get_CanvasGroup_m5ECE8FB51688E341A9AC77C6B1FF1D868B4F8F68 (void);
// 0x00000153 UnityEngine.RectTransform SS.View.SceneDefaultAnimation::get_CanvasRectTransform()
extern void SceneDefaultAnimation_get_CanvasRectTransform_mC2B6DCB77B5D85DBD95FCE63DE5A120D53A7BF38 (void);
// 0x00000154 System.Void SS.View.SceneDefaultAnimation::HideBeforeShowing()
extern void SceneDefaultAnimation_HideBeforeShowing_m24A809E7ABE30A76849947D43555A3C3AB1A1F3B (void);
// 0x00000155 System.Void SS.View.SceneDefaultAnimation::Show()
extern void SceneDefaultAnimation_Show_m77F5C273C1FBD4DBF88B6702AC763841D7F2B289 (void);
// 0x00000156 System.Void SS.View.SceneDefaultAnimation::Hide()
extern void SceneDefaultAnimation_Hide_mA8FE59A6266984909CBB8454B5617D7A9299AC47 (void);
// 0x00000157 System.Void SS.View.SceneDefaultAnimation::ApplyProgress(System.Single)
extern void SceneDefaultAnimation_ApplyProgress_mC747AA774957353E11A773D8CFC01466EE54E257 (void);
// 0x00000158 System.Void SS.View.SceneDefaultAnimation::OnEndAnimation()
extern void SceneDefaultAnimation_OnEndAnimation_m0803A7018EEEBDA64855712981478E554AC8F851 (void);
// 0x00000159 System.Void SS.View.SceneDefaultAnimation::SetImmediate()
extern void SceneDefaultAnimation_SetImmediate_mA0532F3586608ABE9942C0E9AA253F0F691D8531 (void);
// 0x0000015A System.Single SS.View.SceneDefaultAnimation::ScreenHeight()
extern void SceneDefaultAnimation_ScreenHeight_m4448BA9D7ECA17D5E2A4DFB09266E81E57EBF825 (void);
// 0x0000015B System.Single SS.View.SceneDefaultAnimation::ScreenWidth()
extern void SceneDefaultAnimation_ScreenWidth_m6A43ACED0729DCD9C1143E7E10035E7AD9E7F372 (void);
// 0x0000015C System.Void SS.View.SceneDefaultAnimation::.ctor()
extern void SceneDefaultAnimation__ctor_mC733455A9C98DDF8CCCEBA2960CD65058C4C8FC8 (void);
// 0x0000015D UnityEngine.Animation SS.View.SceneLegacyAnimation::get_Animation()
extern void SceneLegacyAnimation_get_Animation_m419C114EC22262704705493648A0E64E38BD3894 (void);
// 0x0000015E System.Void SS.View.SceneLegacyAnimation::Awake()
extern void SceneLegacyAnimation_Awake_m8A1803BA15E8C964EF55C48C7AFFF58C969120A2 (void);
// 0x0000015F System.Void SS.View.SceneLegacyAnimation::HideBeforeShowing()
extern void SceneLegacyAnimation_HideBeforeShowing_m0AB7A6618A64929EBBA4D8AA336654C0E8D73E4C (void);
// 0x00000160 System.Void SS.View.SceneLegacyAnimation::Show()
extern void SceneLegacyAnimation_Show_mBB7AE314793F29A06A0BE564EB8EC19EB629B347 (void);
// 0x00000161 System.Void SS.View.SceneLegacyAnimation::Hide()
extern void SceneLegacyAnimation_Hide_m80D6F18AB860405E49C0CE8BD4047F29D270D8B0 (void);
// 0x00000162 System.Void SS.View.SceneLegacyAnimation::Update()
extern void SceneLegacyAnimation_Update_mBB4AE4173CDBC05F7BB11BF2CCE141314F8E70FC (void);
// 0x00000163 System.Void SS.View.SceneLegacyAnimation::AnimationUpdate()
extern void SceneLegacyAnimation_AnimationUpdate_m8AF43E0B104353C98ECE1FC77A2F5E7058B33F5E (void);
// 0x00000164 System.Void SS.View.SceneLegacyAnimation::PlayAnimation(UnityEngine.Animation,System.String)
extern void SceneLegacyAnimation_PlayAnimation_m5C9D393D0AC018124C87B75251255D8F86B49922 (void);
// 0x00000165 System.Void SS.View.SceneLegacyAnimation::.ctor()
extern void SceneLegacyAnimation__ctor_mE4A67DB6F8BE1B167764FE9F7D47A4559A729AD9 (void);
// 0x00000166 System.Void SS.View.Tweener::ApplyProgress(System.Single)
extern void Tweener_ApplyProgress_m8804FED8AF174E83381110995CC6D4C9CAF0F0F1 (void);
// 0x00000167 System.Void SS.View.Tweener::OnEndAnimation()
extern void Tweener_OnEndAnimation_m163632EB3EB0759E542AE9B805637902C5BA38E3 (void);
// 0x00000168 System.Void SS.View.Tweener::Play()
extern void Tweener_Play_m7B0102D4F6C61BAAF9130D7A92F3097213AF3CA3 (void);
// 0x00000169 System.Void SS.View.Tweener::Stop(System.Boolean)
extern void Tweener_Stop_m63620944978CF85778D8C910314A4CD119E9FAC6 (void);
// 0x0000016A System.Void SS.View.Tweener::Update()
extern void Tweener_Update_m8333C4C226A4333ABF6ADA7CE8DB6F45810C9178 (void);
// 0x0000016B System.Void SS.View.Tweener::UpdateProgress()
extern void Tweener_UpdateProgress_mC591E8FDDA999A1F2AF6874963D81656EE292760 (void);
// 0x0000016C SS.View.Tweener/EasingFunction SS.View.Tweener::GetEasingFunction(SS.View.Tweener/EaseType)
extern void Tweener_GetEasingFunction_mAA6586F38A1C9C744912B77C88062DDC382CAD9D (void);
// 0x0000016D System.Single SS.View.Tweener::linear(System.Single,System.Single,System.Single)
extern void Tweener_linear_mCA0623082B15FD4CDDBF0F9CC2758729D239ED9B (void);
// 0x0000016E System.Single SS.View.Tweener::clerp(System.Single,System.Single,System.Single)
extern void Tweener_clerp_m04CE7A107AF6512C5AE254115B74BE6F6DD8B854 (void);
// 0x0000016F System.Single SS.View.Tweener::spring(System.Single,System.Single,System.Single)
extern void Tweener_spring_mF7C559223385FE9A6913BE98C3AA774F0915606E (void);
// 0x00000170 System.Single SS.View.Tweener::easeInQuad(System.Single,System.Single,System.Single)
extern void Tweener_easeInQuad_mBB2727458A04D15B16AB1D0A1B3E4064C84729D0 (void);
// 0x00000171 System.Single SS.View.Tweener::easeOutQuad(System.Single,System.Single,System.Single)
extern void Tweener_easeOutQuad_m26F980E6E48B7EF370B74C6D0ED168E6E1A12831 (void);
// 0x00000172 System.Single SS.View.Tweener::easeInOutQuad(System.Single,System.Single,System.Single)
extern void Tweener_easeInOutQuad_mC1BF7FA23FF5178002438CA94F68991F7A0EF6FB (void);
// 0x00000173 System.Single SS.View.Tweener::easeInCubic(System.Single,System.Single,System.Single)
extern void Tweener_easeInCubic_mB2FE4070086CA0047F9A7624DB6A7B0BBC0D4BD0 (void);
// 0x00000174 System.Single SS.View.Tweener::easeOutCubic(System.Single,System.Single,System.Single)
extern void Tweener_easeOutCubic_m9972D2AE426FA4A96C0D41A3ABD3EB5841E1D3B0 (void);
// 0x00000175 System.Single SS.View.Tweener::easeInOutCubic(System.Single,System.Single,System.Single)
extern void Tweener_easeInOutCubic_m1BEDF22C3FB0BCA3E62D85334679537701147CB8 (void);
// 0x00000176 System.Single SS.View.Tweener::easeInQuart(System.Single,System.Single,System.Single)
extern void Tweener_easeInQuart_mB56F838DA4031081AA90EE012830B7D90F385F60 (void);
// 0x00000177 System.Single SS.View.Tweener::easeOutQuart(System.Single,System.Single,System.Single)
extern void Tweener_easeOutQuart_mBF985E168196330A31C90CFDDE3E26E2E0AFBA38 (void);
// 0x00000178 System.Single SS.View.Tweener::easeInOutQuart(System.Single,System.Single,System.Single)
extern void Tweener_easeInOutQuart_mB9C14E16EDC609BF5407A4EA8EEB33CD6157D4B3 (void);
// 0x00000179 System.Single SS.View.Tweener::easeInQuint(System.Single,System.Single,System.Single)
extern void Tweener_easeInQuint_mB80FD8DEC3A3F3488889A0D7BB31ADF50BBED703 (void);
// 0x0000017A System.Single SS.View.Tweener::easeOutQuint(System.Single,System.Single,System.Single)
extern void Tweener_easeOutQuint_m3E3F952CAEB6F896111AB6261A7CCE07A5A32DBE (void);
// 0x0000017B System.Single SS.View.Tweener::easeInOutQuint(System.Single,System.Single,System.Single)
extern void Tweener_easeInOutQuint_m7653F738DC6C8E5DA1CF8FB4CE89826D71E0552A (void);
// 0x0000017C System.Single SS.View.Tweener::easeInSine(System.Single,System.Single,System.Single)
extern void Tweener_easeInSine_mF233683A5EA23820A8327025AD8FA7114346D0BC (void);
// 0x0000017D System.Single SS.View.Tweener::easeOutSine(System.Single,System.Single,System.Single)
extern void Tweener_easeOutSine_mACEF8EA8604C6425EDDEEC36DF0ABB97F57761F2 (void);
// 0x0000017E System.Single SS.View.Tweener::easeInOutSine(System.Single,System.Single,System.Single)
extern void Tweener_easeInOutSine_m2D9D2D43DF5AD5270D12BCB0B53D7FCD5822D8BB (void);
// 0x0000017F System.Single SS.View.Tweener::easeInExpo(System.Single,System.Single,System.Single)
extern void Tweener_easeInExpo_mC9978D31740909A348043D5F2BED2F0A73AEA206 (void);
// 0x00000180 System.Single SS.View.Tweener::easeOutExpo(System.Single,System.Single,System.Single)
extern void Tweener_easeOutExpo_m9C8CDAB6774A484DA82CCDF4A1F1F61CE9874701 (void);
// 0x00000181 System.Single SS.View.Tweener::easeInOutExpo(System.Single,System.Single,System.Single)
extern void Tweener_easeInOutExpo_m076C00819B3E8A325F6E4B9A54AB0D5A31B86509 (void);
// 0x00000182 System.Single SS.View.Tweener::easeInCirc(System.Single,System.Single,System.Single)
extern void Tweener_easeInCirc_m85A11DF434362981C19EAED2D72916C1B4496D26 (void);
// 0x00000183 System.Single SS.View.Tweener::easeOutCirc(System.Single,System.Single,System.Single)
extern void Tweener_easeOutCirc_mCDFE60DB11BA255EA348F449336EAFBF0AF34C4C (void);
// 0x00000184 System.Single SS.View.Tweener::easeInOutCirc(System.Single,System.Single,System.Single)
extern void Tweener_easeInOutCirc_m3E035390160C90B33EC9BA590320D3AF98CFC16C (void);
// 0x00000185 System.Single SS.View.Tweener::easeInBounce(System.Single,System.Single,System.Single)
extern void Tweener_easeInBounce_mF284A65C2B4230209EA71F8A7A89F311A0B82411 (void);
// 0x00000186 System.Single SS.View.Tweener::easeOutBounce(System.Single,System.Single,System.Single)
extern void Tweener_easeOutBounce_m01EC3980F647712756854D6B81556623E350A60E (void);
// 0x00000187 System.Single SS.View.Tweener::easeInOutBounce(System.Single,System.Single,System.Single)
extern void Tweener_easeInOutBounce_mF4E96C1515CAF2F78B8D02B986C5685476CF5127 (void);
// 0x00000188 System.Single SS.View.Tweener::easeInBack(System.Single,System.Single,System.Single)
extern void Tweener_easeInBack_m5F9C324680EB70F58B800C1E7CC397AA2EA911B1 (void);
// 0x00000189 System.Single SS.View.Tweener::easeOutBack(System.Single,System.Single,System.Single)
extern void Tweener_easeOutBack_mE2E8174CF702CA3269D3BDDC29A3611E016643CB (void);
// 0x0000018A System.Single SS.View.Tweener::easeInOutBack(System.Single,System.Single,System.Single)
extern void Tweener_easeInOutBack_mCBA5EFB2BBAF2A69FD664C5BBA5AF312536B71DE (void);
// 0x0000018B System.Single SS.View.Tweener::punch(System.Single,System.Single)
extern void Tweener_punch_mDD0C45752A0F5F258C1A099B374439347A91F6D0 (void);
// 0x0000018C System.Single SS.View.Tweener::easeInElastic(System.Single,System.Single,System.Single)
extern void Tweener_easeInElastic_m44B6FD27E4FC65BA3BD998ADF19BBDFD9DD1FBBF (void);
// 0x0000018D System.Single SS.View.Tweener::easeOutElastic(System.Single,System.Single,System.Single)
extern void Tweener_easeOutElastic_m42AD40A7125879554453A5D3D58B068ECF37D4CE (void);
// 0x0000018E System.Single SS.View.Tweener::easeInOutElastic(System.Single,System.Single,System.Single)
extern void Tweener_easeInOutElastic_m8CF6BBA18CD92D7BF2CCBB34E6CBD4F98ADCC56C (void);
// 0x0000018F System.Void SS.View.Tweener::.ctor()
extern void Tweener__ctor_m5746E805DFFDAB0A996A5549587EC0EC8E3428A5 (void);
// 0x00000190 System.Void SS.View.Tweener/EasingFunction::.ctor(System.Object,System.IntPtr)
extern void EasingFunction__ctor_m83CFC8105C9A006241951E66679DB2F80EAB98BB (void);
// 0x00000191 System.Single SS.View.Tweener/EasingFunction::Invoke(System.Single,System.Single,System.Single)
extern void EasingFunction_Invoke_mDB65A14C38B3CD25717CD538DC344FC464A29680 (void);
// 0x00000192 System.IAsyncResult SS.View.Tweener/EasingFunction::BeginInvoke(System.Single,System.Single,System.Single,System.AsyncCallback,System.Object)
extern void EasingFunction_BeginInvoke_m7C04E9669660211BDE6790F5CF3555C52B5285BD (void);
// 0x00000193 System.Single SS.View.Tweener/EasingFunction::EndInvoke(System.IAsyncResult)
extern void EasingFunction_EndInvoke_m718901AE480EB6B4545D0F4D9EBE1C28736079A3 (void);
// 0x00000194 System.Void SS.View.CameraDestroyer::OnDestroy()
extern void CameraDestroyer_OnDestroy_mF495B6FE7A59CB456A896E2F27DD9634BBA2AAF4 (void);
// 0x00000195 System.Void SS.View.CameraDestroyer::.ctor()
extern void CameraDestroyer__ctor_m58290D86C447C96D02A050DFC6FEB7AE5D9570E1 (void);
// 0x00000196 System.String SS.View.IController::SceneName()
// 0x00000197 System.String SS.View.Controller::SceneName()
// 0x00000198 System.Void SS.View.Controller::OnActive(System.Object)
extern void Controller_OnActive_m629201E7FC1E39EF46D2721F096599735E8C8FAF (void);
// 0x00000199 System.Void SS.View.Controller::OnReFocus()
extern void Controller_OnReFocus_m9872BF16366F67BC9F37A2C2735D3C9609FF00C1 (void);
// 0x0000019A System.Void SS.View.Controller::OnShown()
extern void Controller_OnShown_m821DCA48FD6E4E07F8E2211A602207C8E95958B8 (void);
// 0x0000019B System.Void SS.View.Controller::OnHidden()
extern void Controller_OnHidden_mE00B739484A3909F4C09D7568A09D20A38001564 (void);
// 0x0000019C System.Void SS.View.Controller::OnKeyBack()
extern void Controller_OnKeyBack_m3ECCABBD257BEB94BED7694E9303F009155F33BD (void);
// 0x0000019D SS.View.Manager/Data SS.View.Controller::get_Data()
extern void Controller_get_Data_m5FBB080ED88F0FCCE300D8AE0BE74E1B7B66D6B4 (void);
// 0x0000019E System.Void SS.View.Controller::set_Data(SS.View.Manager/Data)
extern void Controller_set_Data_m134299AB8FCB15566C6B2AE114B040FF8FEE2E71 (void);
// 0x0000019F UnityEngine.Canvas SS.View.Controller::get_Canvas()
extern void Controller_get_Canvas_mC069A517D0AB796C905CFD93EB6E9EBD57475234 (void);
// 0x000001A0 System.Void SS.View.Controller::set_Canvas(UnityEngine.Canvas)
extern void Controller_set_Canvas_mBE1F181A21647DE0E3D792CB90718DB4C4CDDD32 (void);
// 0x000001A1 UnityEngine.Camera SS.View.Controller::get_Camera()
extern void Controller_get_Camera_m027116AEDBA981D3D06D67685741FA117AED7820 (void);
// 0x000001A2 System.Void SS.View.Controller::set_Camera(UnityEngine.Camera)
extern void Controller_set_Camera_m9DBF4259A3D45D85837334CC090A06518F199B41 (void);
// 0x000001A3 System.Void SS.View.Controller::Show()
extern void Controller_Show_m2DA6FD38E632DDE33FFDDE549AD775A8430D3D57 (void);
// 0x000001A4 System.Void SS.View.Controller::Hide()
extern void Controller_Hide_m132B3D41161EEB4ABA5072D157B8B00BF199FD6D (void);
// 0x000001A5 System.Void SS.View.Controller::CreateShield()
extern void Controller_CreateShield_m3A3848AA62B889121B1CD61FE0565555E2CE5A60 (void);
// 0x000001A6 System.Void SS.View.Controller::SetupCanvas(System.Int32)
extern void Controller_SetupCanvas_m372036D795E9DAE08D7CC114D024B6FD2DF99CD0 (void);
// 0x000001A7 System.Void SS.View.Controller::.ctor()
extern void Controller__ctor_mC40550E30A8F72CD75EB13CAC75C87BEF1F0F770 (void);
// 0x000001A8 System.Int32 SS.View.Manager::get_stackCount()
extern void Manager_get_stackCount_m02A93B09158482CA804FF6045568FF3E5431FDC2 (void);
// 0x000001A9 SS.View.Controller SS.View.Manager::get_MainController()
extern void Manager_get_MainController_m2FEAD42BEE225E5A75F430BE57EADA0EBB23E29F (void);
// 0x000001AA UnityEngine.Color SS.View.Manager::get_ShieldColor()
extern void Manager_get_ShieldColor_mEDF473CD5B3B0E921EDCC9A8ECDED1A59AEAF66C (void);
// 0x000001AB System.Void SS.View.Manager::set_ShieldColor(UnityEngine.Color)
extern void Manager_set_ShieldColor_mFAC7D6CD1EA0F67EE06DB6900180748261E1ABB9 (void);
// 0x000001AC System.Single SS.View.Manager::get_SceneFadeInDuration()
extern void Manager_get_SceneFadeInDuration_m98102617D5859290DAA2F5E61A47ED72EF3AD434 (void);
// 0x000001AD System.Void SS.View.Manager::set_SceneFadeInDuration(System.Single)
extern void Manager_set_SceneFadeInDuration_m2DA5251F0F6483A90F57212516A186F19A45FAE6 (void);
// 0x000001AE System.Single SS.View.Manager::get_SceneFadeOutDuration()
extern void Manager_get_SceneFadeOutDuration_m2AF5EBBF4DE770502820BE14793AEE44420B7745 (void);
// 0x000001AF System.Void SS.View.Manager::set_SceneFadeOutDuration(System.Single)
extern void Manager_set_SceneFadeOutDuration_mA6B28932A9C0AA18771DCF4B9387CD64176FDEAC (void);
// 0x000001B0 System.Single SS.View.Manager::get_SceneAnimationDuration()
extern void Manager_get_SceneAnimationDuration_m5933AAE547D9E16299D8ED64A5CE8E07256D0020 (void);
// 0x000001B1 System.Void SS.View.Manager::set_SceneAnimationDuration(System.Single)
extern void Manager_set_SceneAnimationDuration_mDCECEFFEAAD73AC441388B41B9AAE73BE20E3ABE (void);
// 0x000001B2 SS.View.ManagerObject SS.View.Manager::get_Object()
extern void Manager_get_Object_m9396DC89955E29CCAA00FA873142C1FED1205D43 (void);
// 0x000001B3 System.Void SS.View.Manager::set_Object(SS.View.ManagerObject)
extern void Manager_set_Object_m89E507CF550DDCFE52A6BFDEDB7EB186E7A3817A (void);
// 0x000001B4 System.Void SS.View.Manager::.cctor()
extern void Manager__cctor_mDBD51282E1B707DF71B4721FD38797708ADE4C9A (void);
// 0x000001B5 System.Void SS.View.Manager::Load(System.String,System.Object)
extern void Manager_Load_mA6A5F24319F8636FA27EB5C96FB7C2E72C6949F2 (void);
// 0x000001B6 System.Void SS.View.Manager::Reload()
extern void Manager_Reload_m4223E473D74F1A9FD4D9A36A8125BB35C4EA41AD (void);
// 0x000001B7 System.Void SS.View.Manager::Add(System.String,System.Object,SS.View.Manager/Callback,SS.View.Manager/Callback)
extern void Manager_Add_m39BCFCC76985263DCAE3F55E484B3E5493F45178 (void);
// 0x000001B8 System.Void SS.View.Manager::Close()
extern void Manager_Close_m431A19DCECC624FDA3C633E920D80B42C6B4913B (void);
// 0x000001B9 System.Void SS.View.Manager::set_LoadingSceneName(System.String)
extern void Manager_set_LoadingSceneName_m80C7A5F15EF70BD56230200EB76F95BC50743886 (void);
// 0x000001BA System.String SS.View.Manager::get_LoadingSceneName()
extern void Manager_get_LoadingSceneName_m52F0D661A0E22355C41B21F34537DFABFD4F3B9A (void);
// 0x000001BB System.Void SS.View.Manager::LoadingAnimation(System.Boolean,System.Object)
extern void Manager_LoadingAnimation_mE02DBFE5C5F8B77C54A5DDDF6EE0EB2CA55CF3A0 (void);
// 0x000001BC System.Void SS.View.Manager::OnShown(SS.View.Controller)
extern void Manager_OnShown_mE27E1092890D34C436E8C29B625C0F3DDC8CA279 (void);
// 0x000001BD System.Void SS.View.Manager::OnHidden(SS.View.Controller)
extern void Manager_OnHidden_mCDCA7B81F0198D0D0FEAB49A729BE0124912910C (void);
// 0x000001BE System.Boolean SS.View.Manager::IsActiveShield()
extern void Manager_IsActiveShield_m2B5F80D0D079F33F28CA3D7C1B225C74F6E8CCBA (void);
// 0x000001BF SS.View.Controller SS.View.Manager::TopController()
extern void Manager_TopController_mBF9FC0FB451A507568C11907DD30F1F530431C6C (void);
// 0x000001C0 System.Void SS.View.Manager::OnFadedIn()
extern void Manager_OnFadedIn_m4646E21CFA05904255A17AB54ED3916992498AA7 (void);
// 0x000001C1 System.Void SS.View.Manager::OnFadedOut()
extern void Manager_OnFadedOut_mB82930E3DF30A199B3C9A03D4218BA73BEF7DD88 (void);
// 0x000001C2 System.Void SS.View.Manager::ActivatePreviousController(System.Boolean)
extern void Manager_ActivatePreviousController_mF6F981BA01D16AF2E1B3F9EE866C414491CDEEDA (void);
// 0x000001C3 System.Void SS.View.Manager::ActivatePreviousController(SS.View.Controller,System.Boolean)
extern void Manager_ActivatePreviousController_m342B80D0A34D49E74859FF4BB2C4060D7421DB89 (void);
// 0x000001C4 System.Void SS.View.Manager::Unload()
extern void Manager_Unload_mA57DD1898227D983D954E6C815D7D59C91CE63B4 (void);
// 0x000001C5 System.Void SS.View.Manager::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void Manager_OnSceneLoaded_mA6D4E6D38FF6DEA7426FC652F17860D6A4321F8A (void);
// 0x000001C6 SS.View.Controller SS.View.Manager::GetController(UnityEngine.SceneManagement.Scene)
extern void Manager_GetController_m765FE9A1B057117A05AF80E57199FA4739FC5EC8 (void);
// 0x000001C7 System.Void SS.View.Manager::.ctor()
extern void Manager__ctor_mA5ED09CF20B857DC843EF0E097C657F790113B90 (void);
// 0x000001C8 System.Void SS.View.Manager/Data::.ctor(System.Object,System.String,SS.View.Manager/Callback,SS.View.Manager/Callback)
extern void Data__ctor_mAF4E759868718E64398C1C3BE139AF7F69E9D1F8 (void);
// 0x000001C9 System.Void SS.View.Manager/Callback::.ctor(System.Object,System.IntPtr)
extern void Callback__ctor_m920612C6793B3C37AB7E198CF2FB4B4E11BB68E2 (void);
// 0x000001CA System.Void SS.View.Manager/Callback::Invoke()
extern void Callback_Invoke_m0C199CA22DB833B7A69AA879809960D45B1674B7 (void);
// 0x000001CB System.IAsyncResult SS.View.Manager/Callback::BeginInvoke(System.AsyncCallback,System.Object)
extern void Callback_BeginInvoke_mAE9B9C586BB09DD8E3638CED470E604A7F65D347 (void);
// 0x000001CC System.Void SS.View.Manager/Callback::EndInvoke(System.IAsyncResult)
extern void Callback_EndInvoke_m2C318BA02B0D6EDF2EEE9CFCACE124057E05CA3C (void);
// 0x000001CD System.Void SS.View.Manager/ShowBannerDelegate::.ctor(System.Object,System.IntPtr)
extern void ShowBannerDelegate__ctor_m10C2A19277315193B7155C677EC30ED25D16824E (void);
// 0x000001CE System.Void SS.View.Manager/ShowBannerDelegate::Invoke(System.Single)
extern void ShowBannerDelegate_Invoke_m5BBB41C58347FF11A953BEFBF5F8644C1C469A75 (void);
// 0x000001CF System.IAsyncResult SS.View.Manager/ShowBannerDelegate::BeginInvoke(System.Single,System.AsyncCallback,System.Object)
extern void ShowBannerDelegate_BeginInvoke_m1167F32DB2940E1F4E6A801A0DC7FE6B766821AF (void);
// 0x000001D0 System.Void SS.View.Manager/ShowBannerDelegate::EndInvoke(System.IAsyncResult)
extern void ShowBannerDelegate_EndInvoke_mBC93C3D7D932BF1EBEFA996EF478794A3840635D (void);
// 0x000001D1 System.Void SS.View.Manager/HideBannerDelegate::.ctor(System.Object,System.IntPtr)
extern void HideBannerDelegate__ctor_m2C036D15240E4E723161D00511E66B59EF9D7B0F (void);
// 0x000001D2 System.Void SS.View.Manager/HideBannerDelegate::Invoke()
extern void HideBannerDelegate_Invoke_mEC52DC813FA76542A42A68A0C76B25CC6443CDCC (void);
// 0x000001D3 System.IAsyncResult SS.View.Manager/HideBannerDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void HideBannerDelegate_BeginInvoke_m9A8491F46F95D5E3133F41AEC4470625D440FFEB (void);
// 0x000001D4 System.Void SS.View.Manager/HideBannerDelegate::EndInvoke(System.IAsyncResult)
extern void HideBannerDelegate_EndInvoke_mFE2D522B76A823BE3CC8AECAAEB16FA7EB68C9FB (void);
// 0x000001D5 System.Void SS.View.ManagerObject::ActivateBackgroundCamera(System.Boolean)
extern void ManagerObject_ActivateBackgroundCamera_m093FE7618848D184AC19953867378B620CC77BF4 (void);
// 0x000001D6 UnityEngine.Camera SS.View.ManagerObject::get_UICamera()
extern void ManagerObject_get_UICamera_m993C42564ADEFCB2FDDB3BE84C78393F42E5B891 (void);
// 0x000001D7 System.Void SS.View.ManagerObject::ShieldOff()
extern void ManagerObject_ShieldOff_mB71246D3053EDDBCAD91203CEF5213CA9023D1ED (void);
// 0x000001D8 System.Void SS.View.ManagerObject::ShieldOn()
extern void ManagerObject_ShieldOn_m4531FABCE185C05D7AAA925EC790ABFFFD33B7A2 (void);
// 0x000001D9 System.Void SS.View.ManagerObject::ShieldOnColor()
extern void ManagerObject_ShieldOnColor_m5F595D421C8643263923ABCD0A2BC08B94648EFE (void);
// 0x000001DA System.Void SS.View.ManagerObject::FadeInScene()
extern void ManagerObject_FadeInScene_m5A9C28BB268C93822FE671C1C799B9F1495E2EC1 (void);
// 0x000001DB System.Void SS.View.ManagerObject::FadeOutScene()
extern void ManagerObject_FadeOutScene_mE3693E5E300E9A68C8BA0CE0CEC6F08C5A5A5ABF (void);
// 0x000001DC System.Void SS.View.ManagerObject::OnFadedIn()
extern void ManagerObject_OnFadedIn_m17313D976F90EE10259133B5DE6A1B3736199CA7 (void);
// 0x000001DD System.Void SS.View.ManagerObject::OnFadedOut()
extern void ManagerObject_OnFadedOut_mE5DCFC3455AFA1AE4EAADD981B52C49DADDC3AEF (void);
// 0x000001DE System.Boolean SS.View.ManagerObject::get_Active()
extern void ManagerObject_get_Active_mCE7D422E51414DA2707A9D58082A19D08C6A08C9 (void);
// 0x000001DF System.Void SS.View.ManagerObject::set_Active(System.Boolean)
extern void ManagerObject_set_Active_m299A151347D379C449976630C9991724505F5135 (void);
// 0x000001E0 System.Void SS.View.ManagerObject::ApplyProgress(System.Single)
extern void ManagerObject_ApplyProgress_m8D72FBF756DC1706EBA381CC219F32D6573BD0A3 (void);
// 0x000001E1 System.Void SS.View.ManagerObject::OnEndAnimation()
extern void ManagerObject_OnEndAnimation_m67C9C87ABAFB0A7F04E05458D7E553D6E005CFC0 (void);
// 0x000001E2 System.Void SS.View.ManagerObject::Awake()
extern void ManagerObject_Awake_m2D2270DB51450C265D4B0219A3A8AA0A7045362E (void);
// 0x000001E3 System.Collections.IEnumerator SS.View.ManagerObject::Start()
extern void ManagerObject_Start_m3CC4A4092C1D4D8FB7DBD6C80156FA6BA028EEB7 (void);
// 0x000001E4 System.Void SS.View.ManagerObject::Update()
extern void ManagerObject_Update_m7B6F62CFCE8D32B9F90E1908F8FD24D0D588AFC5 (void);
// 0x000001E5 System.Void SS.View.ManagerObject::UpdateInput()
extern void ManagerObject_UpdateInput_mFE5AF8902B3CC354303C6B47A0CB1B0943BBD444 (void);
// 0x000001E6 System.Void SS.View.ManagerObject::.ctor()
extern void ManagerObject__ctor_mEBC38560C142185471943C1D9E9C9003705998D1 (void);
// 0x000001E7 System.Void SS.View.ManagerObject/<Start>d__30::.ctor(System.Int32)
extern void U3CStartU3Ed__30__ctor_m3278BA8A2B7B1FE63ABBE55EF001B33E930B64C1 (void);
// 0x000001E8 System.Void SS.View.ManagerObject/<Start>d__30::System.IDisposable.Dispose()
extern void U3CStartU3Ed__30_System_IDisposable_Dispose_mA073B863AD0267E5ED9DE807DC0884E5F880EF12 (void);
// 0x000001E9 System.Boolean SS.View.ManagerObject/<Start>d__30::MoveNext()
extern void U3CStartU3Ed__30_MoveNext_mD943E92BD103477C99C71C0FF5F60C496D3F8417 (void);
// 0x000001EA System.Object SS.View.ManagerObject/<Start>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35A692CC36E0BA65212115187F232830504A9BA7 (void);
// 0x000001EB System.Void SS.View.ManagerObject/<Start>d__30::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__30_System_Collections_IEnumerator_Reset_m1DB36B586D00CDF821E2C9D38F12E26E1182C5B2 (void);
// 0x000001EC System.Object SS.View.ManagerObject/<Start>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__30_System_Collections_IEnumerator_get_Current_m38675A3A033151C858C740BBB4D18165212F6C60 (void);
// 0x000001ED System.Void SS.View.TabBar::Awake()
extern void TabBar_Awake_m773C1963497C66A73DDF8A8C834B941CD86FBFD7 (void);
// 0x000001EE System.Void SS.View.TabBar::SetCurrentTab(System.Int32)
extern void TabBar_SetCurrentTab_m79CC71C71A81C59BBD91A4C7C2569AF09C42122B (void);
// 0x000001EF System.Void SS.View.TabBar::.ctor()
extern void TabBar__ctor_m1981FF95A829EE77AAC654E093DAE05A80A4AF7C (void);
// 0x000001F0 System.Void SS.View.TabButton::Awake()
extern void TabButton_Awake_m2CFEA3B30D49F3F7CCEED8C72213CFBE8DB9F9D5 (void);
// 0x000001F1 System.Void SS.View.TabButton::Resize(System.Single,System.Single)
extern void TabButton_Resize_m12C7883CE7BBBD6E0EAC043CA8AC96DEA8245D76 (void);
// 0x000001F2 System.Void SS.View.TabButton::SetNormal()
extern void TabButton_SetNormal_m5DFDD164DF5EE6D2C0BF3D581DA43A4BB2BD4AE2 (void);
// 0x000001F3 System.Void SS.View.TabButton::SetActive()
extern void TabButton_SetActive_m036247A248B1499FCEBF3BEC9BC8C7927FA9FC15 (void);
// 0x000001F4 System.Void SS.View.TabButton::.ctor()
extern void TabButton__ctor_m7B48CC66E48962A5C92D80E6A6D1484EE5FAA307 (void);
// 0x000001F5 SS.View.TabSubController SS.View.TabController::get_Current()
extern void TabController_get_Current_m124072306811E0C914C4306A1AFAA6E9CC84B4E1 (void);
// 0x000001F6 System.Void SS.View.TabController::set_Current(SS.View.TabSubController)
extern void TabController_set_Current_m9F7548FAC6A89F8F23292605F7278EAA538BA494 (void);
// 0x000001F7 System.String SS.View.TabController::SceneName()
extern void TabController_SceneName_m59638E8CC7B495FCB24DA95EDA02321050887F2A (void);
// 0x000001F8 System.Void SS.View.TabController::OnKeyBack()
extern void TabController_OnKeyBack_m4891B07612078E768004721B6C107559AB9882F8 (void);
// 0x000001F9 System.Void SS.View.TabController::Load(System.String,System.Object,System.String)
extern void TabController_Load_m1C679431E6B9C4E950DE48792463AE25A848C525 (void);
// 0x000001FA System.Void SS.View.TabController::GoToScreen(System.Int32)
extern void TabController_GoToScreen_mD2991D198F7B097A895B8083893FB637408DC11E (void);
// 0x000001FB System.Void SS.View.TabController::Awake()
extern void TabController_Awake_m9376B07F2837B5B62747E142AD412447F8AE3A81 (void);
// 0x000001FC System.Void SS.View.TabController::Start()
extern void TabController_Start_mE19D2EBC0A8C3D3F9EEBFA502CECCB593CF856CE (void);
// 0x000001FD System.Void SS.View.TabController::OnShown()
extern void TabController_OnShown_m997735E12436EBE3878431FBA3300286C0457D17 (void);
// 0x000001FE System.Void SS.View.TabController::OnSelectionChangeEndEvent(System.Int32)
extern void TabController_OnSelectionChangeEndEvent_m938A404F867E68C87A27812FE8DFBA0B9D7D7D16 (void);
// 0x000001FF System.Void SS.View.TabController::CopyRectTransform(UnityEngine.RectTransform,UnityEngine.RectTransform)
extern void TabController_CopyRectTransform_mDBDF4DD03C3CF2C762FA4648F558447E3FAEF67C (void);
// 0x00000200 System.Int32 SS.View.TabController::SortBySiblingIndex(SS.View.TabSubController,SS.View.TabSubController)
extern void TabController_SortBySiblingIndex_mD0615A4CB1F8535ECAE4F0F9D3FC81825AF4A1F9 (void);
// 0x00000201 System.Void SS.View.TabController::.ctor()
extern void TabController__ctor_m941DD841F78FF45B3788B1D6B51609BC9196B307 (void);
// 0x00000202 SS.View.TabController SS.View.TabSubController::get_tab()
extern void TabSubController_get_tab_mF46921860DC621EBB80E7F2EDF23FAA59B2FB421 (void);
// 0x00000203 System.Void SS.View.TabSubController::set_tab(SS.View.TabController)
extern void TabSubController_set_tab_m0077B05DB648639D5AF793DF548126E57B2A9C30 (void);
// 0x00000204 System.String SS.View.TabSubController::SceneName()
extern void TabSubController_SceneName_m3EFDE29FF6346C7D7530A661ECFB5D6CD6E060AA (void);
// 0x00000205 System.Void SS.View.TabSubController::OnKeyBack()
extern void TabSubController_OnKeyBack_m6FDF18A17F47A39B561E661A2CA06F479F0605AD (void);
// 0x00000206 System.Void SS.View.TabSubController::OnActive(System.Object)
extern void TabSubController_OnActive_mE95AE1AABF3711D7668B023BD551863766F2578A (void);
// 0x00000207 System.Void SS.View.TabSubController::OnShown()
extern void TabSubController_OnShown_m6C77E215783400EE85A38E20FAF737EEE2327408 (void);
// 0x00000208 System.Void SS.View.TabSubController::OnHidden()
extern void TabSubController_OnHidden_mEE079AFA4F6FBC8412590AED87E25FB2CA21CD28 (void);
// 0x00000209 System.Void SS.View.TabSubController::Hide()
extern void TabSubController_Hide_mC3A6C2DEC1CC717E3A374556A30D3CDC041C35FD (void);
// 0x0000020A System.Void SS.View.TabSubController::ApplyProgress(System.Single)
extern void TabSubController_ApplyProgress_m062D427274A25BC039473AF914DBF275C140A761 (void);
// 0x0000020B System.Void SS.View.TabSubController::OnEndAnimation()
extern void TabSubController_OnEndAnimation_m0D8EDECDA26D3D1CA8E8249CF88BB871C25565A5 (void);
// 0x0000020C System.Void SS.View.TabSubController::Awake()
extern void TabSubController_Awake_mDBA9908509DD95AA6D0DFFB8E1E616B660D77F31 (void);
// 0x0000020D System.Collections.IEnumerator SS.View.TabSubController::OnShownFake()
extern void TabSubController_OnShownFake_mD1BCB8520C389DD75B15A22137C006A9A2D772D9 (void);
// 0x0000020E System.Void SS.View.TabSubController::.ctor()
extern void TabSubController__ctor_mDA60F5E196321B6BCC7A12ED395498180B041E84 (void);
// 0x0000020F System.Void SS.View.TabSubController/<OnShownFake>d__15::.ctor(System.Int32)
extern void U3COnShownFakeU3Ed__15__ctor_mBDD5A6F79B6B054D1ABB955F21066CBBE307DB0D (void);
// 0x00000210 System.Void SS.View.TabSubController/<OnShownFake>d__15::System.IDisposable.Dispose()
extern void U3COnShownFakeU3Ed__15_System_IDisposable_Dispose_mB8DD835A213DD3A0597AB08AD08A58C087DACD4A (void);
// 0x00000211 System.Boolean SS.View.TabSubController/<OnShownFake>d__15::MoveNext()
extern void U3COnShownFakeU3Ed__15_MoveNext_m43776D9371E7861ABC670E2585749F48F562AB45 (void);
// 0x00000212 System.Object SS.View.TabSubController/<OnShownFake>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnShownFakeU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m931BA7BBF4F775B400B233466BB956C8A034DDDF (void);
// 0x00000213 System.Void SS.View.TabSubController/<OnShownFake>d__15::System.Collections.IEnumerator.Reset()
extern void U3COnShownFakeU3Ed__15_System_Collections_IEnumerator_Reset_m8A93BAC0233FF83645A9B29522A2E000442A3E87 (void);
// 0x00000214 System.Object SS.View.TabSubController/<OnShownFake>d__15::System.Collections.IEnumerator.get_Current()
extern void U3COnShownFakeU3Ed__15_System_Collections_IEnumerator_get_Current_m38136C0C9E43E72FB4150E594AEE59CFE16FDCCB (void);
// 0x00000215 System.Boolean SS.Utils.Banner::get_isSmartBanner()
extern void Banner_get_isSmartBanner_mC04C86A7BF69C5E97835D5F2E6AB90631BAC6401 (void);
// 0x00000216 System.Void SS.Utils.Banner::set_isSmartBanner(System.Boolean)
extern void Banner_set_isSmartBanner_m0F76B11CB3EDEA25DFE884DEAA6AA341642513BF (void);
// 0x00000217 System.Single SS.Utils.Banner::GetSafeInsetBottom()
extern void Banner_GetSafeInsetBottom_m4EC77E26909FB220C242669D9EDAE90E8A8DD301 (void);
// 0x00000218 System.Single SS.Utils.Banner::GetBannerHeight()
extern void Banner_GetBannerHeight_m401D61BB3FA2620A50D1C9F8BEEE623143F372AF (void);
// 0x00000219 System.Single SS.Utils.Banner::GetBannerHeightRatio()
extern void Banner_GetBannerHeightRatio_mE62502B833CFAC642BDAD963E1C9C8E087C74BC6 (void);
// 0x0000021A System.Int32 SS.Utils.Banner::GetSdkInt()
extern void Banner_GetSdkInt_m69A012595A2BFAE13F0A7B07BB2FE25F1BD960F9 (void);
// 0x0000021B System.Void SS.Utils.Banner::.ctor()
extern void Banner__ctor_mA1DC4EDE4C79E1F6B948F23CFC7106A27A7F7D7F (void);
// 0x0000021C System.Single SS.Utils.Screen::GetCanvasScalerMatch()
extern void Screen_GetCanvasScalerMatch_mCC9A5160CEA1A6B4EB66B356996ECD41E2A10579 (void);
// 0x0000021D System.Single SS.Utils.Screen::GetLogicScreenHeight()
extern void Screen_GetLogicScreenHeight_m64DF0BBD745DE3125D1CA384C283483A459E4674 (void);
// 0x0000021E System.Int32 SS.Utils.Screen::GetSdkInt()
extern void Screen_GetSdkInt_m1F90166C847E3DB3E226688ECCBA6D7566B56FFA (void);
// 0x0000021F System.Void SS.Utils.Screen::SetFullScreen()
extern void Screen_SetFullScreen_mD352849000BE2EEEFBB66569D6D5604BEAAE716E (void);
// 0x00000220 System.Void SS.Utils.Screen::.ctor()
extern void Screen__ctor_mC9E02C5FD4872D61FD8DBCFCBD0FACA651C45F9D (void);
// 0x00000221 System.Void SS.Utils.Screen/<>c::.cctor()
extern void U3CU3Ec__cctor_mF606A2194705405CD1119BA0AFC100C616933ABD (void);
// 0x00000222 System.Void SS.Utils.Screen/<>c::.ctor()
extern void U3CU3Ec__ctor_mC9C6A4C89E68D3020AD8FDF211FAF8A6C6793308 (void);
// 0x00000223 System.Void SS.Utils.Screen/<>c::<SetFullScreen>b__4_0()
extern void U3CU3Ec_U3CSetFullScreenU3Eb__4_0_mA7342DBE84240A178017F8842D618DA71B7FB9CD (void);
// 0x00000224 System.Void Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::SetupProvider()
extern void SimpleEncryption_SetupProvider_m062A54324A39BD93751820D5A28C20485E65AF62 (void);
// 0x00000225 System.String Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::EncryptString(System.String)
extern void SimpleEncryption_EncryptString_mF881A228836861A1F6F0A4C4DCD4DE90D27AE269 (void);
// 0x00000226 System.String Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::DecryptString(System.String)
extern void SimpleEncryption_DecryptString_m506A08EBBB62F40BCED8E6DBD0DBB5F7A93BE834 (void);
// 0x00000227 System.String Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::EncryptFloat(System.Single)
extern void SimpleEncryption_EncryptFloat_m1974C78D4058D6BE84CBF657D2A694670283A9C4 (void);
// 0x00000228 System.String Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::EncryptInt(System.Int32)
extern void SimpleEncryption_EncryptInt_m3CE34CEE950215F8B4DB3A9E1DB4FFADE0D48299 (void);
// 0x00000229 System.String Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::EncryptBool(System.Boolean)
extern void SimpleEncryption_EncryptBool_m671235C931F7374A7D57DFED5F70B8E74E3AE0F4 (void);
// 0x0000022A System.Single Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::DecryptFloat(System.String)
extern void SimpleEncryption_DecryptFloat_mEB28B16A281B4CC9B599FDE33E647F71FE0BF58A (void);
// 0x0000022B System.Int32 Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::DecryptInt(System.String)
extern void SimpleEncryption_DecryptInt_m0530DF4FFFF499E22CA9C021279F314357BE6420 (void);
// 0x0000022C System.Boolean Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::DecryptBool(System.String)
extern void SimpleEncryption_DecryptBool_mF5B50F272F51850A91C13883D90688C016CD458B (void);
// 0x0000022D System.Void Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::.cctor()
extern void SimpleEncryption__cctor_m70AF6AC558970D030C7D90133BA8DFE7C46E16BE (void);
// 0x0000022E System.Object DeltaDNA.MiniJSON.Json::Deserialize(System.String)
extern void Json_Deserialize_mBBFB36E78850A1303259517EDE27BF76C57A2EDC (void);
// 0x0000022F System.String DeltaDNA.MiniJSON.Json::Serialize(System.Object)
extern void Json_Serialize_mC59C9F9BB9BF6CDB4749B896203B9314C54C5D88 (void);
// 0x00000230 System.Boolean DeltaDNA.MiniJSON.Json/Parser::IsWordBreak(System.Char)
extern void Parser_IsWordBreak_mF99D33120E987D267B8EEA33B9F6CD280FC1099A (void);
// 0x00000231 System.Void DeltaDNA.MiniJSON.Json/Parser::.ctor(System.String)
extern void Parser__ctor_m8E7B1E3413CB0ACB52B7EC3D42570BD50835CF07 (void);
// 0x00000232 System.Object DeltaDNA.MiniJSON.Json/Parser::Parse(System.String)
extern void Parser_Parse_mA51F5F7E7F54A8BD238A1F9FE42E38BEFC63ADAB (void);
// 0x00000233 System.Void DeltaDNA.MiniJSON.Json/Parser::Dispose()
extern void Parser_Dispose_mC5E065B71F9282AE393E76552E87F12E225BB471 (void);
// 0x00000234 System.Collections.Generic.Dictionary`2<System.String,System.Object> DeltaDNA.MiniJSON.Json/Parser::ParseObject()
extern void Parser_ParseObject_m3F2F42E3C1643E992303AD52733844635AAE185E (void);
// 0x00000235 System.Collections.Generic.List`1<System.Object> DeltaDNA.MiniJSON.Json/Parser::ParseArray()
extern void Parser_ParseArray_m77AE0FC5101EC5395B675E5E48BCE02829BFF926 (void);
// 0x00000236 System.Object DeltaDNA.MiniJSON.Json/Parser::ParseValue()
extern void Parser_ParseValue_m380E37286A8F41902BDFF469A62314E0450ADD9D (void);
// 0x00000237 System.Object DeltaDNA.MiniJSON.Json/Parser::ParseByToken(DeltaDNA.MiniJSON.Json/Parser/TOKEN)
extern void Parser_ParseByToken_mE2527D6EEB3A02B878D32032921DB5453980007E (void);
// 0x00000238 System.String DeltaDNA.MiniJSON.Json/Parser::ParseString()
extern void Parser_ParseString_m6DFDC932B48CB2140FCD74416FF0C07ABF34702A (void);
// 0x00000239 System.Object DeltaDNA.MiniJSON.Json/Parser::ParseNumber()
extern void Parser_ParseNumber_mCFE1CCFAE9CA433971DCDAAD73BC32E3E7DB401C (void);
// 0x0000023A System.Void DeltaDNA.MiniJSON.Json/Parser::EatWhitespace()
extern void Parser_EatWhitespace_mD0804D4B845AF21A5B8D2525F790898BDA2290FD (void);
// 0x0000023B System.Char DeltaDNA.MiniJSON.Json/Parser::get_PeekChar()
extern void Parser_get_PeekChar_mA03F49AC460B398EFECEB43713F62B3DA4E397C2 (void);
// 0x0000023C System.Char DeltaDNA.MiniJSON.Json/Parser::get_NextChar()
extern void Parser_get_NextChar_m2F9F54F34F726B5758B4CB735A84FBA854F3DDDE (void);
// 0x0000023D System.String DeltaDNA.MiniJSON.Json/Parser::get_NextWord()
extern void Parser_get_NextWord_m00AE793021AC94171F0CBF824885513DF239D8AF (void);
// 0x0000023E DeltaDNA.MiniJSON.Json/Parser/TOKEN DeltaDNA.MiniJSON.Json/Parser::get_NextToken()
extern void Parser_get_NextToken_mA87FAFC85B2F55E74C985D92BAD0FCC2E3F944A3 (void);
// 0x0000023F System.Void DeltaDNA.MiniJSON.Json/Serializer::.ctor()
extern void Serializer__ctor_m9723BEB6C7BF139A9C18D8A488BE501CADE2A7CA (void);
// 0x00000240 System.String DeltaDNA.MiniJSON.Json/Serializer::Serialize(System.Object)
extern void Serializer_Serialize_m645317E54BD40151210F873650D3D7D12B06A389 (void);
// 0x00000241 System.Void DeltaDNA.MiniJSON.Json/Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_m72FE1385F3CF53BA14FA18AA503B9E0733A26877 (void);
// 0x00000242 System.Void DeltaDNA.MiniJSON.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_m49CF3C61AD626B4B126C7CC776B5F6BEE0DC25AE (void);
// 0x00000243 System.Void DeltaDNA.MiniJSON.Json/Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_m68C42E473E846D9314DC9D9A6EF2C374F3A0899C (void);
// 0x00000244 System.Void DeltaDNA.MiniJSON.Json/Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_m8960049B70B1E72FD1C478DD8628F03317ABBCB9 (void);
// 0x00000245 System.Void DeltaDNA.MiniJSON.Json/Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_m1397447055994BF1BE52AD7099357421DCD8C3CD (void);
// 0x00000246 System.Void EpicToonFX.ETFXButtonScript::Start()
extern void ETFXButtonScript_Start_m25A1DDC0BEB25CBCB20EB6FBE9E71F006138B3C1 (void);
// 0x00000247 System.Void EpicToonFX.ETFXButtonScript::Update()
extern void ETFXButtonScript_Update_m22634A284FB8C975D9929F15988F09FA1ACDF332 (void);
// 0x00000248 System.Void EpicToonFX.ETFXButtonScript::getProjectileNames()
extern void ETFXButtonScript_getProjectileNames_mA13342862E48B4A8B6BC0666BC7EFE2BB5625D3E (void);
// 0x00000249 System.Boolean EpicToonFX.ETFXButtonScript::overButton()
extern void ETFXButtonScript_overButton_mEFE1C511D8EEA91AE9E54B97CD0A155E542290B0 (void);
// 0x0000024A System.Void EpicToonFX.ETFXButtonScript::.ctor()
extern void ETFXButtonScript__ctor_m65F38109C2CBF445E711A2C6646B34C2483E0455 (void);
// 0x0000024B System.Void EpicToonFX.ETFXFireProjectile::Start()
extern void ETFXFireProjectile_Start_m6586051FBDDC4104ED997E585ABEA089694B805D (void);
// 0x0000024C System.Void EpicToonFX.ETFXFireProjectile::Update()
extern void ETFXFireProjectile_Update_m4F39FFB37EDCE10D6037FDE13FF274C027F60990 (void);
// 0x0000024D System.Void EpicToonFX.ETFXFireProjectile::nextEffect()
extern void ETFXFireProjectile_nextEffect_m406DBBEAF4133E7C3660AB0F5BC32CD6B3B0C7C5 (void);
// 0x0000024E System.Void EpicToonFX.ETFXFireProjectile::previousEffect()
extern void ETFXFireProjectile_previousEffect_m33C82A976F62A20D43E2FF8C57E3066DCB00AE5F (void);
// 0x0000024F System.Void EpicToonFX.ETFXFireProjectile::AdjustSpeed(System.Single)
extern void ETFXFireProjectile_AdjustSpeed_m0A76F61B5320E509827716B5B302AE41B028288A (void);
// 0x00000250 System.Void EpicToonFX.ETFXFireProjectile::.ctor()
extern void ETFXFireProjectile__ctor_mB48108DD5AB58E5907180ED79690E217D8EB1D65 (void);
// 0x00000251 System.Void EpicToonFX.ETFXLoopScript::Start()
extern void ETFXLoopScript_Start_mE67C5E09B7AB2A78688B1877DAC3766A0868CCA8 (void);
// 0x00000252 System.Void EpicToonFX.ETFXLoopScript::PlayEffect()
extern void ETFXLoopScript_PlayEffect_m2EAE66A60E00C863C15BE46D10AC4522789A218E (void);
// 0x00000253 System.Collections.IEnumerator EpicToonFX.ETFXLoopScript::EffectLoop()
extern void ETFXLoopScript_EffectLoop_m728B76DFD989B681D2C93D7A88E9D76C9696CAF4 (void);
// 0x00000254 System.Void EpicToonFX.ETFXLoopScript::.ctor()
extern void ETFXLoopScript__ctor_mC0B4A260353A71EC89B2E1ADA7834286E0F60E5B (void);
// 0x00000255 System.Void EpicToonFX.ETFXLoopScript/<EffectLoop>d__6::.ctor(System.Int32)
extern void U3CEffectLoopU3Ed__6__ctor_m553F1C586BBB6C68EEB9710F62438A8EDCE136DA (void);
// 0x00000256 System.Void EpicToonFX.ETFXLoopScript/<EffectLoop>d__6::System.IDisposable.Dispose()
extern void U3CEffectLoopU3Ed__6_System_IDisposable_Dispose_m8142A69BF60253C0139648E9350176EB9EFD8BFA (void);
// 0x00000257 System.Boolean EpicToonFX.ETFXLoopScript/<EffectLoop>d__6::MoveNext()
extern void U3CEffectLoopU3Ed__6_MoveNext_mE11D3E615622E410D2DAF1960011C76F8ABC322D (void);
// 0x00000258 System.Object EpicToonFX.ETFXLoopScript/<EffectLoop>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEffectLoopU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m93EE11485A4BA42D80F240C1703D673774889545 (void);
// 0x00000259 System.Void EpicToonFX.ETFXLoopScript/<EffectLoop>d__6::System.Collections.IEnumerator.Reset()
extern void U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_Reset_mB35745D308CDED6CAB69D52DEFC2D84AB6CC5896 (void);
// 0x0000025A System.Object EpicToonFX.ETFXLoopScript/<EffectLoop>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_get_Current_mB56FD34E205892C6B85E8FD18DAE68F19ECBDE2E (void);
// 0x0000025B System.Void EpicToonFX.ETFXMouseOrbit::Start()
extern void ETFXMouseOrbit_Start_m4FB8182859BA3A3D8139156DFFFF58A6001F9297 (void);
// 0x0000025C System.Void EpicToonFX.ETFXMouseOrbit::LateUpdate()
extern void ETFXMouseOrbit_LateUpdate_m27A95AC3E473534D0DAD68B5B76579A0811D8CEC (void);
// 0x0000025D System.Single EpicToonFX.ETFXMouseOrbit::ClampAngle(System.Single,System.Single,System.Single)
extern void ETFXMouseOrbit_ClampAngle_mA204BF97BDDC49A732917C75BDC934D4CF56B314 (void);
// 0x0000025E System.Void EpicToonFX.ETFXMouseOrbit::.ctor()
extern void ETFXMouseOrbit__ctor_mB2E0C52CC44B7FE5D064C3183F9DED7714DDCBDC (void);
// 0x0000025F System.Void EpicToonFX.ETFXTarget::Start()
extern void ETFXTarget_Start_m60CA8E1079E17A7C9A47D10C9FA8AE9220DB1E9F (void);
// 0x00000260 System.Void EpicToonFX.ETFXTarget::SpawnTarget()
extern void ETFXTarget_SpawnTarget_m00485209171AF5E1A8DB572C256C99B0E821BF96 (void);
// 0x00000261 System.Void EpicToonFX.ETFXTarget::OnTriggerEnter(UnityEngine.Collider)
extern void ETFXTarget_OnTriggerEnter_m057AF200E64829505145549ED629FAA79935B1F5 (void);
// 0x00000262 System.Collections.IEnumerator EpicToonFX.ETFXTarget::Respawn()
extern void ETFXTarget_Respawn_m679D0B0188B3DA281D2BE313E7212FC1D6F2B905 (void);
// 0x00000263 System.Void EpicToonFX.ETFXTarget::.ctor()
extern void ETFXTarget__ctor_mD0F9BFFB2CC402DC5C6FF274EEDD58EB891A22C6 (void);
// 0x00000264 System.Void EpicToonFX.ETFXTarget/<Respawn>d__7::.ctor(System.Int32)
extern void U3CRespawnU3Ed__7__ctor_m415513F665DE8F3777A3B21AF9C5B45A90CB1BF3 (void);
// 0x00000265 System.Void EpicToonFX.ETFXTarget/<Respawn>d__7::System.IDisposable.Dispose()
extern void U3CRespawnU3Ed__7_System_IDisposable_Dispose_m63C5D414DB8D33EB86AFF6D5569B6C4E18C32615 (void);
// 0x00000266 System.Boolean EpicToonFX.ETFXTarget/<Respawn>d__7::MoveNext()
extern void U3CRespawnU3Ed__7_MoveNext_m58D6E6D0CFF4964FDE4083115A10119C4FB6FEE3 (void);
// 0x00000267 System.Object EpicToonFX.ETFXTarget/<Respawn>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRespawnU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF70DB558C29B7FFE4A790553093711C3478A2C54 (void);
// 0x00000268 System.Void EpicToonFX.ETFXTarget/<Respawn>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRespawnU3Ed__7_System_Collections_IEnumerator_Reset_m37DFF9115E528EA7997B687DD51321AB843D9BE7 (void);
// 0x00000269 System.Object EpicToonFX.ETFXTarget/<Respawn>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRespawnU3Ed__7_System_Collections_IEnumerator_get_Current_mA84FD67FBB5DE784889C67146E66570FC2E5D80E (void);
// 0x0000026A System.Void EpicToonFX.ETFXLightFade::Start()
extern void ETFXLightFade_Start_m80EEDD11315E813676DA8E550C806F8C96374733 (void);
// 0x0000026B System.Void EpicToonFX.ETFXLightFade::Update()
extern void ETFXLightFade_Update_m3705B92646C8612BC70987DF39BC94B8F88EC49D (void);
// 0x0000026C System.Void EpicToonFX.ETFXLightFade::.ctor()
extern void ETFXLightFade__ctor_mCDF41DC7334BC08F86DD3EF815FC1F3EA4C44F11 (void);
// 0x0000026D System.Void EpicToonFX.ETFXPitchRandomizer::Start()
extern void ETFXPitchRandomizer_Start_m64BE3B77C622908C2E324C6D3DFD17CB9092D21E (void);
// 0x0000026E System.Void EpicToonFX.ETFXPitchRandomizer::.ctor()
extern void ETFXPitchRandomizer__ctor_m8493E0829884B225C1FC98134FA1EFCC44CDD238 (void);
// 0x0000026F System.Void EpicToonFX.ETFXRotation::Start()
extern void ETFXRotation_Start_m3DC21DA9C26DBA1E14C27B9B997F46BEBBFB22C7 (void);
// 0x00000270 System.Void EpicToonFX.ETFXRotation::Update()
extern void ETFXRotation_Update_mED37E3F435A6166C540A1DA30B49354F05279100 (void);
// 0x00000271 System.Void EpicToonFX.ETFXRotation::.ctor()
extern void ETFXRotation__ctor_mC1C7849D64611953B026F069EE879C10B1399ABA (void);
// 0x00000272 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOFade(UnityEngine.AudioSource,System.Single,System.Single)
extern void DOTweenModuleAudio_DOFade_mEA99A941484D71522BCDD1CCC43B790A6AF4A0B3 (void);
// 0x00000273 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOPitch(UnityEngine.AudioSource,System.Single,System.Single)
extern void DOTweenModuleAudio_DOPitch_m6512EC4BEE6C70E110DDFC1048BFCE13FA87E914 (void);
// 0x00000274 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOSetFloat(UnityEngine.Audio.AudioMixer,System.String,System.Single,System.Single)
extern void DOTweenModuleAudio_DOSetFloat_m8831E89C2E951DDAC9C20A46D3AAD329BAD9C09C (void);
// 0x00000275 System.Int32 DG.Tweening.DOTweenModuleAudio::DOComplete(UnityEngine.Audio.AudioMixer,System.Boolean)
extern void DOTweenModuleAudio_DOComplete_m49C5BD0D315241E09EA8D6398F8011C5A34646AE (void);
// 0x00000276 System.Int32 DG.Tweening.DOTweenModuleAudio::DOKill(UnityEngine.Audio.AudioMixer,System.Boolean)
extern void DOTweenModuleAudio_DOKill_m2CED6088E868165A2D91768409FAFFD980993747 (void);
// 0x00000277 System.Int32 DG.Tweening.DOTweenModuleAudio::DOFlip(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOFlip_m080FDDE3ECDF68901289FBC6EA1297EF278D9D36 (void);
// 0x00000278 System.Int32 DG.Tweening.DOTweenModuleAudio::DOGoto(UnityEngine.Audio.AudioMixer,System.Single,System.Boolean)
extern void DOTweenModuleAudio_DOGoto_mD4A9A702C6187927FDBDD3220570820533459D16 (void);
// 0x00000279 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPause(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPause_mD42B2DFA89EA1635A6F9E813F7B852C852F282E5 (void);
// 0x0000027A System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlay(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlay_m95942072A27A597B1FF4FA58A56EC35EC3A8DC15 (void);
// 0x0000027B System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlayBackwards(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlayBackwards_mA2814EE20AB3527ADC90DAC391BDF4AD2DA63104 (void);
// 0x0000027C System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlayForward(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlayForward_m808D504EA5FCA7B43292D4024735C69339BEC168 (void);
// 0x0000027D System.Int32 DG.Tweening.DOTweenModuleAudio::DORestart(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DORestart_m53CC4DB3E3B69233410704AC28B0E7E5FE15F9B6 (void);
// 0x0000027E System.Int32 DG.Tweening.DOTweenModuleAudio::DORewind(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DORewind_m1102048CF3E759CC5A0B7AF48B37092DD8625D5E (void);
// 0x0000027F System.Int32 DG.Tweening.DOTweenModuleAudio::DOSmoothRewind(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOSmoothRewind_m4C703A054EDA680CDA0FE09924AD97775BF70DA1 (void);
// 0x00000280 System.Int32 DG.Tweening.DOTweenModuleAudio::DOTogglePause(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOTogglePause_mA1AE6D3A3A8FA9692CE0695E915AA08626EF61CF (void);
// 0x00000281 System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mB8ECD32BCDE8AEE5DA844CB16FB319546FDF65F4 (void);
// 0x00000282 System.Single DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m6447B544882FDCCECF5F03C92CFD462E52F55C3C (void);
// 0x00000283 System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mCBA7DC56068D62779FD9318BF1BF6CC04674AA73 (void);
// 0x00000284 System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mD593A9B61B5F03AF9D1695ED9FA2C1E993F41739 (void);
// 0x00000285 System.Single DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass1_0::<DOPitch>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__0_mB6B65C2ED3194137382A4DC81F72AB5C2C95FEB7 (void);
// 0x00000286 System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass1_0::<DOPitch>b__1(System.Single)
extern void U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__1_m9ED5ED10167C9FA210B584AAC22893154A683518 (void);
// 0x00000287 System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m3D650640D73868903AB5235513EB20B1EF57851E (void);
// 0x00000288 System.Single DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass2_0::<DOSetFloat>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__0_m1B04734B7D8B9A386220DCDED11506098EDF5039 (void);
// 0x00000289 System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass2_0::<DOSetFloat>b__1(System.Single)
extern void U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__1_mDE42A0D8DA684D1F5E21CA1797A119D6BF6252C3 (void);
// 0x0000028A DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMove(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMove_mAD664CEB3F56A94F3481FA83FC799F0D7F8FF53F (void);
// 0x0000028B DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveX(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveX_m4CD527117E19DE2009884655A131DF83B9EAD4A9 (void);
// 0x0000028C DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveY(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveY_m126BCA31B85BF67D1B5E85549A95802B69777A2C (void);
// 0x0000028D DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveZ(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveZ_mDC3444100D64651B456A118C92B6F77C99C32597 (void);
// 0x0000028E DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTweenModulePhysics::DORotate(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern void DOTweenModulePhysics_DORotate_m2BDE8501CE323175A7471D694249E660F5BF068C (void);
// 0x0000028F DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTweenModulePhysics::DOLookAt(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.AxisConstraint,System.Nullable`1<UnityEngine.Vector3>)
extern void DOTweenModulePhysics_DOLookAt_m706C3BF65EDBCCB2562FB115E61151945941C5FC (void);
// 0x00000290 DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics::DOJump(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOJump_mCDD95D56D93498D68A345E57B3C359388A816821 (void);
// 0x00000291 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics_DOPath_mF69DF997EF91CD5347391BC89E6CE268A5CD39DD (void);
// 0x00000292 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics_DOLocalPath_mB056B81341D5048E516138EBE4A680B3F4664D6D (void);
// 0x00000293 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_DOPath_m3C49FFEB71D494F474F7223248D7105E90CF5BE7 (void);
// 0x00000294 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_DOLocalPath_m6F939DAF66D56DA9B52DA62546C70F3862E14193 (void);
// 0x00000295 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mCFBBC383950DE52D74C6F4E85C4F7F3273B10719 (void);
// 0x00000296 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass0_0::<DOMove>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_m8158605D880C075EEDC090F3BD1175AA33E2E107 (void);
// 0x00000297 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m4E41921AB96267B39AAB22B0790884BEB58B467D (void);
// 0x00000298 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass1_0::<DOMoveX>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m0EEE1A4D5C020E63505FB9F7E09A531433FC817D (void);
// 0x00000299 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mA7A465D617CE4C839F719F65F44B2D806976FC71 (void);
// 0x0000029A UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass2_0::<DOMoveY>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m4930252334FB6162D77F0821409D7487A96CB696 (void);
// 0x0000029B System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m9E94ECE0838CF218AB657F8C4B51F881B310EF34 (void);
// 0x0000029C UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass3_0::<DOMoveZ>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOMoveZU3Eb__0_mF75D2479FE18CCE0BFC07AA4A06B1EBE01369D29 (void);
// 0x0000029D System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m1A01BE964AEBF9737E72E7C9B3B5D660D965B936 (void);
// 0x0000029E UnityEngine.Quaternion DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass4_0::<DORotate>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDORotateU3Eb__0_m6B41E1A9419EDB0B9FDE8C9A62BDAA1281D19EF5 (void);
// 0x0000029F System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mF63278B7093557C5BF31EE7878AC0E87D82EEC74 (void);
// 0x000002A0 UnityEngine.Quaternion DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass5_0::<DOLookAt>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOLookAtU3Eb__0_m2019767096427977C234F4A6698E6C2559840795 (void);
// 0x000002A1 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m06AC3FA2094299E1FDE7ECAFCC8F970C5139B4FE (void);
// 0x000002A2 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::<DOJump>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__0_m847B6B84B9DBBB179602A2DAC20A1E47B7CD0C6F (void);
// 0x000002A3 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::<DOJump>b__1()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__1_m488237F614033272B64730F763C4EFCD4009ACDB (void);
// 0x000002A4 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::<DOJump>b__2()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__2_m6F586BA8E7E757466B5E77AB61BD0056F4D86D28 (void);
// 0x000002A5 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::<DOJump>b__3()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__3_mEC9264AAF5B45E16B176AFA00079BFDA173EDDFE (void);
// 0x000002A6 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::<DOJump>b__4()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__4_m15CD7F1F9A30E34BA9AC943FA510B9FF0C15F84E (void);
// 0x000002A7 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m177CD3D0B0169C7DF1FD6F64FB8C39D0F92E7023 (void);
// 0x000002A8 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass7_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__0_m448E92A163A6A98FFE561134611FAD6418C6824B (void);
// 0x000002A9 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m4499D3AB924EEB9EB850233A8F64E5EC4A7AEA3D (void);
// 0x000002AA UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass8_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__0_mAB235341BB5079CA397BF09B075B856C6B950EDF (void);
// 0x000002AB System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass8_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__1_m19CF06E0D31E737EFDEEFE6D4D2361EA0D372730 (void);
// 0x000002AC System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mCA1E6F241784A13DEC15E12A0219479D552BB6BB (void);
// 0x000002AD UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_mD050544C13BA54260FC94E0964EF204B4EDB2628 (void);
// 0x000002AE System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m819824BC9854A0C90662B04D48B88CA7B052BF83 (void);
// 0x000002AF UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_m3089CA33F25546258A5E846EA51421D71AC55D70 (void);
// 0x000002B0 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mD201EFBB4403FBDEC0D377D39A768A581E6CCDDD (void);
// 0x000002B1 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMove(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMove_mC7C9AD67230F89E53BCC36B37BC699E6E3DBFCC9 (void);
// 0x000002B2 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMoveX(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMoveX_m47B42CE055F1892B75820F6FDE992E541394D23E (void);
// 0x000002B3 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMoveY(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMoveY_mBFBABC3C73C6D858C280B8E0E21869BCFC57938F (void);
// 0x000002B4 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModulePhysics2D::DORotate(UnityEngine.Rigidbody2D,System.Single,System.Single)
extern void DOTweenModulePhysics2D_DORotate_m8E6DBA35EFB07C71A288D36A86B1299D99D2BE6A (void);
// 0x000002B5 DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics2D::DOJump(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOJump_m13774D66ED37FC3FF7BA6862C306D32C0B01C351 (void);
// 0x000002B6 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics2D::DOPath(UnityEngine.Rigidbody2D,UnityEngine.Vector2[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics2D_DOPath_mC8E35580B3D00EC82A6EB5150838D2D220978092 (void);
// 0x000002B7 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics2D::DOLocalPath(UnityEngine.Rigidbody2D,UnityEngine.Vector2[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics2D_DOLocalPath_m56F836B516F2576C6CF48E07518EF403ED233844 (void);
// 0x000002B8 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics2D::DOPath(UnityEngine.Rigidbody2D,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics2D_DOPath_m45376A9D96DC70D4150654D08A1A15AD21A158B1 (void);
// 0x000002B9 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics2D::DOLocalPath(UnityEngine.Rigidbody2D,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics2D_DOLocalPath_mE80C06CF4F281FC00E6E4DEDDDADB145C36091E6 (void);
// 0x000002BA System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m28BD574ECE1FAC2A8040F507B4347CA641B883C8 (void);
// 0x000002BB UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass0_0::<DOMove>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_mD0BB74EE7E3ED3D15C2B501656DD41CB4DB1CFB0 (void);
// 0x000002BC System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m0A358EE93B5CBA3BAAFED5EFA3CB29B852CC0A8C (void);
// 0x000002BD UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass1_0::<DOMoveX>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m890A07E5B25CF040E903014587C8C30FC43505CF (void);
// 0x000002BE System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m79644C3E99A1BADC2CF695A33996EFBE999BCA28 (void);
// 0x000002BF UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass2_0::<DOMoveY>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m833E99A0F3AFA6D89F501FE08B0EF7371AE20D4C (void);
// 0x000002C0 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m37C53B07CACF2CE16A6FECB187B45E328882E6CC (void);
// 0x000002C1 System.Single DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass3_0::<DORotate>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDORotateU3Eb__0_mE8799B2B2CD516708D2235A0D1822AF9AEA117DF (void);
// 0x000002C2 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m35FA34381650BF0B9A9C29EE83B8043082D0A482 (void);
// 0x000002C3 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__0_m66684B2056E4B92E933AD98C2EAD6EDD9AF9C174 (void);
// 0x000002C4 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__1_m99B395C93C8827215768541DBD48AAFCAA4014EC (void);
// 0x000002C5 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__2()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__2_m9D2C7FBC51C4DBCCD91E8359A6FCD6D5BD633FB7 (void);
// 0x000002C6 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__3()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__3_mEC32920DB355DC7E1AAF2249C33F3177670806E8 (void);
// 0x000002C7 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__4(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__4_m053F71A269824B99AA058986F6E772F950C453C7 (void);
// 0x000002C8 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__5()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__5_mC21E068A8BCD02C771D362D4397E7C1113CC2E3D (void);
// 0x000002C9 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mC0712263E288AF8337D78ACD1B4187EC2A79507D (void);
// 0x000002CA UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass5_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOPathU3Eb__0_m2DD131B5B01242CEE25D2865DF2F0BC59A4753B0 (void);
// 0x000002CB System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass5_0::<DOPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass5_0_U3CDOPathU3Eb__1_m80D3EF48ACDCE8FCE54C12A18D6A47AEC649ECBC (void);
// 0x000002CC System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m56FEA3991E5AE4A9842780C1DAFF6AF473C21946 (void);
// 0x000002CD UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass6_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CDOLocalPathU3Eb__0_mC6200A1DCC1B49838E2F65C06843DA00BBBA220C (void);
// 0x000002CE System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass6_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass6_0_U3CDOLocalPathU3Eb__1_mA1E8B523414CEE79D40909E7D32EE7347C0E7A8B (void);
// 0x000002CF System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mDAFC1070A51179D6DFA2275AAE0834CE09FA1C91 (void);
// 0x000002D0 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass7_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__0_m72756DA3FF5827EF7CC02F5294B18C2E54F0E261 (void);
// 0x000002D1 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass7_0::<DOPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__1_mAF47A28E7DB74ACE3A323933545400C727F1A180 (void);
// 0x000002D2 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m83F10B0119C62300F29466339E8CC349AFF732B3 (void);
// 0x000002D3 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass8_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__0_mEC0CC796C9C9CD3EF8936CA7549563F049C5ECE1 (void);
// 0x000002D4 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass8_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__1_m60086A5834BCFBA099FAB79A20ED404ECFF49534 (void);
// 0x000002D5 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOColor_mA67D7B77174B479F8AE20F739A74BA7A2BBC9F0F (void);
// 0x000002D6 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOFade(UnityEngine.SpriteRenderer,System.Single,System.Single)
extern void DOTweenModuleSprite_DOFade_mEED04CEE3EE9D32B87478A3CFDB9CFB710307769 (void);
// 0x000002D7 DG.Tweening.Sequence DG.Tweening.DOTweenModuleSprite::DOGradientColor(UnityEngine.SpriteRenderer,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleSprite_DOGradientColor_m47670ABD64AEDC972E8210CAF4C8A778B0B7676D (void);
// 0x000002D8 DG.Tweening.Tweener DG.Tweening.DOTweenModuleSprite::DOBlendableColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOBlendableColor_m51BBDB4A69B0560481736E6734EA2BF52DB1537D (void);
// 0x000002D9 System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mD77E24E24423ADEC43F9983504BD6796D2671E99 (void);
// 0x000002DA UnityEngine.Color DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m12BAC3674340BDF408F41A0E9BEEF6BB87C3F504 (void);
// 0x000002DB System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_m89E34AE3BBC636BD2B38C1AB7300F1F20F25E616 (void);
// 0x000002DC System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m40F18C57ED175BA68D1D65FFA97879AEAF2E3325 (void);
// 0x000002DD UnityEngine.Color DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__0_m8B9E647B54E5DA35EE71D65866372C8C90B7F452 (void);
// 0x000002DE System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__1_m935CE2E2B7CB25D7151DA0BF3B4420C8099E0A45 (void);
// 0x000002DF System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mB0AA92A12DE97E8954819AC2BAD916065D4BECDB (void);
// 0x000002E0 UnityEngine.Color DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__0_m371A7E7E53DB5FC70A6534A7956B27A3A8B7EA03 (void);
// 0x000002E1 System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__1_m36515014C810F349D12C86DC636349E452DA6974 (void);
// 0x000002E2 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mF927A1EDCA91814D8A466A343E3223EEF4ED7478 (void);
// 0x000002E3 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m7067683474EE6A600294C2CCFD941403FA789A6E (void);
// 0x000002E4 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Graphic,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m41638D05EB7678C523D0D39E320FAC80B8E48658 (void);
// 0x000002E5 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mF4B41443760536F3C399612AD0283A52937D95CC (void);
// 0x000002E6 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754 (void);
// 0x000002E7 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFillAmount(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFillAmount_mE4ED52B826A6F8035320E980F7C701B4047C4DBA (void);
// 0x000002E8 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOGradientColor(UnityEngine.UI.Image,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUI_DOGradientColor_mB4B8D1DEDA92464670EB8A6D985973477D51A4D5 (void);
// 0x000002E9 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOFlexibleSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOFlexibleSize_mF0916C4E7A906202EFBAEA89CC2B55A9C3CF48B3 (void);
// 0x000002EA DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOMinSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOMinSize_m54A6919625499AFD6F801C6EC9672965DBDC96E1 (void);
// 0x000002EB DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPreferredSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPreferredSize_mE26AFB2C606F095483EDED449FB8A5A80C08A8DE (void);
// 0x000002EC DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Outline,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m15BC0FCF035E3DEEC3CFC30465EDE0FE6FD6D2EA (void);
// 0x000002ED DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Outline,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m86DB80308D92FC4DFD3E981F9E958FF2A207EA74 (void);
// 0x000002EE DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOScale(UnityEngine.UI.Outline,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOScale_mFA27F29E3DFB6B1CFCAC3CE73F06DF3737A3EB06 (void);
// 0x000002EF DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos_m63999D434CE7E8F6BE66BBA92532DE9D98DC9F79 (void);
// 0x000002F0 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosX_m9B57E02E9902C7A3F1A8DD1021AF630C10478380 (void);
// 0x000002F1 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosY_m14CCA63E545D7C28AD1A76488D0084F55EE94CA8 (void);
// 0x000002F2 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3D(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3D_m6DEB7C4AE4CD2118BF83D268945340F3373DC7BB (void);
// 0x000002F3 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DX_mA40303AC769280BDE74EE58BE5AD80BEA427CE08 (void);
// 0x000002F4 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DY_mE6BE56AE421B959F22CBA6318BABC3CD367A7277 (void);
// 0x000002F5 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DZ(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DZ_m702F7D4FE9DC8E6116FB5E0DD478D2BB88ED1242 (void);
// 0x000002F6 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMax(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMax_mDD97F6A8B4468622E3808507CB6B89A8A109BDCE (void);
// 0x000002F7 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMin(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMin_m60C4775B1EEDBCF2CD3E96A8171C8A83FD7F400D (void);
// 0x000002F8 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivot(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOPivot_mF623939EDCC152F664FECC8B71E321B0AA13F0A9 (void);
// 0x000002F9 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotX(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotX_mC21C031A61BDB7C3A8087719F23CBC6D1D762D6B (void);
// 0x000002FA DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotY(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotY_m9922C28ADECA433362D3C6C816318CA00705996A (void);
// 0x000002FB DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOSizeDelta(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOSizeDelta_mC4FD07525E1CA5BBF577030D571CE0CCEFD4E6B7 (void);
// 0x000002FC DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOPunchAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPunchAnchorPos_m058E3DA140B9C497727F05AEAA296425E585B9EA (void);
// 0x000002FD DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,System.Single,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_m89D1C74E940B9434B2234FB49C031AD8FD1170AB (void);
// 0x000002FE DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,UnityEngine.Vector2,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_m7818867062313A535AB2CD6F86267E2AB9357AC6 (void);
// 0x000002FF DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOJumpAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOJumpAnchorPos_m7046EE1C82AF0159CFAAA460E4C3D22797625480 (void);
// 0x00000300 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DONormalizedPos(UnityEngine.UI.ScrollRect,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DONormalizedPos_m300F112FE2F67D4DC718083340C7FD27BC96F248 (void);
// 0x00000301 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOHorizontalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOHorizontalNormalizedPos_mCB4BCD85B49C458BE871F890A86FEFE0D7F41B42 (void);
// 0x00000302 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOVerticalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOVerticalNormalizedPos_mC0269B4644816A0D5C09717CA92E4229CB31CDF1 (void);
// 0x00000303 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOValue(UnityEngine.UI.Slider,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOValue_mFDF45D60F52C82AACD8B50859A51B8F315A44F08 (void);
// 0x00000304 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mFD36CA1EA683A0D2312EE87BCF3B0A924504E797 (void);
// 0x00000305 DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions> DG.Tweening.DOTweenModuleUI::DOCounter(UnityEngine.UI.Text,System.Int32,System.Int32,System.Single,System.Boolean,System.Globalization.CultureInfo)
extern void DOTweenModuleUI_DOCounter_m6C87AB89E5B14F1FB82630CFF15791678D7DF055 (void);
// 0x00000306 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1 (void);
// 0x00000307 DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions> DG.Tweening.DOTweenModuleUI::DOText(UnityEngine.UI.Text,System.String,System.Single,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern void DOTweenModuleUI_DOText_mC0932E45473278B2FFB1698954CC0EB0422C626F (void);
// 0x00000308 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m4C5E9A68BFBEE876E1FF8D8E9A4BA77856F144C1 (void);
// 0x00000309 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m18196516A24780A550314E9327868D6D6F3EAA4E (void);
// 0x0000030A DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_mC30D1F805FE976FB25EC361AB0ABA94413361B6F (void);
// 0x0000030B DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.CircleOptions> DG.Tweening.DOTweenModuleUI::DOShapeCircle(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShapeCircle_mF3FE405DFB4AB92952A45A6E716682AAC3D57639 (void);
// 0x0000030C UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/Utils::SwitchToRectTransform(UnityEngine.RectTransform,UnityEngine.RectTransform)
extern void Utils_SwitchToRectTransform_m260A15449F1C7A8F4356730DF4A59A386C45D200 (void);
// 0x0000030D System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m14E6397A137D3CB8A7638F9254B4694553B4DC7C (void);
// 0x0000030E System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m6F0CD242FAEF332D726AD4CE2EB2691CCB3748B1 (void);
// 0x0000030F System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mDBDE14547A6DF70C0ADB82403282C26FBB4F6A27 (void);
// 0x00000310 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mCB63EFBBE399EEB9892054E0FAB2AB42E2BBB886 (void);
// 0x00000311 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_mE8D0790E2303D84F8D513854D9F004E5E3127A3C (void);
// 0x00000312 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m07CB6A64D93955BC07E8AE290B01F9F39195A5A9 (void);
// 0x00000313 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m61C588C0764BA908B02EBAB1C19076EBB2898A9E (void);
// 0x00000314 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_m85CC4106263DD670C5BF648B8A64C6BA84297B65 (void);
// 0x00000315 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_m140BDA0B825925A5051A2565C055CCBBF005A680 (void);
// 0x00000316 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m09228DABE56F6866E09591FE27C255A39A71E48D (void);
// 0x00000317 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_mA7BF7CEB8AA94694104E8D048F5045D122A5D980 (void);
// 0x00000318 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_mBC7108789E76320E9507A9087273C7D89227F0F3 (void);
// 0x00000319 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m5CD451B862F3CC71D8D25BC3A69725FCDD6144C4 (void);
// 0x0000031A UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m507CD55516001BB4A76D7E58CB159B5394D6413C (void);
// 0x0000031B System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_mE65F1E4D380EC305AF0AE93E0315B0B23CA36310 (void);
// 0x0000031C System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m879B3843E0C43157D8044AD52E2F865EE50FD041 (void);
// 0x0000031D System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0::<DOFillAmount>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_mAFE3629BC44AE61C84399809F742CEF0235004E1 (void);
// 0x0000031E System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0::<DOFillAmount>b__1(System.Single)
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_m9EB6C21BCE8D1A08FF84815C05A6664637C510CE (void);
// 0x0000031F System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m47B2E10FFFD9FCACC88586DEF6A4E675DC9EC406 (void);
// 0x00000320 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0::<DOFlexibleSize>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_m7A7EA5F66922413743B8C1AB4234645A32098EA8 (void);
// 0x00000321 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0::<DOFlexibleSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_m0E76B7C6F893FD2F7F24E302CB55882765DD1153 (void);
// 0x00000322 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m0953E12771619B6F14FC14CA853AF5CA110A0553 (void);
// 0x00000323 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0::<DOMinSize>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m9BD9F63B884498F9AA02C307EB1CA15FA29BD094 (void);
// 0x00000324 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0::<DOMinSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_m546E530F9DE46CA1359450860E62DCC3BD3D72B3 (void);
// 0x00000325 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m3EB28823EF4D152A06983986C8120490212A8DF7 (void);
// 0x00000326 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0::<DOPreferredSize>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mB87EAB728455B68D07773FA10DCA169804482CBD (void);
// 0x00000327 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0::<DOPreferredSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m24617F67F48227A961C0543BF7925F8D2F2A90B6 (void);
// 0x00000328 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m11212BA4C7EB97E0EE079E78FA1C95C02D517C75 (void);
// 0x00000329 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_m08737F267CCB60C0CA0244070DA3C50845FC8B1F (void);
// 0x0000032A System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_mAFFD9D2D2F3FD2BE4E2423286098B9BCC1C0C193 (void);
// 0x0000032B System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m9B3BB08FF1DC2F3101E0D580891268445B7763CA (void);
// 0x0000032C UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_mA57545026A9AE0CB3A20B6FFCDCF6F2F1CDA6AB0 (void);
// 0x0000032D System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_mCC70305FC1C1B93A838838519D023AC608D3E23E (void);
// 0x0000032E System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m05F6F905C11A344C5681CE1CD406DE85940356E5 (void);
// 0x0000032F UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0::<DOScale>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_mEE927D8596DDB335403416BF63FF4E7E27D49D51 (void);
// 0x00000330 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0::<DOScale>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_m65C60213296203590F596B6A3481E4B8621F96D5 (void);
// 0x00000331 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_mC6D360265628BCFBFDD42CCAF1136983FDD186BE (void);
// 0x00000332 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0::<DOAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_m693A41C7FCF87BDE6211DE0ADED05AA5C476D71F (void);
// 0x00000333 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0::<DOAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_m0F8F9CA39BC8894705C1EE9983E59D24A7BF218B (void);
// 0x00000334 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m3BB3CA37E5912C211B954D7EE850C77F0B04BFF6 (void);
// 0x00000335 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0::<DOAnchorPosX>b__0()
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m02BA203D0A6C9D64BC8149C197099DA29370C872 (void);
// 0x00000336 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0::<DOAnchorPosX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m47B6E3E511B7E234B49FDBD4D6BC32E5EC92B1E9 (void);
// 0x00000337 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m011D15F0644147EABAFA4ED415CD3F5E8782CCE8 (void);
// 0x00000338 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0::<DOAnchorPosY>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_mCBF0F2691FBC7AED64284B0FB4CCD75792AAB51C (void);
// 0x00000339 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0::<DOAnchorPosY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m0E6D417FD9062B34D8C0E8B58C4C688B39CD9603 (void);
// 0x0000033A System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_mF4FDD3E32D1C9FDD48079A148410AAB0CF4855B2 (void);
// 0x0000033B UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0::<DOAnchorPos3D>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_mFBE3FE1AC3F56C148108234A9A1285F87EEF50E8 (void);
// 0x0000033C System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0::<DOAnchorPos3D>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_mFEF34D901D1E18E74D7E93297BDD75ABF146514C (void);
// 0x0000033D System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_mEEC98B74C2A5EDE984A5A59F4289ADFADAB3F804 (void);
// 0x0000033E UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0::<DOAnchorPos3DX>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_mC8D86325D798D952B390643A8BAE1D995368D36A (void);
// 0x0000033F System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0::<DOAnchorPos3DX>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_mAB78FA2C6EEC74D6E259B2ABF94ECBD3CB62DB82 (void);
// 0x00000340 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_mE272C380064D4945F3C7FDC2357A8B765BC52841 (void);
// 0x00000341 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0::<DOAnchorPos3DY>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_m8C53152ED72D09B8F2BE2DE14963561650D0C42B (void);
// 0x00000342 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0::<DOAnchorPos3DY>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_m9E1FC98A6DF461320AC4B6F294F1A69AFD058E3E (void);
// 0x00000343 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_mF4A0F29D8F3315E7DD0BC8103DA0023E16C341CB (void);
// 0x00000344 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_mD27E4DF91E21C64D13997EA12A7659B1E31AA77B (void);
// 0x00000345 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_mD6F74CB34BAEA91A512586538770AB27FFB68A1D (void);
// 0x00000346 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_mCE70C1CB9A605F65BE4D31D224111EE4C6FB7DE6 (void);
// 0x00000347 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0::<DOAnchorMax>b__0()
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_mAB5724DF7472A7CED2070B75A1B0E12378D5E6DD (void);
// 0x00000348 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0::<DOAnchorMax>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_m7E357679D17273D809A39CDF435B5E64E46A64A0 (void);
// 0x00000349 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_m47D7A3B4DA7AE2FE2F83164406834CF9150E5308 (void);
// 0x0000034A UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0::<DOAnchorMin>b__0()
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_m4804C6D34689DD21311DA86CC22008D3B0774391 (void);
// 0x0000034B System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0::<DOAnchorMin>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_m73EF1691143A0E7E0254A73F52FDC73BB5AAA3E6 (void);
// 0x0000034C System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m0D4210D174301CE26FA7A519B4168E7D87F6D92A (void);
// 0x0000034D UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0::<DOPivot>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_mB3D6F3AADE069EED625038A53491AFEA1DAFD9DF (void);
// 0x0000034E System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0::<DOPivot>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_mDCA8630A9398A411ACB0950973AB99C0723CC074 (void);
// 0x0000034F System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_mA47954A6646E43342880718EE9B5E66A0D18FDF2 (void);
// 0x00000350 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0::<DOPivotX>b__0()
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_mFC16CCA332D908526AC3DC2BD8B4456969000CF5 (void);
// 0x00000351 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0::<DOPivotX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_m7E0DAFAB836AC4F133B995B50578D8911BA3113B (void);
// 0x00000352 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m0524772292383368F9C9F6BDFF0A114D60A35F8F (void);
// 0x00000353 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0::<DOPivotY>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_m959088FDF2C1CD6EF40EE35BA531719E60C5327D (void);
// 0x00000354 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0::<DOPivotY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_mCECF34E3A27F03AA2DB5719CA1241927DC99BC5B (void);
// 0x00000355 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_m1ABD5A4A91526CA156C55C23A54822DC898A34AF (void);
// 0x00000356 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0::<DOSizeDelta>b__0()
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_mF87E563BB15F601A91B0DA1012A1604A52FCBDA5 (void);
// 0x00000357 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0::<DOSizeDelta>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_m26B3B99EDB1D47D81931CC6CABFED8406BF4E90E (void);
// 0x00000358 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_mC4836A2FD37334971703D95C49ED35BCAD56AFB8 (void);
// 0x00000359 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0::<DOPunchAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_m357B263CA209B3E6EBB7F95AC352AA8B28A5CFC5 (void);
// 0x0000035A System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0::<DOPunchAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_mE7127F5C6AB73DBA7D40F67FD61BA4076E2B8D23 (void);
// 0x0000035B System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_m07ADC405B65035A17DC97097DED9B4C1D21EFB17 (void);
// 0x0000035C UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_m1AF2B166591EEA50BEE7FD3C2E9B7EFFC0D39F8E (void);
// 0x0000035D System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_mF7180DEAAB00C14DD6F3DB8B815D0C0B2DB64725 (void);
// 0x0000035E System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_m4969157094B09547B26225881BD87025C928B506 (void);
// 0x0000035F UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_mB4E05CBCDBD4732D154E1A64678339D3FC5FBCE9 (void);
// 0x00000360 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_mBDA6378719836E14C155B4648BB47429C125590C (void);
// 0x00000361 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_m00E6EAFA57923B235554FA8A236AD64992F363FE (void);
// 0x00000362 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_mAE0CBA3D52D03EB2F2C37D85C5832B412F816591 (void);
// 0x00000363 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_mB685B77262E778382FAF923C04FD328DC494426C (void);
// 0x00000364 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__2()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_mB23D92AF70DCD33F838372FF59B1F39DD3775824 (void);
// 0x00000365 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__3()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_mB86E86358B9C1671706EA489B6FC30C3874224E7 (void);
// 0x00000366 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__4(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_m1E0D9286E08822DF086E86BA60F0EFF6B62A32C7 (void);
// 0x00000367 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__5()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_m6E6A274B0B852663D3ED8CDD2B4646B9D618E535 (void);
// 0x00000368 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_mCC70550D8AD7B4F88C913940A644E81F11F5898A (void);
// 0x00000369 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0::<DONormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_m92C7B1817960CE4D1D7333872D37C8AF131F68FE (void);
// 0x0000036A System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0::<DONormalizedPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_mFA97248E3617865FEF6C3F36E633266FF044050F (void);
// 0x0000036B System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_m82279F879253DF896CCD126EC0B7E16B295D165A (void);
// 0x0000036C System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_mB33367FCE1348BA9F855A9D1366FA19F5DDA4AEB (void);
// 0x0000036D System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_mEBA7CDAA06332B1E56991D8E2299E24B9348D627 (void);
// 0x0000036E System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_mD93F08EDF6689C6BA223067191EE8C9E0FBE509C (void);
// 0x0000036F System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_m83DD02084574CCFB9F26C19BDE6AF6DE2C6387FE (void);
// 0x00000370 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_mAE94A6E7E27147AA530D056E286A00C8EAE56331 (void);
// 0x00000371 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_mB387BC26BCA1B460D5B3B1CE767228FA244B7703 (void);
// 0x00000372 System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0::<DOValue>b__0()
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_mBB603DBBDA542D7F00142102A667BCB9682EC145 (void);
// 0x00000373 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0::<DOValue>b__1(System.Single)
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_mA3DF76574D5E76992AF228A3C4AFFFC19D6BAE15 (void);
// 0x00000374 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_mA88FD675FEF2971D3F8145822B844ECFFF59BF17 (void);
// 0x00000375 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_m5DC2ED4C56B0A1D8CFCB8A4E2D2078EA79E8B66F (void);
// 0x00000376 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_m9BA1BADA179249B9F0AD1142BE826798FA114E13 (void);
// 0x00000377 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_m5533AE6A72AB74A97282032790D0DE81F0EAF811 (void);
// 0x00000378 System.Int32 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::<DOCounter>b__0()
extern void U3CU3Ec__DisplayClass35_0_U3CDOCounterU3Eb__0_m4E790A70F3934B1A2F06D5692F8550A69C2567A2 (void);
// 0x00000379 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::<DOCounter>b__1(System.Int32)
extern void U3CU3Ec__DisplayClass35_0_U3CDOCounterU3Eb__1_m3207CE6E96DAC0D117EFA238D72BAC0FAEFB2FE2 (void);
// 0x0000037A System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_mE2912C6BF4318E28870DE7AA92E4B05C80D92EE0 (void);
// 0x0000037B UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__0_mA3238766629BBF7E2750683F62F7629C5B097D87 (void);
// 0x0000037C System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__1_m119575F5E470CA6D0C37EF9E1A9C3A2CF8B2AA3A (void);
// 0x0000037D System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_m59CE3FBA46AF3F7C3597AD84DEC898DB9B84EE39 (void);
// 0x0000037E System.String DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0::<DOText>b__0()
extern void U3CU3Ec__DisplayClass37_0_U3CDOTextU3Eb__0_mDB021AB28041C55E82DF362F8DDF94879B7EBA13 (void);
// 0x0000037F System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0::<DOText>b__1(System.String)
extern void U3CU3Ec__DisplayClass37_0_U3CDOTextU3Eb__1_m0724BC3E19FD8643DD8C0FF25D4C08196FB446DD (void);
// 0x00000380 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_mB3E91AEAFAE65DD5FC36DD530E74CE1F4FA24AEF (void);
// 0x00000381 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_mB976655AEC70D25B43AAAF577CC953C7EB03D2EE (void);
// 0x00000382 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_mF14436C4F4EEB5B7664C3444B969B694BC6E3E5E (void);
// 0x00000383 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m1DB7B55A6B3E4B184E35968EACBF9E214E0CED7D (void);
// 0x00000384 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_mE93AC95325F69D83EE746421FA5992DC25009961 (void);
// 0x00000385 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_m45DD6E7ED579816AC18DF6B88901D9F11DB1B36F (void);
// 0x00000386 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_m089AA2605E325970B89E7F1519CF4DD5676CA2AD (void);
// 0x00000387 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass40_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass40_0_U3CDOBlendableColorU3Eb__0_mD869D1DD9D45824831045C0E5638161837B0854B (void);
// 0x00000388 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass40_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass40_0_U3CDOBlendableColorU3Eb__1_m6B0A0EFCA7952B551FD8C87503133EA08D8075A9 (void);
// 0x00000389 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass41_0::.ctor()
extern void U3CU3Ec__DisplayClass41_0__ctor_m300A6B34B897BE337DB4EA1375C1FD22BC663072 (void);
// 0x0000038A UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass41_0::<DOShapeCircle>b__0()
extern void U3CU3Ec__DisplayClass41_0_U3CDOShapeCircleU3Eb__0_m452DB0F79A62938FE398B97F096D1AE657A6494C (void);
// 0x0000038B System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass41_0::<DOShapeCircle>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass41_0_U3CDOShapeCircleU3Eb__1_mC125693EF64469FC5B4319D963F685D471340384 (void);
// 0x0000038C DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_m7E9B0D004D4B3D602EBB852CA1755FE863C6B839 (void);
// 0x0000038D DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.String,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_mA4E191E86F2EF81966259A40B2CDADE375437920 (void);
// 0x0000038E UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForCompletion(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForCompletion_m4BCEDFA5ED1ACF4BC1B3531C0C306A7AC553C1F2 (void);
// 0x0000038F UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForRewind(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForRewind_mD9F37D5AAD9B665EE07F083D026E65194A8E3E6E (void);
// 0x00000390 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForKill(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForKill_m02BD7F3B018BB6B4ED4FCAF47B8DA435EC60BD6B (void);
// 0x00000391 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForElapsedLoops(DG.Tweening.Tween,System.Int32,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForElapsedLoops_m46D46788899029A20F73BE252F7427D853131404 (void);
// 0x00000392 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForPosition(DG.Tweening.Tween,System.Single,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForPosition_m1771625E294B38C5D13DCA40846AEA881C1B23E6 (void);
// 0x00000393 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForStart(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForStart_mB4AAFE83B50AEF3A167F31C14B2F58794527F905 (void);
// 0x00000394 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOOffset(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOOffset_m09CA228EBED193314D3D9A41BBDC97393E1FC4D2 (void);
// 0x00000395 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOTiling(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOTiling_m6A3CD06EC3030A93EC3E93DE678F34048D5C1141 (void);
// 0x00000396 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForCompletion(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForCompletion_m886CBFB2DCD9455C2762F639F065A7AC9F1EC2E5 (void);
// 0x00000397 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForRewind(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForRewind_mCC0B2A591BA46DE9E5CB59EA84461051477D5642 (void);
// 0x00000398 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForKill(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForKill_m11E8731B8A8E89493FB7976A35DCF4DD85E2132B (void);
// 0x00000399 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForElapsedLoops(DG.Tweening.Tween,System.Int32)
extern void DOTweenModuleUnityVersion_AsyncWaitForElapsedLoops_m9F3D38817E779AA9C5F0BA7811E6626A7BFBB5B1 (void);
// 0x0000039A System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForPosition(DG.Tweening.Tween,System.Single)
extern void DOTweenModuleUnityVersion_AsyncWaitForPosition_m6207EF50EF7DE8402EDC806F8FEF6CC313C4DB03 (void);
// 0x0000039B System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForStart(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForStart_m637C3E8C05D413E080897E3A99CF21C4BD859F25 (void);
// 0x0000039C System.Void DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m0EACBC706CDB7B8BCE4C21D3AD250FE25CD825CF (void);
// 0x0000039D UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0::<DOOffset>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m6D616025F8610086B30B83BC8ED13213DB2AC7CC (void);
// 0x0000039E System.Void DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0::<DOOffset>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m4171C9EB0226E0E06E574EFD507A8D352EC5B557 (void);
// 0x0000039F System.Void DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m6CC810F5A3B1779C2569B21779361FD5F94C2C9C (void);
// 0x000003A0 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0::<DOTiling>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_m747CC91BE7EDF7D2644045B72BF689A2552B855F (void);
// 0x000003A1 System.Void DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0::<DOTiling>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_m0D5FC9115C0FFE113CA2277BA5A17562550B19F6 (void);
// 0x000003A2 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::MoveNext()
extern void U3CAsyncWaitForCompletionU3Ed__10_MoveNext_m7A14EB02262CB797C27664F35565B88E8BA060D3 (void);
// 0x000003A3 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_m85F5E4DAF7F7E7AB65762F3F6C95BFAE3F039819 (void);
// 0x000003A4 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::MoveNext()
extern void U3CAsyncWaitForRewindU3Ed__11_MoveNext_m5DBC14BB1136B7BCF18066FB833D90CF116C1549 (void);
// 0x000003A5 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_mAC2B29B1F4653A143591E2BB2388F8AD9F9A0D64 (void);
// 0x000003A6 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::MoveNext()
extern void U3CAsyncWaitForKillU3Ed__12_MoveNext_mAA9DB56F9B8B3A6E9069CC690E90634D7F51A276 (void);
// 0x000003A7 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m47EB31E97C542A8D7AC82CC7EAF613DC8EEDEABD (void);
// 0x000003A8 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::MoveNext()
extern void U3CAsyncWaitForElapsedLoopsU3Ed__13_MoveNext_mD74D8D5D0970E23A40CE0D8B83297BBC68E25712 (void);
// 0x000003A9 System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m5E02C400A516339612E14DF2ED0B6046D90F6F47 (void);
// 0x000003AA System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::MoveNext()
extern void U3CAsyncWaitForPositionU3Ed__14_MoveNext_m69E154F3EFA854C85D27C84F5C8624AE7595D01E (void);
// 0x000003AB System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_mA77CBC54370E798D88ACF48F9D68586A41FC741E (void);
// 0x000003AC System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::MoveNext()
extern void U3CAsyncWaitForStartU3Ed__15_MoveNext_m0BFB04DC7FE239554F4DF0B3DDD139E42A092F20 (void);
// 0x000003AD System.Void DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForStartU3Ed__15_SetStateMachine_mAD7263100F3F3DBD57295879534C47061409AEB0 (void);
// 0x000003AE System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForCompletion::get_keepWaiting()
extern void WaitForCompletion_get_keepWaiting_mCA7642C5A8C37F8C0A2CAE990CE1CB6AEE8FD2D9 (void);
// 0x000003AF System.Void DG.Tweening.DOTweenCYInstruction/WaitForCompletion::.ctor(DG.Tweening.Tween)
extern void WaitForCompletion__ctor_m818111A77A3380EE626346FE03A1A604BB896A1A (void);
// 0x000003B0 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForRewind::get_keepWaiting()
extern void WaitForRewind_get_keepWaiting_mB480319CB28155CA977F94C7FA03EE5353AA1285 (void);
// 0x000003B1 System.Void DG.Tweening.DOTweenCYInstruction/WaitForRewind::.ctor(DG.Tweening.Tween)
extern void WaitForRewind__ctor_m66B575E497C363CB5137629B4D6A00D13B7CD5AE (void);
// 0x000003B2 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForKill::get_keepWaiting()
extern void WaitForKill_get_keepWaiting_m7979151F1AD842D2E8004FE37A2C51B47AB36647 (void);
// 0x000003B3 System.Void DG.Tweening.DOTweenCYInstruction/WaitForKill::.ctor(DG.Tweening.Tween)
extern void WaitForKill__ctor_m1C9624CE32A1C83CEA14BB5EADD587B6AD79D829 (void);
// 0x000003B4 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::get_keepWaiting()
extern void WaitForElapsedLoops_get_keepWaiting_m6F7A59CCCC45BBA5125C6FC7AB667CD24359E8F4 (void);
// 0x000003B5 System.Void DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::.ctor(DG.Tweening.Tween,System.Int32)
extern void WaitForElapsedLoops__ctor_m8E720B450DD1350EE81EC3CCB5B6280BE5C51D8B (void);
// 0x000003B6 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForPosition::get_keepWaiting()
extern void WaitForPosition_get_keepWaiting_m34DFE8356EAFEE916828BFAF4A17A822B47AD687 (void);
// 0x000003B7 System.Void DG.Tweening.DOTweenCYInstruction/WaitForPosition::.ctor(DG.Tweening.Tween,System.Single)
extern void WaitForPosition__ctor_m94DD0A05EF293B8AA83F343A12015C107AF7FDB8 (void);
// 0x000003B8 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForStart::get_keepWaiting()
extern void WaitForStart_get_keepWaiting_m59700AA1AB726A22C76BFAA0C52FCA460F6E337D (void);
// 0x000003B9 System.Void DG.Tweening.DOTweenCYInstruction/WaitForStart::.ctor(DG.Tweening.Tween)
extern void WaitForStart__ctor_mD7AB17A603CF22568EEF0D9861C49F6CFD632284 (void);
// 0x000003BA System.Void DG.Tweening.DOTweenModuleUtils::Init()
extern void DOTweenModuleUtils_Init_mFF2188F42FA7128FE1A9BBC2CB9E6351919C9D15 (void);
// 0x000003BB System.Void DG.Tweening.DOTweenModuleUtils::Preserver()
extern void DOTweenModuleUtils_Preserver_mBE8D1FE2AE913FBFFCCF43B5A615D941B3706F43 (void);
// 0x000003BC System.Void DG.Tweening.DOTweenModuleUtils/Physics::SetOrientationOnPath(DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform)
extern void Physics_SetOrientationOnPath_mCC376173A621DA244564EDF8A6347AB2A0F47816 (void);
// 0x000003BD System.Boolean DG.Tweening.DOTweenModuleUtils/Physics::HasRigidbody2D(UnityEngine.Component)
extern void Physics_HasRigidbody2D_m86FAA0450979B8AFE6A9EF5E27837387C57765C1 (void);
// 0x000003BE System.Boolean DG.Tweening.DOTweenModuleUtils/Physics::HasRigidbody(UnityEngine.Component)
extern void Physics_HasRigidbody_m3082D72A658CCDDF6BB1F3D4A3EFBD6397D6E862 (void);
// 0x000003BF DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModuleUtils/Physics::CreateDOTweenPathTween(UnityEngine.MonoBehaviour,System.Boolean,System.Boolean,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void Physics_CreateDOTweenPathTween_m94D41B65E37500E5ACCF6B6F3B5D53FB031C4245 (void);
static Il2CppMethodPointer s_methodPointers[959] = 
{
	DemoController_Update_m65BE6DD3EFE5FD35CDCA82EA64CC5EA05CC39B54,
	DemoController_NextAnim_mF4414379F85B18B68EA5206859D0ADFEE320C41A,
	DemoController_PrevAnim_m661CCFC17C6A3F0F8A873654B86E71CAFD39A75A,
	DemoController_PlayAnim_m88268B9A1C8A18933FA9495C32CF19053265DAF1,
	DemoController_GoToWebsite_m6D1775DA170EE55597781D45B7D474338E10D7C8,
	DemoController__ctor_mDCA85316804A352961C2DA6570E01FF23E7C9853,
	CamController_Start_mEDF9FE7BEA5DA271CF400F632CCB2AEAFEA7E039,
	CamController_CamSetting_m51B10FA34446CA7A543892B60C360FA24FC672CD,
	CamController__ctor_mE9C81B1081FF7B4230FF66A1013698288C4C90C1,
	U3CCamSettingU3Ed__3__ctor_m8B0D0AE60BC45D32C578403CA715C618E42A93E0,
	U3CCamSettingU3Ed__3_System_IDisposable_Dispose_m8D61F6E5004619A2C3A93159F4ECA329F1284C45,
	U3CCamSettingU3Ed__3_MoveNext_m87A559BA121C6B5E4755D74ACDB8984F47F4A528,
	U3CCamSettingU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m247DD3E72107F6522844A2FAE04922F9B90E7756,
	U3CCamSettingU3Ed__3_System_Collections_IEnumerator_Reset_m83570DB6DDBB45080E2D6EDE312014E7B9919F2E,
	U3CCamSettingU3Ed__3_System_Collections_IEnumerator_get_Current_m9AAC4E7D2536AD94D4C056662DA0C5661245EAB4,
	CheckPoint_Start_m9BB59A2CFAB9E1D880D35A3C90C3583C1D3FA954,
	CheckPoint_Update_m6C4383DA634F30FA674194F900C08158D445EE96,
	CheckPoint__ctor_m567092658676E17DD5CC0FB6FE90689AAED74C47,
	ControllerAnimal_Start_m4071928C042DE4AD4D7C2963DB05799C5B6E382A,
	ControllerAnimal_SetAngularDrive_m0C70560E814CB6F2B2BAF9864E260E783910FA09,
	ControllerAnimal_FixedUpdate_m7BC6BA1FA1F11D0CB18DFF893198E50C31F4DEC8,
	ControllerAnimal__ctor_m28C13EFF33C021CC3487D77A8D2295F1FE40F73A,
	PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794,
	PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB,
	DataModelHere_Start_mA9D1240509FFC87EA0370D5B92875B7D0915E09A,
	DataModelHere_Update_m290A9F7E90291DB342E3B6278CF7636EBB1C49EC,
	DataModelHere__ctor_mD5E53C001DB6E74AE4ECFD7CDAFC0F47EA79090C,
	HomeController_SceneName_mE4F0B8EB70C0F0C9DF473D5D6AE1F47C4731DD4A,
	HomeController__ctor_mA8077DB22E606DA009CD773F9C00536876A72DF4,
	techGameController_SceneName_mC465BADB84969F3EE807CFF975BF07DB243322F2,
	techGameController__ctor_mE8A36E60FC426F1AF56F01B249ECF16A885EAB88,
	Const__ctor_m0CD639302F73E17069A919829CCE30742348BAB9,
	RandomFlyer_Start_mFAE78D640C06D0491ADA60F16815417B14B35044,
	RandomFlyer_Update_m782E691C9BF4DB9C9C822126773567CAC61D5193,
	RandomFlyer_OnDestroy_m29AE94FFC785D3F5043E5FE12AE2AF0127E7EFC9,
	RandomFlyer__ctor_m21045CFD31F13DD36FE7A385B8681582EC3E8045,
	RectTransformExtensions_SetLeft_mA86C085FF57265E25F8732197C2FA7E4E3377F05,
	RectTransformExtensions_SetRight_m41A7631DD85392C568598C634B73D1A7CA9C3BA8,
	RectTransformExtensions_SetTop_m8E0112409A0B4AE8678D326E04149A7422CC0702,
	RectTransformExtensions_SetBottom_mD6AC62815DA40BDA0158DDBF0DBC8AFAF8B9C038,
	Static_GoHome_m6D69A14CE0782323AF25473BB2C7F8ECDD36F8F8,
	TrailController_Start_m5A542BA9759CF1B41A92B8F6F7AE5B6F495F74F3,
	TrailController_Update_m88A7B864DFEA4B080C12E83DB5B70D04D2CF485D,
	TrailController_IsPointInPolygon_mDAC90F5177B04E2C48390664EAFD93992C7DB579,
	TrailController_checkButterfly_m1F08704425A38FA76C9236C1272DC7F01481C2BF,
	TrailController_RemoveButterfly_m80C2EA9777A0511B706FCC5A7633BC4984224BAB,
	TrailController_IsPointInPolygon_mC8047FCE999E70EDF4659F63AD51F50C2BBB53F4,
	TrailController_DestroyButterflies_m452106AF577C36D537DFB1F518B734A16A6E20A2,
	TrailController_Centroid_m84A701BA17CBD8BCEC895D1ED6C5EAF2BABCB412,
	TrailController__ctor_m8976E0A734E926FF10177E49BCCC7B47CF152292,
	ZoomCinemachine_Start_m4775EB7067403168E726C9D622C168F1551163C6,
	ZoomCinemachine_LateUpdate_m7235E42B3C435C0D6195639F7849D48554A7EBC4,
	ZoomCinemachine_SetZoom_m47AD781064095E1EB1EC7CB2224E9F01FC6F5EF6,
	ZoomCinemachine__ctor_m34444417E601E43B88B1E2B9802CF68BF796F253,
	AudioManager_get_BgmVolume_m8140F51FBD5BBE8710FE4FE1374EE26101A01200,
	AudioManager_set_BgmVolume_m74CBC81EC4473DEE8C4967D5AF0F69895BF05F97,
	AudioManager_get_SfxVolume_m501DA9D4F00EA0812B00C346D599E9D04F45E790,
	AudioManager_set_SfxVolume_m705E78912C92938CB707CE55A713A6C7019E3699,
	AudioManager_get_VoiceVolume_mE685AAAE24C1758EA6F12C44E8B8322EFF717A91,
	AudioManager_set_VoiceVolume_mA304FB47D3C1159F82700D6C506CC23DFC96D8AA,
	AudioManager_Save_m24B6E6E3D9606F5F08E073D1AB4A35284F25A625,
	AudioManager_get_instance_m36FB14B490ECEFC3EBF150336F7547156DFB1B8D,
	AudioManager_set_instance_m666A0E6C0CF110F46FA9BF050EFFA22214C7D674,
	AudioManager_get_sfxPath_mA2DE394D1B52A4A52739E44DFEF6285F56455326,
	AudioManager_get_bgmPath_m77031744EC0B0B91BB74C8212A52E7EC4038E4F4,
	AudioManager_get_voicePath_m3B37191DC7C27B1EE8439E7125F4B67139707DFF,
	AudioManager_get_voiceSource_m0C77294535A2A6148FAA94018486216874F95C34,
	AudioManager_PlayButtonTapSfx_m8C02B479A0A8EBA87FB7A587DD70857C1E03500B,
	AudioManager_PlaySfx_mECB229ADAD09D0FC58C66CB44AE86B40E3468910,
	AudioManager_PlaySfx_m457CD60AE7C52CD597283013FD8563435C099D11,
	AudioManager_PlaySfx_mD31CC26EA8BE322EC579B25F93EA1FC0E1CD0C1A,
	AudioManager_PlaySfx_m7D9D83919AE72DC8EE98660D89179EEFEE7D0B36,
	AudioManager_StopSfx_m6C2595527290283667844F899753040C255A3F21,
	AudioManager_GetLength_m7B7202D1B105443E204B63B36D974CA4707D0456,
	AudioManager_PlayVoice_m97C378E0638E4A3CD9D7A51F5F2357E8D7F33EBB,
	AudioManager_PlayVoice_m467D007A10EA8419C1F9F13F460398FFA404D061,
	AudioManager_SetVoicePitch_mD5194CAE37FC8314F0AD1464ACD850A40C4441DD,
	AudioManager_PlayBgm_mEB3CA79BA839F3FEFEE42CE72E4471DF2E942F44,
	AudioManager_StopBgm_m2C7CE9153A21D7864FAEBC8FEC69618F3477493C,
	AudioManager_set_pauseBgm_mCA4EDC87EAEA46DCC0EAE56DD7BB8C19EF06825F,
	AudioManager_CacheClip_mF906258F36557F2C3A6656FA892B0FB7549D2717,
	AudioManager_CacheClip_mA4668FD6A1590F236D71F7381A3726B5E501AF19,
	AudioManager_CacheSfx_m2A00939A8DF74D286299246D60ADA03016C37069,
	AudioManager_CacheSfx_mC5F8662B879D16BACBBB9E6768B1BB70906BCC7F,
	AudioManager_ClearCacheClip_mDC5540C013E6A5664A484064980E0C68B33A930A,
	AudioManager_ClearCacheClip_mEA76D00816CD619A5687F307DA4CECE84021EBFC,
	AudioManager_Fadeout_m0A4635CDA9DF5DF7391A540048AAE8B7C2144E74,
	AudioManager_Awake_mD7873A38A3ED577A313A05D24BF6511E59EDEC01,
	AudioManager__ctor_m6C686441D1A1A223E4CF940A8EB0128535D603BD,
	AudioManager__cctor_mCE2018DCC9F41CB769EDFBC4D1C2221125848DED,
	ButtonAudio_Awake_mF58556EAEC0A0472C8EDAEC0F02CD6A6D99009DD,
	ButtonAudio_Play_mB026D8C95FDBCB680A998A3EBDE1EF66CCAF98C3,
	ButtonAudio__ctor_m2899BFF7C26853C5C5C617AA9333B53E43B58B69,
	ETFXProjectileScript_Start_mE8756011C883DDCF287343C6BF881B36B7ADB59C,
	ETFXProjectileScript_FixedUpdate_m959BFE684CC8CF77305ABF0C54256007023E2FFD,
	ETFXProjectileScript__ctor_mDB3BFAED1E91A0DC99930E80169BF2F1EE26CFB5,
	ETFXSceneManager_LoadScene2DDemo_m11E37C513C423B288C88C336A3F77E846AA6B358,
	ETFXSceneManager_LoadSceneCards_m2374101144F9C56EB968B3A3B7BEACA75BB21C03,
	ETFXSceneManager_LoadSceneCombat_mDA5B85860C1746C93E4D81CE992E7C4887D89E0C,
	ETFXSceneManager_LoadSceneDecals_m0D919CCFC190AAB40C20D990A8DEBBDF79075F2E,
	ETFXSceneManager_LoadSceneDecals2_mEFA6A5777999D5F3D87E1E8226A361880433DCF6,
	ETFXSceneManager_LoadSceneEmojis_m01E3A7533C8A4D8CF6115E0EE1DC687455AB4D4E,
	ETFXSceneManager_LoadSceneEmojis2_m6005349247D781727A558BA01D2085C893AF6438,
	ETFXSceneManager_LoadSceneExplosions_m1984686B3E5CE79E49BDDA11BDD09596F8E7C42E,
	ETFXSceneManager_LoadSceneExplosions2_mB04546364A96740B6CA14B50B2DCC3E80C28CF90,
	ETFXSceneManager_LoadSceneFire_m9E31C2B57B979A46E2C6F727C858947CF9C0EA19,
	ETFXSceneManager_LoadSceneFire2_mEAE89BF11E50292B18F69A327923ED3A8B652256,
	ETFXSceneManager_LoadSceneFire3_mD9919F751EC9CFEF0D95691414A042A22B55C831,
	ETFXSceneManager_LoadSceneFireworks_m2827DAD7FCC3996841900263F223F5201E8A3C43,
	ETFXSceneManager_LoadSceneFlares_mD740C35453DD10F374658441ED58411126845D38,
	ETFXSceneManager_LoadSceneMagic_m225C27B1375FCB1B4C215A3D67BDF33C01BDE04E,
	ETFXSceneManager_LoadSceneMagic2_m08E7D2EF64DD9F9534B7525E3905324264DCAAC7,
	ETFXSceneManager_LoadSceneMagic3_m58FC3634AA94E283420B22E080D9E91A2A52A737,
	ETFXSceneManager_LoadSceneMainDemo_m030D3019FECBBAF948E3FAC03B7229FAAF0839C4,
	ETFXSceneManager_LoadSceneMissiles_mEE0267B462F61EC2B7BD88460F69CEE7139A6E8D,
	ETFXSceneManager_LoadScenePortals_m4932DCDC59F52F6B75F7E00FE384C15CAACD4371,
	ETFXSceneManager_LoadScenePortals2_mE5701BAD231A3926A5F3DD95E7FB029E9378C89C,
	ETFXSceneManager_LoadScenePowerups_m49AE6A1640136507D16DFF611DC78B4CDA28FEC2,
	ETFXSceneManager_LoadScenePowerups2_m548B7BFF09B8A133EF48FE2B130A91D796EC6A33,
	ETFXSceneManager_LoadSceneSparkles_m9463E60A6ED35D9897EEEF18D7B94880359B75A5,
	ETFXSceneManager_LoadSceneSwordCombat_m2040FBE4FE83D2256F5208F20D5A0A817472052E,
	ETFXSceneManager_LoadSceneSwordCombat2_m3C8D0447A800B6C4616D4CCE669711CC6236C13A,
	ETFXSceneManager_LoadSceneMoney_mE3B3B3433BE6C2E37B56CCF7CF0ACA0AF274B795,
	ETFXSceneManager_LoadSceneHealing_m73F368066D2E63E359E36079EB20A4B51006BA56,
	ETFXSceneManager_LoadSceneWind_mB2FF2390377FF263B5ACAEE93DD389C9D666BE2E,
	ETFXSceneManager_Update_mB2FB561ADC4764627A10DECF88F6C3F17891B9C1,
	ETFXSceneManager__ctor_mC6363CFDCD6B6646152AA5FEC471088DFC20367A,
	PEButtonScript_Start_mED60465004404B45D9A5BE8129015C39394C9DC0,
	PEButtonScript_OnPointerEnter_m89340DB88B2D0EF0CDD762F27C1EB455A6876CB6,
	PEButtonScript_OnPointerExit_m8F731E714619F43F056DD2D4E27A24B5B045774C,
	PEButtonScript_OnButtonClicked_m8D7DB17EB5C69D54E2F95931AB6C90A7490159B2,
	PEButtonScript__ctor_mBFA614FD383BF4104DE7ADEE7AD88A91AC8083D6,
	ParticleEffectsLibrary_Awake_mDC6AFDB7CD4E3DA4C5163BFDDCC04A76EED147CE,
	ParticleEffectsLibrary_Start_m82B0B1F30A8C0E6B1060FEEC21C5280500D7F771,
	ParticleEffectsLibrary_GetCurrentPENameString_m067B9C792580A9DFD2DE2B4318AEC99254BF5285,
	ParticleEffectsLibrary_PreviousParticleEffect_m4A6BB63833B9C4D91B2F2801741C731C8174836E,
	ParticleEffectsLibrary_NextParticleEffect_mD095F239D4054E69278390CA3A404D033F8A658E,
	ParticleEffectsLibrary_SpawnParticleEffect_m17AE1DA6F799AFF7DE1C64DC0ECF3D716E22355A,
	ParticleEffectsLibrary__ctor_m777070C2175441BEB93AFF11748BC74588651C7B,
	UICanvasManager_Awake_m8D84AB004D6FEEBB3BDC5FBB9C5101B75ACDC396,
	UICanvasManager_Start_m9652DFED3C5A6CD686ABFB2D77A474270D5F7656,
	UICanvasManager_Update_mA2771AB72770EC84D9D24FA819E57BB122AD7FC0,
	UICanvasManager_UpdateToolTip_m13510C3590867DC2E5B2F8D45E0BA522479103D7,
	UICanvasManager_ClearToolTip_mFA69FDBDB5CADD0F18A31D1AC407D38D81F2B3BF,
	UICanvasManager_SelectPreviousPE_m88ABF3CB63D07DBBEFCAE908864103FF09C21E30,
	UICanvasManager_SelectNextPE_m5838FA28DBEEC3DFF2A84E671842B86C366A2367,
	UICanvasManager_SpawnCurrentParticleEffect_m8E4B86D6B40EBB353D2EA0D33B22785FB3A55B1B,
	UICanvasManager_UIButtonClick_m0BE04EA2C0BA8E35CE5B22BFF4CBD4E9128B3CE4,
	UICanvasManager__ctor_m8BDA3D4D6FD64344E6CCBEF6F7B314DEE7180E22,
	MgAnimation_Awake_m5B4A5D1CC62CA757F225BDE2638BC2B680E93E5C,
	MgAnimation_OnDisable_m1A85B1EB5716A14A49AAAF1CDE1AFEB64377767D,
	MgAnimation_Show_mD2031973358F61C21D5DD997588BF30A83E6A0B6,
	MgAnimation_Hide_m7FA361F9FBCBEF105C8136DCF5E9B7D6760AF333,
	MgAnimation__ctor_m07038ED8FAC63A68B4606E49DC4C636EDB584001,
	MgAnimationFade_Show_m5F746CDC4D6B21AA1CF31412775E3FA9FA237C88,
	MgAnimationFade_Hide_mEAFE957483B4B6B08053263B95D897A2B1BB4149,
	MgAnimationFade__ctor_mC758352CB7A96D237717ACD30B4E06B50A85F6E6,
	MgAnimationLink_Show_m99FCD8504556358C90525767D3A3AAC24E73E653,
	MgAnimationLink_CoShow_mE5861550ECA367E4BACE78F17D99DF226500D8CC,
	MgAnimationLink_Hide_m8F3950BA24B56DD4419D01FFE367FC966C822A66,
	MgAnimationLink_CoHide_m8A0E7D3AD3E93E482FC47FE0DC472A5A5286538B,
	MgAnimationLink__ctor_m9A186DAFC787888B8FBB55DF5D2FC0DDCCF00693,
	U3CCoShowU3Ed__4__ctor_m554EB8574D3EC74588B8C229FEA1C0B0A291546F,
	U3CCoShowU3Ed__4_System_IDisposable_Dispose_m07C82A5E84196EA1B1972E780C6DD1FE46574BC2,
	U3CCoShowU3Ed__4_MoveNext_m8E62859902229C59605109F4A8D8458C57992FA6,
	U3CCoShowU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC9030BE2687FCFE6F576F56732CE39EEC045140B,
	U3CCoShowU3Ed__4_System_Collections_IEnumerator_Reset_m964546A98807BDFDDFB6F9B948CFEA111004BC87,
	U3CCoShowU3Ed__4_System_Collections_IEnumerator_get_Current_m5F2C6965EC88D18494A8D38BDD4716692853BB18,
	U3CCoHideU3Ed__6__ctor_m4090DB038FFA544ADA13CD42C5846426F206DC1F,
	U3CCoHideU3Ed__6_System_IDisposable_Dispose_m6FDB5A58039591E3210957C83AE99716A836B3D8,
	U3CCoHideU3Ed__6_MoveNext_m42BF49D8A3A3801ED73880017FE6277A12528643,
	U3CCoHideU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE3180BCF97A6E0B69FEC3031203639BF46203B01,
	U3CCoHideU3Ed__6_System_Collections_IEnumerator_Reset_m08C249CB2DAB83DFC80C43F0AADBF65BEF26E138,
	U3CCoHideU3Ed__6_System_Collections_IEnumerator_get_Current_mAC9F050CAA3A19D7A27B8D7BEFC50B0F77039748,
	MgAnimationLinkObject_Show_mFC203AD9ECE02BF200860EA7C732C016BB9FBDE9,
	MgAnimationLinkObject_CoShow_mDC6098417478F81E119F7A1ED3CFF28504F10B8C,
	MgAnimationLinkObject_Hide_mD7454B93B42F63613CC0D2629A4D7C2D8BBAC380,
	MgAnimationLinkObject_CoHide_m343BB45CFA8555A43752D7CC98DBD9AA14F2FF2C,
	MgAnimationLinkObject__ctor_m2AFD544FB91CC91D4942793F913F61354B8671A9,
	U3CCoShowU3Ed__4__ctor_m1726143C8B21533706114E0E90EFA33FAC5D33DA,
	U3CCoShowU3Ed__4_System_IDisposable_Dispose_mBD83DECBF938630809CFC6823D1BBF2287B14AF1,
	U3CCoShowU3Ed__4_MoveNext_m792E5483DA82469F94CE7BD373C9A6EF16E1F899,
	U3CCoShowU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6AA628E39ECD848EB5417D012CCABAA563FAC3EB,
	U3CCoShowU3Ed__4_System_Collections_IEnumerator_Reset_mA33FD166C110C0853517FB614C738B06DEC9520D,
	U3CCoShowU3Ed__4_System_Collections_IEnumerator_get_Current_mC02634706ABCCA9E4F9C808BE0475E9042DA01AF,
	U3CCoHideU3Ed__6__ctor_m1B9365AD609078A363564D5938D192A89625AF71,
	U3CCoHideU3Ed__6_System_IDisposable_Dispose_mBD6FC0B545911FAB141FC358F1C67688DEA32191,
	U3CCoHideU3Ed__6_MoveNext_mB97D9FCB8066EBD7EF130FC72365FB6536C92A36,
	U3CCoHideU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43DF0B545FB7DD89B2572D0705D2F83C4F05CF9D,
	U3CCoHideU3Ed__6_System_Collections_IEnumerator_Reset_m304762ABA63837D8B6483A489789E6061CBAA982,
	U3CCoHideU3Ed__6_System_Collections_IEnumerator_get_Current_m7F143AAD687B3EE3B6309FBF1FAB3E547A681355,
	MgAnimationMove_Show_m49B01EC48EA1DFE17CF55BEF0712F4DA7E09AA3D,
	MgAnimationMove_Hide_m7BD03283D712D327B22BAFB712806F83B7559DA9,
	MgAnimationMove_Move_m6CBF19B2EDDAE9F2C14E95CB1A43D83F0DF05B65,
	MgAnimationMove_SetStart_m545D1B0A99469A5AF1BCE31349160E941B47BA24,
	MgAnimationMove_SetEnd_m1465522316BACBC965434DE4DB74CFE7B4431F7A,
	MgAnimationMove__ctor_mB525943200881949DDF9515837FDE64AA1ED49B5,
	MgAnimationPScaleLoop_Show_m9B8A5D1E964A0037838C6E16DA6109C368A94435,
	MgAnimationPScaleLoop_Hide_mF7991768A2390FCD35B9B63E511151CD8FD1CB75,
	MgAnimationPScaleLoop_LoopAnim_m0656BED0FED06007A0F94B9CD48AD58DED967C0B,
	MgAnimationPScaleLoop_Update_mCAC0979BAFDC2CFC41E99DA3EAF003089CE8DCD1,
	MgAnimationPScaleLoop__ctor_mEF23296C56380A71AD8F54381553AD604FF45858,
	MgAnimationPScaleLoop_U3CLoopAnimU3Eb__5_0_m62EB2B13EFD62CF7C44BFEFA5FE44EF1ADAC7292,
	MgAnimationScale_Show_m4F8BE69CF8E06FC561D30B901DC307432B225D65,
	MgAnimationScale_Hide_mBBAA096748C05F4C4C447EB6295B52C5B093B643,
	MgAnimationScale__ctor_mA80B2D7C14E49B0DC02D05136BC7AB87340CD4F9,
	MgAnimationSpine__ctor_m81118013A294643467B38C06C6AF48C5AC34B52F,
	MgAnimationUnity_Awake_m4B08BDF238A2D17273A0A5DC7F0676E8A42642A8,
	MgAnimationUnity_Show_m836DA71E2742371AD7F5FC12C8280C771471F202,
	MgAnimationUnity_CoShow_m45D03ACE03ADF8509F5F30851DF9333DE68A0B87,
	MgAnimationUnity_Hide_m06EBB32C46C5AC20658F3724669EC17DF56DCE1D,
	MgAnimationUnity__ctor_m6F56C44A408DA9255F0B94FAED0B871A6EB85A56,
	U3CCoShowU3Ed__6__ctor_mFFA3E76623E10E9F668CA1CBF411F626D53964A1,
	U3CCoShowU3Ed__6_System_IDisposable_Dispose_m0072088E4B8ED1D71D8EA1F6AEB9FAF65095113B,
	U3CCoShowU3Ed__6_MoveNext_m55FBF7C7C23CB36B51D4FE92DF4BF6BDD2D5BFCF,
	U3CCoShowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72E89C7F88FFDBF545B5F398A67CA5515106A0BB,
	U3CCoShowU3Ed__6_System_Collections_IEnumerator_Reset_m19937A01FE7B527C7B7DE39A1360330701041EC5,
	U3CCoShowU3Ed__6_System_Collections_IEnumerator_get_Current_mC04A44C677D01377DA40F515F01E51814858CD9D,
	MgAnimationWiggle__ctor_m209FCF71DD6D763AA0659D123374349081282FFC,
	MgObject_Awake_mD07D8FFABEECA4AB456A97C1EEC44E4E8DE43437,
	MgObject_OnDisable_m12F36A7F3310E34BA37B299976CE30B951313EC4,
	MgObject_FindAnims_m92D66820112ABA64BE1070B401665F22EDBFC4EA,
	MgObject_FindButtonTargetGraphic_m7DEDE39EC2D0924828C8DAA39B528C7A3DA741A3,
	MgObject_Show_mF9DEBC79B916777A8AF07B1A44338476EB7575C9,
	MgObject_Hide_m62A42590BE451A413B2122C29FF01552F0F65C42,
	MgObject_Move_m2882FA32D74BC45D458E0AC200F43B996E3A9799,
	MgObject_Shake_m6EBEB8C986F0C6C39D676AC6D5262A9F69F89EC3,
	MgObject_PunchScale_mC8F5951DD64927B38BA0C56203F441C8289557BE,
	MgObject__ctor_m5E8C46DCCBC211060CF1006165EAE244D28BA71A,
	MgObject_U3CShakeU3Eb__10_0_mCA287CDA1850D00060EE3FCAF67CAF86CB310910,
	MgObject_U3CPunchScaleU3Eb__11_0_m523F3F64B37548FC411AF2A549713B8806752E3F,
	PlayerPrefsUtility_IsEncryptedKey_m153D18311A0A907C56DBD065234CFDDEDF0504DE,
	PlayerPrefsUtility_DecryptKey_m0C8668431AF36978F9949F007361ACD61A0F5E39,
	PlayerPrefsUtility_SetEncryptedFloat_m8262DCCF487E79A7609D5DC6DBB2B2CBD90AEAF4,
	PlayerPrefsUtility_SetEncryptedInt_m5D8068FE27C12A16FCC0436786E1D91D12393794,
	PlayerPrefsUtility_SetEncryptedString_mC765E9F22085FAD92EA1C987D81374E56A2932E1,
	PlayerPrefsUtility_SetEncryptedBool_m916E9BBF325F84B32F0D190C00D40EA2F4B1EACE,
	PlayerPrefsUtility_GetEncryptedValue_m9EEB67190666A392E5AA90BE93C11BB908C73067,
	PlayerPrefsUtility_GetEncryptedFloat_mB95681423A11D6E853A8470CA26E4C9922A0103D,
	PlayerPrefsUtility_GetEncryptedInt_mD36FE4990A860C11AE673EAFE6A4ECDED035A808,
	PlayerPrefsUtility_GetEncryptedString_mFA30471895C42226DFE0AF1FBDF5B79F1970BF6D,
	PlayerPrefsUtility_GetEncryptedBool_mE7846072D3313D290C8D0CAD167E9913B0D316BF,
	PlayerPrefsUtility_SetBool_m4A72E1C6139F903A68D36344541E50D033E9A100,
	PlayerPrefsUtility_GetBool_m08778496D06A242626F7E986555649193CDB6D5A,
	PlayerPrefsUtility_SetEnum_mF5B172507AC0E48E54B8C0DCD3E1C84F70B0197C,
	NULL,
	PlayerPrefsUtility_GetEnum_mCDE3318023FDE7ED73B4DA5D27DE75D292BB9AC1,
	PlayerPrefsUtility_SetDateTime_m6623F3E91B0BA4D31420B3FA6D00ED67F165E903,
	PlayerPrefsUtility_GetDateTime_m35A3CA929E76A58B7567F5FDF408FD9E65E3BA5A,
	PlayerPrefsUtility_SetTimeSpan_mD053352AE057FD02671A5A550D961CF2AB1BA712,
	PlayerPrefsUtility_GetTimeSpan_m219D47B173B4B461A396F010218647AA807935C0,
	DontChangeCanvasCamera_Start_m59AFBE23EB40A50C5FBCBC8727E85BE5E71E6853,
	DontChangeCanvasCamera_Update_m306EBCF45B877244DA19B7D899B669625F3956A7,
	DontChangeCanvasCamera__ctor_m31C20B947EE72E5AE4F6714C4FC821A044E4B255,
	DGameController_SceneName_mF56A5F407051FBFA507661A9F8E64D07356EA81A,
	DGameController_OnActive_mD07AE3534DC097B571C77E09DD7DACB03CAD8C90,
	DGameController_OnShown_mC14E85EDAC2530D4D97B6999D8C434CBA5C7ABF7,
	DGameController_OnHidden_m8DB8802D9EEE965744CB9FBD398F983F79ADCBC7,
	DGameController_OnButtonTap_m30F012ECDFB8F5ECB3B4E8BC67F40D65BEE79F84,
	DGameController_LoadingToTop_m16D87897ACBBDCDB17EA184E1CD158D8EDF252FC,
	DGameController__ctor_mCBA872D897A7AE21006509EB6DEA313CACE78482,
	U3CLoadingToTopU3Ed__6__ctor_m4DF50C93EAB6A04080FF76FE71AFB1D9E2C85569,
	U3CLoadingToTopU3Ed__6_System_IDisposable_Dispose_m55B486431095FC8199CE1CC08EDC327E83B15BC0,
	U3CLoadingToTopU3Ed__6_MoveNext_mBEFD615067F5B44C94300159F5D256A24388B7CC,
	U3CLoadingToTopU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m359167E9C986E70CB8901CA63CBE7C418B2F3A7D,
	U3CLoadingToTopU3Ed__6_System_Collections_IEnumerator_Reset_mFBBF4C2124B15D0F6FF9516AB15AB66D50A53879,
	U3CLoadingToTopU3Ed__6_System_Collections_IEnumerator_get_Current_m0D7A10CDEA3F5BB3B9B90102ED2E62EB48B600EB,
	DLoadingController_SceneName_mC4CB22DDDB72522096D0A132CB8CFCB5180382F6,
	DLoadingController__ctor_mFC74F6C16562E76956E5969A63ECE80117938F5C,
	DMain_Start_mCB88609F3C58BD213EAC9D1203B1A69610179042,
	DMain__ctor_mEEB7DA9FC2BFC12D2FEF488BBB7BDAC65E4D4A3A,
	U3CStartU3Ed__0__ctor_mE4C50085C2DAE6B9AE876845A98A256A8CFD3E3B,
	U3CStartU3Ed__0_System_IDisposable_Dispose_mCB8CDA3D64F78F8C356E9A9F80DA4E48EE53F5CB,
	U3CStartU3Ed__0_MoveNext_m5C85067F52F9D9EEE79D8C8419D0B201663E2761,
	U3CStartU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mACE8A452F4692BC6E0FC38AC9DBB35072A007AC1,
	U3CStartU3Ed__0_System_Collections_IEnumerator_Reset_m0CB66531627AC524221E203F6FFBC2A527CDEC74,
	U3CStartU3Ed__0_System_Collections_IEnumerator_get_Current_mC2698620ADBE0D6372150B44731A486A9F313AA3,
	DPopupController_SceneName_mCF634B1B49A06D1BF5D11351885DD887C5515C5A,
	DPopupController_OnActive_mF1C1754A56B58DCFF2C89E0E43C9110B201C1305,
	DPopupController_OnShown_mA7BF50B405677EA7C64C1BD4E87A9CB02534EDBE,
	DPopupController_OnHidden_m2AD1FD24C84B558FBA7819F627D901900C3CD442,
	DPopupController_OnReFocus_mE524740C938E810959100DFDF4F621EF57485D99,
	DPopupController__ctor_m955A2278FC1364CB9163C12A72B4F341217CB264,
	DSelectController_SceneName_m3CF7851C34C394E12BCB182697F3AF053194D5B4,
	DSelectController_OnGameButtonTap_mAB9100AAD46320537758EED92239D2717896E148,
	DSelectController_OnActive_m1DE21C0494CB357C3EA992FBDF3CD06E532F4F8F,
	DSelectController_OnShown_m90085A878E25392D5C16DBF0F96C82158593D63F,
	DSelectController_OnHidden_m6F5BD004A9611E77F14FD2CECCE062C622DF8323,
	DSelectController__ctor_mD626C4CEB9E3CCFA8A38B7F0A53F3FDEE910D10E,
	DTopController_SceneName_mD3B2A27B9068DC2F810021D037235FEA3841D6C5,
	DTopController_OnButtonTap_mB3A9F9678B40B512B96A1E312EF3BF84AF65226A,
	DTopController_OnSelectTap_m139C2400CE1D0CC125393CB2FAA09E9E92CDCE51,
	DTopController_OnActive_m0367C90A0D3E5E9A5E46E797D4DE793B110435CF,
	DTopController_OnShown_m1B33B053C5DA2ED8F8F983EDC447918158174A36,
	DTopController_OnHidden_mA8BFE616C6C6CA9DB0AF591CA1C4879A68D640C2,
	DTopController_OnReFocus_m1C38998A612764B5C55FAB843AF4C1A89414C0E0,
	DTopController__ctor_mF089A1A358CBF3C55F4FED67457D22E4EDEF3131,
	TemplateController_SceneName_m0A78BB5FE2F1DC32E7A205FFD00445BF58812999,
	TemplateController__ctor_mB11577D492A599B3EFAF220CAB63FCD779C8E561,
	VibrationExample_Start_mF0B4CAB3164B468E52B2370FEC0011A00148F5E3,
	VibrationExample_Update_m939AA1C591B6CB5E7BDF4D6D69323F3FD71E3DEC,
	VibrationExample_TapVibrate_m63329F9D30D7AE4F1BA5DA0C8DCD6A2C9E6CC1FF,
	VibrationExample_TapVibrateCustom_m6122E6D3FFE06D1E3724F9FE383051D3D1E03EAF,
	VibrationExample_TapVibratePattern_m48AFB13721C230A9029A43407EB6C5E179F9D01B,
	VibrationExample_TapCancelVibrate_m6AE952B415A91E6C314F5ADEDF9ED67916EA8871,
	VibrationExample_TapPopVibrate_m2ED910558E27BB9A7D1C3552BB29054D150F4B39,
	VibrationExample_TapPeekVibrate_m7DA1A24E7E8AF899B669DF2D50DEEBEEF0B1A65A,
	VibrationExample_TapNopeVibrate_mDED3B4EEE392C3B27DEDCE4E4CD95F4D1006F7E1,
	VibrationExample__ctor_mCE008E33FA2C908D34FC1DB9D2B7BC7B04FF3498,
	U3CU3Ec__cctor_m2C5EE4840E068A5155F09B30ADBCE6FB2DAE7DF8,
	U3CU3Ec__ctor_m45B1DA3293EDF09FF55AF8D1E564E48677272658,
	U3CU3Ec_U3CTapVibratePatternU3Eb__7_0_m799184AAB33F6087AEDD03D3F120CBE663943304,
	HapticManager_Vibrate_mF81CCA1381A597C8DB4B1DE44C508D8BD0DD3601,
	HapticManager__ctor_m3D2DA2169921098312A9904E078177C446D35D6A,
	Vibration_VibratePop_m2B90720D71C697422070969ED7105A83D0AF49D8,
	Vibration_VibratePeek_m8079731D7A93AA29A7C7523123CA8FE383705EE5,
	Vibration_VibrateNope_m9671B5FBECD5092A9735C44A510092E81AB13FDD,
	Vibration__cctor_m0427AD99DC33B45FE3ABA9A144D2D64F94BB452A,
	Vibration_Vibrate_m91817995D0C33973EC132EED8AFEC455EB4E8646,
	Vibration_Vibrate_m63E0CA1B52DD80D1D974DAB43FD2307E02639D54,
	Vibration_Cancel_mD7FBC56CD7B33E2E13F0E3439F1EC919026C7607,
	Vibration_HasVibrator_m42E56D4451A27203FC2B6B6A146D81AB90A39002,
	Vibration_Vibrate_mB6A7CC969A4BA2D6F53B2E9E468F90B492C01D64,
	SceneAnimation_HideBeforeShowing_mFE8CC43AD11E0CAF48DE5DE811A791E558924077,
	SceneAnimation_Show_m53C8135BB458AE1732A10C6297ED5C42B14D6F51,
	SceneAnimation_Hide_m5EA65AF2E2A2A9C109FF405DAD648B7362B853EF,
	SceneAnimation_StartShow_m5ADE2AA4D68C4B4611F00D5592CE8CEEB058D8B5,
	SceneAnimation_StartHide_m25DA528F3099F0B0335D9A247C33585BBF5DF0AA,
	SceneAnimation_OnShown_m41E4CE6EEACE40125B1D833CF59C83543AA67FD6,
	SceneAnimation_OnHidden_m368DCC81FCADEC93B4A8C5E6181BD7974E3A29B0,
	SceneAnimation_SetImmediate_m61CA2DB4FAB1C2C199506422A2143774DA9104BD,
	SceneAnimation_Start_mD64193360095C3B6BBBDFB2F16F8D14A7FC51FD6,
	SceneAnimation_UpdateFrameCounter_mABD8E477458782B0CD46EB28102023BE4DFBD452,
	SceneAnimation_Update_m35E88BB50FA2DDDB27400A6E5018E940D07059F6,
	SceneAnimation__ctor_m71A3993FFFE3C6893A1FFC48D60884B29A7B0DB6,
	SceneDefaultAnimation_Awake_m6087ED6ED823B89D8B1B83BBF06E22734BC74395,
	SceneDefaultAnimation_get_RectTransform_mFB006EF43A9F3FDDD70D08A6CB55B202CAFC9AAF,
	SceneDefaultAnimation_get_CanvasGroup_m5ECE8FB51688E341A9AC77C6B1FF1D868B4F8F68,
	SceneDefaultAnimation_get_CanvasRectTransform_mC2B6DCB77B5D85DBD95FCE63DE5A120D53A7BF38,
	SceneDefaultAnimation_HideBeforeShowing_m24A809E7ABE30A76849947D43555A3C3AB1A1F3B,
	SceneDefaultAnimation_Show_m77F5C273C1FBD4DBF88B6702AC763841D7F2B289,
	SceneDefaultAnimation_Hide_mA8FE59A6266984909CBB8454B5617D7A9299AC47,
	SceneDefaultAnimation_ApplyProgress_mC747AA774957353E11A773D8CFC01466EE54E257,
	SceneDefaultAnimation_OnEndAnimation_m0803A7018EEEBDA64855712981478E554AC8F851,
	SceneDefaultAnimation_SetImmediate_mA0532F3586608ABE9942C0E9AA253F0F691D8531,
	SceneDefaultAnimation_ScreenHeight_m4448BA9D7ECA17D5E2A4DFB09266E81E57EBF825,
	SceneDefaultAnimation_ScreenWidth_m6A43ACED0729DCD9C1143E7E10035E7AD9E7F372,
	SceneDefaultAnimation__ctor_mC733455A9C98DDF8CCCEBA2960CD65058C4C8FC8,
	SceneLegacyAnimation_get_Animation_m419C114EC22262704705493648A0E64E38BD3894,
	SceneLegacyAnimation_Awake_m8A1803BA15E8C964EF55C48C7AFFF58C969120A2,
	SceneLegacyAnimation_HideBeforeShowing_m0AB7A6618A64929EBBA4D8AA336654C0E8D73E4C,
	SceneLegacyAnimation_Show_mBB7AE314793F29A06A0BE564EB8EC19EB629B347,
	SceneLegacyAnimation_Hide_m80D6F18AB860405E49C0CE8BD4047F29D270D8B0,
	SceneLegacyAnimation_Update_mBB4AE4173CDBC05F7BB11BF2CCE141314F8E70FC,
	SceneLegacyAnimation_AnimationUpdate_m8AF43E0B104353C98ECE1FC77A2F5E7058B33F5E,
	SceneLegacyAnimation_PlayAnimation_m5C9D393D0AC018124C87B75251255D8F86B49922,
	SceneLegacyAnimation__ctor_mE4A67DB6F8BE1B167764FE9F7D47A4559A729AD9,
	Tweener_ApplyProgress_m8804FED8AF174E83381110995CC6D4C9CAF0F0F1,
	Tweener_OnEndAnimation_m163632EB3EB0759E542AE9B805637902C5BA38E3,
	Tweener_Play_m7B0102D4F6C61BAAF9130D7A92F3097213AF3CA3,
	Tweener_Stop_m63620944978CF85778D8C910314A4CD119E9FAC6,
	Tweener_Update_m8333C4C226A4333ABF6ADA7CE8DB6F45810C9178,
	Tweener_UpdateProgress_mC591E8FDDA999A1F2AF6874963D81656EE292760,
	Tweener_GetEasingFunction_mAA6586F38A1C9C744912B77C88062DDC382CAD9D,
	Tweener_linear_mCA0623082B15FD4CDDBF0F9CC2758729D239ED9B,
	Tweener_clerp_m04CE7A107AF6512C5AE254115B74BE6F6DD8B854,
	Tweener_spring_mF7C559223385FE9A6913BE98C3AA774F0915606E,
	Tweener_easeInQuad_mBB2727458A04D15B16AB1D0A1B3E4064C84729D0,
	Tweener_easeOutQuad_m26F980E6E48B7EF370B74C6D0ED168E6E1A12831,
	Tweener_easeInOutQuad_mC1BF7FA23FF5178002438CA94F68991F7A0EF6FB,
	Tweener_easeInCubic_mB2FE4070086CA0047F9A7624DB6A7B0BBC0D4BD0,
	Tweener_easeOutCubic_m9972D2AE426FA4A96C0D41A3ABD3EB5841E1D3B0,
	Tweener_easeInOutCubic_m1BEDF22C3FB0BCA3E62D85334679537701147CB8,
	Tweener_easeInQuart_mB56F838DA4031081AA90EE012830B7D90F385F60,
	Tweener_easeOutQuart_mBF985E168196330A31C90CFDDE3E26E2E0AFBA38,
	Tweener_easeInOutQuart_mB9C14E16EDC609BF5407A4EA8EEB33CD6157D4B3,
	Tweener_easeInQuint_mB80FD8DEC3A3F3488889A0D7BB31ADF50BBED703,
	Tweener_easeOutQuint_m3E3F952CAEB6F896111AB6261A7CCE07A5A32DBE,
	Tweener_easeInOutQuint_m7653F738DC6C8E5DA1CF8FB4CE89826D71E0552A,
	Tweener_easeInSine_mF233683A5EA23820A8327025AD8FA7114346D0BC,
	Tweener_easeOutSine_mACEF8EA8604C6425EDDEEC36DF0ABB97F57761F2,
	Tweener_easeInOutSine_m2D9D2D43DF5AD5270D12BCB0B53D7FCD5822D8BB,
	Tweener_easeInExpo_mC9978D31740909A348043D5F2BED2F0A73AEA206,
	Tweener_easeOutExpo_m9C8CDAB6774A484DA82CCDF4A1F1F61CE9874701,
	Tweener_easeInOutExpo_m076C00819B3E8A325F6E4B9A54AB0D5A31B86509,
	Tweener_easeInCirc_m85A11DF434362981C19EAED2D72916C1B4496D26,
	Tweener_easeOutCirc_mCDFE60DB11BA255EA348F449336EAFBF0AF34C4C,
	Tweener_easeInOutCirc_m3E035390160C90B33EC9BA590320D3AF98CFC16C,
	Tweener_easeInBounce_mF284A65C2B4230209EA71F8A7A89F311A0B82411,
	Tweener_easeOutBounce_m01EC3980F647712756854D6B81556623E350A60E,
	Tweener_easeInOutBounce_mF4E96C1515CAF2F78B8D02B986C5685476CF5127,
	Tweener_easeInBack_m5F9C324680EB70F58B800C1E7CC397AA2EA911B1,
	Tweener_easeOutBack_mE2E8174CF702CA3269D3BDDC29A3611E016643CB,
	Tweener_easeInOutBack_mCBA5EFB2BBAF2A69FD664C5BBA5AF312536B71DE,
	Tweener_punch_mDD0C45752A0F5F258C1A099B374439347A91F6D0,
	Tweener_easeInElastic_m44B6FD27E4FC65BA3BD998ADF19BBDFD9DD1FBBF,
	Tweener_easeOutElastic_m42AD40A7125879554453A5D3D58B068ECF37D4CE,
	Tweener_easeInOutElastic_m8CF6BBA18CD92D7BF2CCBB34E6CBD4F98ADCC56C,
	Tweener__ctor_m5746E805DFFDAB0A996A5549587EC0EC8E3428A5,
	EasingFunction__ctor_m83CFC8105C9A006241951E66679DB2F80EAB98BB,
	EasingFunction_Invoke_mDB65A14C38B3CD25717CD538DC344FC464A29680,
	EasingFunction_BeginInvoke_m7C04E9669660211BDE6790F5CF3555C52B5285BD,
	EasingFunction_EndInvoke_m718901AE480EB6B4545D0F4D9EBE1C28736079A3,
	CameraDestroyer_OnDestroy_mF495B6FE7A59CB456A896E2F27DD9634BBA2AAF4,
	CameraDestroyer__ctor_m58290D86C447C96D02A050DFC6FEB7AE5D9570E1,
	NULL,
	NULL,
	Controller_OnActive_m629201E7FC1E39EF46D2721F096599735E8C8FAF,
	Controller_OnReFocus_m9872BF16366F67BC9F37A2C2735D3C9609FF00C1,
	Controller_OnShown_m821DCA48FD6E4E07F8E2211A602207C8E95958B8,
	Controller_OnHidden_mE00B739484A3909F4C09D7568A09D20A38001564,
	Controller_OnKeyBack_m3ECCABBD257BEB94BED7694E9303F009155F33BD,
	Controller_get_Data_m5FBB080ED88F0FCCE300D8AE0BE74E1B7B66D6B4,
	Controller_set_Data_m134299AB8FCB15566C6B2AE114B040FF8FEE2E71,
	Controller_get_Canvas_mC069A517D0AB796C905CFD93EB6E9EBD57475234,
	Controller_set_Canvas_mBE1F181A21647DE0E3D792CB90718DB4C4CDDD32,
	Controller_get_Camera_m027116AEDBA981D3D06D67685741FA117AED7820,
	Controller_set_Camera_m9DBF4259A3D45D85837334CC090A06518F199B41,
	Controller_Show_m2DA6FD38E632DDE33FFDDE549AD775A8430D3D57,
	Controller_Hide_m132B3D41161EEB4ABA5072D157B8B00BF199FD6D,
	Controller_CreateShield_m3A3848AA62B889121B1CD61FE0565555E2CE5A60,
	Controller_SetupCanvas_m372036D795E9DAE08D7CC114D024B6FD2DF99CD0,
	Controller__ctor_mC40550E30A8F72CD75EB13CAC75C87BEF1F0F770,
	Manager_get_stackCount_m02A93B09158482CA804FF6045568FF3E5431FDC2,
	Manager_get_MainController_m2FEAD42BEE225E5A75F430BE57EADA0EBB23E29F,
	Manager_get_ShieldColor_mEDF473CD5B3B0E921EDCC9A8ECDED1A59AEAF66C,
	Manager_set_ShieldColor_mFAC7D6CD1EA0F67EE06DB6900180748261E1ABB9,
	Manager_get_SceneFadeInDuration_m98102617D5859290DAA2F5E61A47ED72EF3AD434,
	Manager_set_SceneFadeInDuration_m2DA5251F0F6483A90F57212516A186F19A45FAE6,
	Manager_get_SceneFadeOutDuration_m2AF5EBBF4DE770502820BE14793AEE44420B7745,
	Manager_set_SceneFadeOutDuration_mA6B28932A9C0AA18771DCF4B9387CD64176FDEAC,
	Manager_get_SceneAnimationDuration_m5933AAE547D9E16299D8ED64A5CE8E07256D0020,
	Manager_set_SceneAnimationDuration_mDCECEFFEAAD73AC441388B41B9AAE73BE20E3ABE,
	Manager_get_Object_m9396DC89955E29CCAA00FA873142C1FED1205D43,
	Manager_set_Object_m89E507CF550DDCFE52A6BFDEDB7EB186E7A3817A,
	Manager__cctor_mDBD51282E1B707DF71B4721FD38797708ADE4C9A,
	Manager_Load_mA6A5F24319F8636FA27EB5C96FB7C2E72C6949F2,
	Manager_Reload_m4223E473D74F1A9FD4D9A36A8125BB35C4EA41AD,
	Manager_Add_m39BCFCC76985263DCAE3F55E484B3E5493F45178,
	Manager_Close_m431A19DCECC624FDA3C633E920D80B42C6B4913B,
	Manager_set_LoadingSceneName_m80C7A5F15EF70BD56230200EB76F95BC50743886,
	Manager_get_LoadingSceneName_m52F0D661A0E22355C41B21F34537DFABFD4F3B9A,
	Manager_LoadingAnimation_mE02DBFE5C5F8B77C54A5DDDF6EE0EB2CA55CF3A0,
	Manager_OnShown_mE27E1092890D34C436E8C29B625C0F3DDC8CA279,
	Manager_OnHidden_mCDCA7B81F0198D0D0FEAB49A729BE0124912910C,
	Manager_IsActiveShield_m2B5F80D0D079F33F28CA3D7C1B225C74F6E8CCBA,
	Manager_TopController_mBF9FC0FB451A507568C11907DD30F1F530431C6C,
	Manager_OnFadedIn_m4646E21CFA05904255A17AB54ED3916992498AA7,
	Manager_OnFadedOut_mB82930E3DF30A199B3C9A03D4218BA73BEF7DD88,
	Manager_ActivatePreviousController_mF6F981BA01D16AF2E1B3F9EE866C414491CDEEDA,
	Manager_ActivatePreviousController_m342B80D0A34D49E74859FF4BB2C4060D7421DB89,
	Manager_Unload_mA57DD1898227D983D954E6C815D7D59C91CE63B4,
	Manager_OnSceneLoaded_mA6D4E6D38FF6DEA7426FC652F17860D6A4321F8A,
	Manager_GetController_m765FE9A1B057117A05AF80E57199FA4739FC5EC8,
	Manager__ctor_mA5ED09CF20B857DC843EF0E097C657F790113B90,
	Data__ctor_mAF4E759868718E64398C1C3BE139AF7F69E9D1F8,
	Callback__ctor_m920612C6793B3C37AB7E198CF2FB4B4E11BB68E2,
	Callback_Invoke_m0C199CA22DB833B7A69AA879809960D45B1674B7,
	Callback_BeginInvoke_mAE9B9C586BB09DD8E3638CED470E604A7F65D347,
	Callback_EndInvoke_m2C318BA02B0D6EDF2EEE9CFCACE124057E05CA3C,
	ShowBannerDelegate__ctor_m10C2A19277315193B7155C677EC30ED25D16824E,
	ShowBannerDelegate_Invoke_m5BBB41C58347FF11A953BEFBF5F8644C1C469A75,
	ShowBannerDelegate_BeginInvoke_m1167F32DB2940E1F4E6A801A0DC7FE6B766821AF,
	ShowBannerDelegate_EndInvoke_mBC93C3D7D932BF1EBEFA996EF478794A3840635D,
	HideBannerDelegate__ctor_m2C036D15240E4E723161D00511E66B59EF9D7B0F,
	HideBannerDelegate_Invoke_mEC52DC813FA76542A42A68A0C76B25CC6443CDCC,
	HideBannerDelegate_BeginInvoke_m9A8491F46F95D5E3133F41AEC4470625D440FFEB,
	HideBannerDelegate_EndInvoke_mFE2D522B76A823BE3CC8AECAAEB16FA7EB68C9FB,
	ManagerObject_ActivateBackgroundCamera_m093FE7618848D184AC19953867378B620CC77BF4,
	ManagerObject_get_UICamera_m993C42564ADEFCB2FDDB3BE84C78393F42E5B891,
	ManagerObject_ShieldOff_mB71246D3053EDDBCAD91203CEF5213CA9023D1ED,
	ManagerObject_ShieldOn_m4531FABCE185C05D7AAA925EC790ABFFFD33B7A2,
	ManagerObject_ShieldOnColor_m5F595D421C8643263923ABCD0A2BC08B94648EFE,
	ManagerObject_FadeInScene_m5A9C28BB268C93822FE671C1C799B9F1495E2EC1,
	ManagerObject_FadeOutScene_mE3693E5E300E9A68C8BA0CE0CEC6F08C5A5A5ABF,
	ManagerObject_OnFadedIn_m17313D976F90EE10259133B5DE6A1B3736199CA7,
	ManagerObject_OnFadedOut_mE5DCFC3455AFA1AE4EAADD981B52C49DADDC3AEF,
	ManagerObject_get_Active_mCE7D422E51414DA2707A9D58082A19D08C6A08C9,
	ManagerObject_set_Active_m299A151347D379C449976630C9991724505F5135,
	ManagerObject_ApplyProgress_m8D72FBF756DC1706EBA381CC219F32D6573BD0A3,
	ManagerObject_OnEndAnimation_m67C9C87ABAFB0A7F04E05458D7E553D6E005CFC0,
	ManagerObject_Awake_m2D2270DB51450C265D4B0219A3A8AA0A7045362E,
	ManagerObject_Start_m3CC4A4092C1D4D8FB7DBD6C80156FA6BA028EEB7,
	ManagerObject_Update_m7B6F62CFCE8D32B9F90E1908F8FD24D0D588AFC5,
	ManagerObject_UpdateInput_mFE5AF8902B3CC354303C6B47A0CB1B0943BBD444,
	ManagerObject__ctor_mEBC38560C142185471943C1D9E9C9003705998D1,
	U3CStartU3Ed__30__ctor_m3278BA8A2B7B1FE63ABBE55EF001B33E930B64C1,
	U3CStartU3Ed__30_System_IDisposable_Dispose_mA073B863AD0267E5ED9DE807DC0884E5F880EF12,
	U3CStartU3Ed__30_MoveNext_mD943E92BD103477C99C71C0FF5F60C496D3F8417,
	U3CStartU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35A692CC36E0BA65212115187F232830504A9BA7,
	U3CStartU3Ed__30_System_Collections_IEnumerator_Reset_m1DB36B586D00CDF821E2C9D38F12E26E1182C5B2,
	U3CStartU3Ed__30_System_Collections_IEnumerator_get_Current_m38675A3A033151C858C740BBB4D18165212F6C60,
	TabBar_Awake_m773C1963497C66A73DDF8A8C834B941CD86FBFD7,
	TabBar_SetCurrentTab_m79CC71C71A81C59BBD91A4C7C2569AF09C42122B,
	TabBar__ctor_m1981FF95A829EE77AAC654E093DAE05A80A4AF7C,
	TabButton_Awake_m2CFEA3B30D49F3F7CCEED8C72213CFBE8DB9F9D5,
	TabButton_Resize_m12C7883CE7BBBD6E0EAC043CA8AC96DEA8245D76,
	TabButton_SetNormal_m5DFDD164DF5EE6D2C0BF3D581DA43A4BB2BD4AE2,
	TabButton_SetActive_m036247A248B1499FCEBF3BEC9BC8C7927FA9FC15,
	TabButton__ctor_m7B48CC66E48962A5C92D80E6A6D1484EE5FAA307,
	TabController_get_Current_m124072306811E0C914C4306A1AFAA6E9CC84B4E1,
	TabController_set_Current_m9F7548FAC6A89F8F23292605F7278EAA538BA494,
	TabController_SceneName_m59638E8CC7B495FCB24DA95EDA02321050887F2A,
	TabController_OnKeyBack_m4891B07612078E768004721B6C107559AB9882F8,
	TabController_Load_m1C679431E6B9C4E950DE48792463AE25A848C525,
	TabController_GoToScreen_mD2991D198F7B097A895B8083893FB637408DC11E,
	TabController_Awake_m9376B07F2837B5B62747E142AD412447F8AE3A81,
	TabController_Start_mE19D2EBC0A8C3D3F9EEBFA502CECCB593CF856CE,
	TabController_OnShown_m997735E12436EBE3878431FBA3300286C0457D17,
	TabController_OnSelectionChangeEndEvent_m938A404F867E68C87A27812FE8DFBA0B9D7D7D16,
	TabController_CopyRectTransform_mDBDF4DD03C3CF2C762FA4648F558447E3FAEF67C,
	TabController_SortBySiblingIndex_mD0615A4CB1F8535ECAE4F0F9D3FC81825AF4A1F9,
	TabController__ctor_m941DD841F78FF45B3788B1D6B51609BC9196B307,
	TabSubController_get_tab_mF46921860DC621EBB80E7F2EDF23FAA59B2FB421,
	TabSubController_set_tab_m0077B05DB648639D5AF793DF548126E57B2A9C30,
	TabSubController_SceneName_m3EFDE29FF6346C7D7530A661ECFB5D6CD6E060AA,
	TabSubController_OnKeyBack_m6FDF18A17F47A39B561E661A2CA06F479F0605AD,
	TabSubController_OnActive_mE95AE1AABF3711D7668B023BD551863766F2578A,
	TabSubController_OnShown_m6C77E215783400EE85A38E20FAF737EEE2327408,
	TabSubController_OnHidden_mEE079AFA4F6FBC8412590AED87E25FB2CA21CD28,
	TabSubController_Hide_mC3A6C2DEC1CC717E3A374556A30D3CDC041C35FD,
	TabSubController_ApplyProgress_m062D427274A25BC039473AF914DBF275C140A761,
	TabSubController_OnEndAnimation_m0D8EDECDA26D3D1CA8E8249CF88BB871C25565A5,
	TabSubController_Awake_mDBA9908509DD95AA6D0DFFB8E1E616B660D77F31,
	TabSubController_OnShownFake_mD1BCB8520C389DD75B15A22137C006A9A2D772D9,
	TabSubController__ctor_mDA60F5E196321B6BCC7A12ED395498180B041E84,
	U3COnShownFakeU3Ed__15__ctor_mBDD5A6F79B6B054D1ABB955F21066CBBE307DB0D,
	U3COnShownFakeU3Ed__15_System_IDisposable_Dispose_mB8DD835A213DD3A0597AB08AD08A58C087DACD4A,
	U3COnShownFakeU3Ed__15_MoveNext_m43776D9371E7861ABC670E2585749F48F562AB45,
	U3COnShownFakeU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m931BA7BBF4F775B400B233466BB956C8A034DDDF,
	U3COnShownFakeU3Ed__15_System_Collections_IEnumerator_Reset_m8A93BAC0233FF83645A9B29522A2E000442A3E87,
	U3COnShownFakeU3Ed__15_System_Collections_IEnumerator_get_Current_m38136C0C9E43E72FB4150E594AEE59CFE16FDCCB,
	Banner_get_isSmartBanner_mC04C86A7BF69C5E97835D5F2E6AB90631BAC6401,
	Banner_set_isSmartBanner_m0F76B11CB3EDEA25DFE884DEAA6AA341642513BF,
	Banner_GetSafeInsetBottom_m4EC77E26909FB220C242669D9EDAE90E8A8DD301,
	Banner_GetBannerHeight_m401D61BB3FA2620A50D1C9F8BEEE623143F372AF,
	Banner_GetBannerHeightRatio_mE62502B833CFAC642BDAD963E1C9C8E087C74BC6,
	Banner_GetSdkInt_m69A012595A2BFAE13F0A7B07BB2FE25F1BD960F9,
	Banner__ctor_mA1DC4EDE4C79E1F6B948F23CFC7106A27A7F7D7F,
	Screen_GetCanvasScalerMatch_mCC9A5160CEA1A6B4EB66B356996ECD41E2A10579,
	Screen_GetLogicScreenHeight_m64DF0BBD745DE3125D1CA384C283483A459E4674,
	Screen_GetSdkInt_m1F90166C847E3DB3E226688ECCBA6D7566B56FFA,
	Screen_SetFullScreen_mD352849000BE2EEEFBB66569D6D5604BEAAE716E,
	Screen__ctor_mC9E02C5FD4872D61FD8DBCFCBD0FACA651C45F9D,
	U3CU3Ec__cctor_mF606A2194705405CD1119BA0AFC100C616933ABD,
	U3CU3Ec__ctor_mC9C6A4C89E68D3020AD8FDF211FAF8A6C6793308,
	U3CU3Ec_U3CSetFullScreenU3Eb__4_0_mA7342DBE84240A178017F8842D618DA71B7FB9CD,
	SimpleEncryption_SetupProvider_m062A54324A39BD93751820D5A28C20485E65AF62,
	SimpleEncryption_EncryptString_mF881A228836861A1F6F0A4C4DCD4DE90D27AE269,
	SimpleEncryption_DecryptString_m506A08EBBB62F40BCED8E6DBD0DBB5F7A93BE834,
	SimpleEncryption_EncryptFloat_m1974C78D4058D6BE84CBF657D2A694670283A9C4,
	SimpleEncryption_EncryptInt_m3CE34CEE950215F8B4DB3A9E1DB4FFADE0D48299,
	SimpleEncryption_EncryptBool_m671235C931F7374A7D57DFED5F70B8E74E3AE0F4,
	SimpleEncryption_DecryptFloat_mEB28B16A281B4CC9B599FDE33E647F71FE0BF58A,
	SimpleEncryption_DecryptInt_m0530DF4FFFF499E22CA9C021279F314357BE6420,
	SimpleEncryption_DecryptBool_mF5B50F272F51850A91C13883D90688C016CD458B,
	SimpleEncryption__cctor_m70AF6AC558970D030C7D90133BA8DFE7C46E16BE,
	Json_Deserialize_mBBFB36E78850A1303259517EDE27BF76C57A2EDC,
	Json_Serialize_mC59C9F9BB9BF6CDB4749B896203B9314C54C5D88,
	Parser_IsWordBreak_mF99D33120E987D267B8EEA33B9F6CD280FC1099A,
	Parser__ctor_m8E7B1E3413CB0ACB52B7EC3D42570BD50835CF07,
	Parser_Parse_mA51F5F7E7F54A8BD238A1F9FE42E38BEFC63ADAB,
	Parser_Dispose_mC5E065B71F9282AE393E76552E87F12E225BB471,
	Parser_ParseObject_m3F2F42E3C1643E992303AD52733844635AAE185E,
	Parser_ParseArray_m77AE0FC5101EC5395B675E5E48BCE02829BFF926,
	Parser_ParseValue_m380E37286A8F41902BDFF469A62314E0450ADD9D,
	Parser_ParseByToken_mE2527D6EEB3A02B878D32032921DB5453980007E,
	Parser_ParseString_m6DFDC932B48CB2140FCD74416FF0C07ABF34702A,
	Parser_ParseNumber_mCFE1CCFAE9CA433971DCDAAD73BC32E3E7DB401C,
	Parser_EatWhitespace_mD0804D4B845AF21A5B8D2525F790898BDA2290FD,
	Parser_get_PeekChar_mA03F49AC460B398EFECEB43713F62B3DA4E397C2,
	Parser_get_NextChar_m2F9F54F34F726B5758B4CB735A84FBA854F3DDDE,
	Parser_get_NextWord_m00AE793021AC94171F0CBF824885513DF239D8AF,
	Parser_get_NextToken_mA87FAFC85B2F55E74C985D92BAD0FCC2E3F944A3,
	Serializer__ctor_m9723BEB6C7BF139A9C18D8A488BE501CADE2A7CA,
	Serializer_Serialize_m645317E54BD40151210F873650D3D7D12B06A389,
	Serializer_SerializeValue_m72FE1385F3CF53BA14FA18AA503B9E0733A26877,
	Serializer_SerializeObject_m49CF3C61AD626B4B126C7CC776B5F6BEE0DC25AE,
	Serializer_SerializeArray_m68C42E473E846D9314DC9D9A6EF2C374F3A0899C,
	Serializer_SerializeString_m8960049B70B1E72FD1C478DD8628F03317ABBCB9,
	Serializer_SerializeOther_m1397447055994BF1BE52AD7099357421DCD8C3CD,
	ETFXButtonScript_Start_m25A1DDC0BEB25CBCB20EB6FBE9E71F006138B3C1,
	ETFXButtonScript_Update_m22634A284FB8C975D9929F15988F09FA1ACDF332,
	ETFXButtonScript_getProjectileNames_mA13342862E48B4A8B6BC0666BC7EFE2BB5625D3E,
	ETFXButtonScript_overButton_mEFE1C511D8EEA91AE9E54B97CD0A155E542290B0,
	ETFXButtonScript__ctor_m65F38109C2CBF445E711A2C6646B34C2483E0455,
	ETFXFireProjectile_Start_m6586051FBDDC4104ED997E585ABEA089694B805D,
	ETFXFireProjectile_Update_m4F39FFB37EDCE10D6037FDE13FF274C027F60990,
	ETFXFireProjectile_nextEffect_m406DBBEAF4133E7C3660AB0F5BC32CD6B3B0C7C5,
	ETFXFireProjectile_previousEffect_m33C82A976F62A20D43E2FF8C57E3066DCB00AE5F,
	ETFXFireProjectile_AdjustSpeed_m0A76F61B5320E509827716B5B302AE41B028288A,
	ETFXFireProjectile__ctor_mB48108DD5AB58E5907180ED79690E217D8EB1D65,
	ETFXLoopScript_Start_mE67C5E09B7AB2A78688B1877DAC3766A0868CCA8,
	ETFXLoopScript_PlayEffect_m2EAE66A60E00C863C15BE46D10AC4522789A218E,
	ETFXLoopScript_EffectLoop_m728B76DFD989B681D2C93D7A88E9D76C9696CAF4,
	ETFXLoopScript__ctor_mC0B4A260353A71EC89B2E1ADA7834286E0F60E5B,
	U3CEffectLoopU3Ed__6__ctor_m553F1C586BBB6C68EEB9710F62438A8EDCE136DA,
	U3CEffectLoopU3Ed__6_System_IDisposable_Dispose_m8142A69BF60253C0139648E9350176EB9EFD8BFA,
	U3CEffectLoopU3Ed__6_MoveNext_mE11D3E615622E410D2DAF1960011C76F8ABC322D,
	U3CEffectLoopU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m93EE11485A4BA42D80F240C1703D673774889545,
	U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_Reset_mB35745D308CDED6CAB69D52DEFC2D84AB6CC5896,
	U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_get_Current_mB56FD34E205892C6B85E8FD18DAE68F19ECBDE2E,
	ETFXMouseOrbit_Start_m4FB8182859BA3A3D8139156DFFFF58A6001F9297,
	ETFXMouseOrbit_LateUpdate_m27A95AC3E473534D0DAD68B5B76579A0811D8CEC,
	ETFXMouseOrbit_ClampAngle_mA204BF97BDDC49A732917C75BDC934D4CF56B314,
	ETFXMouseOrbit__ctor_mB2E0C52CC44B7FE5D064C3183F9DED7714DDCBDC,
	ETFXTarget_Start_m60CA8E1079E17A7C9A47D10C9FA8AE9220DB1E9F,
	ETFXTarget_SpawnTarget_m00485209171AF5E1A8DB572C256C99B0E821BF96,
	ETFXTarget_OnTriggerEnter_m057AF200E64829505145549ED629FAA79935B1F5,
	ETFXTarget_Respawn_m679D0B0188B3DA281D2BE313E7212FC1D6F2B905,
	ETFXTarget__ctor_mD0F9BFFB2CC402DC5C6FF274EEDD58EB891A22C6,
	U3CRespawnU3Ed__7__ctor_m415513F665DE8F3777A3B21AF9C5B45A90CB1BF3,
	U3CRespawnU3Ed__7_System_IDisposable_Dispose_m63C5D414DB8D33EB86AFF6D5569B6C4E18C32615,
	U3CRespawnU3Ed__7_MoveNext_m58D6E6D0CFF4964FDE4083115A10119C4FB6FEE3,
	U3CRespawnU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF70DB558C29B7FFE4A790553093711C3478A2C54,
	U3CRespawnU3Ed__7_System_Collections_IEnumerator_Reset_m37DFF9115E528EA7997B687DD51321AB843D9BE7,
	U3CRespawnU3Ed__7_System_Collections_IEnumerator_get_Current_mA84FD67FBB5DE784889C67146E66570FC2E5D80E,
	ETFXLightFade_Start_m80EEDD11315E813676DA8E550C806F8C96374733,
	ETFXLightFade_Update_m3705B92646C8612BC70987DF39BC94B8F88EC49D,
	ETFXLightFade__ctor_mCDF41DC7334BC08F86DD3EF815FC1F3EA4C44F11,
	ETFXPitchRandomizer_Start_m64BE3B77C622908C2E324C6D3DFD17CB9092D21E,
	ETFXPitchRandomizer__ctor_m8493E0829884B225C1FC98134FA1EFCC44CDD238,
	ETFXRotation_Start_m3DC21DA9C26DBA1E14C27B9B997F46BEBBFB22C7,
	ETFXRotation_Update_mED37E3F435A6166C540A1DA30B49354F05279100,
	ETFXRotation__ctor_mC1C7849D64611953B026F069EE879C10B1399ABA,
	DOTweenModuleAudio_DOFade_mEA99A941484D71522BCDD1CCC43B790A6AF4A0B3,
	DOTweenModuleAudio_DOPitch_m6512EC4BEE6C70E110DDFC1048BFCE13FA87E914,
	DOTweenModuleAudio_DOSetFloat_m8831E89C2E951DDAC9C20A46D3AAD329BAD9C09C,
	DOTweenModuleAudio_DOComplete_m49C5BD0D315241E09EA8D6398F8011C5A34646AE,
	DOTweenModuleAudio_DOKill_m2CED6088E868165A2D91768409FAFFD980993747,
	DOTweenModuleAudio_DOFlip_m080FDDE3ECDF68901289FBC6EA1297EF278D9D36,
	DOTweenModuleAudio_DOGoto_mD4A9A702C6187927FDBDD3220570820533459D16,
	DOTweenModuleAudio_DOPause_mD42B2DFA89EA1635A6F9E813F7B852C852F282E5,
	DOTweenModuleAudio_DOPlay_m95942072A27A597B1FF4FA58A56EC35EC3A8DC15,
	DOTweenModuleAudio_DOPlayBackwards_mA2814EE20AB3527ADC90DAC391BDF4AD2DA63104,
	DOTweenModuleAudio_DOPlayForward_m808D504EA5FCA7B43292D4024735C69339BEC168,
	DOTweenModuleAudio_DORestart_m53CC4DB3E3B69233410704AC28B0E7E5FE15F9B6,
	DOTweenModuleAudio_DORewind_m1102048CF3E759CC5A0B7AF48B37092DD8625D5E,
	DOTweenModuleAudio_DOSmoothRewind_m4C703A054EDA680CDA0FE09924AD97775BF70DA1,
	DOTweenModuleAudio_DOTogglePause_mA1AE6D3A3A8FA9692CE0695E915AA08626EF61CF,
	U3CU3Ec__DisplayClass0_0__ctor_mB8ECD32BCDE8AEE5DA844CB16FB319546FDF65F4,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m6447B544882FDCCECF5F03C92CFD462E52F55C3C,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mCBA7DC56068D62779FD9318BF1BF6CC04674AA73,
	U3CU3Ec__DisplayClass1_0__ctor_mD593A9B61B5F03AF9D1695ED9FA2C1E993F41739,
	U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__0_mB6B65C2ED3194137382A4DC81F72AB5C2C95FEB7,
	U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__1_m9ED5ED10167C9FA210B584AAC22893154A683518,
	U3CU3Ec__DisplayClass2_0__ctor_m3D650640D73868903AB5235513EB20B1EF57851E,
	U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__0_m1B04734B7D8B9A386220DCDED11506098EDF5039,
	U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__1_mDE42A0D8DA684D1F5E21CA1797A119D6BF6252C3,
	DOTweenModulePhysics_DOMove_mAD664CEB3F56A94F3481FA83FC799F0D7F8FF53F,
	DOTweenModulePhysics_DOMoveX_m4CD527117E19DE2009884655A131DF83B9EAD4A9,
	DOTweenModulePhysics_DOMoveY_m126BCA31B85BF67D1B5E85549A95802B69777A2C,
	DOTweenModulePhysics_DOMoveZ_mDC3444100D64651B456A118C92B6F77C99C32597,
	DOTweenModulePhysics_DORotate_m2BDE8501CE323175A7471D694249E660F5BF068C,
	DOTweenModulePhysics_DOLookAt_m706C3BF65EDBCCB2562FB115E61151945941C5FC,
	DOTweenModulePhysics_DOJump_mCDD95D56D93498D68A345E57B3C359388A816821,
	DOTweenModulePhysics_DOPath_mF69DF997EF91CD5347391BC89E6CE268A5CD39DD,
	DOTweenModulePhysics_DOLocalPath_mB056B81341D5048E516138EBE4A680B3F4664D6D,
	DOTweenModulePhysics_DOPath_m3C49FFEB71D494F474F7223248D7105E90CF5BE7,
	DOTweenModulePhysics_DOLocalPath_m6F939DAF66D56DA9B52DA62546C70F3862E14193,
	U3CU3Ec__DisplayClass0_0__ctor_mCFBBC383950DE52D74C6F4E85C4F7F3273B10719,
	U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_m8158605D880C075EEDC090F3BD1175AA33E2E107,
	U3CU3Ec__DisplayClass1_0__ctor_m4E41921AB96267B39AAB22B0790884BEB58B467D,
	U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m0EEE1A4D5C020E63505FB9F7E09A531433FC817D,
	U3CU3Ec__DisplayClass2_0__ctor_mA7A465D617CE4C839F719F65F44B2D806976FC71,
	U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m4930252334FB6162D77F0821409D7487A96CB696,
	U3CU3Ec__DisplayClass3_0__ctor_m9E94ECE0838CF218AB657F8C4B51F881B310EF34,
	U3CU3Ec__DisplayClass3_0_U3CDOMoveZU3Eb__0_mF75D2479FE18CCE0BFC07AA4A06B1EBE01369D29,
	U3CU3Ec__DisplayClass4_0__ctor_m1A01BE964AEBF9737E72E7C9B3B5D660D965B936,
	U3CU3Ec__DisplayClass4_0_U3CDORotateU3Eb__0_m6B41E1A9419EDB0B9FDE8C9A62BDAA1281D19EF5,
	U3CU3Ec__DisplayClass5_0__ctor_mF63278B7093557C5BF31EE7878AC0E87D82EEC74,
	U3CU3Ec__DisplayClass5_0_U3CDOLookAtU3Eb__0_m2019767096427977C234F4A6698E6C2559840795,
	U3CU3Ec__DisplayClass6_0__ctor_m06AC3FA2094299E1FDE7ECAFCC8F970C5139B4FE,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__0_m847B6B84B9DBBB179602A2DAC20A1E47B7CD0C6F,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__1_m488237F614033272B64730F763C4EFCD4009ACDB,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__2_m6F586BA8E7E757466B5E77AB61BD0056F4D86D28,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__3_mEC9264AAF5B45E16B176AFA00079BFDA173EDDFE,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__4_m15CD7F1F9A30E34BA9AC943FA510B9FF0C15F84E,
	U3CU3Ec__DisplayClass7_0__ctor_m177CD3D0B0169C7DF1FD6F64FB8C39D0F92E7023,
	U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__0_m448E92A163A6A98FFE561134611FAD6418C6824B,
	U3CU3Ec__DisplayClass8_0__ctor_m4499D3AB924EEB9EB850233A8F64E5EC4A7AEA3D,
	U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__0_mAB235341BB5079CA397BF09B075B856C6B950EDF,
	U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__1_m19CF06E0D31E737EFDEEFE6D4D2361EA0D372730,
	U3CU3Ec__DisplayClass9_0__ctor_mCA1E6F241784A13DEC15E12A0219479D552BB6BB,
	U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_mD050544C13BA54260FC94E0964EF204B4EDB2628,
	U3CU3Ec__DisplayClass10_0__ctor_m819824BC9854A0C90662B04D48B88CA7B052BF83,
	U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_m3089CA33F25546258A5E846EA51421D71AC55D70,
	U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mD201EFBB4403FBDEC0D377D39A768A581E6CCDDD,
	DOTweenModulePhysics2D_DOMove_mC7C9AD67230F89E53BCC36B37BC699E6E3DBFCC9,
	DOTweenModulePhysics2D_DOMoveX_m47B42CE055F1892B75820F6FDE992E541394D23E,
	DOTweenModulePhysics2D_DOMoveY_mBFBABC3C73C6D858C280B8E0E21869BCFC57938F,
	DOTweenModulePhysics2D_DORotate_m8E6DBA35EFB07C71A288D36A86B1299D99D2BE6A,
	DOTweenModulePhysics2D_DOJump_m13774D66ED37FC3FF7BA6862C306D32C0B01C351,
	DOTweenModulePhysics2D_DOPath_mC8E35580B3D00EC82A6EB5150838D2D220978092,
	DOTweenModulePhysics2D_DOLocalPath_m56F836B516F2576C6CF48E07518EF403ED233844,
	DOTweenModulePhysics2D_DOPath_m45376A9D96DC70D4150654D08A1A15AD21A158B1,
	DOTweenModulePhysics2D_DOLocalPath_mE80C06CF4F281FC00E6E4DEDDDADB145C36091E6,
	U3CU3Ec__DisplayClass0_0__ctor_m28BD574ECE1FAC2A8040F507B4347CA641B883C8,
	U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_mD0BB74EE7E3ED3D15C2B501656DD41CB4DB1CFB0,
	U3CU3Ec__DisplayClass1_0__ctor_m0A358EE93B5CBA3BAAFED5EFA3CB29B852CC0A8C,
	U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m890A07E5B25CF040E903014587C8C30FC43505CF,
	U3CU3Ec__DisplayClass2_0__ctor_m79644C3E99A1BADC2CF695A33996EFBE999BCA28,
	U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m833E99A0F3AFA6D89F501FE08B0EF7371AE20D4C,
	U3CU3Ec__DisplayClass3_0__ctor_m37C53B07CACF2CE16A6FECB187B45E328882E6CC,
	U3CU3Ec__DisplayClass3_0_U3CDORotateU3Eb__0_mE8799B2B2CD516708D2235A0D1822AF9AEA117DF,
	U3CU3Ec__DisplayClass4_0__ctor_m35FA34381650BF0B9A9C29EE83B8043082D0A482,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__0_m66684B2056E4B92E933AD98C2EAD6EDD9AF9C174,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__1_m99B395C93C8827215768541DBD48AAFCAA4014EC,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__2_m9D2C7FBC51C4DBCCD91E8359A6FCD6D5BD633FB7,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__3_mEC32920DB355DC7E1AAF2249C33F3177670806E8,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__4_m053F71A269824B99AA058986F6E772F950C453C7,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__5_mC21E068A8BCD02C771D362D4397E7C1113CC2E3D,
	U3CU3Ec__DisplayClass5_0__ctor_mC0712263E288AF8337D78ACD1B4187EC2A79507D,
	U3CU3Ec__DisplayClass5_0_U3CDOPathU3Eb__0_m2DD131B5B01242CEE25D2865DF2F0BC59A4753B0,
	U3CU3Ec__DisplayClass5_0_U3CDOPathU3Eb__1_m80D3EF48ACDCE8FCE54C12A18D6A47AEC649ECBC,
	U3CU3Ec__DisplayClass6_0__ctor_m56FEA3991E5AE4A9842780C1DAFF6AF473C21946,
	U3CU3Ec__DisplayClass6_0_U3CDOLocalPathU3Eb__0_mC6200A1DCC1B49838E2F65C06843DA00BBBA220C,
	U3CU3Ec__DisplayClass6_0_U3CDOLocalPathU3Eb__1_mA1E8B523414CEE79D40909E7D32EE7347C0E7A8B,
	U3CU3Ec__DisplayClass7_0__ctor_mDAFC1070A51179D6DFA2275AAE0834CE09FA1C91,
	U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__0_m72756DA3FF5827EF7CC02F5294B18C2E54F0E261,
	U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__1_mAF47A28E7DB74ACE3A323933545400C727F1A180,
	U3CU3Ec__DisplayClass8_0__ctor_m83F10B0119C62300F29466339E8CC349AFF732B3,
	U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__0_mEC0CC796C9C9CD3EF8936CA7549563F049C5ECE1,
	U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__1_m60086A5834BCFBA099FAB79A20ED404ECFF49534,
	DOTweenModuleSprite_DOColor_mA67D7B77174B479F8AE20F739A74BA7A2BBC9F0F,
	DOTweenModuleSprite_DOFade_mEED04CEE3EE9D32B87478A3CFDB9CFB710307769,
	DOTweenModuleSprite_DOGradientColor_m47670ABD64AEDC972E8210CAF4C8A778B0B7676D,
	DOTweenModuleSprite_DOBlendableColor_m51BBDB4A69B0560481736E6734EA2BF52DB1537D,
	U3CU3Ec__DisplayClass0_0__ctor_mD77E24E24423ADEC43F9983504BD6796D2671E99,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m12BAC3674340BDF408F41A0E9BEEF6BB87C3F504,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_m89E34AE3BBC636BD2B38C1AB7300F1F20F25E616,
	U3CU3Ec__DisplayClass1_0__ctor_m40F18C57ED175BA68D1D65FFA97879AEAF2E3325,
	U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__0_m8B9E647B54E5DA35EE71D65866372C8C90B7F452,
	U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__1_m935CE2E2B7CB25D7151DA0BF3B4420C8099E0A45,
	U3CU3Ec__DisplayClass3_0__ctor_mB0AA92A12DE97E8954819AC2BAD916065D4BECDB,
	U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__0_m371A7E7E53DB5FC70A6534A7956B27A3A8B7EA03,
	U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__1_m36515014C810F349D12C86DC636349E452DA6974,
	DOTweenModuleUI_DOFade_mF927A1EDCA91814D8A466A343E3223EEF4ED7478,
	DOTweenModuleUI_DOColor_m7067683474EE6A600294C2CCFD941403FA789A6E,
	DOTweenModuleUI_DOFade_m41638D05EB7678C523D0D39E320FAC80B8E48658,
	DOTweenModuleUI_DOColor_mF4B41443760536F3C399612AD0283A52937D95CC,
	DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754,
	DOTweenModuleUI_DOFillAmount_mE4ED52B826A6F8035320E980F7C701B4047C4DBA,
	DOTweenModuleUI_DOGradientColor_mB4B8D1DEDA92464670EB8A6D985973477D51A4D5,
	DOTweenModuleUI_DOFlexibleSize_mF0916C4E7A906202EFBAEA89CC2B55A9C3CF48B3,
	DOTweenModuleUI_DOMinSize_m54A6919625499AFD6F801C6EC9672965DBDC96E1,
	DOTweenModuleUI_DOPreferredSize_mE26AFB2C606F095483EDED449FB8A5A80C08A8DE,
	DOTweenModuleUI_DOColor_m15BC0FCF035E3DEEC3CFC30465EDE0FE6FD6D2EA,
	DOTweenModuleUI_DOFade_m86DB80308D92FC4DFD3E981F9E958FF2A207EA74,
	DOTweenModuleUI_DOScale_mFA27F29E3DFB6B1CFCAC3CE73F06DF3737A3EB06,
	DOTweenModuleUI_DOAnchorPos_m63999D434CE7E8F6BE66BBA92532DE9D98DC9F79,
	DOTweenModuleUI_DOAnchorPosX_m9B57E02E9902C7A3F1A8DD1021AF630C10478380,
	DOTweenModuleUI_DOAnchorPosY_m14CCA63E545D7C28AD1A76488D0084F55EE94CA8,
	DOTweenModuleUI_DOAnchorPos3D_m6DEB7C4AE4CD2118BF83D268945340F3373DC7BB,
	DOTweenModuleUI_DOAnchorPos3DX_mA40303AC769280BDE74EE58BE5AD80BEA427CE08,
	DOTweenModuleUI_DOAnchorPos3DY_mE6BE56AE421B959F22CBA6318BABC3CD367A7277,
	DOTweenModuleUI_DOAnchorPos3DZ_m702F7D4FE9DC8E6116FB5E0DD478D2BB88ED1242,
	DOTweenModuleUI_DOAnchorMax_mDD97F6A8B4468622E3808507CB6B89A8A109BDCE,
	DOTweenModuleUI_DOAnchorMin_m60C4775B1EEDBCF2CD3E96A8171C8A83FD7F400D,
	DOTweenModuleUI_DOPivot_mF623939EDCC152F664FECC8B71E321B0AA13F0A9,
	DOTweenModuleUI_DOPivotX_mC21C031A61BDB7C3A8087719F23CBC6D1D762D6B,
	DOTweenModuleUI_DOPivotY_m9922C28ADECA433362D3C6C816318CA00705996A,
	DOTweenModuleUI_DOSizeDelta_mC4FD07525E1CA5BBF577030D571CE0CCEFD4E6B7,
	DOTweenModuleUI_DOPunchAnchorPos_m058E3DA140B9C497727F05AEAA296425E585B9EA,
	DOTweenModuleUI_DOShakeAnchorPos_m89D1C74E940B9434B2234FB49C031AD8FD1170AB,
	DOTweenModuleUI_DOShakeAnchorPos_m7818867062313A535AB2CD6F86267E2AB9357AC6,
	DOTweenModuleUI_DOJumpAnchorPos_m7046EE1C82AF0159CFAAA460E4C3D22797625480,
	DOTweenModuleUI_DONormalizedPos_m300F112FE2F67D4DC718083340C7FD27BC96F248,
	DOTweenModuleUI_DOHorizontalNormalizedPos_mCB4BCD85B49C458BE871F890A86FEFE0D7F41B42,
	DOTweenModuleUI_DOVerticalNormalizedPos_mC0269B4644816A0D5C09717CA92E4229CB31CDF1,
	DOTweenModuleUI_DOValue_mFDF45D60F52C82AACD8B50859A51B8F315A44F08,
	DOTweenModuleUI_DOColor_mFD36CA1EA683A0D2312EE87BCF3B0A924504E797,
	DOTweenModuleUI_DOCounter_m6C87AB89E5B14F1FB82630CFF15791678D7DF055,
	DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1,
	DOTweenModuleUI_DOText_mC0932E45473278B2FFB1698954CC0EB0422C626F,
	DOTweenModuleUI_DOBlendableColor_m4C5E9A68BFBEE876E1FF8D8E9A4BA77856F144C1,
	DOTweenModuleUI_DOBlendableColor_m18196516A24780A550314E9327868D6D6F3EAA4E,
	DOTweenModuleUI_DOBlendableColor_mC30D1F805FE976FB25EC361AB0ABA94413361B6F,
	DOTweenModuleUI_DOShapeCircle_mF3FE405DFB4AB92952A45A6E716682AAC3D57639,
	Utils_SwitchToRectTransform_m260A15449F1C7A8F4356730DF4A59A386C45D200,
	U3CU3Ec__DisplayClass0_0__ctor_m14E6397A137D3CB8A7638F9254B4694553B4DC7C,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m6F0CD242FAEF332D726AD4CE2EB2691CCB3748B1,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mDBDE14547A6DF70C0ADB82403282C26FBB4F6A27,
	U3CU3Ec__DisplayClass1_0__ctor_mCB63EFBBE399EEB9892054E0FAB2AB42E2BBB886,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_mE8D0790E2303D84F8D513854D9F004E5E3127A3C,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m07CB6A64D93955BC07E8AE290B01F9F39195A5A9,
	U3CU3Ec__DisplayClass2_0__ctor_m61C588C0764BA908B02EBAB1C19076EBB2898A9E,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_m85CC4106263DD670C5BF648B8A64C6BA84297B65,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_m140BDA0B825925A5051A2565C055CCBBF005A680,
	U3CU3Ec__DisplayClass3_0__ctor_m09228DABE56F6866E09591FE27C255A39A71E48D,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_mA7BF7CEB8AA94694104E8D048F5045D122A5D980,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_mBC7108789E76320E9507A9087273C7D89227F0F3,
	U3CU3Ec__DisplayClass4_0__ctor_m5CD451B862F3CC71D8D25BC3A69725FCDD6144C4,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m507CD55516001BB4A76D7E58CB159B5394D6413C,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_mE65F1E4D380EC305AF0AE93E0315B0B23CA36310,
	U3CU3Ec__DisplayClass5_0__ctor_m879B3843E0C43157D8044AD52E2F865EE50FD041,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_mAFE3629BC44AE61C84399809F742CEF0235004E1,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_m9EB6C21BCE8D1A08FF84815C05A6664637C510CE,
	U3CU3Ec__DisplayClass7_0__ctor_m47B2E10FFFD9FCACC88586DEF6A4E675DC9EC406,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_m7A7EA5F66922413743B8C1AB4234645A32098EA8,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_m0E76B7C6F893FD2F7F24E302CB55882765DD1153,
	U3CU3Ec__DisplayClass8_0__ctor_m0953E12771619B6F14FC14CA853AF5CA110A0553,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m9BD9F63B884498F9AA02C307EB1CA15FA29BD094,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_m546E530F9DE46CA1359450860E62DCC3BD3D72B3,
	U3CU3Ec__DisplayClass9_0__ctor_m3EB28823EF4D152A06983986C8120490212A8DF7,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mB87EAB728455B68D07773FA10DCA169804482CBD,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m24617F67F48227A961C0543BF7925F8D2F2A90B6,
	U3CU3Ec__DisplayClass10_0__ctor_m11212BA4C7EB97E0EE079E78FA1C95C02D517C75,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_m08737F267CCB60C0CA0244070DA3C50845FC8B1F,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_mAFFD9D2D2F3FD2BE4E2423286098B9BCC1C0C193,
	U3CU3Ec__DisplayClass11_0__ctor_m9B3BB08FF1DC2F3101E0D580891268445B7763CA,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_mA57545026A9AE0CB3A20B6FFCDCF6F2F1CDA6AB0,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_mCC70305FC1C1B93A838838519D023AC608D3E23E,
	U3CU3Ec__DisplayClass12_0__ctor_m05F6F905C11A344C5681CE1CD406DE85940356E5,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_mEE927D8596DDB335403416BF63FF4E7E27D49D51,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_m65C60213296203590F596B6A3481E4B8621F96D5,
	U3CU3Ec__DisplayClass13_0__ctor_mC6D360265628BCFBFDD42CCAF1136983FDD186BE,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_m693A41C7FCF87BDE6211DE0ADED05AA5C476D71F,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_m0F8F9CA39BC8894705C1EE9983E59D24A7BF218B,
	U3CU3Ec__DisplayClass14_0__ctor_m3BB3CA37E5912C211B954D7EE850C77F0B04BFF6,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m02BA203D0A6C9D64BC8149C197099DA29370C872,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m47B6E3E511B7E234B49FDBD4D6BC32E5EC92B1E9,
	U3CU3Ec__DisplayClass15_0__ctor_m011D15F0644147EABAFA4ED415CD3F5E8782CCE8,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_mCBF0F2691FBC7AED64284B0FB4CCD75792AAB51C,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m0E6D417FD9062B34D8C0E8B58C4C688B39CD9603,
	U3CU3Ec__DisplayClass16_0__ctor_mF4FDD3E32D1C9FDD48079A148410AAB0CF4855B2,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_mFBE3FE1AC3F56C148108234A9A1285F87EEF50E8,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_mFEF34D901D1E18E74D7E93297BDD75ABF146514C,
	U3CU3Ec__DisplayClass17_0__ctor_mEEC98B74C2A5EDE984A5A59F4289ADFADAB3F804,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_mC8D86325D798D952B390643A8BAE1D995368D36A,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_mAB78FA2C6EEC74D6E259B2ABF94ECBD3CB62DB82,
	U3CU3Ec__DisplayClass18_0__ctor_mE272C380064D4945F3C7FDC2357A8B765BC52841,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_m8C53152ED72D09B8F2BE2DE14963561650D0C42B,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_m9E1FC98A6DF461320AC4B6F294F1A69AFD058E3E,
	U3CU3Ec__DisplayClass19_0__ctor_mF4A0F29D8F3315E7DD0BC8103DA0023E16C341CB,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_mD27E4DF91E21C64D13997EA12A7659B1E31AA77B,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_mD6F74CB34BAEA91A512586538770AB27FFB68A1D,
	U3CU3Ec__DisplayClass20_0__ctor_mCE70C1CB9A605F65BE4D31D224111EE4C6FB7DE6,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_mAB5724DF7472A7CED2070B75A1B0E12378D5E6DD,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_m7E357679D17273D809A39CDF435B5E64E46A64A0,
	U3CU3Ec__DisplayClass21_0__ctor_m47D7A3B4DA7AE2FE2F83164406834CF9150E5308,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_m4804C6D34689DD21311DA86CC22008D3B0774391,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_m73EF1691143A0E7E0254A73F52FDC73BB5AAA3E6,
	U3CU3Ec__DisplayClass22_0__ctor_m0D4210D174301CE26FA7A519B4168E7D87F6D92A,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_mB3D6F3AADE069EED625038A53491AFEA1DAFD9DF,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_mDCA8630A9398A411ACB0950973AB99C0723CC074,
	U3CU3Ec__DisplayClass23_0__ctor_mA47954A6646E43342880718EE9B5E66A0D18FDF2,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_mFC16CCA332D908526AC3DC2BD8B4456969000CF5,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_m7E0DAFAB836AC4F133B995B50578D8911BA3113B,
	U3CU3Ec__DisplayClass24_0__ctor_m0524772292383368F9C9F6BDFF0A114D60A35F8F,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_m959088FDF2C1CD6EF40EE35BA531719E60C5327D,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_mCECF34E3A27F03AA2DB5719CA1241927DC99BC5B,
	U3CU3Ec__DisplayClass25_0__ctor_m1ABD5A4A91526CA156C55C23A54822DC898A34AF,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_mF87E563BB15F601A91B0DA1012A1604A52FCBDA5,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_m26B3B99EDB1D47D81931CC6CABFED8406BF4E90E,
	U3CU3Ec__DisplayClass26_0__ctor_mC4836A2FD37334971703D95C49ED35BCAD56AFB8,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_m357B263CA209B3E6EBB7F95AC352AA8B28A5CFC5,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_mE7127F5C6AB73DBA7D40F67FD61BA4076E2B8D23,
	U3CU3Ec__DisplayClass27_0__ctor_m07ADC405B65035A17DC97097DED9B4C1D21EFB17,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_m1AF2B166591EEA50BEE7FD3C2E9B7EFFC0D39F8E,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_mF7180DEAAB00C14DD6F3DB8B815D0C0B2DB64725,
	U3CU3Ec__DisplayClass28_0__ctor_m4969157094B09547B26225881BD87025C928B506,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_mB4E05CBCDBD4732D154E1A64678339D3FC5FBCE9,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_mBDA6378719836E14C155B4648BB47429C125590C,
	U3CU3Ec__DisplayClass29_0__ctor_m00E6EAFA57923B235554FA8A236AD64992F363FE,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_mAE0CBA3D52D03EB2F2C37D85C5832B412F816591,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_mB685B77262E778382FAF923C04FD328DC494426C,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_mB23D92AF70DCD33F838372FF59B1F39DD3775824,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_mB86E86358B9C1671706EA489B6FC30C3874224E7,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_m1E0D9286E08822DF086E86BA60F0EFF6B62A32C7,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_m6E6A274B0B852663D3ED8CDD2B4646B9D618E535,
	U3CU3Ec__DisplayClass30_0__ctor_mCC70550D8AD7B4F88C913940A644E81F11F5898A,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_m92C7B1817960CE4D1D7333872D37C8AF131F68FE,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_mFA97248E3617865FEF6C3F36E633266FF044050F,
	U3CU3Ec__DisplayClass31_0__ctor_m82279F879253DF896CCD126EC0B7E16B295D165A,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_mB33367FCE1348BA9F855A9D1366FA19F5DDA4AEB,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_mEBA7CDAA06332B1E56991D8E2299E24B9348D627,
	U3CU3Ec__DisplayClass32_0__ctor_mD93F08EDF6689C6BA223067191EE8C9E0FBE509C,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_m83DD02084574CCFB9F26C19BDE6AF6DE2C6387FE,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_mAE94A6E7E27147AA530D056E286A00C8EAE56331,
	U3CU3Ec__DisplayClass33_0__ctor_mB387BC26BCA1B460D5B3B1CE767228FA244B7703,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_mBB603DBBDA542D7F00142102A667BCB9682EC145,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_mA3DF76574D5E76992AF228A3C4AFFFC19D6BAE15,
	U3CU3Ec__DisplayClass34_0__ctor_mA88FD675FEF2971D3F8145822B844ECFFF59BF17,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_m5DC2ED4C56B0A1D8CFCB8A4E2D2078EA79E8B66F,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_m9BA1BADA179249B9F0AD1142BE826798FA114E13,
	U3CU3Ec__DisplayClass35_0__ctor_m5533AE6A72AB74A97282032790D0DE81F0EAF811,
	U3CU3Ec__DisplayClass35_0_U3CDOCounterU3Eb__0_m4E790A70F3934B1A2F06D5692F8550A69C2567A2,
	U3CU3Ec__DisplayClass35_0_U3CDOCounterU3Eb__1_m3207CE6E96DAC0D117EFA238D72BAC0FAEFB2FE2,
	U3CU3Ec__DisplayClass36_0__ctor_mE2912C6BF4318E28870DE7AA92E4B05C80D92EE0,
	U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__0_mA3238766629BBF7E2750683F62F7629C5B097D87,
	U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__1_m119575F5E470CA6D0C37EF9E1A9C3A2CF8B2AA3A,
	U3CU3Ec__DisplayClass37_0__ctor_m59CE3FBA46AF3F7C3597AD84DEC898DB9B84EE39,
	U3CU3Ec__DisplayClass37_0_U3CDOTextU3Eb__0_mDB021AB28041C55E82DF362F8DDF94879B7EBA13,
	U3CU3Ec__DisplayClass37_0_U3CDOTextU3Eb__1_m0724BC3E19FD8643DD8C0FF25D4C08196FB446DD,
	U3CU3Ec__DisplayClass38_0__ctor_mB3E91AEAFAE65DD5FC36DD530E74CE1F4FA24AEF,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_mB976655AEC70D25B43AAAF577CC953C7EB03D2EE,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_mF14436C4F4EEB5B7664C3444B969B694BC6E3E5E,
	U3CU3Ec__DisplayClass39_0__ctor_m1DB7B55A6B3E4B184E35968EACBF9E214E0CED7D,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_mE93AC95325F69D83EE746421FA5992DC25009961,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_m45DD6E7ED579816AC18DF6B88901D9F11DB1B36F,
	U3CU3Ec__DisplayClass40_0__ctor_m089AA2605E325970B89E7F1519CF4DD5676CA2AD,
	U3CU3Ec__DisplayClass40_0_U3CDOBlendableColorU3Eb__0_mD869D1DD9D45824831045C0E5638161837B0854B,
	U3CU3Ec__DisplayClass40_0_U3CDOBlendableColorU3Eb__1_m6B0A0EFCA7952B551FD8C87503133EA08D8075A9,
	U3CU3Ec__DisplayClass41_0__ctor_m300A6B34B897BE337DB4EA1375C1FD22BC663072,
	U3CU3Ec__DisplayClass41_0_U3CDOShapeCircleU3Eb__0_m452DB0F79A62938FE398B97F096D1AE657A6494C,
	U3CU3Ec__DisplayClass41_0_U3CDOShapeCircleU3Eb__1_mC125693EF64469FC5B4319D963F685D471340384,
	DOTweenModuleUnityVersion_DOGradientColor_m7E9B0D004D4B3D602EBB852CA1755FE863C6B839,
	DOTweenModuleUnityVersion_DOGradientColor_mA4E191E86F2EF81966259A40B2CDADE375437920,
	DOTweenModuleUnityVersion_WaitForCompletion_m4BCEDFA5ED1ACF4BC1B3531C0C306A7AC553C1F2,
	DOTweenModuleUnityVersion_WaitForRewind_mD9F37D5AAD9B665EE07F083D026E65194A8E3E6E,
	DOTweenModuleUnityVersion_WaitForKill_m02BD7F3B018BB6B4ED4FCAF47B8DA435EC60BD6B,
	DOTweenModuleUnityVersion_WaitForElapsedLoops_m46D46788899029A20F73BE252F7427D853131404,
	DOTweenModuleUnityVersion_WaitForPosition_m1771625E294B38C5D13DCA40846AEA881C1B23E6,
	DOTweenModuleUnityVersion_WaitForStart_mB4AAFE83B50AEF3A167F31C14B2F58794527F905,
	DOTweenModuleUnityVersion_DOOffset_m09CA228EBED193314D3D9A41BBDC97393E1FC4D2,
	DOTweenModuleUnityVersion_DOTiling_m6A3CD06EC3030A93EC3E93DE678F34048D5C1141,
	DOTweenModuleUnityVersion_AsyncWaitForCompletion_m886CBFB2DCD9455C2762F639F065A7AC9F1EC2E5,
	DOTweenModuleUnityVersion_AsyncWaitForRewind_mCC0B2A591BA46DE9E5CB59EA84461051477D5642,
	DOTweenModuleUnityVersion_AsyncWaitForKill_m11E8731B8A8E89493FB7976A35DCF4DD85E2132B,
	DOTweenModuleUnityVersion_AsyncWaitForElapsedLoops_m9F3D38817E779AA9C5F0BA7811E6626A7BFBB5B1,
	DOTweenModuleUnityVersion_AsyncWaitForPosition_m6207EF50EF7DE8402EDC806F8FEF6CC313C4DB03,
	DOTweenModuleUnityVersion_AsyncWaitForStart_m637C3E8C05D413E080897E3A99CF21C4BD859F25,
	U3CU3Ec__DisplayClass8_0__ctor_m0EACBC706CDB7B8BCE4C21D3AD250FE25CD825CF,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m6D616025F8610086B30B83BC8ED13213DB2AC7CC,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m4171C9EB0226E0E06E574EFD507A8D352EC5B557,
	U3CU3Ec__DisplayClass9_0__ctor_m6CC810F5A3B1779C2569B21779361FD5F94C2C9C,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_m747CC91BE7EDF7D2644045B72BF689A2552B855F,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_m0D5FC9115C0FFE113CA2277BA5A17562550B19F6,
	U3CAsyncWaitForCompletionU3Ed__10_MoveNext_m7A14EB02262CB797C27664F35565B88E8BA060D3,
	U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_m85F5E4DAF7F7E7AB65762F3F6C95BFAE3F039819,
	U3CAsyncWaitForRewindU3Ed__11_MoveNext_m5DBC14BB1136B7BCF18066FB833D90CF116C1549,
	U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_mAC2B29B1F4653A143591E2BB2388F8AD9F9A0D64,
	U3CAsyncWaitForKillU3Ed__12_MoveNext_mAA9DB56F9B8B3A6E9069CC690E90634D7F51A276,
	U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m47EB31E97C542A8D7AC82CC7EAF613DC8EEDEABD,
	U3CAsyncWaitForElapsedLoopsU3Ed__13_MoveNext_mD74D8D5D0970E23A40CE0D8B83297BBC68E25712,
	U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m5E02C400A516339612E14DF2ED0B6046D90F6F47,
	U3CAsyncWaitForPositionU3Ed__14_MoveNext_m69E154F3EFA854C85D27C84F5C8624AE7595D01E,
	U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_mA77CBC54370E798D88ACF48F9D68586A41FC741E,
	U3CAsyncWaitForStartU3Ed__15_MoveNext_m0BFB04DC7FE239554F4DF0B3DDD139E42A092F20,
	U3CAsyncWaitForStartU3Ed__15_SetStateMachine_mAD7263100F3F3DBD57295879534C47061409AEB0,
	WaitForCompletion_get_keepWaiting_mCA7642C5A8C37F8C0A2CAE990CE1CB6AEE8FD2D9,
	WaitForCompletion__ctor_m818111A77A3380EE626346FE03A1A604BB896A1A,
	WaitForRewind_get_keepWaiting_mB480319CB28155CA977F94C7FA03EE5353AA1285,
	WaitForRewind__ctor_m66B575E497C363CB5137629B4D6A00D13B7CD5AE,
	WaitForKill_get_keepWaiting_m7979151F1AD842D2E8004FE37A2C51B47AB36647,
	WaitForKill__ctor_m1C9624CE32A1C83CEA14BB5EADD587B6AD79D829,
	WaitForElapsedLoops_get_keepWaiting_m6F7A59CCCC45BBA5125C6FC7AB667CD24359E8F4,
	WaitForElapsedLoops__ctor_m8E720B450DD1350EE81EC3CCB5B6280BE5C51D8B,
	WaitForPosition_get_keepWaiting_m34DFE8356EAFEE916828BFAF4A17A822B47AD687,
	WaitForPosition__ctor_m94DD0A05EF293B8AA83F343A12015C107AF7FDB8,
	WaitForStart_get_keepWaiting_m59700AA1AB726A22C76BFAA0C52FCA460F6E337D,
	WaitForStart__ctor_mD7AB17A603CF22568EEF0D9861C49F6CFD632284,
	DOTweenModuleUtils_Init_mFF2188F42FA7128FE1A9BBC2CB9E6351919C9D15,
	DOTweenModuleUtils_Preserver_mBE8D1FE2AE913FBFFCCF43B5A615D941B3706F43,
	Physics_SetOrientationOnPath_mCC376173A621DA244564EDF8A6347AB2A0F47816,
	Physics_HasRigidbody2D_m86FAA0450979B8AFE6A9EF5E27837387C57765C1,
	Physics_HasRigidbody_m3082D72A658CCDDF6BB1F3D4A3EFBD6397D6E862,
	Physics_CreateDOTweenPathTween_m94D41B65E37500E5ACCF6B6F3B5D53FB031C4245,
};
extern void U3CAsyncWaitForCompletionU3Ed__10_MoveNext_m7A14EB02262CB797C27664F35565B88E8BA060D3_AdjustorThunk (void);
extern void U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_m85F5E4DAF7F7E7AB65762F3F6C95BFAE3F039819_AdjustorThunk (void);
extern void U3CAsyncWaitForRewindU3Ed__11_MoveNext_m5DBC14BB1136B7BCF18066FB833D90CF116C1549_AdjustorThunk (void);
extern void U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_mAC2B29B1F4653A143591E2BB2388F8AD9F9A0D64_AdjustorThunk (void);
extern void U3CAsyncWaitForKillU3Ed__12_MoveNext_mAA9DB56F9B8B3A6E9069CC690E90634D7F51A276_AdjustorThunk (void);
extern void U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m47EB31E97C542A8D7AC82CC7EAF613DC8EEDEABD_AdjustorThunk (void);
extern void U3CAsyncWaitForElapsedLoopsU3Ed__13_MoveNext_mD74D8D5D0970E23A40CE0D8B83297BBC68E25712_AdjustorThunk (void);
extern void U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m5E02C400A516339612E14DF2ED0B6046D90F6F47_AdjustorThunk (void);
extern void U3CAsyncWaitForPositionU3Ed__14_MoveNext_m69E154F3EFA854C85D27C84F5C8624AE7595D01E_AdjustorThunk (void);
extern void U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_mA77CBC54370E798D88ACF48F9D68586A41FC741E_AdjustorThunk (void);
extern void U3CAsyncWaitForStartU3Ed__15_MoveNext_m0BFB04DC7FE239554F4DF0B3DDD139E42A092F20_AdjustorThunk (void);
extern void U3CAsyncWaitForStartU3Ed__15_SetStateMachine_mAD7263100F3F3DBD57295879534C47061409AEB0_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[12] = 
{
	{ 0x060003A2, U3CAsyncWaitForCompletionU3Ed__10_MoveNext_m7A14EB02262CB797C27664F35565B88E8BA060D3_AdjustorThunk },
	{ 0x060003A3, U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_m85F5E4DAF7F7E7AB65762F3F6C95BFAE3F039819_AdjustorThunk },
	{ 0x060003A4, U3CAsyncWaitForRewindU3Ed__11_MoveNext_m5DBC14BB1136B7BCF18066FB833D90CF116C1549_AdjustorThunk },
	{ 0x060003A5, U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_mAC2B29B1F4653A143591E2BB2388F8AD9F9A0D64_AdjustorThunk },
	{ 0x060003A6, U3CAsyncWaitForKillU3Ed__12_MoveNext_mAA9DB56F9B8B3A6E9069CC690E90634D7F51A276_AdjustorThunk },
	{ 0x060003A7, U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m47EB31E97C542A8D7AC82CC7EAF613DC8EEDEABD_AdjustorThunk },
	{ 0x060003A8, U3CAsyncWaitForElapsedLoopsU3Ed__13_MoveNext_mD74D8D5D0970E23A40CE0D8B83297BBC68E25712_AdjustorThunk },
	{ 0x060003A9, U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m5E02C400A516339612E14DF2ED0B6046D90F6F47_AdjustorThunk },
	{ 0x060003AA, U3CAsyncWaitForPositionU3Ed__14_MoveNext_m69E154F3EFA854C85D27C84F5C8624AE7595D01E_AdjustorThunk },
	{ 0x060003AB, U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_mA77CBC54370E798D88ACF48F9D68586A41FC741E_AdjustorThunk },
	{ 0x060003AC, U3CAsyncWaitForStartU3Ed__15_MoveNext_m0BFB04DC7FE239554F4DF0B3DDD139E42A092F20_AdjustorThunk },
	{ 0x060003AD, U3CAsyncWaitForStartU3Ed__15_SetStateMachine_mAD7263100F3F3DBD57295879534C47061409AEB0_AdjustorThunk },
};
static const int32_t s_InvokerIndices[959] = 
{
	3856,
	3856,
	3856,
	3856,
	3191,
	3856,
	3856,
	3775,
	3856,
	3172,
	3856,
	3812,
	3775,
	3856,
	3775,
	3856,
	3856,
	3856,
	3856,
	1966,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3775,
	3856,
	3775,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	5421,
	5421,
	5421,
	5421,
	5813,
	3856,
	3856,
	1610,
	1982,
	3856,
	2882,
	3191,
	1176,
	3856,
	3856,
	3856,
	3263,
	3856,
	5802,
	5738,
	5802,
	5738,
	5802,
	5738,
	5813,
	5784,
	5731,
	3775,
	3775,
	3775,
	3775,
	3856,
	1993,
	1993,
	1993,
	851,
	3856,
	2962,
	1642,
	1276,
	3238,
	1993,
	3220,
	3220,
	1986,
	1986,
	3191,
	3191,
	1990,
	3856,
	3856,
	3856,
	3856,
	5813,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3191,
	3191,
	3856,
	3856,
	3856,
	3856,
	3775,
	3856,
	3856,
	3263,
	3856,
	3856,
	3856,
	3856,
	3172,
	3856,
	3856,
	3856,
	3856,
	3172,
	3856,
	3856,
	3856,
	3856,
	3220,
	3856,
	3856,
	3220,
	3856,
	3856,
	3775,
	3220,
	3775,
	3856,
	3172,
	3856,
	3812,
	3775,
	3856,
	3775,
	3172,
	3856,
	3812,
	3775,
	3856,
	3775,
	3856,
	3775,
	3220,
	3775,
	3856,
	3172,
	3856,
	3812,
	3775,
	3856,
	3775,
	3172,
	3856,
	3812,
	3775,
	3856,
	3775,
	3856,
	3220,
	3856,
	3263,
	3263,
	3856,
	3856,
	3220,
	3856,
	3856,
	3856,
	3856,
	3856,
	3220,
	3856,
	3856,
	3856,
	3856,
	3775,
	3220,
	3856,
	3172,
	3856,
	3812,
	3775,
	3856,
	3775,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3220,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	5673,
	5623,
	5421,
	5412,
	5417,
	5420,
	5193,
	5327,
	5136,
	5193,
	5285,
	5420,
	5285,
	5417,
	-1,
	4807,
	5409,
	5092,
	5423,
	5345,
	3856,
	3856,
	3856,
	3775,
	3191,
	3856,
	3856,
	3856,
	3775,
	3856,
	3172,
	3856,
	3812,
	3775,
	3856,
	3775,
	3775,
	3856,
	3775,
	3856,
	3172,
	3856,
	3812,
	3775,
	3856,
	3775,
	3775,
	3191,
	3856,
	3856,
	3856,
	3856,
	3775,
	3856,
	3191,
	3856,
	3856,
	3856,
	3775,
	3856,
	3856,
	3191,
	3856,
	3856,
	3856,
	3856,
	3775,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	5813,
	3856,
	2514,
	5813,
	3856,
	5813,
	5813,
	5813,
	5813,
	5728,
	5412,
	5813,
	5799,
	5813,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3775,
	3775,
	3775,
	3856,
	3856,
	3856,
	3238,
	3856,
	3856,
	3828,
	3828,
	3856,
	3775,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	1986,
	3856,
	3238,
	3856,
	3856,
	3220,
	3856,
	3856,
	2548,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1159,
	1648,
	1159,
	1159,
	1159,
	3856,
	1984,
	1159,
	332,
	2962,
	3856,
	3856,
	3775,
	3775,
	3191,
	3856,
	3856,
	3856,
	3856,
	3775,
	3191,
	3775,
	3191,
	3775,
	3191,
	3856,
	3856,
	3856,
	3172,
	3856,
	5777,
	5784,
	5760,
	5725,
	5802,
	5738,
	5802,
	5738,
	5802,
	5738,
	5784,
	5731,
	5813,
	5417,
	5813,
	4681,
	5813,
	5731,
	5784,
	5432,
	5731,
	5731,
	5799,
	5784,
	5813,
	5813,
	5735,
	5420,
	5813,
	5434,
	5630,
	3856,
	824,
	1984,
	3856,
	1478,
	3191,
	1984,
	3238,
	1029,
	3191,
	1984,
	3856,
	1478,
	3191,
	3220,
	3775,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3812,
	3220,
	3238,
	3856,
	3856,
	3775,
	3856,
	3856,
	3856,
	3172,
	3856,
	3812,
	3775,
	3856,
	3775,
	3856,
	3172,
	3856,
	3856,
	2018,
	3856,
	3856,
	3856,
	3775,
	3191,
	3775,
	3856,
	1256,
	3172,
	3856,
	3856,
	3856,
	3172,
	1986,
	1399,
	3856,
	3775,
	3191,
	3775,
	3856,
	3191,
	3856,
	3856,
	3856,
	3238,
	3856,
	3856,
	3775,
	3856,
	3172,
	3856,
	3812,
	3775,
	3856,
	3775,
	5799,
	5735,
	5802,
	5802,
	5802,
	5777,
	3856,
	5802,
	5802,
	5777,
	5813,
	3856,
	5813,
	3856,
	3856,
	5813,
	5623,
	5623,
	5631,
	5620,
	5629,
	5693,
	5563,
	5673,
	5813,
	5623,
	5623,
	5668,
	3191,
	5623,
	3856,
	3775,
	3775,
	3775,
	2548,
	3775,
	3775,
	3856,
	3755,
	3755,
	3775,
	3756,
	3856,
	5623,
	3191,
	3191,
	3191,
	3191,
	3191,
	3856,
	3856,
	3856,
	3812,
	3856,
	3856,
	3856,
	3856,
	3856,
	3238,
	3856,
	3856,
	3856,
	3775,
	3856,
	3172,
	3856,
	3812,
	3775,
	3856,
	3775,
	3856,
	3856,
	4893,
	3856,
	3856,
	3856,
	3191,
	3775,
	3856,
	3172,
	3856,
	3812,
	3775,
	3856,
	3775,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	3856,
	4818,
	4818,
	4534,
	5139,
	5139,
	5563,
	4752,
	5563,
	5563,
	5563,
	5563,
	5563,
	5563,
	5563,
	5563,
	3856,
	3828,
	3238,
	3856,
	3828,
	3238,
	3856,
	3828,
	3238,
	4560,
	4551,
	4551,
	4551,
	4559,
	4281,
	4093,
	4017,
	4017,
	4532,
	4532,
	3856,
	3851,
	3856,
	3851,
	3856,
	3851,
	3856,
	3851,
	3856,
	3790,
	3856,
	3790,
	3856,
	3851,
	3856,
	3851,
	3851,
	3856,
	3856,
	3851,
	3856,
	3851,
	3263,
	3856,
	3851,
	3856,
	3851,
	3263,
	4556,
	4551,
	4551,
	4818,
	4090,
	4017,
	4017,
	4532,
	4532,
	3856,
	3849,
	3856,
	3849,
	3856,
	3849,
	3856,
	3828,
	3856,
	3849,
	3261,
	3856,
	3849,
	3261,
	3856,
	3856,
	3851,
	3263,
	3856,
	3851,
	3263,
	3856,
	3851,
	3263,
	3856,
	3851,
	3263,
	4793,
	4818,
	4810,
	4793,
	3856,
	3711,
	3122,
	3856,
	3711,
	3122,
	3856,
	3711,
	3122,
	4818,
	4793,
	4818,
	4793,
	4818,
	4818,
	4810,
	4556,
	4556,
	4556,
	4793,
	4818,
	4820,
	4556,
	4551,
	4551,
	4560,
	4551,
	4551,
	4551,
	4556,
	4556,
	4820,
	4818,
	4818,
	4556,
	4090,
	4022,
	4023,
	4090,
	4556,
	4551,
	4551,
	4551,
	4793,
	4077,
	4818,
	4082,
	4793,
	4793,
	4793,
	4091,
	5352,
	3856,
	3828,
	3238,
	3856,
	3711,
	3122,
	3856,
	3711,
	3122,
	3856,
	3711,
	3122,
	3856,
	3711,
	3122,
	3856,
	3828,
	3238,
	3856,
	3849,
	3261,
	3856,
	3849,
	3261,
	3856,
	3849,
	3261,
	3856,
	3711,
	3122,
	3856,
	3711,
	3122,
	3856,
	3849,
	3261,
	3856,
	3849,
	3261,
	3856,
	3849,
	3261,
	3856,
	3849,
	3261,
	3856,
	3851,
	3263,
	3856,
	3851,
	3263,
	3856,
	3851,
	3263,
	3856,
	3851,
	3263,
	3856,
	3849,
	3261,
	3856,
	3849,
	3261,
	3856,
	3849,
	3261,
	3856,
	3849,
	3261,
	3856,
	3849,
	3261,
	3856,
	3849,
	3261,
	3856,
	3851,
	3263,
	3856,
	3851,
	3263,
	3856,
	3851,
	3263,
	3856,
	3849,
	3261,
	3856,
	3849,
	3261,
	3856,
	3856,
	3849,
	3261,
	3856,
	3828,
	3238,
	3856,
	3828,
	3238,
	3856,
	3828,
	3238,
	3856,
	3711,
	3122,
	3856,
	3756,
	3172,
	3856,
	3711,
	3122,
	3856,
	3775,
	3191,
	3856,
	3711,
	3122,
	3856,
	3711,
	3122,
	3856,
	3711,
	3122,
	3856,
	3849,
	3261,
	4810,
	4530,
	5194,
	5194,
	5194,
	4801,
	4817,
	5194,
	4554,
	4554,
	5623,
	5623,
	5623,
	5188,
	5195,
	5623,
	3856,
	3849,
	3261,
	3856,
	3849,
	3261,
	3856,
	3191,
	3856,
	3191,
	3856,
	3191,
	3856,
	3191,
	3856,
	3191,
	3856,
	3191,
	3812,
	3191,
	3812,
	3191,
	3812,
	3191,
	3812,
	1982,
	3812,
	1993,
	3812,
	3191,
	5813,
	5813,
	4689,
	5673,
	5673,
	4086,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x060000F6, { 0, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[2] = 
{
	{ (Il2CppRGCTXDataType)1, 256 },
	{ (Il2CppRGCTXDataType)2, 256 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	959,
	s_methodPointers,
	12,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	2,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
